<?
  class calendar {
    var $daynamefont,$daynamebgcolor,$daynamecolor,$daynamesize,$daynamebold,$daynameitalic,$dayfont,$daybgcolor,$daycolor,$dayactivecolor,$daysize,$daybold,$dayitalic,$showdate,$bordersize,$timestamp;

    function calendar(){
	}

    function show(){
      $day=date("j",$this->timestamp);
      $month=date("n",$this->timestamp);
      $year=date("Y",$this->timestamp);
      if($this->daynamebold==true){
        $daynametextprefix="<b>";
        $daynametextsuffix="</b>";
      }
      if($this->daynameitalic==true){
        $daynametextprefix.="<i>";
        $daynametextsuffix="</i>".$daynametextsuffix;
      }
      if($this->daybold==true){
        $daytextprefix="<b>";
        $daytextsuffix="</b>";
      }
      if($this->dayitalic==true){
        $daytextprefix.="<i>";
        $daytextsuffix="</i>".$daytextsuffix;
      }
      if(checkdate($month,$day,$year)==true){
        $maxdays=31;
        while(checkdate($month,$maxdays,$year)==false)$maxdays--;
        $startday=1-date("w",mktime(0,0,0,$month,1,$year));
        print("<table width='300' border='".$this->bordersize."' cellspacing='0' cellpadding='0'><tr bgcolor='".$this->daybgcolor."'><td>");
        print("  <table width='300' border='0' cellspacing='0' cellpadding='5'>");
        if($this->showdate==true)print("    <tr bgcolor='".$this->daynamebgcolor."'><td colspan='7' align='center'><font face='".$this->daynamefont."' color='".$this->daynamecolor."' size='".$this->daynamesize."'>".$daynametextprefix.date("F",mktime(0,0,0,$month,$day,$year))." $year".$daynametextsuffix."</font></td></tr>");
        print("    <tr bgcolor='".$this->daynamebgcolor."'><td align='center'><font face='".$this->daynamefont."' color='".$this->daynamecolor."' size='".$this->daynamesize."'>".$daynametextprefix." S ".$daynametextsuffix."</font></td><td align='center'><font face='".$this->daynamefont."' color='".$this->daynamecolor."' size='".$this->daynamesize."'>".$daynametextprefix." M ".$daynametextsuffix."</font></td><td align='center'><font face='".$this->daynamefont."' color='".$this->daynamecolor."' size='".$this->daynamesize."'>".$daynametextprefix." T ".$daynametextsuffix."</font></td><td align='center'><font face='".$this->daynamefont."' color='".$this->daynamecolor."' size='".$this->daynamesize."'>".$daynametextprefix." W ".$daynametextsuffix."</font></td><td align='center'><font face='".$this->daynamefont."' color='".$this->daynamecolor."' size='".$this->daynamesize."'>".$daynametextprefix." T ".$daynametextsuffix."</font></td><td align='center'><font face='".$this->daynamefont."' color='".$this->daynamecolor."' size='".$this->daynamesize."'>".$daynametextprefix." F ".$daynametextsuffix."</font></td><td align='center'><font face='".$this->daynamefont."' color='".$this->daynamecolor."' size='".$this->daynamesize."'>".$daynametextprefix." S ".$daynametextsuffix."</font></td>");
        $weekdaycount=0;
        for($daycount=$startday;$daycount<=$maxdays;$daycount++){
          if(($weekdaycount%7)==0)print("</tr>    <tr bgcolor='".$this->daybgcolor."'>");
          if($daycount>0){
            print("<td align='center'>");
            print('<font face="'.$this->dayfont.'" color="'.$this->daycolor.'" size="'.$this->daysize.'" class="calendar">'.$daytextprefix.'  <a href="inspection_booking.php?date='.$year.'-'.date("m",mktime(0,0,0,$month,$day,$year)).'-'.$daycount.'">'.$daycount.'</a> '.$daytextsuffix.'</font>');
          } else print("<td>");
          print("</td>");
          $weekdaycount++;
        }
        while($weekdaycount%7<>0){
          print("<td></td>");
          $weekdaycount++;
        }
        print("</tr>  </table>");
        print("</td></tr></table>");
      } else print("Incorrect date");
    }
  }
?>
