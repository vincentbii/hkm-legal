<?php require_once('../connections/hr.php'); ?>
<?php include('../activelog.php'); 
mysqli_select_db($database_hr, $hr);

$code=getnextcode('authcode' ,'users');

if (isset($_POST['save'])) { 
		
		if (isset($_POST['groupid'])) {
			$groupid=$_POST['groupid'];
		} else {
			?>
			<script> 
				alert('You have to select a default group !');
				javascript:location='newuser.php';
			</script>
			 <?php 
		}
		
		if (isset($_POST['fulname'])) {
			$names=$_POST['fulname'];
		} else {
			$names="";
		}
		
		if (isset($_POST['worktel'])) {
			$worktel=$_POST['worktel'];
		} else {
			$worktel=0;
		}
		
		
		if (isset($_POST['hometel'])) {
			$hometel=$_POST['hometel'];
		} else {
			$hometel=0;
		}
		if (isset($_POST['mobtel'])) {
			$mobtel=$_POST['mobtel'];
		} else {
			$mobtel=0;
		}
		
		if (isset($_POST['email'])) {
			$emailaddr=$_POST['email'];
		} else {
			$emailaddr="";
		}
		
		if (isset($_POST['username'])) {
			$uname=$_POST['username'];
		} else {
			$uname="error";
		}
		if (isset($_POST['designation'])) {
			$designation=$_POST['designation'];
		} else {
			$designation="error";
		}
		if (isset($_POST['umd5'])) {
			$password=$_POST['umd5'];
		}
		
		if (isset($_POST['CUMD5'])) {
			$secdet=$_POST['CUMD5'];
		} 
		if (isset($_POST['member'])) {
			$member=$_POST['member'];
		} 
		if (trim($password)!==trim($secdet)){
			?>
			<script> 
				alert('Passwords do not match !');
				javascript:location='newuser.php';
			</script>
			 <?php
		}
		
		if (isset($_POST['PIN'])) {
			$pin=$_POST['PIN'];
		}
		
		// Generate Necessary Variables
		// -----------------------------------------------------------------
		$MD5 = md5(trim($password)); // md5 encrypted password
		$MDPIN = md5(trim($pin)); // md5 encrypted password
		
		// -----------------------------------------------------------------
		
		
				//Check if user exists for this company
				if (userExists($uname)) {
					?>
					<SCRIPT LANGUAGE=JAVASCRIPT>
					{
						alert ("The 'User name' you entered already exists. Enter a unique username");
					}
					</SCRIPT>
					<?php
				} else { 
					
					 $insertSQL = sprintf("INSERT INTO users (fulname, username, umd5, worktel, hometel, mobtel, email,  uregdate, authcode,groupid, com_id,pincode,status,designation,department) VALUES (%s, %s, %s, %s, %s, %s, %s,  %s, %s, %s, %s,%s,%s,%s,%s)",
									   GetSQLValueString($names, "text"),
									   GetSQLValueString($uname, "text"),
									   GetSQLValueString($MD5, "text"),
									   GetSQLValueString($worktel, "text"),
									   GetSQLValueString($hometel, "text"),
									   GetSQLValueString($mobtel, "text"),
									   GetSQLValueString($emailaddr, "text"),
									   GetSQLValueString(time(), "int"),
									   GetSQLValueString($code, "text"),
									   GetSQLValueString($groupid, "text"),
									   GetSQLValueString($_SESSION['company'], "text"),
									   GetSQLValueString($MDPIN, "text"),
									   GetSQLValueString('A', "text"),
									   GetSQLValueString($designation, "text"),
									   GetSQLValueString($_POST['department'], "text"));
//echo $insertSQL;
				  mysqli_select_db($database_hr, $hr);
					
				  $Result1 = mysqli_query($insertSQL, $hr) or die('ERROR CREATING USER: '.mysqli_error());
				 	
				 
					InsertUserGroups($code,$groupid);
				  
				 
				   
					} 
				
				
				?>
				<SCRIPT LANGUAGE=JAVASCRIPT>
				{
					javascript:location='usr.php';
				}
				</SCRIPT>	
				<?php  
			
	
} 
?>

<html>
<head>
<title>ECOTOURISM KENYA MANAGEMENT INFORMATION  SYSTEM</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../styles/default.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../js/funcs.js"></script>
<?php 
// BRING IN THE JSCRIPT FILE WITH THE DROPDOWN DEstockITIONS
// --------------------------------------------------------
$url = "../tmp/".$_SESSION['company']."/jbs.js";
addJavascript($url); 
// --------------------------------------------------------
?>

<style type="text/css">
<!--
#Layer1 {
	position:absolute;
	width:451px;
	height:115px;
	z-index:1;
	left: 6px;
	top: 88px;
}
-->
</style>

<script language="JavaScript" type="text/javascript">
	
</script>

</head>
<body onLoad="initialize(dp, ut);">
 <form action="" method="post" name="newuser" id="newuser">
<table width="100%" border="0" cellspacing="0" cellpadding="5">
  <tr>
    <td width="49%" valign="top">
     
        <fieldset>
        <legend>User Details</legend>
        <table width="100%" border="0" cellspacing="0" cellpadding="4">
		 
          
  <tr valign="baseline">
    <td width="90" align="right" nowrap>Full Name:</td>
    <td width="237"><input name="fulname" type="text" class="forms" id="fulname" maxlength="50"></td>
    </tr>
       <tr valign="baseline">
    <td align="right" nowrap>Department:</td>
    <td width="465"><select name="department" id="department"  onFocus="this.style.backgroundColor='#ffff00'"  onBlur="this.style.backgroundColor='#F2FAFF'"  class='forms' ><?php 
 	showcustomers($_SESSION['department']); ?>
</select> </td>
  </tr>
    <tr valign="baseline">
    <td align="right" nowrap>Designation:</td>
    <td width="465"><input name="designation" type="text" class="forms" id="designation" value="" maxlength="50"></td>
  </tr>
  <tr valign="baseline">
    <td colspan="2" align="right" nowrap><hr size="1"></td>
    </tr>
  <tr valign="baseline">
    <td nowrap align="right">      Work Tel:</td>
    <td><input name="worktel" type="text" class="forms" value="" maxlength="30"></td>
    <?php 
	
	$selectSQL = "SELECT  groups.groupname, groups.groupid, groups.groupdesc, groups.sys FROM groups WHERE groups.groupname <> 'System administrator' AND groups.groupname <> 'Company administrator' ORDER BY groups.groupname";
	
	$done = mysqli_query($selectSQL, $hr) or die("ERROR: Trying to retrieve groups list - ".mysqli_error());
	$groupslist = mysqli_fetch_assoc($done);
	?>
    </tr>
  <tr valign="baseline">
    <td nowrap align="right">Home Tel:</td>
    <td><input name="hometel" type="text" class="forms" value="" maxlength="30"></td>
    </tr>
  <tr valign="baseline">
    <td nowrap align="right">Mobile Tel:</td>
    <td><input name="mobtel" type="text" class="forms" value="" maxlength="30"></td>
    </tr>
  <tr valign="baseline">
    <td nowrap align="right">Email:</td>
    <td><input name="email" type="text" class="forms" value="" maxlength="50"></td>
    </tr>
  <tr valign="baseline">
    <td colspan="2" align="right" nowrap><hr size="1"></td>
  </tr>
  <tr valign="baseline">
    <td align="right" nowrap>User Name:</td>
    <td><input name="username" type="text" class="forms" value="" size="40"></td>
    </tr>
  
  <tr valign="baseline">
    <td nowrap align="right">Password:</td>
    <td><input name="umd5" type="password" class="forms" value="" size="40"></td>
    </tr>
  <tr valign="baseline">
    <td nowrap align="right">Confirm Password:</td>
    <td><input name="CUMD5" type="password" class="forms" id="CUMD5" value="" size="40" ></td>
    </tr>
  <tr valign="baseline">
    <td nowrap align="right">PIN number: </td>
    <td><input name="PIN" type="password" class="forms" id="PIN" value="" size="40" ></td>
    </tr>
    <tr valign="baseline">
    <td align="right" nowrap>User code:</td>
    <td><?php echo $code?></td>
    </tr>
	<!-- <tr valign="baseline">
    <td align="right" nowrap>Select member if not an internal fke user:</td>
    <td><select name="member" id="member" class="forms">
			       <?php //showmembers();?>
			       </select></td>
    </tr>-->
</table>
</fieldset>
   </td>
   <td width="51%" valign="top">
   <fieldset>
<legend>Groups </legend>
<table width="100%" border="0" cellspacing="0" cellpadding="4">
  <tr valign="baseline">
    
	<?php 
	
	$selectSQL = "SELECT groups.groupname, groups.groupid, groups.groupdesc, groups.sys FROM groups WHERE groups.groupname <> 'System administrator' AND groups.groupname <> 'Company Administrator' ORDER BY groups.groupname";
	
	$done = mysqli_query($selectSQL, $hr) or die("ERROR: Trying to retrieve groups list - ".mysqli_error());
	$groupslist = mysqli_fetch_assoc($done);
	?>
    <td width="472" rowspan="4">
	<?php if (mysqli_num_rows($done)>0) {  ?>
		<table width="100%" border="0" > 
		<tr>
		    <td width="47%">&nbsp;</td>
		    <td width="32%">Default group </td>
		    <td width="21%">Other</td>
		    </tr>
		  <?php do {  ?>
		 
		  <tr>
			<td><?php echo $groupslist['groupname']; ?></td>
			<td><label>
			  <input name="groupid" id="groupid" type="radio" value="<?php echo $groupslist['groupid'];  ?>">
			</label></td>
			<td><input name="<?php echo $groupslist['groupid'];  ?>" id="<?php echo $groupslist['groupid'];  ?>" type="checkbox" value="<?php echo $groupslist['groupid'];  ?>"></td>
		  </tr>
		  <?php } while ($groupslist = mysqli_fetch_assoc($done)); ?>
		</table>
	<?php }  ?>	</td>
    
    </tr>
  <tr valign="baseline">
    <td colspan="4" align="right" nowrap><hr size="1"></td>
  </tr>
</table>
</fieldset>
   </td>
   
  </tr>
  <tr align="center"><td colspan="3"><fieldset>
		<table width="100%" cellpadding="1" cellspacing="0">
          
          <tr valign="baseline">
           <td > <div align="center">
             <input type="hidden" name="MM_insert" value="form1">
             <input name="save" type="submit" class="formsBlue" value="Save">
             <input name="button" type="button" class="formsorg" onClick="javascript:location='usr.php'" value="Cancel">
           </div></td>
          </tr>
        </table>
</fieldset></td></tr>
</table></form>
</body>
</html>
