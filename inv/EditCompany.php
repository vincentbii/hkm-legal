<?php require_once('../connections/hr.php'); ?>
<?php
include('../activelog.php');
mysqli_select_db($database_hr, $hr);
$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "updatecompany")) {
  $updateSQL = sprintf("UPDATE companyinfo SET company=%s,address=%s,  telphone=%s, faxphone=%s, mobphone=%s, email=%s, location=%s, contactperson=%s  WHERE com_id=%s",
    GetSQLValueString($_POST['comp'], "text"),           
	GetSQLValueString($_POST['address'], "text"),
	GetSQLValueString($_POST['telphone'], "text"),
	GetSQLValueString($_POST['faxphone'], "text"),
	GetSQLValueString($_POST['mobphone'], "text"),
	GetSQLValueString($_POST['email'], "text"),
	GetSQLValueString($_POST['location'], "text"),
	GetSQLValueString($_POST['contactperson'], "text"),
	GetSQLValueString($_POST['com_id'], "text"));
  $Result1 =mysqli_query($updateSQL,$hr) or die(mysqli_error());

  $updateGoTo = "index.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
    $updateGoTo .= $_SERVER['QUERY_STRING'];
  }
  //header(sprintf("location: %s", $updateGoTo));
	?>
	<script language="JavaScript" type="text/javascript">
		location="index.php";
	</script>
	<?php
}
$query_ec = "SELECT * FROM companyinfo WHERE com_id = '".$_SESSION['company']."'";
$ec =mysqli_query($query_ec,$hr) or die(mysqli_error());
$row_ec =mysqli_fetch_assoc($ec);
$totalRows_ec =mysqli_num_rows($ec);

?>

<html>
<head>
<title>ECOTOURISM KENYA MANAGEMENT INFORMATION  SYSTEM</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../styles/default.css" rel="stylesheet" type="text/css">
</head>
<script>
<!--
function shiftFrameURL(newpage) {
	var fr = document.getElementById('costcenter');
	fr.src = newpage;
}
-->
</script>
<body>
<table width="100%"  border="2" cellpadding="0" cellspacing="3" bordercolor="#0A6EC3" >
  <tr>
    <td valign="top"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td valign="top"><?php include('../includes/header.php'); ?></td>
      </tr>
      <tr>
        <td valign="top"><img src="../images/template_06.gif" width="100%" height="6"></td>
      </tr>
      <tr>
        <td valign="top"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="100%" class="spacer5"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td><?php include("../includes/cadmstrip.php"); ?></td>
              </tr>
              <tr>
                <td colspan="4" class="intspace"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="100%"><table width="100%"  border="0" cellspacing="0" cellpadding="4">
                      <tr valign="top">
                        <td width="170">
                        	<?php include(getusermenu()); ?>
                        </td>
                        <td><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                          
                          <tr>
                            <td colspan="2"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td valign="top">
								<table width="100%"  border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td valign="top" class="baseline">Update a  company's information using the form below.</td>
                            </tr>
                            <tr>
                              <td valign="top" class="spacer5">
                                <form action="<?php echo $editFormAction; ?>" method="POST" name="updatecompany" id="updatecompany">
                                  <fieldset>
								  <legend>Company Details</legend>
                                  <table width="350" border="0" cellspacing="0" cellpadding="5">
                                    <tr valign="baseline">
                                      <td width="59" align="right" nowrap>Company:</td>
                                      <td width="271"><input name="comp" type="text" class="forms" id="comp" value="<?php echo $row_ec['company']; ?>" size="50"></td>
                                    </tr>
                                    
                                    <tr valign="baseline">
                                      <td nowrap align="right" valign="top">Address:</td>
                                      <td><textarea name="address" cols="50" rows="4" class="forms"><?php echo $row_ec['address']; ?></textarea>
                                      </td>
                                    </tr>
                                  </table>
                                  </fieldset>
                                  <br>
                                  <fieldset>
								  <legend>Company Contacts</legend>
								  <table width="350" border="0" cellspacing="0" cellpadding="5">
                                    <tr valign="baseline">
                                      <td nowrap align="right">Contact Person:</td>
                                      <td><input name="contactperson" type="text" class="forms" id="contactperson" value="<?php echo $row_ec['contactperson']; ?>" size="50"></td>
                                    </tr>
                                    <tr valign="baseline">
                                      <td nowrap align="right">Telephone:</td>
                                      <td><input name="telphone" type="text" class="forms" value="<?php echo $row_ec['telphone']; ?>" size="32"></td>
                                    </tr>
                                    <tr valign="baseline">
                                      <td nowrap align="right">Fax:</td>
                                      <td><input name="faxphone" type="text" class="forms" value="<?php echo $row_ec['faxphone']; ?>" size="32"></td>
                                    </tr>
                                    <tr valign="baseline">
                                      <td nowrap align="right">Mobile:</td>
                                      <td><input name="mobphone" type="text" class="forms" value="<?php echo $row_ec['mobphone']; ?>" size="32"></td>
                                    </tr>
                                    <tr valign="baseline">
                                      <td nowrap align="right">Email:</td>
                                      <td><input name="email" type="text" class="forms" value="<?php echo $row_ec['email']; ?>" size="50"></td>
                                    </tr>
                                  </table>
                                  </fieldset>
                                  <br>
                                  <fieldset>
								  <legend>Company Contacts</legend>
								  <table width="350" cellpadding="5" cellspacing="0">
                                    <tr valign="baseline">
                                      <td width="76" align="right" valign="top" nowrap>Location:</td>
                                      <td width="252"><textarea name="location" cols="30" rows="3" id="location"><?php echo $row_ec['location']; ?></textarea></td>
                                    </tr>
                                    <tr valign="baseline">
                                      <td colspan="2" align="right" nowrap><hr size="1"></td>
                                      </tr>
                                    <tr align="center" valign="baseline">
                                      <td colspan="2" nowrap><input type="hidden" name="MM_update" value="updatecompany">
<input type="hidden" name="com_id" value="<?php echo $row_ec['com_id']; ?>">                                        
<input type="submit" class="formsBlue" value="Update company Information">
                                        <input type="button" class="formsorg" value="Cancel" onClick="javascript:history.go(-1);"></td>
                                      </tr>
                                  </table>
								  </fieldset>
                                    </form>
                                </td>
                            </tr>
                        </table>
								</td>
                              </tr>
                            </table></td>
                          </tr>
                        </table></td>
                      </tr>
                    </table></td>
                    </tr>
                </table></td>
              </tr>
            </table></td>
            </tr>
        </table></td>
      </tr>
      <tr>
        <td ><?php include('../includes/footer.php'); ?></td>
      </tr>
    </table></td>
  </tr>
</table>
</body>
</html>