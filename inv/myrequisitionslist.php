<?php   require_once('../connections/eProc.php'); ?>
<?php require_once('../activelog.php'); ?>
<?php
$employee=getuserid();
$_SESSION['emp']=$employee;
$currentPage = $_SERVER["PHP_SELF"];

$maxRows_Requisition = 30;
$pageNum_Requisition = 0;
if (isset($_GET['pageNum_Requisition'])) {
  $pageNum_Requisition = $_GET['pageNum_Requisition'];
}
$startRow_Requisition = $pageNum_Requisition * $maxRows_Requisition;

mysqli_select_db($eProc, $database_eProc);
$query_Requisition = "SELECT * FROM requisitions WHERE requestedby='".$_SESSION['emp']."' ORDER BY id DESC  ";
$query_limit_Requisition = sprintf("%s LIMIT %d, %d", $query_Requisition, $startRow_Requisition, $maxRows_Requisition);
$Requisition = mysqli_query($eProc, $query_limit_Requisition) or die(mysqli_error($eProc));
$row_Requisition = mysqli_fetch_assoc($Requisition);

if (isset($_GET['totalRows_Requisition'])) {
  $totalRows_Requisition = $_GET['totalRows_Requisition'];
} else {
  $all_Requisition = mysqli_query($eProc, $query_Requisition);
  $totalRows_Requisition = mysqli_num_rows($all_Requisition);
}
$totalPages_Requisition = ceil($totalRows_Requisition/$maxRows_Requisition)-1;

$queryString_Requisition = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_Requisition") == false && 
        stristr($param, "totalRows_Requisition") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_Requisition = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_Requisition = sprintf("&totalRows_Requisition=%d%s", $totalRows_Requisition, $queryString_Requisition);
?>

<html>
<head>
<title>ECOTOURISM KENYA MANAGEMENT INFORMATION  SYSTEM</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../styles/default.css" rel="stylesheet" type="text/css">

<style type="text/css">
<!--
.style1 {
	color: #FF0000;
	font-weight: bold;
}
-->
</style>
</head>
<body>

<table width="100%" border="0" cellpadding="4" cellspacing="0" >

  <tr>
   <td width="9%"   class="inputdef" style="font-weight: bold">Proposal  No </td>
    <td width="15%"   class="inputdef" style="font-weight: bold"> Date Prepared</td>
	<td width="18%"   class="inputdef" style="font-weight: bold">Proposal Status</td>
	<td width="29%"   class="inputdef" style="font-weight: bold">Proposed  Amount</td>
  </tr>
  <?php if ($totalRows_Requisition > 0) { ?>
  <?php do { ?>
  <tr>
  <td ><a href="../inv/viewrequisition.php?id=<?php echo $row_Requisition['id'] ?>"><strong><?php echo $row_Requisition['id']?></strong></a> </td>  
  <td ><?php echo $row_Requisition['tdate']?></td>
   <td ><?php echo $row_Requisition['status']?></td>
    <td ><?php echo $row_Requisition['tcost']?></td>
    </tr>
  <?php } while ($row_Requisition = mysqli_fetch_assoc($Requisition)); ?>
  <tr>
    <td colspan="12" class="mainbase"><table width="97%"  border="0" cellspacing="0" cellpadding="3">
      <tr align="center">
        <td width="45%">
          <table width="78%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
              <td width="23%" align="center"><?php if ($pageNum_Requisition > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_Requisition=%d%s", $currentPage, 0, $queryString_Requisition); ?>">First</a>
                <?php } // Show if not first page ?>              </td>
              <td width="31%" align="center"><?php if ($pageNum_Requisition > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_Requisition=%d%s", $currentPage, max(0, $pageNum_Requisition - 1), $queryString_Requisition); ?>">Previous</a>
                <?php } // Show if not first page ?>              </td>
              <td width="23%" align="center"><?php if ($pageNum_Requisition < $totalPages_Requisition) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_Requisition=%d%s", $currentPage, min($totalPages_Requisition, $pageNum_Requisition + 1), $queryString_Requisition); ?>">Next</a>
                <?php } // Show if not last page ?>              </td>
              <td width="23%" align="center"><?php if ($pageNum_Requisition < $totalPages_Requisition) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_Requisition=%d%s", $currentPage, $totalPages_Requisition, $queryString_Requisition); ?>">last</a>
                <?php } // Show if not last page ?>              </td>
            </tr>
          </table></td>
        <td width="55%">&nbsp; Showing <strong><?php echo ($startRow_Requisition + 1) ?></strong> to <strong><?php echo min($startRow_Requisition + $maxRows_Requisition, $totalRows_Requisition) ?></strong> of <strong><?php echo $totalRows_Requisition ?></strong> </td>
      </tr>
    </table></td>
  </tr>
  <?php } else { ?>
  <tr>
    <td colspan="12" class="mainbase"><span class="style1">No records to display! </span></td>
  </tr>
  <?php } ?>
</table>
</body>
</html>
<?php
mysqli_free_result($Requisition);
?>

