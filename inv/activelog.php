<SCRIPT TYPE="text/javascript">
<!--
function popupform(myform, windowname) {
	if (! window.focus)return true;
	window.open('', windowname, 'width=600,left=200,top=200');
	myform.target=windowname;
	return true;
}

function OpenPopup(myform, windowname, height, windth) {
	if (! window.focus)return true;
	window.open('', windowname, 'height=height,width=width,left=400,top=200');
	myform.target=windowname;
	return true;
}
//-->

/**
 * DHTML date validation script. Courtesy of SmartWebby.com (http://www.smartwebby.com/dhtml/)
 */
// Declaring valid date character, minimum year and maximum year
var dtCh= "/";
var minYear=1900;
var maxYear=2100;

function isInteger(s){
	var i;
    for (i = 0; i < s.length; i++){
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    // All characters are numbers.
    return true;
}

function stripCharsInBag(s, bag){
	var i;
    var returnString = "";
    // Search through string's characters one by one.
    // If character is not in bag, append to returnString.
    for (i = 0; i < s.length; i++){
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
}

function daysInFebruary (year){
	// February has 29 days in any year evenly divisible by four,
    // EXCEPT for centurial years which are not also divisible by 400.
    return (((year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28 );
}
function DaysArray(n) {
	for (var i = 1; i <= n; i++) {
		this[i] = 31
		if (i==4 || i==6 || i==9 || i==11) {this[i] = 30}
		if (i==2) {this[i] = 29}
   }
   return this
}

function isDate(dtStr){
	var daysInMonth = DaysArray(12)
	var pos1=dtStr.indexOf(dtCh)
	var pos2=dtStr.indexOf(dtCh,pos1+1)
	var strMonth=dtStr.substring(0,pos1)
	var strDay=dtStr.substring(pos1+1,pos2)
	var strYear=dtStr.substring(pos2+1)
	strYr=strYear
	if (strDay.charAt(0)=="0" && strDay.length>1) strDay=strDay.substring(1)
	if (strMonth.charAt(0)=="0" && strMonth.length>1) strMonth=strMonth.substring(1)
	for (var i = 1; i <= 3; i++) {
		if (strYr.charAt(0)=="0" && strYr.length>1) strYr=strYr.substring(1)
	}
	month=parseInt(strMonth)
	day=parseInt(strDay)
	year=parseInt(strYr)
	if (pos1==-1 || pos2==-1){
		alert("The date format should be : mm/dd/yyyy")
		return false
	}
	if (strMonth.length<1 || month<1 || month>12){
		alert("Please enter a valid month")
		return false
	}
	if (strDay.length<1 || day<1 || day>31 || (month==2 && day>daysInFebruary(year)) || day > daysInMonth[month]){
		alert("Please enter a valid day")
		return false
	}
	if (strYear.length != 4 || year==0 || year<minYear || year>maxYear){
		alert("Please enter a valid 4 digit year between "+minYear+" and "+maxYear)
		return false
	}
	if (dtStr.indexOf(dtCh,pos2+1)!=-1 || isInteger(stripCharsInBag(dtStr, dtCh))==false){
		alert("Please enter a valid date")
		return false
	}
return true
}

function ValidateForm(){
	var dt=document.frmSample.txtDate
	if (isDate(dt.value)==false){
		dt.focus()
		return false
	}
    return true
 }

function round_decimals(original_number, decimals) {
    var result1 = original_number * Math.pow(10, decimals)
    var result2 = Math.round(result1)
    var result3 = result2 / Math.pow(10, decimals)
    return pad_with_zeros(result3, decimals)
}

function pad_with_zeros(rounded_value, decimal_places) {

    // Convert the number to a string
    var value_string = rounded_value.toString()

    // Locate the decimal point
    var decimal_location = value_string.indexOf(".")

    // Is there a decimal point?
    if (decimal_location == -1) {

        // If no, then all decimal places will be padded with 0s
        decimal_part_length = 0

        // If decimal_places is greater than zero, tack on a decimal point
        value_string += decimal_places > 0 ? "." : ""
    }
    else {

        // If yes, then only the extra decimal places will be padded with 0s
        decimal_part_length = value_string.length - decimal_location - 1
    }

    // Calculate the number of decimal places that need to be padded with 0s
    var pad_total = decimal_places - decimal_part_length

    if (pad_total > 0) {

        // Pad the string with 0s
        for (var counter = 1; counter <= pad_total; counter++)
            value_string += "0"
        }
    return value_string
}

</SCRIPT>
<?php
// Report all errors except E_NOTICE
error_reporting(E_ALL ^ E_NOTICE);

// DATABASE CONNECTION
// -----------------------
define("HOSTNAME","localhost","true");
define("DB","hrsystem","true");
define("username","root","true");
define("PWD","berk**","true");


// ------------------------------------------------------------------------------
// AUXILLIARY FUNCTIONS
// ------------------------------------------------------------------------------
// DATE FUNCTION
$dt = getdate();
$date = sprintf("%s-%s-%s", $dt['year'], $dt['mon'], $dt['mday']);
$long_date = sprintf("%s %s, %s", $dt['month'], $dt['mday'], $dt['year']);
$time = sprintf("%s:%s:%s", $dt['hours'], $dt['minutes'], $dt['seconds']);
// ------------------------------------------------------------------------------
// MONTH ARRAY
// ------------------------------------------------------------------------------
		$MONTH = array(1=>'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
		$DAY = array(1=>'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');
// ------------------------------------------------------------------------------
//Includes for functions not on this file but required
include('includes/scripts/user.php');
include('includes/scripts/requisitions.php');
include('includes/scripts/catalogue.php');
include_once('includes/ean13class.php');
//--------------------------------------------------------------------
function log_activity($uid, $action, $section, $act) {
	// --------------------------------------------------------------
	// FUNCTION FOR LOGGING USER ACTIVITY WITHIN THE DONOR DATABASE
	// --------------------------------------------------------------
	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	// --------------------------------------------------------------
	  $dt = getdate(); $ts = time();
	  $date = sprintf("%s-%s-%s", $dt['year'], $dt['mon'], $dt['mday']);
	  $time = sprintf("%s:%s:%s", $dt['hours'], $dt['minutes'], $dt['seconds']);
	  $logSQL = "INSERT INTO activelog (uid, action, adate, atime, section, ACT, TSTAMP) VALUES ('". $uid ."','". $action ."','". $date ."','". $time ."','". $section ."','". $act ."','". $ts ."')";
	// --------------------------------------------------------------
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("LOG ERROR: Trying to save trackable user activity - ".mysqli_error());
	// --------------------------------------------------------------
		return $done;
	}

function evalNull($value, $nullmsg, $msg) {
	if (is_null($value) || $value == "") {
		$message = $nullmsg;
	} else {
		$message = $msg;
	}
	return $message;
}

// This function writes the Javascript url onto the page
// Important when you need PHP generated urls to use!
function addJavascript($url) {
	echo '<script language="javascript" src="'.$url.'"></script>';
}

function assessVAL($val) {
	if ($val == 1) {
		$ret = "YES";
	} else {
		$ret = "NO";
	}
	return $ret;
}

function GetSQLValueString($theValue, $theType, $theDehredValue = "", $theNotDehredValue = "")
	{
	  $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

	  switch ($theType) {
		case "text":
		  $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
		  break;
		case "long":
		case "int":
		  $theValue = ($theValue != "") ? intval($theValue) : "NULL";
		  break;
		case "double":
		  $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
		  break;
		case "date":
		  $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
		  break;
		case "dehred":
		  $theValue = ($theValue != "") ? $theDehredValue : $theNotDehredValue;
		  break;
	  }
	  return $theValue;
}

function make_time($day, $mon, $year, $time) {
	// Break time to hours and minutes
	// ------------------------------------
	list($hr, $min) = explode(':', $time);
	// ------------------------------------
	$timestamp = mktime($hr, $min, 0 ,$mon, $day, $year);

	return $timestamp;
}
// --------------------------------------------------------------------------------
// Function to process timestamps and return date
// --------------------------------------------------------------------------------
function process_time($ts, $form) {

	if ($ts == '') { $ts = time(); }

	$pdt = getdate($ts);
	$pdate = sprintf("%s-%s-%s", $pdt['year'], $pdt['mon'], $pdt['mday']);
	$pshort_date = sprintf("%s-%s-%s", $pdt['year'], $pdt['month'], $pdt['mday']);
	$pshorter_date = sprintf("%s-%s-%s", $pdt['year'], substr($pdt['month'], 0, 3), $pdt['mday']);
	$plong_date = sprintf("%s %s, %s", $pdt['month'], $pdt['mday'], $pdt['year']);
	$ptime = sprintf("%s:%s", $pdt['hours'], $pdt['minutes']);
	$pdate_time = $pdate."&nbsp;&nbsp; ".$ptime;
	$pdate_time =  date("Y-m-d H:i:s");

	switch ($form) {
		case '1':
		$FORMATTED_DATE = $pdate;
		break;

		case '2':
		$FORMATTED_DATE = $pshort_date;
		break;

		case '3':
		$FORMATTED_DATE = $plong_date;
		break;

		case '4':
		$FORMATTED_DATE = $pdate_time;
		break;

		case '5':
		$FORMATTED_DATE = $pshorter_date;
		break;

		default:
		$FORMATTED_DATE = $pdate;
		break;
	}

	return $FORMATTED_DATE;
}



function recordexists($tablename,$columnname,$value) {
	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
  
	 	 $logSQL = "SELECT ".$columnname." FROM ".$tablename." WHERE ".$columnname." = '".$value."'";
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to check record exists 9 - ".mysqli_error(). $logSQL);
	  //$rowuser = mysqli_fetch_assoc($done);
	  $num_rows = mysqli_num_rows($done); //Get number of rows
	if ($num_rows > 0) {
	   return 'yes';
	} else {
	   return 'no';
	}
}
function recordexists3cols($tablename,$col1,$col2,$val1,$val2) {
	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);  
	 	 $logSQL = "SELECT * FROM ".$tablename." WHERE ".$col1." = '".$val1."' AND ".$col2." = '".$val2."' ";
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to check record exists  - ".mysqli_error(). $logSQL);
	  //$rowuser = mysqli_fetch_assoc($done);
	  $num_rows = mysqli_num_rows($done); //Get number of rows
	if ($num_rows > 0) {
	   return TRUE;
	} else {
	   return FALSE;
	}
}

function recordexists2cols($tablename,$col1,$col2,$val1,$val2) {
	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);  
	 	 $logSQL = "SELECT * FROM ".$tablename." WHERE ".$col1." = '".$val1."' AND ".$col2." = '".$val2."' ";
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to check record exists  - ".mysqli_error(). $logSQL);
	  $rowuser = mysqli_fetch_assoc($done);
	  $num_rows = mysqli_num_rows($done); //Get number of rows
	if ($num_rows > 0) {
	   return TRUE;
	} else {
	   return FALSE;
	}
}
function getEndValue($rowuser) {
	end($rowuser);
	$value = current($rowuser);
	return $value;
}

function getStartValue($rowuser) {
	reset($rowuser);
	$value = current($rowuser);
	return $value;
}



function remFunnyChars($DIRTY) {
	$CLEAN = str_replace('/','',$DIRTY);
	$CLEAN = str_replace('\\','',$CLEAN);
	$CLEAN = str_replace(' ',',',$CLEAN);
	return $CLEAN;
}

// -------------------------------------------------------------------------
// Create name hash for unique identification of event
// -------------------------------------------------------------------------
function createHASH($name = '') {

	// RANDOM ARRAY FEED
	//-----------------------------------------------------------------
	$alpha = array('a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z');
	$numer = array('0','1','2','3','4','5','6','7','8','9');
	// ---------------------------------------------------------------------------------------------------------------------

	$word = explode(" ",$name);
	$new_sen = "";

	// Happens only when name is not sent

	if ($name == '') {
	// Create Random String
	for ($m = 1; $m <= 10; $m++) {
		$id = mt_rand(1,count($alpha));
		$id2 = mt_rand(1,count($numer));
		// Append to string
		
		$new_sen .= $alpha[$id].$numer[$id2];
			}
	} else {

	$rand = rand(12345, 99999);
		for ($i = 1; $i <= count($word); $i++) {
			if ($i == 1) {
				$new_sen .= $rand;
				$new_sen .= $word[$i];
			} else {
				$new_sen .= $word[$i];
			}
		}
	}
	return strtoupper($new_sen);
	}

function rem_space($name) {
	$word = explode(" ",$name);
	$new_sen = substr_replace($name,"",0,1);
	$new_sen = substr_replace($name,"",strlen($name),-1);
	
	return strtoupper($new_sen);
}

function listShow($divs, $table, $main, $id, $url = '') {

	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT ".$main.",".$id." FROM ".$table;
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve data - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $tot_rows = mysqli_num_rows($done);
	  $dist = floor($tot_rows / $divs);
	  $d_mod = $tot_rows % $divs;
	  $div = $divs;
	  $c = array();
	  // for ($x = 1; $x <= $div; $x++) { $c[$x] = $dist; }
	  for ($x = 1; $x <= $div; $x++) { $c[$x] = $dist; }
	  $mt[1] = 0; $mt[2] = $dist; $mt[3] = $dist * 2; $mt[4] = $dist * 3;
	  if ($d_mod > 0) { $c[1] = $c[1] + $d_mod; $c[2] = $c[2] + $d_mod; }// $div = $divs + 1;
	
		$l = 0;
			do {
				$l++;
				$CAT[$l] = $rowuser[$main];
				$IDS[$l] = $rowuser[$id];
			} while ($rowuser = mysqli_fetch_assoc($done));
			//print_r(array_values($CAT));

		echo "<table width=\"100%\"  border=\"0\" align=\"left\" cellpadding=\"3\" cellspacing=\"0\">
				<tr valign=\"top\">"; $nct = 0;
	
				for ($m = 1; $m <= $div; $m++) {
					echo "<td><table width=\"100%\"  border=\"0\" cellspacing=\"0\" cellpadding=\"3\">";
				
						for ($z = 1; $z <= $c[$m]; $z++) {
							$a = $z + $mt[$m];
							echo "<tr>
								  <td width=\"5%\"><img src=\"bullet.gif\"></td>";
								  if ($url != '') {
									echo "<td width=\"95%\"><a href=\"".$url."?id=".$IDS[$a]."\">".$CAT[$a]."</a></td>";
								  } else {
									echo "<td width=\"95%\">".$CAT[$a]."</td>";
								  }
							echo "</tr>";
						}
					echo "</table></td>";
				}

		echo "</tr>
			 </table>";
	}

function arrayListShow($divs, $commalist) {

	$array = explode(',',$commalist);
	
	$tot_rows = count($array);
	$dist = floor($tot_rows / $divs);
	$d_mod = $tot_rows % $divs;
	$div = $divs;
	$c = array();
	// for ($x = 1; $x <= $div; $x++) { $c[$x] = $dist; }
	for ($x = 1; $x <= $div; $x++) { $c[$x] = $dist; }
	$mt[1] = 0; $mt[2] = $dist; $mt[3] = $dist * 2; $mt[4] = $dist * 3;
	if ($d_mod > 0) { $c[1] = $c[1] + $d_mod; $c[2] = $c[2] + $d_mod; }// $div = $divs + 1;

	echo "<table width=\"100%\"  border=\"0\" align=\"left\" cellpadding=\"3\" cellspacing=\"0\">
			<tr valign=\"top\">"; $nct = 0;
	
			for ($m = 1; $m <= $div; $m++) {
				echo "<td><table width=\"100%\"  border=\"0\" cellspacing=\"0\" cellpadding=\"3\">";
			
					for ($z = 1; $z <= $c[$m]; $z++) {
						$a = $z + $mt[$m];
						if (empty($array[$a])) {
							// do nothing
						} else {
						echo "<tr>
							  <td width=\"5%\"><img src=\"bullet.gif\"></td>";
								echo "<td width=\"95%\">".$array[$a]."</td>";
						echo "</tr>";
						}
					}
				echo "</table></td>";
			}

	echo "</tr>
		 </table>";
	}


function showCompanies($showadmin="",$cc="") {

	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
		if ($_SESSION['USERTYPE']=="") {

		} else {

		}
	  $logSQL = "SELECT * FROM companyinfo ";

	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve companies list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
	
	$selected="n";
		if ($nums > 0) {
		  do {
				if ($cc != '' && $cc == $rowuser['com_id']) {
					echo "<option selected value=\"".$rowuser['com_id']."\">".$rowuser['company']."</option>";
					$selected="y";
				} else {
					if ($rowuser['default']=="Yes") {
						if ($selected=="n") {
							echo "<option selected value=\"".$rowuser['com_id']."\">".$rowuser['company']."</option>";
						}
					} else {
						echo "<option value=\"".$rowuser['com_id']."\">".$rowuser['company']."</option>";
					}
				}

		  } while($rowuser = mysqli_fetch_assoc($done));
		  if ($showadmin == 'SHOWADMIN') {
					echo "<option value='ADM' class='admred'>Administrative Login</option>";
				}
		} else {
				echo "<option value='ADM' class='admred'>Administrative Login</option>";
				echo "<option>No Companies Created</option>";
		}
	}

function showGroups($dp = '', $IncludeCoAdmin=FALSE,$showid="1") {

	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);

		if ($IncludeCoAdmin==FALSE) {
			$logSQL = "SELECT groups.groupid, groups.groupname, groups.groupid, groups.groupdesc, groups.sys
	FROM groups WHERE groups.sys =0 AND groups.groupname <> 'company administrator'   ORDER BY groups.groupname";
		} else {
			  $logSQL = "SELECT groups.groupid, groups.groupname, groups.groupid, groups.groupdesc, groups.sys FROM groups WHERE groups.groupname <> 'company administrator' ORDER BY groups.groupname";
		}


	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve groups list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);

	  $id=0;
	  $nm="";

		if ($nums > 0) {
		echo "<option > </option>";
		  do {
		  
				$id=$rowuser['groupid'];
				$nm=$rowuser['groupname'];
				if ($dp != '' && $dp == $rowuser['groupid']) {
					if ($showid=="1") {
						echo "<option selected value=\"".$id."\">".$nm."</option>";
					} else {
						echo "<option selected value=\"".$nm."\">".$nm."</option>";
					}
				} else {
					if ($showid=="1") {
						echo "<option value=\"".$id."\">".$nm."</option>";
					} else {
						echo "<option value=\"".$nm."\">".$nm."</option>";
					}
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No groups Selected</option>";
		}
	}

function showGrouptypes($selected = '') {
	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	
	$logSQL = "SELECT * FROM grouptypes ";

	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve group types list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);

		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				$id=$rowuser['id'];
				$nm=$rowuser['grouptypename'];
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['grouptypename']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['grouptypename']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No groups types Selected </option>";
		}
}


function getgrouptype($selected) {

	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	
		$logSQL = "SELECT * FROM grouptypes WHERE id = '".$selected."'";

	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve group types list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  
	  return  $rowuser['grouptypename'];
	 
} 


function InsertUserGroups  ($authcode,$groupid) { 
		$CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);

		mysqli_select_db(DB, $CON);
	  //groups 
	  $sql="DELETE FROM user_groups WHERE userid = '".$authcode."'";
		$Result1 = mysqli_query($sql, $CON) or die(mysqli_error());
		
		$sql="INSERT INTO user_groups (userid,groupid) VALUES ('".$authcode."','".$groupid."')";
		//echo $sql; 
		
		$Result1 = mysqli_query($sql, $CON) or die(mysqli_error());
					
		$selectSQL = "SELECT  * FROM groups WHERE groups.groupname <> 'System administrator'  ";
		$done = mysqli_query($selectSQL, $CON) or die("ERROR: Trying to retrieve groups list - ".mysqli_error());
		$groupslist = mysqli_fetch_assoc($done);
		
		if (mysqli_num_rows($done)>0) 
		{ 
			do { 
				if (isset($_POST[$groupslist['groupid']]) && $_POST[$groupslist['groupid']]!='' && $_POST[$groupslist['groupid']]!=$groupid) {
					$sql="INSERT INTO user_groups (userid,groupid) VALUES ('".$authcode."','".$groupslist['groupid']."')";
					//echo $sql; 
					
					$Result1 = mysqli_query($sql, $CON) or die(mysqli_error());
				}
			} while ($groupslist = mysqli_fetch_assoc($done));
		} 
} 



function getcompanyadmingroup() {
	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
		$logSQL = "SELECT groups.groupid, groups.groupname,groups.groupid, groups.groupdesc, groups.sys
	FROM groups WHERE groups.sys = 0 AND groups.groupname = 'Company Administrator'";
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve groups list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
			return $rowuser['groupid'];
		}
		
	}


function getcompany($enum) {
	
	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT com_id, company FROM companyinfo WHERE com_id = '".$enum."'";
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve company name - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  return $rowuser['company'];
	}

 

function getcompanyID($enum) {
	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	 trim ($enum); 
	  $logSQL = "SELECT com_id FROM companyinfo WHERE company = '".$enum."'";
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve company name - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);

	  return $rowuser['com_id'];
	}

function getGroup($enum) {

  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
  $logSQL = "SELECT groupname FROM groups WHERE groupid = '".$enum."'";
  mysqli_select_db(DB, $CON);
  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve group name - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
  return $rowuser['groupname'];
}

function getGroupID($enum) {
  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
  $logSQL = "SELECT groupid FROM groups WHERE groupname = '".$enum."'";
  mysqli_select_db(DB, $CON);
  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve group name - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
  return $rowuser['groupid'];
}

function getgrouptypeid($id) 
{ 
  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
  $logSQL = "SELECT grouptypeid FROM groups WHERE groupid = '".$id."'";
  mysqli_select_db(DB, $CON);
  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve group type id - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
  return $rowuser['grouptypeid'];
} 
function scopeValid($valid) {
	// Break Apart
	list($time, $period) = explode(' ', $valid, 2);
	switch ($period) {

		case 'M':
		$per = 'Months';
		break;

		case 'Y':
		$per = 'Years';
		break;

		default:
		$per = 'Months';
		break;
	}

	$ret = $time." ".$per;

	return $ret;
}

// REQ SESSION status
// --------------------------------------------------------------
function showIcon($sessval) {
	switch ($sessval) {
		case 'N':
		$icon = 'grymkr.gif';
		break;

		case 'I':
		$icon = 'orgmkr.gif';
		break;

		case 'C':
		$icon = 'grntck.gif';
		break;

		default:
		$icon = 'grymkr.gif';
		break;
	}

	return $icon;
}


function msgbox($msg) {
	?>
	<script language="JavaScript" type="text/javascript">
		alert('<?php echo $msg ?>')
	</script>

	<?php
}



function getUsername($enum) {

  $hr = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error($hr),E_USER_ERROR); 
  $logSQL = "SELECT * FROM users WHERE authcode = '".$enum."'";
  mysqli_select_db($hr,DB);
  $done = mysqli_query($hr,$logSQL) or die("ERROR: Trying to retrieve user details - ".mysqli_error($hr));
  $rowuser = mysqli_fetch_assoc($done);
  return $rowuser['fulname'] ;
}




function getadminEmail() {
  $hr = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error($hr),E_USER_ERROR); 
  $logSQL = "SELECT email FROM users WHERE admin ='1'";
  mysqli_select_db($hr,DB);
  $done = mysqli_query($hr,$logSQL) or die("ERROR: Trying to retrieve user details - ".mysqli_error($hr));
  $rowuser = mysqli_fetch_assoc($done);
  return $rowuser['email'] ;
}

function getusergroup($user) {
  $hr = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error($hr),E_USER_ERROR); 
  $logSQL = "SELECT  groupid FROM users WHERE  authcode ='".$user."'";
  mysqli_select_db($hr,DB);
  $done = mysqli_query($hr,$logSQL) or die("ERROR: Trying to retrieve user details - ".mysqli_error($hr));
  $rowuser = mysqli_fetch_assoc($done);
  return $rowuser['groupid'] ;
}

function createbarcode($value) { 

	$ean13 = new ean13;
	$ean13->article = $value;   // initial article code
	$ean13->article .= $ean13->generate_checksum();   // add the proper checksum value
	//$ean13->reverse();   // the string is printed backwards
	$value = $ean13->codestring();   // returns a string as input for the truetype font
	return $value ;  // render the image as PNG image

} 

function getcompanyaddress ($enum) {
	 
	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM companyinfo WHERE com_id = '".$enum."'";
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve company details - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  return $rowuser['address'];
}

function getcompanyaddress2 ($enum) {

	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM companyinfo WHERE com_id = '".$enum."'";
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve company details - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  return $rowuser['address2'];

}

function getcompanytown ($enum) {

	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM companyinfo WHERE com_id = '".$enum."'";
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve company details - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  return $rowuser['town'];

}

function getcompanycountry ($enum) {

	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM companyinfo WHERE com_id = '".$enum."'";
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve company details - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  return $rowuser['country'];

}

function getcompanylocation ($enum) {

	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM companyinfo WHERE com_id = '".$enum."'";
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve company details - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  return $rowuser['location'];

} 

function getcompanytel ($enum) {

	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM companyinfo WHERE com_id = '".$enum."'";
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve company details - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  return $rowuser['telphone'];

}  

function getcompanyfax ($enum) {

	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM companyinfo WHERE com_id = '".$enum."'";
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve company details - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  return $rowuser['faxphone'];

} 

function getcompanyemail ($enum) {

	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM companyinfo WHERE com_id = '".$enum."'";
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve company details - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  return $rowuser['email'];

} 

function getcompanywebsite ($enum) {

	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
      $logSQL = "SELECT * FROM companyinfo WHERE com_id = '".$enum."'";
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve company details - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  return $rowuser['website'];

}


function getusertype($enum) {
  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
  $logSQL = "SELECT * FROM users WHERE  authcode = '".$enum."'";
  mysqli_select_db(DB, $CON);
  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve  usertype - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
  return $rowuser['admin'];
}



 



function showyears($selected = '') {
	
	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM academicyear ORDER BY  name ASC ";
	  //echo $logSQL;
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve years list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No Years Selected </option>";
		}
}

function showbranches($selected = '') {
	
	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM companybranch ORDER BY name ASC ";
	  //echo $logSQL;
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve branch - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No branch Selected </option>";
		}
}
function showprobations ($selected = '') {
	
	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM probationtype ORDER BY name ASC ";
	  //echo $logSQL;
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve probationtype - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No probationtype Selected </option>";
		}
}

function showdocuments($selected = '') {
	
	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM doc ORDER BY name ASC ";
	  //echo $logSQL;
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve doc - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No doc Selected </option>";
		}
}


function showbanks($selected = '') {
	
	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM bank ORDER BY name ASC ";
	  //echo $logSQL;
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve Bank - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No Bank Selected </option>";
		}
}
function showbankbranches($bank,$selected = '') {
	
	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM bankbranch  WHERE bank='".$bank."'ORDER BY name ASC ";
	  //echo $logSQL;
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve Bank branch - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No Bank branch Selected </option>";
		}
}


function showstudentclass($selected = '') {
	
	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM classlevel ORDER BY  name ASC ";
	  //echo $logSQL;
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve class list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No classes Selected </option>";
		}
}


function showmyclass($selected = '') {
	
	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT classlevel.name,studentclass.classlevel AS id FROM classlevel INNER JOIN studentclass ON classlevel.id=studentclass.classlevel WHERE studentclass.student='".getuserid($_SESSION['UNQ'])."' AND studentclass.academicyear='".$_SESSION['year']."' ";
	  echo $logSQL;
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve class list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No classes Selected </option>";
		}
}

function showmyclasses($selected = '') {
	
	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT classlevel.name,teacherclass.classlevel AS id FROM classlevel INNER JOIN teacherclass ON classlevel.id=teacherclass.classlevel WHERE teacherclass.teacher='".getuserid($_SESSION['UNQ'])."' AND teacherclass.academicyear='".$_SESSION['year']."' ";
	  echo $logSQL;
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve class list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No classes Selected </option>";
		}
}

function shownationality($selected = '') {
	
	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM nationality ORDER BY  name ASC ";
	  //echo $logSQL;
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve nationality list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No Nationality Selected </option>";
		}
}

function showdeptemployees($selected) {
	
	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM employee WHERE department='".$selected."' ORDER BY  name ASC ";
	  //echo $logSQL;
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve employee list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."   (".$rowuser['empcode'].")"."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']."   (".$rowuser['empcode'].")"."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No EmployeeSelected </option>";
		}
}

function showpretaxdeductions($selected = '') {
	
	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM pretaxdeduction ORDER BY  name ASC ";
	  //echo $logSQL;
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve pretaxdeductions list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']." </option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No pretaxdeductions Selected </option>";
		}
}

function showallowances($selected = '') {
	
	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM allowance ORDER BY  name ASC ";
	  //echo $logSQL;
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve allowance list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']." </option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No allowance Selected </option>";
		}
}

function showleavetypes($selected = '') {
	
	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM leavetype ORDER BY  name ASC ";
	  //echo $logSQL;
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve leave type list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']." </option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No leave type Selected </option>";
		}
}
function showempleaves($selected = '',$emp) {
	
	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT  leavetype.* FROM empleave INNER JOIN leavetype ON empleave.empleave=leavetype.id WHERE  empleave.employee='".$emp."'  ORDER BY leavetype.name ASC ";
	  echo $logSQL;
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve leave type list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']." </option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No leave type Selected </option>";
		}
}



function showlevels($selected = '',$leave) {
	
	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT level FROM leavetype  WHERE id='".$leave."'ORDER BY  name ASC ";
	  //echo $logSQL;
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve leave type list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		
		  for($i=1;$i<=$rowuser['level'];$i++){
				
				if ($selected != '' && $selected == $i) 
				{
						echo "<option selected value=\"".$i."\">".$i."</option>";
				} else {
						echo "<option value=\"".$i."\">".$i." </option>";
				}
		  } 
		} else {
				echo "<option>No leave level defined  </option>";
		}
}


function showdeductions($selected = '') {
	
	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM pretaxdeduction ORDER BY  name ASC ";
	  //echo $logSQL;
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve deduction list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']." </option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No deduction Selected </option>";
		}
}

function showpostdeductions($selected = '') {
	
	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM posttaxdeduction ORDER BY  name ASC ";
	  //echo $logSQL;
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve deduction list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']." </option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No deduction Selected </option>";
		}
}




function showstudents($selected = '') {
	
	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM student ORDER BY  lname ASC ";
	  //echo $logSQL;
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve teachers list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['lname']." ". $rowuser['mname']." ".$rowuser['fname']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['lname']." ". $rowuser['mname']." ".$rowuser['fname']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No student Selected </option>";
		}
}

function showparents($selected = '') {
	
	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM parent ORDER BY  guardian ASC ";
	  //echo $logSQL;
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve parent list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['guardian']." </option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['guardian']." </option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No parent Selected </option>";
		}
}

function showstaff($selected = '') {
	
	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM staff ORDER BY  lname ASC ";
	  echo $logSQL;
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve teachers list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['lname']." ". $rowuser['mname']." ".$rowuser['fname']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['lname']." ". $rowuser['mname']." ".$rowuser['fname']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No staff Selected </option>";
		}
}



function showpositions($selected = '') {
	
	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM jobposition ORDER BY  name ASC ";
	  //echo $logSQL;
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve job position list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No Job Position Selected </option>";
		}
}

function showacademiccourses($selected = '') {
	
	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM academiccourse ORDER BY  name ASC ";
	  //echo $logSQL;
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve Academic Course list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No Academic Course Selected </option>";
		}
}

function showproffesionalcourses($selected = '') {
	
	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM proffesionalcourse ORDER BY  name ASC ";
	  //echo $logSQL;
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve proffesional Course list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No proffesional Course Selected </option>";
		}
}


function showacademicawards($selected = '') {
	
	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM academicaward ORDER BY  name ASC ";
	  //echo $logSQL;
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve Academic award list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No Academic award Selected </option>";
		}
}

function showproffesionalawards($selected = '') {
	
	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM proffesionalaward ORDER BY  name ASC ";
	  //echo $logSQL;
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve proffesional award list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No proffesional award Selected </option>";
		}
}



function showempterms($selected = '') {
	
	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM employmentterm ORDER BY  name ASC ";
	  //echo $logSQL;
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve employement term list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No Employment term  Selected </option>";
		}
}

function showdepartments($selected = '') {
	
	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM department ORDER BY  name ASC ";
	  //echo $logSQL;
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve department list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No department Selected </option>";
		}
}
function showcategory($selected = '') {
	
	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM  employeecategory ORDER BY  name ASC ";
	  //echo $logSQL;
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve staffcategory list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No employee category Selected </option>";
		}
}

function showterms($year,$selected = '') {
	
	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM term  WHERE year='".$year."' ORDER BY  name ASC ";
	  //echo $logSQL;
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve term list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);

	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No Terms Selected </option>";
		}
}

function showproducts($selected = '') {
	
	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM product ORDER BY  name ASC ";
	  //echo $logSQL;
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve product list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No product Selected </option>";
		}
}

function showsuppliers($selected = '') {
	
	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM companyinfo WHERE type='supplier' ORDER BY  company ASC ";
	  //echo $logSQL;
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve supplier list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['comid']) 
				{
						echo "<option selected value=\"".$rowuser['comid']."\">".$rowuser['company']."</option>";
				} else {
						echo "<option value=\"".$rowuser['comid']."\">".$rowuser['company']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No supplier Selected </option>";
		}
}
function showcustomers($selected = '') {
	
	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM customer ORDER BY  name ASC ";
	  echo $logSQL;
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve customer list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No customer Selected </option>";
		}
}
function create_temp_items () { 	
	drop_temp_items ();	
	$CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
   	mysqli_select_db(DB, $CON);	
	$_SESSION['temp_items'] = "temp_items".$_SESSION['UNQ'];
	  $sql = 'CREATE TABLE `'.$_SESSION['temp_items'].'` ('
		. ' `name` VARCHAR(50) NULL ,'
		. ' `description` VARCHAR(50) NULL ,'
		. ' `item` int(10)  NULL '
							
		. ' )'
		. ' ENGINE = innodb;';

	$done = mysqli_query($sql, $CON) or die("ERROR: Trying to create temp table - ".mysqli_error());
	
} 


function drop_temp_items () { 	
	$CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
   	mysqli_select_db(DB, $CON);
	
	$_SESSION['temp_items'] = "temp_items".$_SESSION['UNQ'];		  
	$deleteSQL = "DROP TABLE IF EXISTS ".$_SESSION['temp_items']."";
	$done = mysqli_query($deleteSQL, $CON) or die("ERROR: Trying to delete temp table - ".mysqli_error());
	
}

function showreligion($selected = '') {
	
	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM religion ORDER BY  name ASC ";
	  //echo $logSQL;
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve religions list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No religion Selected </option>";
		}
}


function getmaxid($field,$table) {
 $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
$logSQL =  "SELECT   max(".$field.") AS num FROM ".$table."   LIMIT 1";
;
  mysqli_select_db(DB, $CON);
	$prq1 = mysqli_query($logSQL, $CON) or die(mysqli_error());
	$row_prq1 = mysqli_fetch_assoc($prq1);
	$nums = mysqli_num_rows($prq1);
	if($nums>0){
  		return  $row_prq1['num'];
	}else{
		return 1;
	}
  } 
  
  
  function checkitemstatus($table,$field) {
 $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
$logSQL =  "SELECT  * FROM ".$table."  WHERE serialno='".$field."' LIMIT 1";
//echo $logSQL;
  mysqli_select_db(DB, $CON);
	$prq1 = mysqli_query($logSQL, $CON) or die(mysqli_error());
	$row_prq1 = mysqli_fetch_assoc($prq1);
	$nums = mysqli_num_rows($prq1);
	if($nums>0){
  		return  $row_prq1['status'];
	}else{
		return 'Invalid serial';
		}
	
  }
  
  function showfilters($selected = '') {	 	
		
				$store='Available';
				$sname='IN STORE';
				$issued='Issued';
				$isname='Issued';
				$returned='Returned';
				$retname='Returned By Customer';
				if ($selected != '' && $selected == $sname) 
				{
						echo "<option selected value=\"".$sname."\">".$store."</option>";
						echo "<option> </option>";
						echo "<option value=\"".$isname."\">".$issued."</option>";
						echo "<option value=\"".$retname."\">".$returned."</option>";
						
				} elseif ($selected != '' && $selected == $isname)  {
				
						echo "<option selected value=\"".$isname."\">".$issued."</option>";
						echo "<option> </option>";
						echo "<option value=\"".$sname."\">".$store."</option>";
						echo "<option value=\"".$retname."\">".$returned."</option>";
						
				} elseif ($selected != '' && $selected == $retname)  {
				
						echo "<option selected value=\"".$retname."\">".$returned."</option>";
						echo "<option> </option>";
						echo "<option value=\"".$sname."\">".$store."</option>";
						echo "<option value=\"".$isname."\">".$issued."</option>";
						
				}else {
					echo "<option> </option>";
					echo "<option value=\"".$sname."\">".$store."</option>";
					echo "<option value=\"".$isname."\">".$issued."</option>";
					echo "<option value=\"".$retname."\">".$returned."</option>";
				}
}




function calculatedate($val){

	$d=date('Y-m-d');
		if($val>=12){
		$year=floor($val/12);
		$add= fmod($val,12);
		 $m=(intval(substr($d,5,2))+$add);
		
		 if($m<10){
		 	$month='0'.$m;
			}else{
			$month=$m;
			}
		
		}else{
		
			$tmonths=intval(substr($d,5,2))+$val;
			if($tmonths>11){
				$year=floor($tmonths/12);
				$add= fmod($tmonths,12);
				if($add<10){
					$month='0'.$add;
				}else{
					$month=$add;
				}
						
			}else{
				if($tmonths<10){
					$month='0'.$tmonths;
					$year=0;
				}else{
					$month=$tmonths;
					$year=0;
				}
			}
			
		}		
		
	$pdate=(intval(substr($d,0,4))+$year)."-".$month."-".substr($d,8,2);
			
	return($pdate);
}
 
  function getsupplier($field) {
 $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
$logSQL =  "SELECT  * FROM companyinfo  WHERE comid='".$field."' LIMIT 1";
//echo $logSQL;
  mysqli_select_db(DB, $CON);
	$prq1 = mysqli_query($logSQL, $CON) or die(mysqli_error());
	$row_prq1 = mysqli_fetch_assoc($prq1);
	$nums = mysqli_num_rows($prq1);
	
		return $row_prq1['company'];
	
  }
  function getcourse($field) {
 $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
$logSQL =  "SELECT  * FROM academiccourse  WHERE id='".$field."' LIMIT 1";
//echo $logSQL;
  mysqli_select_db(DB, $CON);
	$prq1 = mysqli_query($logSQL, $CON) or die(mysqli_error());
	$row_prq1 = mysqli_fetch_assoc($prq1);
	$nums = mysqli_num_rows($prq1);
	
		return $row_prq1['name'];
	
  }
  
  function getpcourse($field) {
 $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
$logSQL =  "SELECT  * FROM proffesionalcourse  WHERE id='".$field."' LIMIT 1";
//echo $logSQL;
  mysqli_select_db(DB, $CON);
	$prq1 = mysqli_query($logSQL, $CON) or die(mysqli_error());
	$row_prq1 = mysqli_fetch_assoc($prq1);
	$nums = mysqli_num_rows($prq1);
	
		return $row_prq1['name'];
	
  }
  
  function getaward($field) {
 $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
$logSQL =  "SELECT  * FROM academicaward  WHERE id='".$field."' LIMIT 1";
//echo $logSQL;
  mysqli_select_db(DB, $CON);
	$prq1 = mysqli_query($logSQL, $CON) or die(mysqli_error());
	$row_prq1 = mysqli_fetch_assoc($prq1);
	$nums = mysqli_num_rows($prq1);
	
		return $row_prq1['name'];
	
  }
  
   function getpaward($field) {
 $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
$logSQL =  "SELECT  * FROM proffesionalaward  WHERE id='".$field."' LIMIT 1";
//echo $logSQL;
  mysqli_select_db(DB, $CON);
	$prq1 = mysqli_query($logSQL, $CON) or die(mysqli_error());
	$row_prq1 = mysqli_fetch_assoc($prq1);
	$nums = mysqli_num_rows($prq1);
	
		return $row_prq1['name'];
	
  }
  
  function getuserid() {
 $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
$logSQL =  "SELECT  employee  FROM users  WHERE authcode='".$_SESSION['UNQ']."' LIMIT 1";
//echo $logSQL;
  mysqli_select_db(DB, $CON);
	$prq1 = mysqli_query($logSQL, $CON) or die(mysqli_error());
	$row_prq1 = mysqli_fetch_assoc($prq1);
	$nums = mysqli_num_rows($prq1);
	
		return $row_prq1['employee'];
	
  }
  
  
    
  function getusrdept() {
 $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
$logSQL =  "SELECT  department  FROM employee  WHERE id='".getuserid()."' LIMIT 1";
//echo $logSQL;
  mysqli_select_db(DB, $CON);
	$prq1 = mysqli_query($logSQL, $CON) or die(mysqli_error());
	$row_prq1 = mysqli_fetch_assoc($prq1);
	$nums = mysqli_num_rows($prq1);
	
		return $row_prq1['department'];
	
  }

  function getdepartment($field) {
 $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
$logSQL =  "SELECT  * FROM  department  WHERE id='".$field."' LIMIT 1";
//echo $logSQL;
  mysqli_select_db(DB, $CON);
	$prq1 = mysqli_query($logSQL, $CON) or die(mysqli_error());
	$row_prq1 = mysqli_fetch_assoc($prq1);
	$nums = mysqli_num_rows($prq1);
	
		return $row_prq1['name'];
	
  }
  
  function getdeduction($field) {
 $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
$logSQL =  "SELECT  * FROM pretaxdeduction  WHERE id='".$field."' LIMIT 1";
//echo $logSQL;
  mysqli_select_db(DB, $CON);
	$prq1 = mysqli_query($logSQL, $CON) or die(mysqli_error());
	$row_prq1 = mysqli_fetch_assoc($prq1);
	$nums = mysqli_num_rows($prq1);
	
		return $row_prq1['name'];
	
  }
  
   function getjobposition($field) {
 $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
$logSQL =  "SELECT  * FROM jobposition  WHERE id='".$field."' LIMIT 1";
//echo $logSQL;
  mysqli_select_db(DB, $CON);
	$prq1 = mysqli_query($logSQL, $CON) or die(mysqli_error());
	$row_prq1 = mysqli_fetch_assoc($prq1);
	$nums = mysqli_num_rows($prq1);
	
		return $row_prq1['name'];
	
  }
  
   function getjobpos($field) {
 $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
$logSQL =  "SELECT jobposition FROM employee  WHERE id='".$field."' LIMIT 1";
//echo $logSQL;
  mysqli_select_db(DB, $CON);
	$prq1 = mysqli_query($logSQL, $CON) or die(mysqli_error());

	$row_prq1 = mysqli_fetch_assoc($prq1);
	$nums = mysqli_num_rows($prq1);
	
		return getjobposition($row_prq1['jobposition']);
	
  }
  
   function getallowance($field) {
 $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
$logSQL =  "SELECT  * FROM allowance  WHERE id='".$field."' LIMIT 1";
//echo $logSQL;
  mysqli_select_db(DB, $CON);
	$prq1 = mysqli_query($logSQL, $CON) or die(mysqli_error());
	$row_prq1 = mysqli_fetch_assoc($prq1);
	$nums = mysqli_num_rows($prq1);
	
		return $row_prq1['name'];
	
  }
   function getleave($field) {
 $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
$logSQL =  "SELECT  * FROM leavetype  WHERE id='".$field."' LIMIT 1";
//echo $logSQL;
  mysqli_select_db(DB, $CON);
	$prq1 = mysqli_query($logSQL, $CON) or die(mysqli_error());
	$row_prq1 = mysqli_fetch_assoc($prq1);
	$nums = mysqli_num_rows($prq1);
	
		return $row_prq1['name'];
	
  }
  
  
  
   function getyearbyid($field) {
 $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
$logSQL =  "SELECT  * FROM academicyear WHERE name='".$field."' LIMIT 1";
//echo $logSQL;
  mysqli_select_db(DB, $CON);
	$prq1 = mysqli_query($logSQL, $CON) or die(mysqli_error());
	$row_prq1 = mysqli_fetch_assoc($prq1);
	$nums = mysqli_num_rows($prq1);
	
		return $row_prq1['id'];
	
  }
  
   function getyear($field) {
 $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
$logSQL =  "SELECT  * FROM academicyear WHERE id='".$field."' LIMIT 1";
//echo $logSQL;
  mysqli_select_db(DB, $CON);
	$prq1 = mysqli_query($logSQL, $CON) or die(mysqli_error());
	$row_prq1 = mysqli_fetch_assoc($prq1);
	$nums = mysqli_num_rows($prq1);
	
		return $row_prq1['name'];
	
  }
   function getempid() {
 $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
$logSQL =  "SELECT  * FROM users WHERE authcode='".$_SESSION['UNQ']."' LIMIT 1";
//echo $logSQL;
  mysqli_select_db(DB, $CON);
	$prq1 = mysqli_query($logSQL, $CON) or die(mysqli_error());
	$row_prq1 = mysqli_fetch_assoc($prq1);
	$nums = mysqli_num_rows($prq1);
	
		return $row_prq1['uid'];
	
  }
   function getmyempid() {
 $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
$logSQL =  "SELECT  * FROM users WHERE authcode='".$_SESSION['UNQ']."' LIMIT 1";
//echo $logSQL;
  mysqli_select_db(DB, $CON);
	$prq1 = mysqli_query($logSQL, $CON) or die(mysqli_error());
	$row_prq1 = mysqli_fetch_assoc($prq1);
	$nums = mysqli_num_rows($prq1);
	
		return $row_prq1['uid'];
	
  }
   function getterm($field) {
 $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
$logSQL =  "SELECT  * FROM term WHERE id='".$field."' LIMIT 1";
//echo $logSQL;
  mysqli_select_db(DB, $CON);
	$prq1 = mysqli_query($logSQL, $CON) or die(mysqli_error());
	$row_prq1 = mysqli_fetch_assoc($prq1);
	$nums = mysqli_num_rows($prq1);
	
		return $row_prq1['name'];
	
  }
  
  function getclass($field) {
 $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
$logSQL =  "SELECT  * FROM classlevel WHERE id='".$field."' LIMIT 1";
//echo $logSQL;
  mysqli_select_db(DB, $CON);
	$prq1 = mysqli_query($logSQL, $CON) or die(mysqli_error());
	$row_prq1 = mysqli_fetch_assoc($prq1);
	$nums = mysqli_num_rows($prq1);
	
		return $row_prq1['name'];
	
  }
  
/*function enteryears() {
 $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
 $k=1980;
 while($k<=2090){ 
  mysqli_select_db(DB, $CON);
$logSQL =  "INSERT INTO academicyear (name) VALUES('".$k."')";
$prq1 = mysqli_query($logSQL, $CON) or die(mysqli_error());
$k++;
}
	
  }*/
  
   function checkwarrantyvalidity($table,$field) {
   $d=date('Y-m-d');
 $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
$logSQL =  "SELECT   warrantyexpiry  FROM ".$table."  WHERE serialno='".$field."' LIMIT 1";
//echo $logSQL;
  mysqli_select_db(DB, $CON);
	$prq1 = mysqli_query($logSQL, $CON) or die(mysqli_error());
	$row_prq1 = mysqli_fetch_assoc($prq1);
	
	if( $row_prq1['warrantyexpiry']>=$d){
  		return  'ok';
	}else{
		return 'Warranty has expired';
		}
	
  }
  function getnextcode($field,$table) {
 $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
$logSQL =  "SELECT   max(".$field.") AS num FROM ".$table."   LIMIT 1";
;
  mysqli_select_db(DB, $CON);
	$prq1 = mysqli_query($logSQL, $CON) or die(mysqli_error());
	$row_prq1 = mysqli_fetch_assoc($prq1);
  return  $row_prq1[num]+1;
  }
  
  function getemployee($enum) {
  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
  $logSQL = "SELECT * FROM employee WHERE  id = '".$enum."'";
  mysqli_select_db(DB, $CON);
  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve  employee - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
  $result="EMP CODE : ".$rowuser['empcode']."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Name : ".$rowuser['name'];
 // echo $result
  return  $result;
}

function getempl($enum) {
  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
  $logSQL = "SELECT * FROM employee WHERE  id = '".$enum."'";
  mysqli_select_db(DB, $CON);
  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve  employee - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
  $result= $rowuser['name'];
 // echo $result
  return  $result;
}
 
 
  function getstudid($enum) {
  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
  $logSQL = "SELECT id FROM users WHERE  authcode = '".$enum."'";
  mysqli_select_db(DB, $CON);
  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve  student - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
  return $rowuser['id'] ;
}

function getteacher($enum) {
  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
  $logSQL = "SELECT * FROM teachers WHERE  id = '".$enum."'";
  mysqli_select_db(DB, $CON);
  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve  teacher - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
  return $rowuser['lname']." ".$rowuser['mname']." ".$rowuser['fname'];
}

function getstudentregno($enum) {
  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
  $logSQL = "SELECT * FROM student WHERE  id = '".$enum."'";
  mysqli_select_db(DB, $CON);
  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve  student - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
  return $rowuser['regno'];
}

function getteacherStaffno($enum) {
  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
  $logSQL = "SELECT * FROM teachers WHERE  id = '".$enum."'";
  mysqli_select_db(DB, $CON);
  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve  teacher - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
  return $rowuser['staffno'];
}




function getteacherclass($enum) {
	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);  
	 	 $logSQL = "SELECT classlevel.name FROM teacherclass INNER JOIN classlevel ON  classlevel.id=teacherclass.classlevel WHERE teacherclass.academicyear = '".$_SESSION['year']."' AND teacherclass.teacher = '".$enum."' ";
		// echo $logSQL;
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to check record exists  - ".mysqli_error(). $logSQL);
	  $rowuser = mysqli_fetch_assoc($done);
	  $num_rows = mysqli_num_rows($done); //Get number of rows
	if ($num_rows > 0) {
	   return $rowuser['name'];
	} 
}

function showsubjects($selected = '') {
	
	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM subject ORDER BY  name ASC ";
	  //echo $logSQL;
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve subjects list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No subject Selected </option>";
		}
}

function showteacherssubjects($class,$selected = '') {
	
	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT classsubjects.subject AS id,subject.name FROM classsubjects INNER JOIN subject ON subject.id=classsubjects.subject WHERE classsubjects.class='".$class."' AND classsubjects.subject IN(SELECT subject FROM lecturersubjects  WHERE teacher='".getuserid()."' )ORDER BY  subject.name ASC ";
	  //echo $logSQL;
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve subjects list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No subject Selected </option>";
		}
}



function showclasses($selected = '') {
	
	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM classlevel ORDER BY  name ASC ";
	  //echo $logSQL;
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve subjects list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No classes Selected </option>";
		}
}

function showclassstudents($year,$class,$selected = '') {
	
	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM studentclass  WHERE academicyear= '".$year."' AND classlevel='".$class."' ";
	  //echo $logSQL;
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve subjects list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['student']) 
				{
						echo "<option selected value=\"".$rowuser['student']."\">".getstudent($rowuser['id'])."</option>";
				} else {
						echo "<option value=\"".$rowuser['student']."\">".getstudent($rowuser['id'])."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No classes Selected </option>";
		}
}


function create_temp_students() { 	
	drop_temp_students ();	
	$CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
   	mysqli_select_db(DB, $CON);	
	$_SESSION['temp_students'] = "temp_students".$_SESSION['UNQ'];
	  $sql = 'CREATE TABLE `'.$_SESSION['temp_students'].'` ('
	  	. ' `student` int(10) NULL ,'
		. ' `regno` VARCHAR(30) NULL , '
		. ' `name` VARCHAR(100) NULL  '					
		. ' )'
		. ' ENGINE = innodb;';

	$done = mysqli_query($sql, $CON) or die("ERROR: Trying to create temp table - ".mysqli_error());
	
} 


function drop_temp_students () { 	
	$CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
   	mysqli_select_db(DB, $CON);
	
	$_SESSION['temp_students']= "temp_students".$_SESSION['UNQ'];		  
	$deleteSQL = "DROP TABLE IF EXISTS ".$_SESSION['temp_students']."";
	$done = mysqli_query($deleteSQL, $CON) or die("ERROR: Trying to delete temp table - ".mysqli_error());
	
}


function create_temppayroll () { 	
	drop_temppayroll ();	
	$CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
   	mysqli_select_db(DB, $CON);	
	$_SESSION['temppayroll'] = "temppayroll".$_SESSION['UNQ'];
	  $sql = 'CREATE TABLE `'.$_SESSION['temppayroll'].'` ('
		. ' `employee` INT(10) NOT NULL ,'
		. ' `pretax` DECIMAL(10,2) NOT NULL  DEFAULT "0.00",  '
		. ' `posttax` DECIMAL(10,2)  NOT NULL  DEFAULT "0.00",  '
		. ' `allowance` DECIMAL(10,2)  NOT NULL  DEFAULT "0.00",  '
		. ' `grosspay` DECIMAL(10,2)  NOT NULL  DEFAULT "0.00", '
		. ' `taxableamount` DECIMAL(10,2)  NOT NULL  DEFAULT "0.00" , '
		. ' `Payee` DECIMAL(10,2)  NOT NULL  DEFAULT "0.00" ,'
		. ' `deduction` DECIMAL(10,2)  NOT NULL  DEFAULT "0.00" , '
		. ' `tdeduction` DECIMAL(10,2)  NOT NULL  DEFAULT "0.00" ,'
		. ' `netpay` DECIMAL(10,2)  NOT NULL  DEFAULT "0.00" '		
								
		. ' )'
		. ' ENGINE = innodb;';

	$done = mysqli_query($sql, $CON) or die("ERROR: Trying to create temppayroll  table- ".mysqli_error());
	
} 


function drop_temppayroll () { 	
	$CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
   	mysqli_select_db(DB, $CON);
	
	$_SESSION['temppayroll'] = "temppayroll".$_SESSION['UNQ'];		  
	$deleteSQL = "DROP TABLE IF EXISTS ".$_SESSION['temppayroll']."";
	$done = mysqli_query($deleteSQL, $CON) or die("ERROR: Trying to delete temppayroll table - ".mysqli_error());
	
}

function insertrecords ($table,$field1,$val1) { 	
	$CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
   	mysqli_select_db(DB, $CON);
	
		  $sql = "INSERT INTO  ".$table." (".$field1.") VALUES(".$val1.")";
		$done = mysqli_query($sql, $CON) or die("ERROR: Trying to save record- ".mysqli_error());
	
} 

function deleterecords () { 
	
	$CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
   	mysqli_select_db(DB, $CON);	
		  $sql1 = "DELETE FROM  ".$_SESSION['temppayroll']." ";
		  $done1 = mysqli_query($sql1, $CON) or die("ERROR: Trying to delete record- ".mysqli_error());
		  
		 	
} 

function updaterecords ($table,$field1,$val1,$emp) { 	
	$CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
   	mysqli_select_db(DB, $CON);
	
		  $sql = "UPDATE  ".$table." SET ".$field1."=".$val1." WHERE employee=".$emp."";
		  //echo  $sql."<BR>";
		$done = mysqli_query($sql, $CON) or die("ERROR: Trying to update record- ".mysqli_error());
	
} 

function create_temppayee  () { 	
	drop_temppayee ();	
	$CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
   	mysqli_select_db(DB, $CON);	
	$_SESSION['temppayee'] = "temppayee".$_SESSION['UNQ'];
	  $sql = 'CREATE TABLE `'.$_SESSION['temppayee'].'` ('
		. ' `employee` INT(10)  NULL ,'
		. ' `total` DECIMAL(10,2)  NULL  '
								
		. ' )'
		. ' ENGINE = innodb;';

	$done = mysqli_query($sql, $CON) or die("ERROR: Trying to create temppayee   table- ".mysqli_error());
	
} 


function drop_temppayee  () { 	
	$CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
   	mysqli_select_db(DB, $CON);	
	$_SESSION['temppayee'] = "temppayee".$_SESSION['UNQ'];		  
	$deleteSQL = "DROP TABLE IF EXISTS ".$_SESSION['temppayee']."";
	//echo $deleteSQL;
	$done = mysqli_query($deleteSQL, $CON) or die("ERROR: Trying to delete temppayee  table - ".mysqli_error());
	
}

 function getproducts($enum) {

  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
  $logSQL = "SELECT * FROM product WHERE id = '".$enum."'";
 mysqli_select_db(DB, $CON);
  $done =  mysqli_query($logSQL, $CON) or die(mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
  return $rowuser['name'] ;
}

 function checksupplierprice($supplier,$field) {
 $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
$logSQL =  "SELECT  * FROM supplierprice  WHERE item='".$field."' AND supplier='".$supplier."' LIMIT 1";
//echo $logSQL;
  mysqli_select_db(DB, $CON);
	$prq1 = mysqli_query($logSQL, $CON) or die(mysqli_error());
	$row_prq1 = mysqli_fetch_assoc($prq1);
	$nums = mysqli_num_rows($prq1);
	//echo "the nums".$nums;
	if($nums<=0){
  		return 'invalid';
	}else{
	
		return  $row_prq1['price'];
		
		}
	
  }
  
  function create_temp_serials () { 	
	drop_temp_serials ();	
	$CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
   	mysqli_select_db(DB, $CON);	
	$_SESSION['temp_serials'] = "temp_serials".$_SESSION['UNQ'];
	  $sql = 'CREATE TABLE `'.$_SESSION['temp_serials'].'` ('
	  . ' `item` int(10) NOT NULL ,'
	  . ' `serial` VARCHAR(30) NULL '
		
		. ' )'
		. ' ENGINE = innodb;';

	$done = mysqli_query($sql, $CON) or die("ERROR: Trying to create temp table - ".mysqli_error());
	
} 

function drop_temp_serials () { 	
	$CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
   	mysqli_select_db(DB, $CON);
	
	$_SESSION['temp_serials'] = "temp_serials".$_SESSION['UNQ'];		  
	$deleteSQL = "DROP TABLE IF EXISTS ".$_SESSION['temp_serials']."";
	$done = mysqli_query($deleteSQL, $CON) or die("ERROR: Trying to delete temp table - ".mysqli_error());
	
}
 function create_tempserials () { 	
	drop_tempserials ();	
	$CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
   	mysqli_select_db(DB, $CON);	
	$_SESSION['tempserials'] = "tempserials".$_SESSION['UNQ'];
	
	  $sql = 'CREATE TABLE `'.$_SESSION['tempserials'].'` ('
	  . ' `item` int(10) NOT NULL ,'
	  . ' `reqitem` VARCHAR(30) NULL, '
	  . ' `requisition` VARCHAR(30) NULL, '
	  . ' `mserial` VARCHAR(30) NULL ,'
	  . ' `serial` VARCHAR(30) NULL, '
	  . ' `qty` INT(10) NULL ,'
	  . ' `status` CHAR(10) NOT NULL DEFAULT "N"'
		
		. ' )'
		. ' ENGINE = innodb;';

	$done = mysqli_query($sql, $CON) or die("ERROR: Trying to create temp table - ".mysqli_error());
	
} 

function drop_tempserials () { 	
	$CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
   	mysqli_select_db(DB, $CON);
	
	$_SESSION['tempserials'] = "tempserials".$_SESSION['UNQ'];		  
	$deleteSQL = "DROP TABLE IF EXISTS ".$_SESSION['tempserials']."";
	$done = mysqli_query($deleteSQL, $CON) or die("ERROR: Trying to delete temp table - ".mysqli_error());
	
}

function create_tempstock () { 	
	drop_tempstock ();	
	$CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
   	mysqli_select_db(DB, $CON);	
	$_SESSION['tempstock'] = "tempstock".$_SESSION['UNQ'];
	  $sql = 'CREATE TABLE `'.$_SESSION['tempstock'].'` ('
	  . ' `item` int(10) NOT NULL ,'
	  . ' `reqitem` VARCHAR(30) NULL, '
	  . ' `lpo` VARCHAR(30) NULL, '
	  . ' `mserial` VARCHAR(30) NULL ,'
	  . ' `serial` VARCHAR(30) NULL, '
	  . ' `qty` INT(10) NULL ,'
	  . ' `status` CHAR(10) NOT NULL DEFAULT "N",'
	  . ' `amount` decimal(10,2) NOT NULL '
		
		. ' )'
		. ' ENGINE = innodb;';

	$done = mysqli_query($sql, $CON) or die("ERROR: Trying to create temp table - ".mysqli_error());
	
} 

function showempls($selected = '') {
	
	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM users ORDER BY  fulname ASC ";
	  //echo $logSQL;
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve user list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['uid']) 
				{
						echo "<option selected value=\"".$rowuser['uid']."\">".$rowuser['fulname']." (".$rowuser['authcode'].")</option>";
				} else {
						echo "<option value=\"".$rowuser['uid']."\">".$rowuser['fulname']." (".$rowuser['authcode'].")</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No user Selected </option>";
		}
}

function drop_tempstock () { 	
	$CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
   	mysqli_select_db(DB, $CON);
	
	$_SESSION['tempstock'] = "tempstock".$_SESSION['UNQ'];		  
	$deleteSQL = "DROP TABLE IF EXISTS ".$_SESSION['tempstock']."";
	$done = mysqli_query($deleteSQL, $CON) or die("ERROR: Trying to delete temp table - ".mysqli_error());
	
}

function create_temp_req () { 	
	drop_temp_req ();	
	$CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
   	mysqli_select_db(DB, $CON);	
	$_SESSION['temp_req'] = "temp_req".$_SESSION['UNQ'];
	  $sql = 'CREATE TABLE `'.$_SESSION['temp_req'].'` ('
		. ' `item` int(10) NULL ,'
		. ' `qty` int(10) NULL ,'
		. ' `product` int(10) NULL '							
		. ' )'
		. ' ENGINE = innodb;';

	$done = mysqli_query($sql, $CON) or die("ERROR: Trying to create temp table - ".mysqli_error());
	
} 


function drop_temp_req () { 	
	$CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
   	mysqli_select_db(DB, $CON);
	
	$_SESSION['temp_req'] = "temp_req".$_SESSION['UNQ'];		  
	$deleteSQL = "DROP TABLE IF EXISTS ".$_SESSION['temp_req']."";
	$done = mysqli_query($deleteSQL, $CON) or die("ERROR: Trying to delete temp table - ".mysqli_error());
	
}

 function getnextlpo() {
 $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
$logSQL =  "SELECT   max(lpono) AS num FROM lpo   LIMIT 1";
;
  mysqli_select_db(DB, $CON);
	$prq1 = mysqli_query($logSQL, $CON) or die(mysqli_error());
	$row_prq1 = mysqli_fetch_assoc($prq1);
	$nums = mysqli_num_rows($prq1);
	if($nums>0){
  		return  $row_prq1['num']+1;
	}else{
		return 1;
	}
  } 
  
   function getitem($field) {
 $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
$logSQL =  "SELECT  * FROM item  WHERE id='".$field."' LIMIT 1";
//echo $logSQL;
  mysqli_select_db(DB, $CON);
	$prq1 = mysqli_query($logSQL, $CON) or die(mysqli_error());
	$row_prq1 = mysqli_fetch_assoc($prq1);
	$nums = mysqli_num_rows($prq1);
	
		return $row_prq1['name'];
	
  }
   function getitemdesc($field) {
 $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
$logSQL =  "SELECT  * FROM item  WHERE id='".$field."' LIMIT 1";
//echo $logSQL;
  mysqli_select_db(DB, $CON);
	$prq1 = mysqli_query($logSQL, $CON) or die(mysqli_error());
	$row_prq1 = mysqli_fetch_assoc($prq1);
	$nums = mysqli_num_rows($prq1);
	
		return $row_prq1['description'];
	
  }
  
  function getitemid($field) {
 $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
$logSQL =  "SELECT  reqitems.item FROM reqitems  WHERE reqitems.id='".$field."' LIMIT 1";
//echo $logSQL;
  mysqli_select_db(DB, $CON);
	$prq1 = mysqli_query($logSQL, $CON) or die(mysqli_error());
	$row_prq1 = mysqli_fetch_assoc($prq1);
	$nums = mysqli_num_rows($prq1);
	
		return $row_prq1['item'];
	
  }
  function getdesignation($enum) {
  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
  $logSQL = "SELECT * FROM users WHERE  authcode = '".$enum."'";
  mysqli_select_db(DB, $CON);
  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve  usertype - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
  return $rowuser['designation'];
}
  
  function getmaxserial($field) {
 $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
$logSQL =  "SELECT   max(serial) AS serial FROM logitemreceipts WHERE item= '".$field."'" ;
  mysqli_select_db(DB, $CON);
	$prq1 = mysqli_query($logSQL, $CON) or die(mysqli_error());
	$row_prq1 = mysqli_fetch_assoc($prq1);
	$nums = mysqli_num_rows($prq1);
	if($nums>0){
  		return  $row_prq1['serial'];
	}else{
		return 1;
	}
  } 
  
  function checkserials($tablename,$columnname,$value) {
	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
  
	 	 $logSQL = "SELECT ".$columnname." FROM ".$tablename." WHERE ".$columnname." = '".$value."'";
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to check record exists  - ".mysqli_error(). $logSQL);
	  //$rowuser = mysqli_fetch_assoc($done);
	  $num_rows = mysqli_num_rows($done); //Get number of rows
	if ($num_rows > 0) {
	   return TRUE;
	} else {
	   return FALSE;
	}
}

function getuserdept($user) {
  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
  $logSQL = "SELECT department.name FROM department INNER JOIN users ON department.id=users.department WHERE  users.authcode ='".$user."'";
  mysqli_select_db(DB, $CON);
  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to user details - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
  return $rowuser['name'] ;
}


function showemployees($selected = '') {
	
	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM employee ORDER BY  name ASC ";
	  //echo $logSQL;
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve employee list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No employee Selected </option>";
		}
}

function getauthoriser($enum) {

 $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);

  $logSQL = "SELECT * FROM employee WHERE id = '".$enum."'";
  mysqli_select_db(DB, $CON);
   $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve employee list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
  return $rowuser['name'] ;
}


?>
