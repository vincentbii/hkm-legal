<?php require_once('../connections/eProc.php');

require_once('../activelog.php');
?>
<?php
$currentPage = $_SERVER["PHP_SELF"];

$maxRows_leavetype = 30;
$pageNum_leavetype = 0;
if (isset($_GET['pageNum_leavetype'])) {
    $pageNum_leavetype = $_GET['pageNum_leavetype'];
}
$startRow_leavetype = $pageNum_leavetype * $maxRows_leavetype;

mysqli_select_db($eProc, $database_eProc);
$query_leavetype = "SELECT * FROM leavetype ";
$query_limit_leavetype = sprintf("%s LIMIT %d, %d", $query_leavetype, $startRow_leavetype, $maxRows_leavetype);
$leavetype = mysqli_query($eProc, $query_limit_leavetype) or die(mysqli_error());
$row_leavetype = mysqli_fetch_assoc($leavetype);

if (isset($_GET['totalRows_leavetype'])) {
    $totalRows_leavetype = $_GET['totalRows_leavetype'];
} else {    $all_leavetype = mysqli_query($eProc, $query_leavetype);
    $totalRows_leavetype = mysqli_num_rows($all_leavetype);
}
$totalPages_leavetype = ceil($totalRows_leavetype / $maxRows_leavetype) - 1;

$queryString_leavetype = "";
if (!empty($_SERVER['QUERY_STRING'])) {
    $params = explode("&", $_SERVER['QUERY_STRING']);
    $newParams = array();
    foreach ($params as $param) {
        if (stristr($param, "pageNum_leavetype") == false &&
                stristr($param, "totalRows_leavetype") == false) {
            array_push($newParams, $param);
        }
    }
    if (count($newParams) != 0) {
        $queryString_leavetype = "&" . htmlentities(implode("&", $newParams));
    }
}
$queryString_leavetype = sprintf("&totalRows_leavetype=%d%s", $totalRows_leavetype, $queryString_leavetype);
?>

<html>
    <head>
        <title>LEGAL  MANAGEMENT INFORMATION  SYSTEM</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="../styles/default.css" rel="stylesheet" type="text/css">

        <style type="text/css">
            <!--            .style1 {
                color: #FF0000;
                font-weight: bold;
            }-->
        </style>
    </head>
    <body>
        <fieldset>
            <table width="100%" border="0" cellpadding="4" cellspacing="0" >

                <tr >
                    <td width="22%"   class="inputdeft" style="font-weight: bold">Leave Type</td>
                    <td width="32%"   class="inputdeft" style="font-weight: bold">Max Days</td>
                    <td width="46%"   class="inputdeft" style="font-weight: bold" ><div align="right">
                            <table width="154"  border="0" cellspacing="0" cellpadding="3">
                                <tr class="inputdeft">
                                    <td width="19"  ><img src="../images/icons/newproject.gif" width="15" height="13" border="0" /></td>

                                    <td width="123"   ><a href="../lv/newleavetype.php">Add Leave Type </a></td>
                                </tr>
                            </table>
                        </div></td>
                </tr>
                <?php if ($totalRows_leavetype > 0) { ?>
    <?php do { ?>
                        <tr>
                            <td ><?php echo $row_leavetype['name']?> </td>
                            <td ><?php echo $row_leavetype['days']?> </td>
                            <td ><a href="../lv/newleavetype.php?id=<?php echo $row_leavetype['id'] ?>">Edit</a></td>
                        </tr>  <?php } while ($row_leavetype = mysqli_fetch_assoc($leavetype)); ?>
                    <tr>
                    <td colspan="11" class="mainbase"><table width="97%"  border="0" cellspacing="0" cellpadding="3">
                            <tr align="center">
                                <td width="45%">
                                    <table width="78%" border="0" align="center" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td width="23%" align="center"><?php if ($pageNum_leavetype > 0) { // Show if not first page  ?>
                                                    <a href="<?php printf("%s?pageNum_leavetype=%d%s", $currentPage, 0, $queryString_leavetype); ?>">First</a>
    <?php } // Show if not first page  ?>              </td>
                                            <td width="31%" align="center"><?php if ($pageNum_leavetype > 0) { // Show if not first page  ?>
                                                    <a href="<?php printf("%s?pageNum_leavetype=%d%s", $currentPage, max(0, $pageNum_leavetype - 1), $queryString_leavetype); ?>">Previous</a>
    <?php } // Show if not first page  ?>              </td>
                                            <td width="23%" align="center"><?php if ($pageNum_leavetype < $totalPages_leavetype) { // Show if not last page  ?>
                                                    <a href="<?php printf("%s?pageNum_leavetype=%d%s", $currentPage, min($totalPages_leavetype, $pageNum_leavetype + 1), $queryString_leavetype); ?>">Next</a>
    <?php } // Show if not last page  ?>              </td>
                                            <td width="23%" align="center"><?php if ($pageNum_leavetype < $totalPages_leavetype) { // Show if not last page  ?>
                                                    <a href="<?php printf("%s?pageNum_leavetype=%d%s", $currentPage, $totalPages_leavetype, $queryString_leavetype); ?>">last</a>
    <?php } // Show if not last page  ?>              </td>
                                        </tr>
                                    </table></td>
                                <td width="55%">&nbsp; Showing <strong><?php echo ($startRow_leavetype + 1) ?></strong> to <strong><?php echo min($startRow_leavetype + $maxRows_leavetype, $totalRows_leavetype) ?></strong> of <strong><?php echo $totalRows_leavetype ?></strong> </td>
                            </tr>
                        </table></td>
                        </tr>
<?php } else { ?>
                        <tr>
                            <td colspan="11" class="mainbase"><span class="style1">No leavetypes Created! </span></td>
                        </tr>
<?php } ?>
            </table>
        </fieldset>
    </body>
</html>
<?php
mysqli_free_result($leavetype);
?>

