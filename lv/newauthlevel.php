<?php require_once('../connections/eProc.php'); 
include('../activelog.php');
mysqli_select_db($eProc, $database_eProc);
unset($_SESSION['authlevel']);

unset($_SESSION['leavetype']);
if(isset($_GET['id']) && $_GET['id']!=''){ 
	$_SESSION['authlevel']=$_GET['id'];
	}
$selectSQL = "SELECT authlevel.*,leavetype.name AS ltype  FROM authlevel INNER JOIN leavetype ON authlevel.leavetype=leavetype.id   WHERE authlevel.id = '".$_SESSION['authlevel']."'";
	//echo $selectSQL;	
	$sql_select = mysqli_query($eProc, $selectSQL) or die(mysqli_error($eProc));
	$row_sql = mysqli_fetch_assoc($sql_select);
	$_SESSION['leavetype']=	$row_sql['id'];

if(isset($_POST['leavetype']) && $_POST['leavetype']!=''){ 
	$_SESSION['leavetype']=$_POST['leavetype'];
	}
	
if(isset($_POST['leavetype']) && $_POST['leavetype']==''){ 
	unset($_SESSION['leavetype']);
	}


	if ($_POST['update']!="" && $_POST['update']=="Update" ) {
		
		$updateSQL = "UPDATE authlevel SET  level='".$_POST['level']."',leavetype='".$_POST['leavetype']."'  WHERE  id ='".$_SESSION['authlevel']."' AND company='".$_SESSION['company']."' ";
			//echo $updateSQL; 
			$Result1 = mysqli_query($eProc, $updateSQL) or die(mysqli_error($eProc));
			
			?> 
			<script language="JavaScript" type="text/javascript">
				location='authlevellist.php';
			</script>
			<?php  
	} 
		
	if ($_POST['save']!="" && $_POST['save']=="Save" ) {
	$exist=recordexists("authlevel","leavetype",$_POST['leavetype']) ;		
		if($exist=='no'){
					
			$insertSQL =sprintf( "INSERT INTO authlevel (level,leavetype,company) VALUES (%s,%s,%s)",
			 GetSQLValueString($_POST['level'], "text"),
			 GetSQLValueString($_POST['leavetype'], "text"),
			 GetSQLValueString($_SESSION['company'], "text"));	
			
						
			//echo $insertSQL;
		$Result2 = mysqli_query($eProc, $insertSQL) or die(mysqli_error($eProc));
					 
			?> 
			<script language="JavaScript" type="text/javascript">
				location='authlevellist.php';
			</script>
			<?php
			
			}else{
	?> 
			<script language="JavaScript" type="text/javascript">
				alert("This leave type exists. \n Duplicates not allowed");
				location='newauthlevel.php';
			</script>
			<?php
			}
	}
	
	if ( isset($_GET['del']) && $_GET['del']!="" ) {
	
		$DeleteSQL = "DELETE FROM authlevel WHERE  id ='".$_GET['del']."'";
		//echo $DeleteSQL;
		if(!$Result3 = mysqli_query($eProc, $DeleteSQL)){?>
         
		<script language="JavaScript" type="text/javascript">
			alert("The authlevel has transactions.Please delete the transactions first");
			location='authlevellist.php';
		
		</script>
		<?php }else{?>
	
		<script language="JavaScript" type="text/javascript">
			location='authlevellist.php';
		</script>
		<?php
	}
	}
	

	

?>
<html>
<head>
<title>LEGAL  MANAGEMENT INFORMATION  SYSTEM</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../styles/<?php echo $_SESSION['Theme'] ; ?>/default.css" REL="stylesheet" type="text/css">
<script language="javascript" type="text/javascript">
   function MM_valIDateForm() { 
 	  if(document.newauthlevel.name.value=='' ){
 		alert('You must enter the Name to continue');
 		document.newauthlevel.name.focus();
 		return false ;
 		} 
 	} 

	function valIDatechars(evt)
		{
			// Prevents the entry of special characters and spaces into a field
			evt=(evt)? evt: window.event
			var charCode= (evt.which)? evt.which: evt.keyCode
			if( charCode>31 && (charCode<48 || charCode>57)) {
				if (charCode<64 || charCode>90) {
					if ((charCode<95 || charCode>122) && charCode!=8 ) {
						if (charCode!=127 && charCode!=46 && charCode!=45 ) {
							if ((charCode<37 || charCode>40) && charCode!=32) {
								alert('This field accepts only 0-9, a-z, A_Z, space, backspace & del characters !');
								status='This field accepts only 0-9, a-z, A_Z, space & del characters !'
								return false;
							}
						}
					}
				}
			}
			status=''
			return true
		}
 		function confirmdelete(name,id){
 		    	if (confirm('Are you sure you want to delete  authlevel ' +name+ '?')) {
 					location.href = 'newauthlevel.php?del='+id 
 					} 
  				}
</script>
</head>
<body>
<table width="100%" cellpadding="5">
<tr align="authlevel">
<td width="50%" valign="top">
<fieldset>
<legend>Auth Levels Details</legend>
<form onSubmit='return MM_valIDateForm()' action="" method="post" name="newauthlevel" ID="newauthlevel">
<input name="ID" type="hidden" value="<?php echo $row_sql['id'] ?>" />
<table width="100%" border="0" cellspacing="0" cellpadding="4">
<tr valign="baseline">
<td width="40%" align='right' nowrap><strong>Leave Type :</strong></td>
<td width="60%">
<?php if(isset($_SESSION['authlevel']) && $_SESSION['authlevel']!=''){
	echo $row_sql['ltype'];
	?>
    <input name="leavetype" type="hidden" value="<? echo $row_sql['leavetype']?>">
<?php }else{?>
<select name="leavetype" id="leavetype"  onFocus="this.style.backgroundColor='#ffff00'"  onBlur="this.style.backgroundColor='#F2FAFF'"  onChange="submit()" class='forms' >
  <?php showleavetypes($_SESSION['leavetype']) ?>
</select>
<?php }?></td>
</tr>
<tr valign="baseline">
<td align='right' nowrap><strong>Authorization Levels Required</strong></td>
<td>
<input size="10" maxlength="100" name="level" ID="level"  onFocus="this.style.backgroundColor='#ffff00'"  onBlur="this.style.backgroundColor='#F2FAFF'"  class='forms'   onkeypress='return checkNumbers(event)' value="<?php echo $row_sql['level']; ?>" ></td>
</tr>
<tr valign='authlevel'>
<td colspan='2' ><div align='center'>
 <?php if (isset($_SESSION['authlevel'])){?>
 <input name="update" type="submit" class="formsBlue" id="update" value="Update" />
<a href="newauthlevel.php?id=<?php $row_sql['id']; ?>"> <a onclick='javascript:confirmdelete("<?php  echo $row_sql['name']?>","<?php echo $row_sql['id']?> ")' ><input name='delete' type='button' class='formsRed' value='Delete'  > </a> </a>
<?php }else{?>
<input name='save' type='submit' class='formsBlue' value='Save'>
<?php }?>
<input name='cancel' type='button' onClick="location='authlevellist.php'" class='formsorg' value='Cancel'>
</div></td>
</tr>
</table>
</form>
</fieldset
></table>
</body>
</html>
