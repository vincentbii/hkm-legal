<?php require_once('../connections/eProc.php'); 

 require_once('../activelog.php'); ?>
<?php
unset($_SESSION['level']);
unset($_SESSION['leavetype']);
$currentPage = $_SERVER["PHP_SELF"];

$maxRows_approverlevel = 30;
$pageNum_approverlevel = 0;
if (isset($_GET['pageNum_approverlevel'])) {
  $pageNum_approverlevel = $_GET['pageNum_approverlevel'];
}
$startRow_approverlevel = $pageNum_approverlevel * $maxRows_approverlevel;

mysqli_select_db($eProc, $database_eProc);
$query_approverlevel = "SELECT approvallevel.*,leavetype.name AS leavetype,employee.name AS Employee,department.name AS department,jobposition.name AS position
							FROM approvallevel 
							INNER JOIN leavetype  ON  approvallevel.leave=leavetype.id
							INNER JOIN approvers ON approvallevel.approver=approvers.id
							INNER JOIN employee ON approvers.employee=employee.id 
							INNER JOIN department  ON  employee.department=department.id
							INNER JOIN jobposition ON employee.jobposition=jobposition.id
							ORDER BY approvallevel.leave,employee.name ASC ";
							
$query_limit_approverlevel = sprintf("%s LIMIT %d, %d", $query_approverlevel, $startRow_approverlevel, $maxRows_approverlevel);
$approverlevel = mysqli_query($query_limit_approverlevel,$eProc) or die(mysqli_error());
$row_approverlevel = mysqli_fetch_assoc($approverlevel);

if (isset($_GET['totalRows_approverlevel'])) {
  $totalRows_approverlevel = $_GET['totalRows_approverlevel'];
} else {
  $all_approverlevel = mysqli_query($query_approverlevel);
  $totalRows_approverlevel = mysqli_num_rows($all_approverlevel);
}
$totalPages_approverlevel = ceil($totalRows_approverlevel/$maxRows_approverlevel)-1;

$queryString_approverlevel = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_approverlevel") == false && 
        stristr($param, "totalRows_approverlevel") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_approverlevel = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_approverlevel = sprintf("&totalRows_approverlevel=%d%s", $totalRows_approverlevel, $queryString_approverlevel);
?>

<html>
<head>
<title>LEGAL  MANAGEMENT INFORMATION  SYSTEM</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../styles/default.css" rel="stylesheet" type="text/css">

<style type="text/css">
<!--
.style1 {
	color: #FF0000;
	font-weight: bold;
}
-->
</style>
</head>
<body>
<fieldset>
<table width="100%" border="0" cellpadding="4" cellspacing="0" >

  <tr >
   <td width="18%"   class="inputdeft" style="font-weight: bold">Authoriser</td>
    <td width="16%"   class="inputdeft" style="font-weight: bold">Job Position</td>
    <td width="19%"   class="inputdeft" style="font-weight: bold">Department</td>
    <td width="17%"   class="inputdeft" style="font-weight: bold">Leave</td>
    <td width="13%"   class="inputdeft" style="font-weight: bold">Authorization Level</td>
   
	<td width="17%"   class="inputdeft" style="font-weight: bold" ><div align="right">
	  <table width="154"  border="0" cellspacing="0" cellpadding="3">
	    <tr class="inputdeft">
	      <td width="10%"  ><img src="../images/icons/newproject.gif" width="15" height="13" border="0" /></td>
                
			<td width="90%"   ><a href="../lv/newapproverlevel.php">Assign  level </a></td>
          </tr>
	    </table>
    </div></td>
  </tr>
  <?php if ($totalRows_approverlevel > 0) { ?>
  <?php do { ?>
  <tr>
  <td ><? echo $row_approverlevel['Employee']?> </td>  
    <td ><? echo $row_approverlevel['position']?></td>
     <td ><? echo $row_approverlevel['department']?></td>
      <td ><? echo $row_approverlevel['leavetype']?></td>
    <td colspan="2" ><? echo $row_approverlevel['level']?> </td>
  

  </tr>
  <?php } while ($row_approverlevel = mysqli_fetch_assoc($approverlevel)); ?>
  <tr>
    <td colspan="12" class="mainbase"><table width="97%"  border="0" cellspacing="0" cellpadding="3">
      <tr align="center">
        <td width="45%">
          <table width="78%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
              <td width="23%" align="center"><?php if ($pageNum_approverlevel > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_approverlevel=%d%s", $currentPage, 0, $queryString_approverlevel); ?>">First</a>
                <?php } // Show if not first page ?>              </td>
              <td width="31%" align="center"><?php if ($pageNum_approverlevel > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_approverlevel=%d%s", $currentPage, max(0, $pageNum_approverlevel - 1), $queryString_approverlevel); ?>">Previous</a>
                <?php } // Show if not first page ?>              </td>
              <td width="23%" align="center"><?php if ($pageNum_approverlevel < $totalPages_approverlevel) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_approverlevel=%d%s", $currentPage, min($totalPages_approverlevel, $pageNum_approverlevel + 1), $queryString_approverlevel); ?>">Next</a>
                <?php } // Show if not last page ?>              </td>
              <td width="23%" align="center"><?php if ($pageNum_approverlevel < $totalPages_approverlevel) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_approverlevel=%d%s", $currentPage, $totalPages_approverlevel, $queryString_approverlevel); ?>">last</a>
                <?php } // Show if not last page ?>              </td>
            </tr>
          </table></td>
        <td width="55%">&nbsp; Showing <strong><?php echo ($startRow_approverlevel + 1) ?></strong> to <strong><?php echo min($startRow_approverlevel + $maxRows_approverlevel, $totalRows_approverlevel) ?></strong> of <strong><?php echo $totalRows_approverlevel ?></strong> </td>
      </tr>
    </table></td>
  </tr>
  <?php } else { ?>
  <tr>
    <td colspan="12" class="mainbase"><span class="style1">No approverlevels Created! </span></td>
  </tr>
  <?php } ?>
</table>
</fieldset>
</body>
</html>
<?php
mysqli_free_result($approverlevel);
?>

