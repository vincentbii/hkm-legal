<?php
require_once('../connections/eProc.php');
include('../activelog.php');
mysqli_select_db($eProc, $database_eProc);

unset($_SESSION['empleave']);
unset($_SESSION['leavetype']);
if (isset($_GET['emp']) && $_GET['emp'] != '') {
    $_SESSION['emp'] = $_GET['emp'];
    ?> 
    <script language="JavaScript" type="text/javascript">
        location = 'newempleave.php';
    </script>
    <?php
}


$selectSQL = "SELECT empleave.*,leavetype.name FROM empleave INNER JOIN leavetype ON leavetype.id=empleave.empleave  WHERE empleave.employee = '" . $_SESSION['emp'] . "'";
//echo $selectSQL;	
$sql_select = mysqli_query($eProc, $selectSQL) or die(mysqli_error($eProc));
$row_sql = mysqli_fetch_assoc($sql_select);
$num = mysqli_num_rows($sql_select);



if (isset($_GET['id']) && $_GET['id'] != '') {
    $_SESSION['empleave'] = $_GET['id'];
    $selectSQL2 = "SELECT empleave.*,leavetype.name FROM empleave INNER JOIN leavetype ON leavetype.id=empleave.empleave  WHERE empleave.id = '" . $_SESSION['empleave'] . "'";
    //echo $selectSQL;	
    $sql_select2 = mysqli_query($eProc, $selectSQL2) or die(mysqli_error($eProc));
    $row_sql2 = mysqli_fetch_assoc($sql_select2);
    $num2 = mysqli_num_rows($sql_select2);
}

if (isset($_POST['leavetype']) && $_POST['leavetype'] != '') {
    $_SESSION['leavetype'] = $_POST['leavetype'];
}
if (isset($_POST['leavetype']) && $_POST['leavetype'] == '') {
    unset($_SESSION['leavetype']);
}




if (isset($_POST['save']) && $_POST['save'] == "Save") {
    if (!recordexists3cols("empleave", "empleave", "employee", $_POST['leavetype'], $_SESSION['emp'])) {

        $selectdays = "SELECT * FROM leavetype WHERE id = '" . $_POST['leavetype'] . "'";
        //echo $selectdays;	
        $sqlday = mysqli_query($eProc, $selectdays) or die(mysqli_error());
        $rowday = mysqli_fetch_assoc($sqlday);
        $numdays = mysqli_num_rows($sqlday);

        $insertday = sprintf("INSERT INTO leavedays (employee,`leave`,ldays,remdays) VALUES (%s,%s,%s,%s)", GetSQLValueString($_SESSION['emp'], "text"), GetSQLValueString($_POST['leavetype'], "text"), GetSQLValueString($rowday['days'], "text"), GetSQLValueString($rowday['days'], "text"));
        //echo $insertday;
        $resultday = mysqli_query($eProc, $insertday) or die(mysqli_error($eProc));


        $insertSQL = sprintf("INSERT INTO empleave (employee,empleave,company) VALUES (%s,%s,%s)", GetSQLValueString($_SESSION['emp'], "text"), GetSQLValueString($_POST['leavetype'], "text"), GetSQLValueString($_SESSION['company'], "text"));

        //echo $insertSQL;
        $Result2 = mysqli_query($eProc, $insertSQL) or die(mysqli_error($eProc));
        ?> 
        <script language="JavaScript" type="text/javascript">
            location = 'newempleave.php';
        </script>
        <?php
    } else {
        ?> 
        <script language="JavaScript" type="text/javascript">
            alert("Employee already  has  an <?php echo getleave($_POST['leavetype'])?> ");
            location = 'newempleave.php';
        </script>
        <?php
    }
}

if (isset($_GET['del']) && $_GET['del'] != "") {

    $DeleteSQL = "DELETE FROM empleave WHERE  id ='" . $_GET['del'] . "'";
    //echo $DeleteSQL;
    if (!$Result3 = mysqli_query($eProc, $DeleteSQL)) {
        ?>

        <script language="JavaScript" type="text/javascript">
            alert("The empleave has transactions.Please delete the transactions first");
            location = 'newempleave.php';

        </script>
    <?php } else { ?>

        <script language="JavaScript" type="text/javascript">
            location = 'newempleave.php';
        </script>
        <?php
    }
}


$selectSQL1 = "SELECT employee.*,jobposition.name AS Position FROM employee INNER JOIN jobposition ON employee.jobposition=jobposition.id  WHERE employee.id = '" . $_SESSION['emp'] . "'";
//echo $selectSQL1;	
$sqlemployee = mysqli_query($eProc, $selectSQL1) or die(mysqli_error());
$rowemp = mysqli_fetch_assoc($sqlemployee);
$empnum = mysqli_num_rows($sqlemployee);
?>
<html>
    <head>
        <title>LEGAL  MANAGEMENT INFORMATION  SYSTEM</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="../styles/<?php echo $_SESSION['Theme'] ; ?>/default.css" REL="stylesheet" type="text/css">
        <script language="javascript" type="text/javascript">
            function MM_valIDateForm() {
                if (document.newempallowance.name.value == '') {
                    alert('You must enter the Name to continue');
                    document.newempallowance.name.focus();
                    return false;
                }
            }

            function checkNumbers(evt) {
                evt = (evt) ? evt : window.event
                var charCode = (evt.which) ? evt.which : evt.keyCode
                if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                    if ((charCode < 37 || charCode > 40) && charCode != 46) {
                        alert('This field accepts only numeric characters (0-9) and . !');
                        status = 'This field accepts only numbers only and . !'
                        return false;
                    }
                }
                status = ''
                return true
            }
            function valIDatechars(evt)
            {
                // Prevents the entry of special characters and spaces into a field
                evt = (evt) ? evt : window.event
                var charCode = (evt.which) ? evt.which : evt.keyCode
                if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                    if (charCode < 64 || charCode > 90) {
                        if ((charCode < 95 || charCode > 122) && charCode != 8) {
                            if (charCode != 127 && charCode != 46 && charCode != 45) {
                                if ((charCode < 37 || charCode > 40) && charCode != 32) {
                                    alert('This field accepts only 0-9, a-z, A_Z, space, backspace & del characters !');
                                    status = 'This field accepts only 0-9, a-z, A_Z, space & del characters !'
                                    return false;
                                }
                            }
                        }
                    }
                }
                status = ''
                return true
            }
            function confirmdelete(name, id) {
                if (confirm('Are you sure you want to Delete  ' + name + '?')) {
                    location.href = 'newempleave.php?del=' + id
                }
            }
        </script>
    </head>
    <body>
        <table width="100%" cellpadding="5">
            <tr align="empleave">
                <td width="50%" valign="top">
                    <fieldset>
                        <legend>Employee </legend>
                        <form onSubmit='return MM_valIDateForm()' action="" method="post" name="newempleave" ID="newempleave">
                            <input name="ID" type="hidden" value="<?php echo $row_sql['id'] ?>" />

                            <table width="100%" border="0" cellspacing="0" cellpadding="4" >
                                <tr valign="baseline" bgcolor="#00CCCC">
                                    <td width="11%"  nowrap  ><div align="right"><strong>Emp Code :</strong></div></td>
                                    <td width="89%"  nowrap><strong><?php echo $rowemp['empcode']; ?></strong></td>
                                </tr>
                                <tr valign="baseline" bgcolor="#00CCCC">
                                    <td  nowrap><div align="right"><strong>Name :</strong></div></td>
                                    <td  nowrap><strong><?php echo $rowemp['name']; ?></strong></td>
                                </tr>
                                <tr valign="baseline" bgcolor="#00CCCC">
                                    <td  nowrap><div align="right"><strong>Job Position :</strong></div></td>
                                    <td  nowrap><strong><?php echo $rowemp['Position']; ?></strong></td>
                                </tr>

                            </table>

                            <table width="100%" border="0" cellspacing="0" cellpadding="4">
                                <tr valign="baseline">
                                    <td width="29%" align='right' nowrap><strong>Select Leave Type</strong></td>
                                    <td width="71%"> <?php
if (isset($_SESSION['empleave'])) {
    echo $row_sql['name'];
} else {
    ?>

                                            <select name="leavetype" id="leavetype"  onFocus="this.style.backgroundColor = '#ffff00'"  onBlur="this.style.backgroundColor = '#F2FAFF'"  onChange="submit()" class='forms' >
    <?php showleavetypes($_SESSION['leavetype']) ?>
                                            </select>
                                            <?php }?></td>
                                    </tr>
                                    <tr valign='empleave'>
                                        <td colspan='2' ><div align='center'>
                                            <?php if (isset($_SESSION['empleave'])) { ?>

                                                    <a href="newempleave.php?id=<?php $row_sql2['id']; ?>"> <a onclick='javascript:confirmdelete("<?php echo $row_sql2['name']; ?>", "<?php echo $row_sql2['id']; ?> ")' ><input name='delete' type='button' class='formsRed' value='Delete'  > </a> </a>
    <?php } else { ?>
                                                    <input name='save' type='submit' class='formsBlue' value='Save'>
                                                <?php } ?>
                                                <input name='cancel' type='button' onClick="location = 'empleaveslist.php'" class='formsorg' value='Cancel'>
                                            </div></td>
                                    </tr>
                                </table>
                            </form>
                        </fieldset
                        ></table>
                                                <?php if (isset($_SESSION['emp']) && $_SESSION['emp'] != '') { ?>
                <fieldset>
                    <legend> Employee leaves</legend>
                    <table width="100%" border="0" cellpadding="4" cellspacing="0" >

                        <tr>
                            <td width="391"   class="inputdeft" style="font-weight: bold">Leave Types</td>
                            <td width="572"  class="inputdeft"  ><a href="../lv/newempleave.php">
                                    <input name='addall' type='button'  class='formsblue' value='Assign Leave Type'> </a></td>
                        </tr>
                <?php if ($num > 0) { ?>
                    <?php do { ?>
                                <tr>
                                    <td ><?php echo $row_sql ['name']; ?> </td>
                                    <td ><a href="../lv/newempleave.php?id=<?php echo $row_sql ['id'] ?>">Edit</a></td>
                                </tr>
            <?php } while ($row_sql = mysqli_fetch_assoc($sql_select)); ?>

        <?php } else { ?>
                            <tr>
                                <td colspan="11" class="mainbase"><span class="style1">No Employee leaves Created! </span></td>
                            </tr>
                        <?php } ?>
                    </table>
                </fieldset>
    <?php } ?>

    </body>
</html>