<?php require_once('../connections/eProc.php'); 

 require_once('../activelog.php'); ?>
<?php
$employee=getuserid();
$userdept=getusrdept();
$currentPage = $_SERVER["PHP_SELF"];

$maxRows_leaveapplication = 30;
$pageNum_leaveapplication = 0;
if (isset($_GET['pageNum_leaveapplication'])) {
  $pageNum_leaveapplication = $_GET['pageNum_leaveapplication'];
}
$startRow_leaveapplication = $pageNum_leaveapplication * $maxRows_leaveapplication;

mysqli_select_db($eProc, $database_eProc);
$query_leaveapplication = "SELECT * FROM leaveapplication
							WHERE  lstatus <> 'Approved' 
							AND lstatus <> 'Rejected' 
							AND lstatus <> 'L1' 
							AND nxtappr ='".$userdept."'  							 
							AND applevel  IN (SELECT applevel FROM approvers WHERE employee='".$employee."')";
//echo $query_leaveapplication;
$query_limit_leaveapplication = sprintf("%s LIMIT %d, %d", $query_leaveapplication, $startRow_leaveapplication, $maxRows_leaveapplication);
$leaveapplication = mysqli_query($eProc, $query_limit_leaveapplication) or die(mysqli_error($eProc));
$row_sql = mysqli_fetch_assoc($leaveapplication);

if (isset($_GET['totalRows_leaveapplication'])) {
  $totalRows_leaveapplication = $_GET['totalRows_leaveapplication'];
} else {
  $all_leaveapplication = mysqli_query($eProc, $query_leaveapplication);
  $totalRows_leaveapplication = mysqli_num_rows($all_leaveapplication);
}
$totalPages_leaveapplication = ceil($totalRows_leaveapplication/$maxRows_leaveapplication)-1;

$queryString_leaveapplication = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_leaveapplication") == false && 
        stristr($param, "totalRows_leaveapplication") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_leaveapplication = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_leaveapplication = sprintf("&totalRows_leaveapplication=%d%s", $totalRows_leaveapplication, $queryString_leaveapplication);
?>

<html>
<head>
<title>LEGAL  MANAGEMENT INFORMATION  SYSTEM</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../styles/default.css" rel="stylesheet" type="text/css">

<style type="text/css">
<!--
.style1 {
	color: #FF0000;
	font-weight: bold;
}
-->
</style>
</head>
<body>
<fieldset>
<table width="100%" border="0" cellpadding="4" cellspacing="0" >

  <tr >
   <td width="23%"   class="inputdeft" style="font-weight: bold">Aplicant </td>
   <td width="13%"   class="inputdeft" style="font-weight: bold">Leave </td>
    <td width="14%"   class="inputdeft" style="font-weight: bold"> Application date</td>
    <td width="12%"   class="inputdeft" style="font-weight: bold">Start date</td>
    <td width="12%"   class="inputdeft" style="font-weight: bold">End date</td>
   <td width="11%"   class="inputdeft" style="font-weight: bold">Resume date</td>
    <td width="15%"   class="inputdeft" style="font-weight: bold">Application status</td>
  </tr>
  <?php if ($totalRows_leaveapplication > 0) { ?>
  <?php do {
  $d='KSH'; ?>
  <tr>
  <td ><a href="approveleave.php?id=<?php echo $row_sql['id'] ?>"><?php echo getemployee($row_sql['employee'])?></a></td>
  <td ><?php echo getleave($row_sql['leave'])?></td>
   <td ><?php echo substr($row_sql['appdate'],0,10)?></td>
    <td ><?php echo substr($row_sql['startdate'],0,10)?> </td>
     <td ><?php echo substr($row_sql['enddate'],0,10)?> </td>
     <td ><?php echo substr($row_sql['resumedate'],0,10)?> </td>
     <td  ><?php echo $row_sql['lstatus']?></td>
 </tr>
  <?php } while ($row_sql = mysqli_fetch_assoc($leaveapplication)); ?>
  <tr>
    <td colspan="11" class="mainbase"><table width="97%"  border="0" cellspacing="0" cellpadding="3"><tr align="center"><td width="55%"><table width="97%"  border="0" cellspacing="0" cellpadding="3">
      <tr align="center">
        <td width="45%"><table width="78%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
              <td width="23%" align="center"><?php if ($pageNum_leaveapplication > 0) { // Show if not first page ?>
                  <a href="<?php printf("%s?pageNum_leaveapplication=%d%s", $currentPage, 0, $queryString_leaveapplication); ?>">First</a>
                  <?php } // Show if not first page ?>              </td>
              <td width="31%" align="center"><?php if ($pageNum_leaveapplication > 0) { // Show if not first page ?>
                  <a href="<?php printf("%s?pageNum_leaveapplication=%d%s", $currentPage, max(0, $pageNum_leaveapplication - 1), $queryString_leaveapplication); ?>">Previous</a>
                  <?php } // Show if not first page ?>              </td>
              <td width="23%" align="center"><?php if ($pageNum_leaveapplication < $totalPages_leaveapplication) { // Show if not last page ?>
                  <a href="<?php printf("%s?pageNum_leaveapplication=%d%s", $currentPage, min($totalPages_leaveapplication, $pageNum_leaveapplication + 1), $queryString_leaveapplication); ?>">Next</a>
                  <?php } // Show if not last page ?>              </td>
              <td width="23%" align="center"><?php if ($pageNum_leaveapplication < $totalPages_leaveapplication) { // Show if not last page ?>
                  <a href="<?php printf("%s?pageNum_leaveapplication=%d%s", $currentPage, $totalPages_leaveapplication, $queryString_leaveapplication); ?>">last</a>
                  <?php } // Show if not last page ?>              </td>
            </tr>
        </table></td>
        <td width="55%">&nbsp; Showing <strong><?php echo ($startRow_leaveapplication + 1) ?></strong> to <strong><?php echo min($startRow_leaveapplication + $maxRows_leaveapplication, $totalRows_leaveapplication) ?></strong> of <strong><?php echo $totalRows_leaveapplication ?></strong> </td>
      </tr>
    </table>      <strong></strong></td>
      </tr>
    </table></td>
  </tr>
  <?php } else { ?>
  <tr>
    <td colspan="11" class="mainbase"><span class="style1">No leave applications to approve! </span></td>
  </tr>
  <?php } ?>
</table>
</fieldset>
</body>
</html>
<?php
mysqli_free_result($leaveapplication);
?>

