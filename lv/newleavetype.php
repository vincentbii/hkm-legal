<?php
require_once('../connections/eProc.php');
include('../activelog.php');
mysqli_select_db($eProc, $database_eProc);
unset($_SESSION['leavetype']);
if (isset($_GET['id']) && $_GET['id'] != '') {
    $_SESSION['leavetype'] = $_GET['id'];
}


if ($_POST['update'] != "" && $_POST['update'] == "Update") {

    $updateSQL = "UPDATE leavetype SET  name='" . $_POST['name'] . "',days='" . $_POST['days'] . "'  WHERE  id ='" . $_SESSION['leavetype'] . "' AND company='" . $_SESSION['company'] . "' ";
    //echo $updateSQL; 
    $Result1 = mysqli_query($eProc, $updateSQL) or die(mysqli_error($eProc));
    ?> 
    <script language="JavaScript" type="text/javascript">
        location = 'leavetypeslist.php';
    </script>
    <?php  
    } 

    if ($_POST['save']!="" && $_POST['save']=="Save" ) {
    $exist=recordexists("leavetype","name",$_POST['name']) ;		
    if($exist=='no'){

    $insertSQL =sprintf( "INSERT INTO leavetype (name,days,company) VALUES (%s,%s,%s)",
    GetSQLValueString($_POST['name'], "text"),
    GetSQLValueString($_POST['days'], "text"),
    GetSQLValueString($_SESSION['company'], "text"));	


    //echo $insertSQL;
    $Result2 = mysqli_query($eProc, $insertSQL) or die(mysqli_error($eProc));

    ?> 
    <script language="JavaScript" type="text/javascript">
        location = 'leavetypeslist.php';
    </script>
    <?php

    }else{
    ?> 
    <script language="JavaScript" type="text/javascript">
        alert("A  leavetype  with <?php echo $_POST['name']; ?>  name exists.");
        location = 'newleavetype.php';
    </script>
    <?php
    }
    }

    if ( isset($_GET['del']) && $_GET['del']!="" ) {

    $DeleteSQL = "DELETE FROM leavetype WHERE  id ='".$_GET['del']."'";
    //echo $DeleteSQL;
    if(!$Result3 = mysqli_query($eProc, $DeleteSQL)){?>

    <script language="JavaScript" type="text/javascript">
        alert("The leavetype has transactions.Please delete the transactions first");
        location = 'leavetypeslist.php';

    </script>
    <?php }else{?>

    <script language="JavaScript" type="text/javascript">
        location = 'leavetypeslist.php';
    </script>
    <?php
    }
    }


    $selectSQL = "SELECT * FROM leavetype WHERE id = '".$_SESSION['leavetype']."'";
    //echo $selectSQL;	
    $sql_select = mysqli_query($eProc,$selectSQL) or die(mysqli_error($eProc));
    $row_sql = mysqli_fetch_assoc($sql_select);	

    ?>
    <html>
        <head>
            <title>LEGAL  MANAGEMENT INFORMATION  SYSTEM</title>
            <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
            <link href="../styles/default.css" REL="stylesheet" type="text/css">
            <script language="javascript" type="text/javascript">
                function MM_valIDateForm() {
                    if (document.newleavetype.name.value == '') {
                        alert('You must enter the Name to continue');
                        document.newleavetype.name.focus();
                        return false;
                    }
                }
                function checkNumbers(evt) {
                    evt = (evt) ? evt : window.event
                    var charCode = (evt.which) ? evt.which : evt.keyCode
                    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                        if ((charCode < 37 || charCode > 40) && charCode != 46) {
                            alert('This field accepts only numeric characters (0-9) and . !');
                            status = 'This field accepts only numbers only and . !'
                            return false;
                        }
                    }
                    status = ''
                    return true
                }

                function valIDatechars(evt)
                {
                    // Prevents the entry of special characters and spaces into a field
                    evt = (evt) ? evt : window.event
                    var charCode = (evt.which) ? evt.which : evt.keyCode
                    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                        if (charCode < 64 || charCode > 90) {
                            if ((charCode < 95 || charCode > 122) && charCode != 8) {
                                if (charCode != 127 && charCode != 46 && charCode != 45) {
                                    if ((charCode < 37 || charCode > 40) && charCode != 32) {
                                        alert('This field accepts only 0-9, a-z, A_Z, space, backspace & del characters !');
                                        status = 'This field accepts only 0-9, a-z, A_Z, space & del characters !'
                                        return false;
                                    }
                                }
                            }
                        }
                    }
                    status = ''
                    return true
                }
                function confirmdelete(name, id) {
                    if (confirm('Are you sure you want to delete  leavetype ' + name + '?')) {
                        location.href = 'newleavetype.php?del=' + id
                    }
                }


            </script>
        </head>
        <body>
            <table width="100%" cellpadding="5">
                <tr align="leavetype">
                    <td width="50%" valign="top">
                        <fieldset>
                            <legend>Leave Type Details</legend>
                            <form onSubmit='return MM_valIDateForm()' action="" method="post" name="newleavetype" ID="newleavetype">
                                <input name="ID" type="hidden" value="<?php echo $row_sql['id']; ?>" />
                                <table width="100%" border="0" cellspacing="0" cellpadding="4">
                                    <tr valign="baseline">
                                        <td width="35%" align='right' nowrap><strong>Name:</strong></td>
                                        <td width="65%">
                                            <input size="20" maxlength="100" name="name" ID="name"  onFocus="this.style.backgroundColor = '#ffff00'"  onBlur="this.style.backgroundColor = '#F2FAFF'"  class='forms'   onkeypress='return valIDatechars(event)' value="<?php echo $row_sql['name']; ?>" >
                                        </td>
                                    </tr>
                                    <tr valign="baseline">
                                        <td align='right' nowrap><strong>Max Days:</strong></td>
                                        <td>
                                            <input size="20" maxlength="100" name="days" ID="days"  onFocus="this.style.backgroundColor = '#ffff00'"  onBlur="this.style.backgroundColor = '#F2FAFF'"  class='forms'   onkeypress='return checkNumbers(event)' value="<?php echo $row_sql['days']; ?>" >
                                        </td>
                                    </tr>

                                    <tr valign='leavetype'>
                                        <td colspan='2' ><div align='center'>
                                                <?php if (isset($_SESSION['leavetype'])) { ?>
                                                    <input name="update" type="submit" class="formsBlue" id="update" value="Update" />
                                                    <a href="newleavetype.php?id=<?php $row_sql['id']; ?>"> <a onclick='javascript:confirmdelete("<?php echo $row_sql['name'] ?>", "<?php echo $row_sql['id']; ?> ")' ><input name='delete' type='button' class='formsRed' value='Delete'  > </a> </a>
                                                <?php } else { ?>
                                                    <input name='save' type='submit' class='formsBlue' value='Save'>
                                                <?php } ?>
                                            <input name='cancel' type='button' onClick="location = 'leavetypeslist.php'" class='formsorg' value='Cancel'>
                                        </div></td>
                                </tr>
                            </table>
                        </form>
                    </fieldset
                    ></table>
    </body>
</html>
