<?php require_once('../connections/eProc.php'); 
include('../activelog.php');
mysqli_select_db($eProc, $database_eProc);
unset($_SESSION['leaveapplication']);
unset($_SESSION['emp']);
$employee=getuserid();
$_SESSION['emp']=$employee;
$umail=getuseremail($employee);
$uname=getauthusername($employee);
//echo "the employee".$employee;
$appdate=date('Y-m-d');
if(isset($_GET['id']) && $_GET['id']!=''){ 
	$_SESSION['leaveapplication']=$_GET['id'];
	 
	$selectSQL = "SELECT * FROM leaveapplication WHERE id = '".$_SESSION['leaveapplication']."'";
	//echo $selectSQL;	
	$sql_select = mysqli_query($eProc, $selectSQL) or die(mysqli_error($eProc));
	$row_sql = mysqli_fetch_assoc($sql_select);	
	
	
	$selectapprover = "SELECT * FROM employee WHERE id = '".$_SESSION['emp']."'";
	//echo $selectSQL;	
	$sql_selectapprover  = mysqli_query($eProc, $selectapprover) or die(mysqli_error($eProc));
	$row_sqlapprover= mysqli_fetch_assoc($sql_selectapprover );	
	
	//select all the employee details
	$selemp = "SELECT employee.*,department.name  AS department,department.id AS did,jobposition.name AS position
	FROM employee
	INNER JOIN department ON employee.department= department.id
	INNER JOIN jobposition ON employee.jobposition= jobposition.id
	 WHERE employee.id = '".$row_sql['employee']."'";
	//echo $selemp ;	
	$sql_selemp = mysqli_query($eProc, $selemp) or die(mysqli_error($eProc));
	$row_emp = mysqli_fetch_assoc($sql_selemp);	
	$empnums=mysqli_num_rows($sql_selemp);
	
	
	}
	
if ($_POST['Accept']!="" && $_POST['Accept']=="Accept" ) {
	//$exist=recordexists("leaveapplication","name",$_POST['name']) ;		
	//	if($exist=='no'){

	

			$applevel='D';//for hr
			$lstatus='Pending HOD Approval';	
			
		$seldeptapprover = "SELECT approvers.*,employee.email  FROM approvers INNER JOIN employee ON approvers.employee=employee.id  WHERE approvers.department ='".$row_sql['dept']."' AND approvers.applevel='D' ";

	//echo $seldeptapprover;
	$sqlda = mysqli_query($eProc, $seldeptapprover) or die(mysqli_error($eProc));
	$rowda = mysqli_fetch_assoc($sqlda);	

	
		
			$insertSQL =sprintf( "INSERT INTO leaveapproval (authoriser,appl,status,comment) VALUES (%s,%s,%s,%s)",
			 GetSQLValueString($_SESSION['emp'], "text"),
			 GetSQLValueString($_SESSION['leaveapplication'], "text"),
			 GetSQLValueString($lstatus, "text"),
			 GetSQLValueString($_POST['comment'], "text"));						
			//echo $insertSQL;
		$Result2 = mysqli_query($eProc, $insertSQL) or die(mysqli_error($eProc));
		
		$updateSQL = "UPDATE leaveapplication SET  lstatus='".$lstatus."',applevel ='".$applevel."'   WHERE  id ='".$_SESSION['leaveapplication']."'  ";
			//echo $updateSQL; 
			$Result1 = mysqli_query($eProc, $updateSQL) or die(mysqli_error($eProc));
			
	
		
do{

$name = $row_emp['name'];
$email = $rowda['email'];
$msg=$row_emp['name']."  leave application is pending  your approval .\n\n\n";

# -=-=-=- MIME BOUNDARY

$mime_boundary = "----NairobiNet Online----".md5(time());

# -=-=-=- MAIL HEADERS

$to = "$email";
$subject = "Leave Application";

$headers = "From: ".$row_emp['name']." <".$row_emp['email'].">\n";
$headers .= "MIME-Version: 1.0\n";
$headers .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";

# -=-=-=- TEXT EMAIL PART

$message = "--$mime_boundary\n";
$message .= "Content-Type: text/plain; charset=UTF-8\n";
$message .= "Content-Transfer-Encoding: 8bit\n\n";

$message .= "Dear Sir/Madam:\n\n";
$message .= $msg;

$message .= "Thank You.\n\n";



# -=-=-=- SEND MAIL

$mail_sent = @mail( $to, $subject, $message, $headers );
echo $mail_sent ? "Mail sent" : "Mail failed";
		
}while($rowda = mysqli_fetch_assoc($sqlda));


			?> 
			<script language="JavaScript" type="text/javascript">
			location='acceptdutylist.php';
			</script>
			<?php
			
	}
	
	
	
	if ($_POST['reject']!="" && $_POST['reject']=="Reject" ) {
	
	
			$applevel='L1';//for hr
			$lstatus='Rejected';	
	
	
			
			$insertSQL =sprintf( "INSERT INTO leaveapproval (authoriser,appl,status,comment,prevstatus) VALUES (%s,%s,%s,%s,%s)",
			 GetSQLValueString($_SESSION['emp'], "text"),
			 GetSQLValueString($_SESSION['leaveapplication'], "text"),
			 GetSQLValueString($lstatus, "text"),
			 GetSQLValueString($_POST['comment'], "text"),
			 GetSQLValueString($row_sql['lstatus'], "text"));						
		//	echo $insertSQL;
		$Result2 = mysqli_query($eProc, $insertSQL) or die(mysqli_error($eProc));
		
		$updateSQL = "UPDATE leaveapplication SET  lstatus='".$lstatus."' WHERE  id ='".$_SESSION['leaveapplication']."'  ";
			//echo $updateSQL; 
			$Result1 = mysqli_query($eProc, $updateSQL) or die(mysqli_error($eProc));
		
$name = $row_emp['name'];
$email = $row_emp['email'];

# -=-=-=- MIME BOUNDARY

$mime_boundary = "----NairobiNet Online----".md5(time());

# -=-=-=- MAIL HEADERS

$to = "$email";
$subject = "Leave Application Rejected";

$headers = "From: ".getempl($_SESSION['emp'])." <".$row_sqlapprover['email'].">\n";
$headers .= "MIME-Version: 1.0\n";
$headers .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";

# -=-=-=- TEXT EMAIL PART

$message = "--$mime_boundary\n";
$message .= "Content-Type: text/plain; charset=UTF-8\n";
$message .= "Content-Transfer-Encoding: 8bit\n\n";

$message .= "Dear Sir/Madam:\n\n";
$message .="Your leave application has been rejected by ".$row_sqlapprover['name']." .\n\n\n";

$message .= "Thank You.\n\n";

# -=-=-=- FINAL BOUNDARY

$message .= "--$mime_boundary--\n\n";

# -=-=-=- SEND MAIL

$mail_sent = @mail( $to, $subject, $message, $headers );
echo $mail_sent ? "Mail sent" : "Mail failed";
		

			?> 
			<script language="JavaScript" type="text/javascript">
				location='acceptdutylist.php';
			</script>
			<?php
			
	}
	
	
	
	
?>
<html>
<head>
<title>LEGAL  MANAGEMENT INFORMATION  SYSTEM</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../styles/default.css" REL="stylesheet" type="text/css">
<LINK REL=StyleSheet  href="calendar.css"  TYPE="text/css">
<script LANGUAGE="JavaScript"  src="weeklycalendar.js"> </script>
<script language="JavaScript" type="text/javascript">
	
	buildWeeklyCalendar(1);
</script>
<script language="javascript" type="text/javascript">
   function MM_valIDateForm() { 
 	  if(document.newleaveapplication.name.value=='' ){
 		alert('You must enter the Name to continue');
 		document.newleaveapplication.name.focus();
 		return false ;
 		} 
 	} 
	function checkNumbers(evt){ 
		evt=(evt)? evt: window.event
		var charCode= (evt.which)? evt.which: evt.keyCode
		if(charCode>31 && (charCode<48 || charCode>57)){
			if ((charCode<37 || charCode>40) && charCode!=46 ) {
				alert('This field accepts only numeric characters (0-9) and . !');
				status='This field accepts only numbers only and . !'
				return false;
			}
		}
		status=''
		return true
	} 

	function valIDatechars(evt)
		{
			// Prevents the entry of special characters and spaces into a field
			evt=(evt)? evt: window.event
			var charCode= (evt.which)? evt.which: evt.keyCode
			if( charCode>31 && (charCode<48 || charCode>57)) {
				if (charCode<64 || charCode>90) {
					if ((charCode<95 || charCode>122) && charCode!=8 ) {
						if (charCode!=127 && charCode!=46 && charCode!=45 ) {
							if ((charCode<37 || charCode>40) && charCode!=32) {
								alert('This field accepts only 0-9, a-z, A_Z, space, backspace & del characters !');
								status='This field accepts only 0-9, a-z, A_Z, space & del characters !'
								return false;
							}
						}
					}
				}
			}
			status=''
			return true
		}
 		function confirmdelete(name,id){
 		    	if (confirm('Are you sure you want to delete  leaveapplication ' +name+ '?')) {
 					location.href = 'newleaveapplication.php?del='+id 
 					} 
  				}
				
				
</script>
</head>
<body>
<table width="100%" cellpadding="5">
<tr align="leaveapplication">
<td width="50%" valign="top">
<fieldset>
<legend>Leave Application Details</legend>
<form onSubmit='return MM_valIDateForm()' action="" method="post" name="newleaveapplication" ID="newleaveapplication">
<input name="ID" type="hidden" value="<?php echo $row_sql['id'] ?>" />
<?php if($empnums>0){ ?>
<table  width="100%"cellspacing="0" cellpadding="4" border="0"  class="formsoutline">
<tr valign="baseline" bordercolor="#333333">
<td  width="18%"> 
<table  width="17%"cellspacing="0" cellpadding="4" border="1" height="104">
<tr valign="baseline" bordercolor="#333333">
<td > <img  src="<?php echo $row_emp['imageurl']; ?>"  width="150"  height="150"></td>

</tr>
</table>
</td> <td width="82%" valign="top"> <table  width="100%"cellspacing="0" cellpadding="4" border="0" height="166" class="formsoutline">
<tr valign="baseline" bordercolor="#333333">
<td width="21%" > <strong>Emp No :</strong></td>
<td width="79%" > <strong><?php echo $row_emp['empcode']?></strong></td>
</tr>
<tr valign="baseline" bordercolor="#333333">
<td > <strong>Name :</strong></td>
<td > <strong><?php echo $row_emp['name']?></strong></td>
</tr>
<tr valign="baseline" bordercolor="#333333">
<td > <strong>Department :</strong></td>
<td > <strong><?php echo $row_emp['department']?></strong></td>
</tr>
<tr valign="baseline" bordercolor="#333333">
<td > <strong>Job Position :</strong></td><td > <strong><?php echo $row_emp['position']?></strong></td>
</tr>
</table>  </td></tr></table>


<?php }?>
<table width="100%" border="0" cellspacing="0" cellpadding="4" class="formsoutline">
<tr valign="baseline">
<td width="23%" align='right' nowrap><strong>Leave :</strong></td>
<td width="77%"> <?php echo getleave($row_sql['leave']) ?></td>
</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="4" class="formsoutline">
<tr valign="baseline">
<td width="23%" align="right" nowrap valign="middle"><strong>Day Applied For :</strong></td>
<td  width="77%" align="left" ><?php echo $row_sql['daysno'];?></td>
</tr>
<tr valign="baseline">
<td width="23%" align="right" nowrap valign="middle"><strong>Leave Application Date :</strong></td>
<td  width="77%" align="left" ><?php echo $row_sql['appdate'];?></td>
</tr>
<tr valign="baseline">
<td width="23%" align="right" nowrap valign="middle"><strong>Reason For Request :</strong></td>
<td  width="77%" align="left" ><?php echo $row_sql['reason'];?></td>
</tr>
<tr valign="baseline">  
<td width="23%" align='right' nowrap><strong>First Date Of Leave :</strong></td>
<td width="77%">
<?php echo $row_sql['startdate'];?></td>
</tr>
<tr valign="baseline">
<td align='right' nowrap><strong>Last Date Of Leave:</strong></td>
<td>
<?php echo $row_sql['enddate'];?></td>
</tr>
<tr valign="baseline">
<td align='right' nowrap><strong>Return To Work Date:</strong></td>
<td>
<?php echo $row_sql['resumedate'];?></td>
</tr>
<tr valign="baseline">
<td align='right' nowrap><strong>Allocate duties to :</strong></td>
<td>
 
  <?php echo  getemployee($row_sql['incharge'])?>
       </select> </td>
</tr>
<tr valign="baseline">
<td width="23%" align="right" nowrap valign="middle"><strong>Comment :</strong></td>
<td height="88" width="77%" align="left" ><textarea name="comment" cols="50" rows="5" class="forms"><?php echo $row_sql['comment'];?></textarea></td></tr>
<tr valign='leaveapplication'>
<td colspan='2' ><div align='center'>
<input name='Accept' type='submit' class='formsBlue' value='Accept'>
<input name='reject' type='submit' class='formsred' value='Reject'>
<input name='cancel' type='button' onClick="location='leaveapplicationslist.php'" class='formsorg' value='Cancel'>
</div></td>
</tr>
</table>
<?php $selectl = "SELECT * FROM leaveapproval WHERE appl = '".$_SESSION['leaveapplication']."'";
	//echo $selectSQL;	
	$sql_selectl = mysqli_query($eProc, $selectl) or die(mysqli_error($eProc));
	$row_sqll= mysqli_fetch_assoc($sql_selectl );
	$num=mysqli_num_rows($sql_selectl);	
	if($num>0){
	
	?>
<table width="100%" border="0" cellspacing="0" cellpadding="4" class="formsoutline">
<tr valign="baseline">
<td width="18%"  class="inputdef"><strong>Authoriser</strong></td>
<td width="24%"  class="inputdef"><strong>Date Authorised</strong></td>
<td width="58%"  class="inputdef"><strong>Authoriser Comments </strong></td>

</tr>

<?php
do{

?>
<tr valign="baseline">
<td width="18%"><?php echo getauthoriser($row_sqll['authoriser']) ?></td>
<td width="24%"  ><?php echo $row_sqll['appdate'] ?></td>
<td width="58%"  ><?php echo $row_sqll['comment'] ?></td>

</tr>
<?php
 }while($row_sqll= mysqli_fetch_assoc($sql_selectl )); ?>
 </table>
<?php }?>



</form>
</fieldset>
</table>
</body>
</html>
