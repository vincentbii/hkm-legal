<?php require_once('../connections/eProc.php'); 

 require_once('../activelog.php'); ?>
<?php
 unset($_SESSION['employee']);
 unset($_SESSION['Empname']);
 unset($_SESSION['pgdisplay']);
 unset($_SESSION['nationality']);
 unset($_SESSION['religion']);
$currentPage = $_SERVER["PHP_SELF"];

$maxRows_employee = 30;
$pageNum_employee = 0;
if (isset($_GET['pageNum_employee'])) {
  $pageNum_employee = $_GET['pageNum_employee'];
}
$startRow_employee = $pageNum_employee * $maxRows_employee;

mysqli_select_db($eProc, $database_eProc);
$query_employee = "SELECT *  FROM  employee ORDER BY department ASC  ";
$query_limit_employee = sprintf("%s LIMIT %d, %d", $query_employee, $startRow_employee, $maxRows_employee);
$employee = mysqli_query($eProc, $query_limit_employee) or die(mysqli_error($eProc));
$row_employee = mysqli_fetch_assoc($employee);

if (isset($_GET['totalRows_employee'])) {
  $totalRows_employee = $_GET['totalRows_employee'];
} else {
  $all_employee = mysqli_query($eProc, $query_employee);
  $totalRows_employee = mysqli_num_rows($all_employee);
}
$totalPages_employee = ceil($totalRows_employee/$maxRows_employee)-1;

$queryString_employee = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_employee") == false && 
        stristr($param, "totalRows_employee") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_employee = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_employee = sprintf("&totalRows_employee=%d%s", $totalRows_employee, $queryString_employee);
?>

<html>
<head>
<title>LEGAL  MANAGEMENT INFORMATION  SYSTEM</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../styles/default.css" rel="stylesheet" type="text/css">

<style type="text/css">
<!--
.style1 {
	color: #FF0000;
	font-weight: bold;
}
-->
</style>
</head>
<body>

<table width="100%" border="0" cellpadding="4" cellspacing="0" >

  <tr>
   <td width="13%"   class="inputdeft" >Emp Code</td>
   <td width="15%"   class="inputdeft" >Name</td>
   <td width="21%"   class="inputdeft" >Job Position</td>
    <td width="18%"   class="inputdeft" >Department</td>
     <td width="33%"   class="inputdeft" >&nbsp;</td>
   
    	
  </tr>
  <?php if ($totalRows_employee > 0) { ?>
  <?php do { ?>
  <tr>
  <td ><?php echo $row_employee['empcode']?> </td>
   <td ><?php echo $row_employee['name']?> </td>
    <td ><?php echo getjobposition($row_employee['jobposition'])?> </td>
     <td ><?php echo gettheuserdept($row_employee['department'])?> </td>
     <td ><a href="../lv/newempschedule.php?emp=<?php echo $row_employee['id'] ?>">Schedule Leave</a></td>
  </tr>
  <?php } while ($row_employee = mysqli_fetch_assoc($employee)); ?>
  <tr>
    <td colspan="8" class="mainbase"><table width="97%"  border="0" cellspacing="0" cellpadding="3">
      <tr align="center">
        <td width="45%">
          <table width="78%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
              <td width="23%" align="center"><?php if ($pageNum_employee > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_employee=%d%s", $currentPage, 0, $queryString_employee); ?>">First</a>
                <?php } // Show if not first page ?>              </td>
              <td width="31%" align="center"><?php if ($pageNum_employee > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_employee=%d%s", $currentPage, max(0, $pageNum_employee - 1), $queryString_employee); ?>">Previous</a>
                <?php } // Show if not first page ?>              </td>
              <td width="23%" align="center"><?php if ($pageNum_employee < $totalPages_employee) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_employee=%d%s", $currentPage, min($totalPages_employee, $pageNum_employee + 1), $queryString_employee); ?>">Next</a>
                <?php } // Show if not last page ?>              </td>
              <td width="23%" align="center"><?php if ($pageNum_employee < $totalPages_employee) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_employee=%d%s", $currentPage, $totalPages_employee, $queryString_employee); ?>">last</a>
                <?php } // Show if not last page ?>              </td>
            </tr>
          </table></td>
        <td width="55%">&nbsp; Showing <strong><?php echo ($startRow_employee + 1) ?></strong> to <strong><?php echo min($startRow_employee + $maxRows_employee, $totalRows_employee) ?></strong> of <strong><?php echo $totalRows_employee ?></strong> </td>
      </tr>
    </table></td>
  </tr>
  <?php } else { ?>
  <tr>
    <td colspan="8" class="mainbase"><span class="style1">No employees Created! </span></td>
  </tr>
  <?php } ?>
</table>
</body>
</html>
<?php
mysqli_free_result($employee);
?>

