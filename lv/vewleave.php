<?php require_once('../connections/eProc.php'); 
include('../activelog.php');
mysqli_select_db($eProc, $database_eProc);
unset($_SESSION['leaveapplication']);
unset($_SESSION['emp']);
$employee=getuserid();
$_SESSION['emp']=$employee;
//echo "the employee".$employee;
$appdate=date('Y-m-d');
if(isset($_GET['id']) && $_GET['id']!=''){ 
	$_SESSION['leaveapplication']=$_GET['id'];
	


	$selectSQL = "SELECT * FROM leaveapplication WHERE id = '".$_SESSION['leaveapplication']."'";
	//echo $selectSQL;	
	$sql_select = mysqli_query($eProc, $selectSQL) or die(mysqli_error($eProc));
	$row_sql = mysqli_fetch_assoc($sql_select);	
	$_SESSION['emp']=$row_sql['employee'];
	//select all the employee details
	$selemp = "SELECT employee.*,department.name  AS department,department.id AS did,jobposition.name AS position
	FROM employee
	INNER JOIN department ON employee.department= department.id
	INNER JOIN jobposition ON employee.jobposition= jobposition.id
	 WHERE employee.id = '".$_SESSION['emp']."'";
	//echo $selemp ;	
	$sql_selemp = mysqli_query($eProc, $selemp) or die(mysqli_error($eProc));
	$row_emp = mysqli_fetch_assoc($sql_selemp);	
	$empnums=mysqli_num_rows($sql_selemp);
	
	
	}
	
//echo "the user logged in".$_SESSION['UNQ'];
?>
<html>
<head>
<title>LEGAL  MANAGEMENT INFORMATION  SYSTEM</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../styles/default.css" REL="stylesheet" type="text/css">
<LINK REL=StyleSheet  href="calendar.css"  TYPE="text/css">
<script LANGUAGE="JavaScript"  src="weeklycalendar.js"> </script>
<script language="JavaScript" type="text/javascript">
	
	buildWeeklyCalendar(1);
</script>
<script language="javascript" type="text/javascript">
   function MM_valIDateForm() { 
 	  if(document.newleaveapplication.name.value=='' ){
 		alert('You must enter the Name to continue');
 		document.newleaveapplication.name.focus();
 		return false ;
 		} 
 	} 
	function checkNumbers(evt){ 
		evt=(evt)? evt: window.event
		var charCode= (evt.which)? evt.which: evt.keyCode
		if(charCode>31 && (charCode<48 || charCode>57)){
			if ((charCode<37 || charCode>40) && charCode!=46 ) {
				alert('This field accepts only numeric characters (0-9) and . !');
				status='This field accepts only numbers only and . !'
				return false;
			}
		}
		status=''
		return true
	} 

	function valIDatechars(evt)
		{
			// Prevents the entry of special characters and spaces into a field
			evt=(evt)? evt: window.event
			var charCode= (evt.which)? evt.which: evt.keyCode
			if( charCode>31 && (charCode<48 || charCode>57)) {
				if (charCode<64 || charCode>90) {
					if ((charCode<95 || charCode>122) && charCode!=8 ) {
						if (charCode!=127 && charCode!=46 && charCode!=45 ) {
							if ((charCode<37 || charCode>40) && charCode!=32) {
								alert('This field accepts only 0-9, a-z, A_Z, space, backspace & del characters !');
								status='This field accepts only 0-9, a-z, A_Z, space & del characters !'
								return false;
							}
						}
					}
				}
			}
			status=''
			return true
		}
 		function confirmdelete(name,id){
 		    	if (confirm('Are you sure you want to delete  leaveapplication ' +name+ '?')) {
 					location.href = 'newleaveapplication.php?del='+id 
 					} 
  				}
				
				
</script>
</head>
<body>
<table width="100%" cellpadding="5">
<tr align="leaveapplication">
<td width="50%" valign="top">
<fieldset>
<legend>Leave Application Details</legend>
<form onSubmit='return MM_valIDateForm()' action="" method="post" name="newleaveapplication" ID="newleaveapplication">
<input name="ID" type="hidden" value="<?php echo $row_sql['id'] ?>" />
<?php if($empnums>0){ ?>
<table  width="100%"cellspacing="0" cellpadding="4" border="0"  class="formsoutline">
<tr valign="baseline" bordercolor="#333333">
<td  width="18%"> 
<table  width="17%"cellspacing="0" cellpadding="4" border="1" height="104">
<tr valign="baseline" bordercolor="#333333">
<td > <img  src="<?php echo $row_emp['imageurl']; ?>"  width="150"  height="150"></td>

</tr>
</table>
</td> <td width="82%" valign="top"> <table  width="100%"cellspacing="0" cellpadding="4" border="0" height="166" class="formsoutline">
<tr valign="baseline" bordercolor="#333333">
<td width="13%" > <strong>Emp No :</strong></td>
<td width="87%" > <strong><?php echo $row_emp['empcode']?></strong></td>
</tr>
<tr valign="baseline" bordercolor="#333333">
<td > <strong>Name :</strong></td>
<td > <strong><?php echo $row_emp['name']?></strong></td>
</tr>
<tr valign="baseline" bordercolor="#333333">
<td > <strong>Department :</strong></td>
<td > <strong><?php echo $row_emp['department']?></strong></td>
</tr>
<tr valign="baseline" bordercolor="#333333">
<td > <strong>Job Position :</strong></td><td > <strong><?php echo $row_emp['position']?></strong></td>
</tr>
</table>  </td></tr></table>

<?php }?>
<table width="100%" border="0" cellspacing="0" cellpadding="4" class="formsoutline">
<tr valign="baseline">
<td width="23%" align='right' nowrap><strong>Leave :</strong></td>
<td width="77%"> <?php echo getleave($row_sql['leave']) ?></td>
</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="4" class="formsoutline">
<tr valign="baseline">
<td width="23%" align="right" nowrap valign="middle"><strong>ReasonFor Request :</strong></td>
<td  width="77%" align="left" ><?php echo $row_sql['reason']; ?></td>
</tr>
<tr valign="baseline">  
<td width="23%" align='right' nowrap><strong>Leave Days Requested :</strong></td>
<td width="77%">
<?php echo $row_sql['daysno'];?></td>
</tr>
<tr valign="baseline">  
<td width="23%" align='right' nowrap><strong>First Date Of Leave :</strong></td>
<td width="77%">
<?php echo $row_sql['startdate'];?></td>
</tr>
<tr valign="baseline">
<td align='right' nowrap><strong>Last Date Of Leave:</strong></td>
<td>
<?php echo $row_sql['enddate'];?></td>
</tr>
<tr valign="baseline">
<td align='right' nowrap><strong>Return To Work Date:</strong></td>
<td>
<?php echo $row_sql['resumedate'];?></td>
</tr>
<tr valign="baseline">
<td align='right' nowrap><strong>Allocate duties to :</strong></td>
<td>
 
  <?php echo  getemployee($row_sql['incharge'])?>
       </select> </td>
</tr>
<tr valign='leaveapplication'>
<td colspan='2' ><div align='center'>

<input name='cancel' type='button' onClick="location='leaveapplicationslist.php'" class='formsorg' value='Cancel'>
</div></td>
</tr>
</table>


</form>
</fieldset>
</table>
    <?php
	$selecta = "SELECT * FROM leaveapproval WHERE appl='".$_SESSION['leaveapplication']."' ";
	//echo $select;	
	$sqlaselect = mysqli_query($eProc, $selecta) or die(mysqli_error($eProc));
	$rowasql = mysqli_fetch_assoc($sqlaselect);
	$numa=mysqli_num_rows($sqlaselect);	
	if($numa>0){
	?>
<legend>Approval History</legend>
	<table width="100%" cellpadding="3">
	<tr >
	<td width="13%" class="inputdef"><strong>Date </strong></td>
    <td width="19%" class="inputdef"><strong>Status</strong></td>
		<td width="21%" class="inputdef"><strong>Authorized By</strong></td>
	<td width="47%" class="inputdef"><strong>Authorizer's Comments </strong></td>
	</tr>
	<?php
	
		do{
		?>	<tr >
			<td><?php echo $rowasql['appdate']?></td>
            <td><?php echo $rowasql['status']?></td>
			<td><?php echo getauthoriser($rowasql['authoriser'])?></td>
			<td><?php echo $rowasql['comment']?></td>
			</tr>
		<?php }while($rowasql = mysqli_fetch_assoc($sqlaselect));
		
	?>
	</table>
	
<?php }?>

</body>
</html>
