<?php require_once('../connections/eProc.php'); ?>
<?php include('../activelog.php'); ?>
<?php
	
	if ($_POST['Edit']=="Edit" && isset($_GET['token'])&& $_GET['token']!=''){
		  $insertSQL = sprintf("UPDATE groups SET groupdesc=%s, groupname=%s , grouptypeid=%s WHERE groupid = %s",
							   GetSQLValueString($_POST['groupdesc'], "text"),
							    GetSQLValueString($_POST['groupname'], "text"),
								 GetSQLValueString($_POST['grouptype'], "text"),
							   GetSQLValueString($_GET['token'], "text"));
							    mysqli_select_db($eProc, $database_eProc);
  $Result1 = mysqli_query($eProc, $insertSQL) or die('ERROR Creating Group: '.mysqli_error($eProc));
  //echo $insertSQL;
							   ?>
	  <script language="JavaScript" type="text/javascript">
			location='groups.php';
	</script>
		
		<?php }
		
		
	if ($_POST['Save']=="Save" && recordexists("groups","groupname",$_POST['groupname'])=='no') {
			$insertSQL = sprintf("INSERT INTO groups (grouptypeid, groupname, groupdesc) VALUES (%s, %s, %s)",
							   GetSQLValueString($_POST['grouptype'], "text"),
							   GetSQLValueString($_POST['groupname'], "text"),
							   GetSQLValueString($_POST['groupdesc'], "text"));
							    mysqli_select_db($eProc, $database_eProc);
  $Result1 = mysqli_query($eProc, $insertSQL) or die('ERROR Creating Group: '.mysqli_error($eProc));
 // echo $insertSQL;
							   				   ?>
	  <script language="JavaScript" type="text/javascript">
			location='groups.php';
	</script>
		
		<?php
}elseif ($_POST['Save']=="Save" && recordexists("groups","groupname",$_POST['groupname'])=='yes') {

							   				   ?>
	  <script language="JavaScript" type="text/javascript">
			alert("A group with <?php echo $_POST['groupname'] ?> name exists");
			location='groups.php';
	</script>
		
		<?php
		}
	 
 


if($_POST['Delete']=="Remove") {
	$insertSQL = "DELETE FROM groups WHERE groupid = ".$_GET['token']."";
				   
				   //echo $insertSQL;
	mysqli_select_db($eProc, $database_eProc);
  $Result1 = mysqli_query($eProc, $insertSQL) or die('ERROR Creating Group: '.mysqli_error($eProc));
   ?>
	  <script language="JavaScript" type="text/javascript">
			location='groups.php';
	</script>
	<?php

} 



if(isset($_GET['token'])) {
	$sql="SELECT * FROM groups WHERE groupid = '".$_GET['token']."'";
	mysqli_select_db($eProc, $database_eProc);
  	$Result1 = mysqli_query($eProc, $sql) or die('ERROR Retrieving group: '.mysqli_error($eProc));
	$rowgr=mysqli_fetch_assoc($Result1);
	//echo $sql;
}

?>
<html>
<head>
<title>Management Information System </title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../styles/default.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.style1 {font-weight: bold}
-->
</style>
</head>
<body>
<table width="100%"  border="2" cellpadding="0" cellspacing="3" bordercolor="#0A6EC3" bgcolor="#FFFFFF">
  <tr>
    <td valign="top"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td valign="top"><?php include('../includes/header.php'); ?></td>
      </tr>
      <tr>
        <td valign="top"><img src="../images/template_06.gif" width="100%" height="6"></td>
      </tr>
      <tr>
        <td valign="top"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="100%" class="spacer5"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
               <tr>
                <td><?php include("../includes/admstrip2.php"); ?></td>
              </tr>
              <tr>
                <td colspan="5" class="intspace">
				<table width="100%"  border="0" cellspacing="0" cellpadding="4">
                    <tr valign="top">
                      <td width="170"><?php include('amenu.php'); ?></td>
                      <td><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                          <tr valign="middle">
                            <td width="63%" class="hOne">Add New Group </td>
                            <td width="37%" align="right" nowrap class="baseline white">last Updated: </td> 
                          </tr>
                        </table>
                          <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td valign="top" class="baseline">Fill in the form below to add a new group to the system.</td>
                            </tr>
                            <tr>
    <td>
<form method="post" name="form1" action="<?php echo $editFormAction; ?>">
<fieldset>
<legend>GroupDetails</legend>
  <table width="400" cellpadding="4" cellspacing="0">
    <tr valign="baseline">
      <td align="right" nowrap>Group Name:</td>
      <td width="268">
		<input name="groupname" class="forms" type="text" id="groupname" size="30" maxlength="50" value="<?php echo $rowgr['groupname']; ?>">
    </td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap>Group Type : </td>
      <td><label>
        <select name="grouptype" class="forms" id="grouptype">
		<?php showGrouptypes($rowgr['grouptypeid']); ?>
        </select>
      </label></td>
    </tr>
   
    <tr valign="baseline">
      <td align="right" valign="top" nowrap>Group Description: </td>
      <td><textarea name="groupdesc" cols="50" class="forms" id="groupdesc"><?php echo $rowgr['groupdesc'] ?></textarea></td>
    </tr>
  </table>
</fieldset>
<br>

<br>
<fieldset>
  <legend></legend>
  <table width="400"  border="0" cellspacing="0" cellpadding="3">
    <tr valign="baseline">
      <td colspan="2" align="right" nowrap><eProc size="1"></td>
    </tr>
    <tr valign="baseline">
      <td width="116" align="right" nowrap><input type="hidden" name="MM_insert" value="form1"></td>
      <td width="272">      	
        
		<?php if(isset($_GET['token'])) { ?>
        <input name="Edit" type="submit" class="formsBlue" id="Edit" value="Edit">
        <input name="Delete" type="submit" class="formsred" id="Delete" value="Remove" />
		<?php } else{?>
		<input name="Save" type="submit" class="formsBlue" id="Save" value="Save">
        <?php } ?>
        <input name="button" type="button" class="formsorg" onClick="javascript:location='groups.php'" value="Cancel"></td>
     </tr>
                                  </table>
                                    </form>
                                </td>
                            </tr>
                        </table></td>
                    </tr>
                </table></td>
              </tr>
            </table></td>
            </tr>
        </table></td>
      </tr>
      <tr>
        <td ><?php include('../includes/footer.php'); ?></td>
      </tr>
    </table></td>
  </tr>
</table>
</body>
</html>

