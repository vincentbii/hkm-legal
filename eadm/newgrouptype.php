<?php require_once('../connections/eProc.php'); ?>
<?php include('../activelog.php'); ?>
<?php

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form1")) {

if($_POST['Save']=="Save") {
	// Generate Group ID

// -----------------
 
	if (recordexists("grouptypes","id","'".$_POST['id']."'")) {
		  $insertSQL = sprintf("UPDATE grouptypes SET grouptypename = %s WHERE id = %s",
							   
							   GetSQLValueString($_POST['grouptypename'], "text"),
							   GetSQLValueString($_POST['id'], "text"));
	} else {
			$insertSQL = sprintf("INSERT INTO grouptypes (grouptypename) VALUES (%s)",
							   
							   GetSQLValueString($_POST['grouptypename'], "text"));
	}
  mysqli_select_db($eProc, $database_eProc);
  $Result1 = mysqli_query($eProc, $insertSQL) or die('ERROR Creating Group: '.mysqli_error($eProc));

} elseif($_POST['Delete']=="Remove") {
	$insertSQL = sprintf("DELETE FROM grouptypes WHERE id = %s",
				   GetSQLValueString($_POST['id'], "text"));
				  // echo $insertSQL;
	mysqli_select_db($eProc, $database_eProc);
  $Result1 = mysqli_query($eProc, $insertSQL) or die('ERROR Creating Group: '.mysqli_error($eProc));
} 

 $insertGoTo = "grouptypes.php";
	  if (isset($_SERVER['QUERY_STRING'])) {
		$insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
		$insertGoTo .= $_SERVER['QUERY_STRING'];
	  }
	  ?>
	  <script language="JavaScript" type="text/javascript">
			location='<?php echo $insertGoTo  ?>';
	</script>
	<?php


}

if(isset($_GET['id'])) {
	$sql="SELECT * FROM grouptypes WHERE id = '".$_GET['id']."'";
	mysqli_select_db($eProc, $database_eProc);
  	$Result1 = mysqli_query($eProc, $sql) or die('ERROR Retrieving group: '.mysqli_error($eProc));
	$rowgr=mysqli_fetch_assoc($Result1);
}

?>
<html>
<head>
<title>eProcure v1.0</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../styles/default.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.style1 {font-weight: bold}
-->
</style>
</head>
<body>
<table width="100%"  border="2" cellpadding="0" cellspacing="3" bordercolor="#0A6EC3" bgcolor="#FFFFFF">
  <tr>
    <td valign="top"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td valign="top"><?php include('../includes/header.php'); ?></td>
      </tr>
      <tr>
        <td valign="top"><img src="../images/template_06.gif" width="100%" height="6"></td>
      </tr>
      <tr>
        <td valign="top"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="100%" class="spacer5"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
               <tr>
                <td><?php include("../includes/admstrip2.php"); ?></td>
              </tr>
              <tr>
                <td colspan="5" class="intspace">
				<table width="100%"  border="0" cellspacing="0" cellpadding="4">
                    <tr valign="top">
                      <td width="170"><?php include('amenu.php'); ?></td>
                      <td><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                          <tr valign="middle">
                            <td width="63%" class="hOne">Add New Group Type </td>
                            <td width="37%" align="right" nowrap class="baseline white">last Updated: </td> 
                          </tr>
                        </table>
                          <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td valign="top" class="baseline">Fill in the form below to add a new group to the system.</td>
                            </tr>
                            <tr>
    <td>
<form method="post" name="form1" action="<?php echo $editFormAction; ?>">
<fieldset>
<legend>Group <span class="hOne">Type </span>Details</legend>
  <table width="400" cellpadding="4" cellspacing="0">
    <tr valign="baseline">
      <td align="right" nowrap>Group Type Name:</td>
      <td width="268"><?php if (isset($_GET['id'])) { ?>
		 
      	<input name="grouptypename" type="text" id="grouptypename" size="30" class="forms" maxlength="50" value="<?php echo $rowgr['grouptypename'] ?>">
      <?php } else {  ?>
      	<input name="grouptypename" type="text" id="grouptypename" size="30" class="forms" maxlength="50" >
	  <?php } ?>
      <input name="id" type="hidden" id="id" value="<?php echo $_GET['id'] ?>">      </td>
    </tr>
   
    <tr valign="baseline">
      <td align="right" valign="top" nowrap>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
  </table>
</fieldset>
<br>

<br>
<fieldset>
  <legend></legend>
  <table width="400"  border="0" cellspacing="0" cellpadding="3">
    <tr valign="baseline">
      <td colspan="2" align="right" nowrap><eProc size="1"></td>
    </tr>
    <tr valign="baseline">
      <td width="116" align="right" nowrap><input type="hidden" name="MM_insert" value="form1"></td>
      <td width="272"><input name="Save" type="submit" class="formsBlue" id="Save" value="Save">
        <input name="button" type="button" class="formsorg" onClick="javascript:location='grouptypes.php'" value="Cancel">
		<?php if(isset($_GET['id'])) { ?>
        <input name="Delete" type="submit" class="formsred" id="Delete" value="Remove" />
		<?php } ?></td>
     </tr>
                                  </table>
                                    </form>
                                </td>
                            </tr>
                        </table></td>
                    </tr>
                </table></td>
              </tr>
            </table></td>
            </tr>
        </table></td>
      </tr>
      <tr>
        <td ><?php include('../includes/footer.php'); ?></td>
      </tr>
    </table></td>
  </tr>
</table>
</body>
</html>

