<?php require_once('../connections/eProc.php'); ?>
<?php
include('../activelog.php');
// -------------------------------------


$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
    $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ($_POST['OK'] != "") {
    if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form1")) {

        // Generate Necessary Variables
        // -----------------------------------------------------------------
        $MD5 = md5($_POST['umd5']); // md5 encrypted password
        $authcode = createHASH($_POST['fulname']); // Hashed authcode
        // -----------------------------------------------------------------

        if ($_POST['co'] == 0) {
            $insertSQL = sprintf("INSERT INTO users (employee, username, umd5, authcode, com_id, groupid, `admin`) VALUES (%s, %s, %s, %s, %s, %s, %s)", GetSQLValueString($_POST['fulname'], "text"),
//                       GetSQLValueString($_SESSION['val'], "text"),
                    GetSQLValueString($_POST['username'], "text"), GetSQLValueString($MD5, "text"),
//						   GetSQLValueString($_POST['accessrole'], "text"),
//						   GetSQLValueString($_POST['worktel'], "text"),
//						   GetSQLValueString($_POST['hometel'], "text"),
//						   GetSQLValueString($_POST['mobtel'], "text"),
//						   GetSQLValueString($_POST['email'], "text"),
//						   GetSQLValueString($_POST['uregdate'], "int"),
                    GetSQLValueString($authcode, "text"), GetSQLValueString($_POST['com_id'], "text"),
//                       GetSQLValueString(getcompanyadmingroup(), "text"),
                    GetSQLValueString($_POST['groupid'], "text"), GetSQLValueString($_POST['admin'], "int"));
        } else {
            $insertSQL = sprintf("UPDATE users SET fulname=%s, username=%s, umd5=%s, accessrole=%s, worktel=%s, hometel=%s, mobtel=%s, email=%s, uregdate=%s, authcode=%s, com_id=%s, groupid=%s, admin=%s WHERE uid=%s", GetSQLValueString($_POST['fulname'], "text"), GetSQLValueString($_POST['username'], "text"), GetSQLValueString($MD5, "text"), GetSQLValueString($_POST['accessrole'], "text"), GetSQLValueString($_POST['worktel'], "text"), GetSQLValueString($_POST['hometel'], "text"), GetSQLValueString($_POST['mobtel'], "text"), GetSQLValueString($_POST['email'], "text"), GetSQLValueString($_POST['uregdate'], "int"), GetSQLValueString($authcode, "text"), GetSQLValueString($_POST['com_id'], "text"), GetSQLValueString(getcompanyadmingroup(), "text"), GetSQLValueString($_POST['admin'], "int"), GetSQLValueString($_POST['uid'], "text"));
        }

        mysqli_select_db($eProc, $database_eProc);
        $Result1 = mysqli_query($eProc, $insertSQL) or die($_SESSION['val'] . '' . "ERROR CREATING ACCOUNT ADMINISTRATOR - " . mysqli_error($eProc));
        $groupid = getcompanyadmingroup();
        InsertUserGroups($authcode, $groupid);

        $insertGoTo = "companylist.php";
        ?>
        <script language="javascript" type="text/javascript">
            location = '<?php echo $insertGoTo ?>';
        </script>
        <?php
    }
}
?>
<html>
    <head>
        <script language="JavaScript" type="text/javascript">
            function submitform() {
                document.form1.submit();
            }
        </script>

        <title>Management Information System </title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="../styles/default.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <table width="100%"  border="2" cellpadding="0" cellspacing="3" bordercolor="#0A6EC3" bgcolor="#FFFFFF">
            <tr>
                <td valign="top"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td valign="top"><?php include('../includes/header.php'); ?></td>
                        </tr>
                        <tr>
                            <td valign="top"><img src="../images/template_06.gif" width="100%" height="6"></td>
                        </tr>
                        <tr>
                            <td valign="top"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="100%" class="spacer5"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td><?php include("../includes/admstrip1.php"); ?></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="5" class="intspace">
                                                        <table width="100%"  border="0" cellspacing="0" cellpadding="4">
                                                            <tr valign="top">
                                                                <td width="170"><?php include('acomenu.php'); ?></td> 
                                                                <td><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                                                                        <tr valign="middle">
                                                                            <td width="63%" class="hOne">Add Company Administrator Login </td>
                                                                            <td width="37%" align="right" nowrap class="baseline white">last Updated: </td> 
                                                                        </tr>
                                                                    </table>
                                                                    <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td valign="top" class="baseline">This section allows you to add an account managing administrator for companies setup. </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td valign="top" class="spacer5">
                                                                                <form method="post" name="form1" action="<?php echo $editFormAction; ?>">
                                                                                    <table width="100%">
                                                                                        <tr>
                                                                                            <td>
                                                                                                <table width="388" cellpadding="5" cellspacing="0" class="tblbdr">
                                                                                                    <tr valign="baseline">
                                                                                                        <td align="right" nowrap>Company:</td>
                                                                                                        <td width="271">
                                                                                                            <select name="com_id" class="forms" id="com_id" onChange="submitform()">
<?php
if (isset($_POST['com_id']) && $_POST['com_id'] != "") {
    ?><option >Select company</option><?php
                                                                                                                    showCompanies("", $_POST['com_id']);
                                                                                                                } else {
                                                                                                                    showCompanies();
                                                                                                                    ?> <option selected>Select company</option> <?php
                                                                                                                }
                                                                                                                ?>
                                                                                                            </select></td>
                                                                                                    </tr>

<?php
mysqli_select_db($eProc, $database_eProc);

$query_rsusers = "SELECT * FROM users WHERE  com_id = '" . $_POST['com_id'] . "' AND  groupid = '" . getcompanyadmingroup() . "'";
$rsusers = mysqli_query($eProc, $query_rsusers) or die(mysqli_error());
$row_rsusers = mysqli_fetch_assoc($rsusers);
$totalRows_rsusers = mysqli_num_rows($rsusers);
$uid = $row_rsusers['uid'];


if ($_POST['com_id'] != "") {
    ?>
                                                                                                        <tr valign="baseline">
                                                                                                            <td colspan="2" align="right" nowrap><eProc size="1"></td>
                                                                                                            </tr>
                                                                                                            <tr valign="baseline">
                                                                                                                <td nowrap align="right">Full Name:</td>
                                                                                                                <td>
                                                                                                                     <!--<input name="fulname" type="text" class="forms" value="<?php //echo $row_rsusers['fulname'];   ?>" size="50">-->
                                                                                                                    <select name="fulname"  onFocus="this.style.backgroundColor = '#ffff00'"  onBlur="this.style.backgroundColor = '#F2FAFF'" class='forms' >
    <?php
    showemployees($_SESSION['val']);
    ?>
                                                                                                                    </select>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr valign="baseline">
                                                                                                                <td colspan="2" align="right" nowrap><eProc size="1"></td>
                                                                                                                </tr>
                                                                                                                <tr valign="baseline">
                                                                                                                    <td nowrap align="right">Username:</td>
                                                                                                                    <td><input name="username" type="text" class="forms" value="<?php echo $row_rsusers['username']; ?>" size="32"></td>
                                                                                                                </tr>
                                                                                                                <tr valign="baseline">
                                                                                                                    <td nowrap align="right">Password:</td>
                                                                                                                    <td><input name="UMD6" type="password" class="forms" id="UMD6" size="32"></td>
                                                                                                                </tr>
                                                                                                                <tr valign="baseline">
                                                                                                                    <td nowrap align="right">Confirm Password:</td>
                                                                                                                    <td><input name="umd5" type="password" class="forms" size="32"></td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td>
    <?php $name = getcompanyadmingroup();
    echo $name;
    ?>
                                                                                                                    </td>
                                                                                                                </tr>
    <?php
    /*
      <tr valign="baseline">
      <td nowrap align="right" valign="top">Access Role:</td>
      <td><textarea name="accessrole" cols="50" rows="5" class="forms"><?php echo $row_rsusers['accessrole
      ']; ?></textarea>                                      </td>
      </tr>
      <tr valign="baseline">
      <td colspan="2" align="right" nowrap><eProc size="1"></td>
      </tr>
      <tr valign="baseline">
      <td nowrap align="right">Work Tel:</td>
      <td><input name="worktel" type="text" class="forms" value="<?php echo $row_rsusers['worktel']; ?>" size="32"></td>
      </tr>
      <tr valign="baseline">
      <td nowrap align="right">Home Tel:</td>
      <td><input name="hometel" type="text" class="forms" value="<?php echo $row_rsusers['hometel']; ?>" size="32"></td>
      </tr>
      <tr valign="baseline">
      <td nowrap align="right">Mobile:</td>
      <td><input name="mobtel" type="text" class="forms" value="<?php echo $row_rsusers['mobtel']; ?>" size="32"></td>
      </tr>
      <tr valign="baseline">
      <td nowrap align="right">Email:</td>
      <td><input name="email" type="text" class="forms" value="<?php echo $row_rsusers['email']; ?>" size="50"></td>
      </tr>
      <?php
     */
    ?>
                                                                                                                <tr valign="baseline">
                                                                                                                    <td colspan="2" align="right" nowrap><eProc size="1"></td>
                                                                                                                    </tr>

<?php } ?>
                                                                                                                </table>
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                <td width="51%" valign="top">
                                                                                                                    <fieldset>
                                                                                                                        <legend>Groups </legend>
                                                                                                                        <table width="100%" border="0" cellspacing="0" cellpadding="4">
                                                                                                                            <tr valign="baseline">

<?php
$selectSQL = "SELECT groups.groupname, groups.groupid, groups.groupdesc, groups.sys FROM groups WHERE groups.groupname <> 'System administrator' AND groups.groupname <> 'Company Administrator' ORDER BY groups.groupname";

$done = mysqli_query($eProc, $selectSQL) or die("ERROR: Trying to retrieve groups list - " . mysqli_error($eProc));
$groupslist = mysqli_fetch_assoc($done);
?>
                                                                                                                                <td width="472" rowspan="4">
                                                                                                                                <?php if (mysqli_num_rows($done) > 0) { ?>
                                                                                                                                        <table width="100%" border="0" > 
                                                                                                                                            <tr>
                                                                                                                                                <td width="47%">&nbsp;</td>
                                                                                                                                                <td width="32%">Default group </td>
                                                                                                                                                <td width="21%">Other</td>
                                                                                                                                            </tr>
    <?php do { ?>

                                                                                                                                                <tr>
                                                                                                                                                    <td><?php echo $groupslist['groupname']; ?></td>
                                                                                                                                                    <td>
                                                                                                                                                        <label>
                                                                                                                                                            <input name="groupid" id="groupid" type="radio" value="<?php echo $groupslist['groupid']; ?>">
                                                                                                                                                        </label>
                                                                                                                                                    </td>
                                                                                                                                                    <td>
                                                                                                                                                        <input name="<?php echo $groupslist['groupid']; ?>" id="<?php echo $groupslist['groupid']; ?>" type="checkbox" value="<?php echo $groupslist['groupid']; ?>">
                                                                                                                                                    </td>
                                                                                                                                                </tr>
    <?php } while ($groupslist = mysqli_fetch_assoc($done)); ?>
                                                                                                                                        </table>
                                                                                                                                        <?php } ?>	</td>
                                                                                                                            </tr>
                                                                                                                            <tr valign="baseline">    

                                                                                                                                <td colspan="4" align="right" nowrap><hr size="1"></td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                    </fieldset>
                                                                                                                </td>
                                                                                                                </td>
                                                                                                                </tr>
                                                                                                                <tr valign="baseline">
                                                                                                                    <td nowrap align="right">&nbsp;</td>
                                                                                                                    <td>
                                                                                                                        <input name="OK" type="submit" class="formsBlue" id="OK" value="Create Account Administrator">
                                                                                                                        <input type="hidden" name="uregdate" value="<?php echo time(); ?>">
                                                                                                                        <input name="uid" type="hidden" id="uid" value="<?php echo $uid ?>">
                                                                                                                        <input type="hidden" name="admin" value="1">
                                                                                                                        <input type="hidden" name="MM_insert" value="form1">
                                                                                                                        <input name="co" type="hidden" id="co" value="<?php echo $totalRows_rsusers; ?>"></td>
                                                                                                                </tr>
                                                                                                                </table>
                                                                                                                </form>
                                                                                                                </td>
                                                                                                                </tr>
                                                                                                                </table></td>
                                                                                                                </tr>
                                                                                                                </table></td>
                                                                                                                </tr>
                                                                                                                </table></td>
                                                                                                                </tr>
                                                                                                                </table></td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td><?php include('../includes/footer.php'); ?></td>
                                                                                                                </tr>
                                                                                                                </table></td>
                                                                                                                </tr>
                                                                                                                </table>
                                                                                                                </body>
                                                                                                                </html>
<?php
mysqli_free_result($rsusers);
?>