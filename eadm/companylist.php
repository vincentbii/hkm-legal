<?php require_once('../connections/eProc.php'); ?>
<?php
$currentPage = $_SERVER["PHP_SELF"];

$maxRows_comlist = 999999;
$pageNum_comlist = 0;
if (isset($_GET['pageNum_comlist'])) {
  $pageNum_comlist = $_GET['pageNum_comlist'];
}
$startRow_comlist = $pageNum_comlist * $maxRows_comlist;

mysqli_select_db($eProc, $database_eProc);
$query_comlist = "SELECT * FROM companyinfo ORDER BY company ASC";
$query_limit_comlist = sprintf("%s LIMIT %d, %d", $query_comlist, $startRow_comlist, $maxRows_comlist);
$comlist = mysqli_query($eProc, $query_limit_comlist) or die(mysqli_error());
$row_comlist = mysqli_fetch_assoc($comlist);

if (isset($_GET['totalRows_comlist'])) {
  $totalRows_comlist = $_GET['totalRows_comlist'];
} else {
  $all_comlist = mysqli_query($eProc, $query_comlist);
  $totalRows_comlist = mysqli_num_rows($all_comlist);
}
$totalPages_comlist = ceil($totalRows_comlist/$maxRows_comlist)-1;

$queryString_comlist = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_comlist") == false && 
        stristr($param, "totalRows_comlist") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_comlist = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_comlist = sprintf("&totalRows_comlist=%d%s", $totalRows_comlist, $queryString_comlist);
?>
include('../activelog.php');
// -------------------------------------
?>
<html>
<head>
<title>Management Information System </title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../styles/default.css" rel="stylesheet" type="text/css"></head>
<body>
<table width="100%"  border="2" cellpadding="0" cellspacing="3" bordercolor="#0A6EC3" bgcolor="#FFFFFF">
  <tr>
    <td valign="top"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td valign="top"><?php include('../includes/header.php'); ?></td>
      </tr>
      <tr>
        <td valign="top"><img src="../images/template_06.gif" width="100%" height="6"></td>
      </tr>
      <tr>
        <td valign="top"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="100%" class="spacer5"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td><?php include("../includes/admstrip1.php"); ?></td>
              </tr>
              <tr>
                <td colspan="5" class="intspace">
				<table width="100%"  border="0" cellspacing="0" cellpadding="4">
                    <tr valign="top">
                      <td width="170"><?php include('acomenu.php'); ?></td> 
                      <td><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                          <tr valign="middle">
                            <td width="63%" class="hOne">Companies List 
							<?php 
							if (isset($_GET['e'])) { 
								echo '- <span class="orange">Edit company Information</span>';
							} elseif (isset($_GET['d'])) {
								echo '- <span class="orange">Delete company Information</span>';
							} else {
								// DO NOTHING!
							}
						    ?>
							</td>
                            <td width="37%" align="right" nowrap class="baseline white">last Updated: </td> 
                          </tr>
                        </table>
                          <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td valign="top" class="baseline">This section  lists the companies on the system</td>
                            </tr>
                            <tr>
                              <td valign="top">
                                <table width="100%" border=" 0" cellpadding="4" cellspacing="0" >
                                  <tr>
                                    <td width="20%" class="inputdef"><strong>Company</strong></td>
                                    
                                    <td width="10%" class="inputdef"><strong>Address</strong></td>
                                    <td width="20%" class="inputdef"><strong>Telephone</strong></td>
                                    <td width="10%" class="inputdef"><strong>Fax</strong></td>
                                    <td width="10%" class="inputdef"><strong>email</strong></td>
                                   
                                    </tr>
									<?php if ($totalRows_comlist > 0) { ?>
                                  <?php do { ?>
                                  <tr valign="top">
                                    <td class="gridCells"><strong>
									<?php
									if (isset($_GET['e'])) {
										$page = "<a href='editcompany.php?id=".$row_comlist['com_id']."'>".$row_comlist['company']."</a>";
									} elseif (isset($_GET['d'])) {
										$page = "<a href='deletecompany.php?d=1&id=".$row_comlist['com_id']."'>".$row_comlist['company']."</a>";
									} else {
										
											$page = $row_comlist['company'];
										
									}
									echo $page; 
									?>
									</strong><br>
                                        <?php echo $row_comlist['location']; ?></td>
                                
                                    <td class="gridCells"><?php echo $row_comlist['address']; ?></td>
                                    <td class="gridCells"><?php echo $row_comlist['telphone']; ?></td>
                                    <td class="gridCells"><?php echo $row_comlist['faxphone']; ?></td>
                                    <td class="gridCells"><a href="mailto:<?php echo $row_comlist['email']; ?>"><?php echo $row_comlist['email']; ?></a></td>
                                  
                                    </tr>
                                  <?php } while ($row_comlist = mysqli_fetch_assoc($comlist)); ?>
                                  <tr valign="top">
                                    <td colspan="7" class="mainbase"><table width="100%"  border="0" cellspacing="0" cellpadding="4">
                                      <tr align="center">
                                        <td width="50%">
                                          <table width="100%" border="0" align="center" cellpadding="3" cellspacing="0">
                                            <tr>
                                              <td width="23%" align="center"><?php if ($pageNum_comlist > 0) { // Show if not first page ?>
                                                <a href="<?php printf("%s?pageNum_comlist=%d%s", $currentPage, 0, $queryString_comlist); ?>">First</a>
                                                <?php } // Show if not first page ?>                                              </td>
                                              <td width="31%" align="center"><?php if ($pageNum_comlist > 0) { // Show if not first page ?>
                                                <a href="<?php printf("%s?pageNum_comlist=%d%s", $currentPage, max(0, $pageNum_comlist - 1), $queryString_comlist); ?>">Previous</a>
                                                <?php } // Show if not first page ?>                                              </td>
                                              <td width="23%" align="center"><?php if ($pageNum_comlist < $totalPages_comlist) { // Show if not last page ?>
                                                <a href="<?php printf("%s?pageNum_comlist=%d%s", $currentPage, min($totalPages_comlist, $pageNum_comlist + 1), $queryString_comlist); ?>">Next</a>
                                                <?php } // Show if not last page ?>                                              </td>
                                              <td width="23%" align="center"><?php if ($pageNum_comlist < $totalPages_comlist) { // Show if not last page ?>
                                                <a href="<?php printf("%s?pageNum_comlist=%d%s", $currentPage, $totalPages_comlist, $queryString_comlist); ?>">last</a>
                                                <?php } // Show if not last page ?>                                              </td>
                                            </tr>
                                          </table></td>
                                        <td width="50%">&nbsp; Records <strong><?php echo ($startRow_comlist + 1) ?></strong> to <strong><?php echo min($startRow_comlist + $maxRows_comlist, $totalRows_comlist) ?></strong> of <strong><?php echo $totalRows_comlist ?></strong> </td>
                                      </tr>
                                    </table></td>
                                    </tr>
                                  <?php } else { ?>
                                  <tr valign="top">
                                    <td colspan="7" class="err">NO COMPANIES CREATED!</td>
                                  </tr>
                                  <?php } ?>
                                </table>
                              </td>
                            </tr>
                        </table></td>
                    </tr>
                </table></td>
              </tr>
            </table></td>
            </tr>
        </table></td>
      </tr>
      <tr>
        <td ><?php include('../includes/footer.php'); ?></td>
      </tr>
    </table></td>
  </tr>
</table>
</body>
</html>
<?php
mysqli_free_result($comlist);
?>
