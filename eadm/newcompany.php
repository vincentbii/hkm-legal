<?php require_once('../connections/eProc.php'); 
include('../activelog.php');
// ----------------------------------------------------------------------
$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "createcompany")) {
  
	// Generate Necessary Variables
	// -----------------------------------------------------------------
	$com_id = createHASH($_POST['company']); // Hashed authcode
	// -----------------------------------------------------------------
	  
  $insertSQL = sprintf("INSERT INTO companyinfo (com_id, company, address, telphone, faxphone, mobphone, email, location, contactperson) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)",
                       GetSQLValueString($com_id, "text"),
                       GetSQLValueString($_POST['company'], "text"),
                       GetSQLValueString($_POST['address'], "text"),
                       GetSQLValueString($_POST['telphone'], "text"),
                       GetSQLValueString($_POST['faxphone'], "text"),
                       GetSQLValueString($_POST['mobphone'], "text"),
                       GetSQLValueString($_POST['email'], "text"),
                       GetSQLValueString($_POST['location'], "text"),
					   GetSQLValueString($_POST['contactperson'], "text"));

  mysqli_select_db($eProc, $database_eProc);
  $Result1 = mysqli_query($eProc, $insertSQL) or die("ERROR ADDING company INFORMATION - ".mysqli_error());
  
  // Generate The System company Folder - Used to store company specific data
  // ------------------------------------------------------------------------
  //$path = "../eadtmp/".$com_id;
 // mkdir($path, 700);
  // ------------------------------------------------------------------------

  $insertGoTo = "companylist.php";
  ?>
	  <script language="JavaScript" type="text/javascript">
			location='<?php echo $insertGoTo  ?>';
	</script>
	<?php
}
?>
<html>
<head>
<title>Management Information System </title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../styles/default.css" rel="stylesheet" type="text/css">
</head>
<body>
<table width="100%"  border="2" cellpadding="0" cellspacing="3" bordercolor="#0A6EC3" bgcolor="#FFFFFF">
  <tr>
    <td valign="top"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td valign="top"><?php include('../includes/header.php'); ?></td>
      </tr>
      <tr>
        <td valign="top"><img src="../images/template_06.gif" width="100%" height="6"></td>
      </tr>
      <tr>
        <td valign="top"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="100%" class="spacer5"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td><?php include("../includes/admstrip1.php"); ?></td>
              </tr>
              <tr>
                <td colspan="5" class="intspace">
				<table width="100%"  border="0" cellspacing="0" cellpadding="4">
                    <tr valign="top">
                      <td width="170"><?php include('acomenu.php'); ?></td> 
                      <td><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                          <tr valign="middle">
                            <td width="63%" class="hOne">New Company</td>
                            <td width="37%" align="right" nowrap class="baseline white">last Updated: </td> 
                          </tr>
                        </table>
                          <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td valign="top" class="baseline">Add a company using the form below.</td>
                            </tr>
                            <tr>
                              <td valign="top" class="spacer5">
                                <form action="<?php echo $editFormAction; ?>" method="POST" name="createcompany" id="createcompany">
                                  <fieldset>
                                  <legend>company Details</legend>
                                  <table width="350" border="0" cellspacing="0" cellpadding="5">
                                    <tr valign="baseline">
                                      <td align="right" nowrap>Company:</td>
                                      <td width="271"><input name="company" type="text" class="forms" size="50"></td>
                                    </tr>
                                    <tr valign="baseline">
                                      <td nowrap align="right" valign="top">Address:</td>
                                      <td><textarea name="address" cols="50" rows="4" class="forms"></textarea>                                      </td>
                                    </tr>
                                  </table>
                                  </fieldset>
                                  <br>
                                  <fieldset>
                                  <legend>Company Contacts</legend>
                                  <table width="350" border="0" cellspacing="0" cellpadding="5">
                                    <tr valign="baseline">
                                      <td nowrap align="right">Contact Person:</td>
                                      <td><input name="contactperson" type="text" class="forms" id="contactperson" size="50"></td>
                                    </tr>
                                    <tr valign="baseline">
                                      <td nowrap align="right">Telephone:</td>
                                      <td><input name="telphone" type="text" class="forms" size="32"></td>
                                    </tr>
                                    <tr valign="baseline">
                                      <td nowrap align="right">Fax:</td>
                                      <td><input name="faxphone" type="text" class="forms" size="32"></td>
                                    </tr>
                                    <tr valign="baseline">
                                      <td nowrap align="right">Mobile:</td>
                                      <td><input name="mobphone" type="text" class="forms" size="32"></td>
                                    </tr>
                                    <tr valign="baseline">
                                      <td nowrap align="right">Email:</td>
                                      <td><input name="email" type="text" class="forms" size="50"></td>
                                    </tr>
                                  </table>
                                  </fieldset>
                                  <br>
                                  <fieldset>
                                  <legend>Company Contacts</legend>
                                  <table width="350" cellpadding="5" cellspacing="0">
                                    <tr valign="baseline">
                                      <td width="57" align="right" valign="top" nowrap>Location:</td>
                                      <td width="271"><textarea name="location" cols="50" rows="4" class="forms"></textarea>
                                      </td>
                                    </tr>
                                    <tr valign="baseline">
                                      <td colspan="2" align="right" nowrap><eProc size="1"></td>
                                    </tr>
                                    <tr align="center" valign="baseline">
                                      <td colspan="2" nowrap><input name="MM_insert" type="hidden" id="MM_insert" value="createcompany">
                                          <input type="submit" class="formsBlue" value="Create New Company">
                                          <input type="button" class="formsorg" value="Cancel" onClick="javascript:location='companylist.php'"></td>
                                    </tr>
                                  </table>
                                  </fieldset>
                                </form></td>
                            </tr>
                        </table></td>
                    </tr>
                </table></td>
              </tr>
            </table></td>
            </tr>
        </table></td>
      </tr>
      <tr>
        <td ><?php include('../includes/footer.php'); ?></td>
      </tr>
    </table></td>
  </tr>
</table>
</body>
</html>