<?php require_once('../connections/eProc.php'); 
include('../activelog.php');
// ----------------------------------------------------------------------

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}
$offlinesupplier="0";

if (isset($_POST['offlinesupplier'])) {
	if ($_POST['offlinesupplier']==1) {
		$offlinesupplier="1";
	}
}


if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "updatecompany")) {
  $updateSQL = sprintf("UPDATE companyinfo SET company=%s, address=%s, telphone=%s, faxphone=%s, mobphone=%s, email=%s, location=%s WHERE com_id=%s",
                       GetSQLValueString($_POST['company'], "text"),
                       GetSQLValueString($_POST['address'], "text"),
                       GetSQLValueString($_POST['telphone'], "text"),
                       GetSQLValueString($_POST['faxphone'], "text"),
                       GetSQLValueString($_POST['mobphone'], "text"),
                       GetSQLValueString($_POST['email'], "text"),
                       GetSQLValueString($_POST['location'], "text"),
                       GetSQLValueString($_POST['com_id'], "text"));

  mysqli_select_db($eProc, $database_eProc);
  $Result1 = mysqli_query($eProc, $updateSQL) or die(mysqli_error());

  $updateGoTo = "companylist.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
    $updateGoTo .= $_SERVER['QUERY_STRING'];
  }
  ?>
	  <script language="JavaScript" type="text/javascript">
			location='<?php echo $insertGoTo  ?>';
	</script>
	<?php
}

$colname_ec = "1";
if (isset($_GET['id'])) {
  $colname_ec = (get_magic_quotes_gpc()) ? $_GET['id'] : addslashes($_GET['id']);
}
mysqli_select_db($eProc, $database_eProc);
$query_ec = sprintf("SELECT * FROM companyinfo WHERE com_id = '%s'", $colname_ec);
$ec = mysqli_query($eProc, $query_ec) or die(mysqli_error());
$row_ec = mysqli_fetch_assoc($ec);
$totalRows_ec = mysqli_num_rows($ec);
?>
<html>
<head>
<title>Management Information System </title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../styles/default.css" rel="stylesheet" type="text/css">
</head>
<body>
<table width="100%"  border="2" cellpadding="0" cellspacing="3" bordercolor="#0A6EC3" bgcolor="#FFFFFF">
  <tr>
    <td valign="top"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td valign="top"><?php include('../includes/header.php'); ?></td>
      </tr>
      <tr>
        <td valign="top"><img src="../images/template_06.gif" width="100%" height="6"></td>
      </tr>
      <tr>
        <td valign="top"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="100%" class="spacer5"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td><?php include("../includes/admstrip1.php"); ?></td>
              </tr>
              <tr>
                <td colspan="5" class="intspace">
				<table width="100%"  border="0" cellspacing="0" cellpadding="4">
                    <tr valign="top">
                      <td width="170"><?php include('acomenu.php'); ?></td> 
                      <td><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                          <tr valign="middle">
                            <td width="63%" class="hOne">Edit Company</td>
                            <td width="37%" align="right" nowrap class="baseline white">last Updated: </td> 
                          </tr>
                        </table>
                          <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td valign="top" class="baseline">Update a  company's information using the form below.</td>
                            </tr>
                            <tr>
                              <td valign="top" class="spacer5">
                                <form action="<?php echo $editFormAction; ?>" method="POST" name="updatecompany" id="updatecompany">
                                  <fieldset>
								  <legend>Company Details</legend>
                                  <table width="350" border="0" cellspacing="0" cellpadding="5">
                                    <tr valign="baseline">
                                      <td width="59" align="right" nowrap>Company:</td>
                                      <td width="271"><input name="company" type="text" class="forms" value="<?php echo $row_ec['company']; ?>" size="50"></td>
                                    </tr>
                                    <tr valign="baseline">
                                      <td nowrap align="right" valign="top">Address:</td>
                                      <td><textarea name="address" cols="50" rows="4" class="forms"><?php echo $row_ec['address']; ?></textarea>
                                      </td>
                                    </tr>
								<?php if ($row_ec['offlinesupplier'] == 1) { ?>
									<tr valign="baseline">
                                      <td nowrap align="right" valign="top">Offline Supplier:</td>
                                      <td>Yes</td>
                                    </tr>
								<?php } ?>
                                  </table>
                                  </fieldset>
                                  <br>
                                  <fieldset>
								  <legend>Company Contacts</legend>
								  <table width="350" border="0" cellspacing="0" cellpadding="5">
                                    <tr valign="baseline">
                                      <td nowrap align="right">Telephone:</td>
                                      <td><input name="telphone" type="text" class="forms" value="<?php echo $row_ec['telphone']; ?>" size="32"></td>
                                    </tr>
                                    <tr valign="baseline">
                                      <td nowrap align="right">Fax:</td>
                                      <td><input name="faxphone" type="text" class="forms" value="<?php echo $row_ec['faxphone']; ?>" size="32"></td>
                                    </tr>
                                    <tr valign="baseline">
                                      <td nowrap align="right">Mobile:</td>
                                      <td><input name="mobphone" type="text" class="forms" value="<?php echo $row_ec['mobphone']; ?>" size="32"></td>
                                    </tr>
                                    <tr valign="baseline">
                                      <td nowrap align="right">Email:</td>
                                      <td><input name="email" type="text" class="forms" value="<?php echo $row_ec['email']; ?>" size="50"></td>
                                    </tr>
                                  </table>
                                  </fieldset>
                                  <br>
                                  <fieldset>
								  <legend>Company Location </legend>
								  <table width="350" cellpadding="5" cellspacing="0">
                                    <tr valign="baseline">
                                      <td width="57" align="right" valign="top" nowrap>Location:</td>
                                      <td width="271"><textarea name="location" cols="50" rows="4" class="forms"><?php echo $row_ec['location']; ?></textarea>
                                      </td>
                                    </tr>
                                    <tr valign="baseline">
                                      <td colspan="2" align="right" nowrap><eProc size="1"></td>
                                      </tr>
                                    <tr align="center" valign="baseline">
                                      <td colspan="2" nowrap><input type="hidden" name="MM_update" value="updatecompany">
<input type="hidden" name="com_id" value="<?php echo $row_ec['com_id']; ?>">                                        <input type="submit" class="formsBlue" value="Update Company Information">
                                        <input type="button" class="formsorg" value="Cancel" onClick="javascript:location='companylist.php'"></td>
                                      </tr>
                                  </table>
								  </fieldset>
                                    </form>
                                </td>
                            </tr>
                        </table></td>
                    </tr>
                </table></td>
              </tr>
            </table></td>
            </tr>
        </table></td>
      </tr>
      <tr>
        <td ><?php include('../includes/footer.php'); ?></td>
      </tr>
    </table></td>
  </tr>
</table>
</body>
</html>
<?php
mysqli_free_result($ec);
?>
