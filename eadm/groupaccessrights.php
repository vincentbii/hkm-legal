<?php require_once('../connections/eProc.php'); ?>
<?php include('../activelog.php'); 

if (isset($_POST['Submit']) && $_POST['Submit']=="Submit") {
	if ($_POST['max']>0) {
		//Save data
		mysqli_select_db($eProc, $database_eProc);
		$sql="DELETE FROM menuaccess WHERE usergroupid = '".$_POST['group']."'";	
		$Result1 = mysqli_query($eProc,$sql) or die("ERROR: Group rights could not be reset - ".mysqli_error());
		
		for ($i = 0; $i<$_POST['max'];$i++) {
			if (isset($_POST[$i]) && $_POST[$i]!="") {
				$sql="INSERT INTO menuaccess (menuname, usergroupid) VALUES ('".$_POST[$i]."','".$_POST['group']."')";	
				$Result1 = mysqli_query($eProc, $sql) or die("ERROR: Group rights could not be updated - ".mysqli_error());			
			}
		}
	}
}
$group="";
if (isset($_POST['group'])) {
	$group=$_POST['group'];
} 

mysqli_select_db($eProc, $database_eProc);
$query_rsmenus = "SELECT menuitems.menuname, menuitems.menugroup, menuitems.category, (SELECT menuaccess.menuname AS allowed FROM menuaccess WHERE menuaccess.menuname = menuitems.menuname AND usergroupid = '".$group."') AS allowed,( SELECT menuaccess.usergroupid  FROM menuaccess WHERE menuaccess.menuname = menuitems.menuname AND usergroupid = '".$group."') AS  usergroupid FROM menuitems  ORDER BY menuitems.category, menuitems.menugroup, menuitems.menuname";

$rsmenus = mysqli_query($eProc, $query_rsmenus) or die(mysqli_error($eProc));
$row_rsmenus = mysqli_fetch_assoc($rsmenus);
$totalRows_rsmenus = mysqli_num_rows($rsmenus);
?>
<html>
<head>
<title>Management Information System </title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../styles/default.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.style1 {font-weight: bold}
.style2 {
	font-size: 12px;
	font-weight: bold;
}
.style3 {font-size: medium}
-->
</style>
</head>

<body>
<table width="100%"  border="2" cellpadding="0" cellspacing="3" bordercolor="#0A6EC3" bgcolor="#FFFFFF">
  <tr>
    <td valign="top"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td valign="top"><?php include('../includes/header.php'); ?></td>
      </tr>
      <tr>
        <td valign="top"><img src="../images/template_06.gif" width="100%" height="6"></td>
      </tr>
      <tr>
        <td valign="top"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="100%" class="spacer5"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
               <tr>
                <td><?php include("../includes/admstrip2.php"); ?></td>
              </tr>
              <tr>
                <td colspan="4" class="intspace"><table width="100%"  border="0" cellspacing="0" cellpadding="4">
                  <tr valign="top">
                    <td width="170"><?php include('amenu.php'); ?></td>
                    <td><table width="100%" height="100%"  border="0" cellpadding="0" cellspacing="0">
                        <tr>
                          <td height="19" class="hOne">Group Access Rights </td>
                        </tr>
                        <tr>
                          <td><form name="form1" method="post" action="">
                              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                	<td colspan="3"><span class="menuhdr">Select The Group:</span>                                    <select name="group" id="group" onChange="document.form1.submit()">
                                        <?php echo showGroups($_POST['group'], TRUE); ?>
                                      </select>                                  </td>
                                  <td width="4%">&nbsp;</td>
                                </tr>
                                <tr>
                                	<td colspan="3">Select the menu items accessible by users of this  group  </td>
                                  <td>&nbsp;</td>
                                </tr>
                                <?php 
								$i=0;
								$category="";
								do { 
									if (trim($group!=trim($row_rsmenus['menugroup']))) {
										$group=$row_rsmenus['menugroup'];
										
										if (trim($category!=trim($row_rsmenus['category']))) {
											$category=$row_rsmenus['category'];
										?>
											 					
																<tr>
											 						<td colspan="3" bgcolor="#66FF99"><span class="style2"><?php echo $row_rsmenus['category'] ?></span></td>
											 						<td>&nbsp;</td>
										 						</tr>
											<?php } ?>
																
											 					<tr>
											 	<td width="4%" >&nbsp;</td>
												<td colspan="2" bgcolor="#CCCCCC"><?php echo $row_rsmenus['menugroup'] ?></td>
												<td>&nbsp;</td>
											  </tr>
										<?php
									}								
								?>								
                                  <tr>
                                  	<?php 
									if (($row_rsmenus['allowed']==$row_rsmenus['menuname']) && ($_POST['group']==$row_rsmenus['usergroupid'])) {
										$allowed="checked=checked";
									} else {
										$allowed="";
									}
									?>
                                    <td>&nbsp;</td>
                                    <td width="3%">&nbsp;</td>
                                    <td width="89%">
                                    	<input type="checkbox" name="<?php echo $i ?>" id="<?php echo $i ?>" value="<?php echo $row_rsmenus['menuname'] ?>" <?php echo $allowed ?>>
																			<?php echo $row_rsmenus['menuname'] ?></td>
                                    <td>&nbsp;</td>
                                  </tr>
                                  <?php 
								  	$i = $i+1;  } 
								  while ($row_rsmenus = mysqli_fetch_assoc($rsmenus)); ?>
								  <tr>
								  	<td colspan="3"><p>&nbsp;
								  	  </p>
								  	  <p>
								  	    <input name="Submit" type="submit" class="formsBlue" value="Submit">									
								  	    <input name="Cancel" type="button" class="formsorg" id="Cancel" value="Cancel" onClick="location='ahome.php'">
								  	    <input name="max" type="hidden" id="max" value="<?php echo $i ?>">
								  	  </p></td>
								  </tr>
                              </table>
                          </form></td>
                        </tr>
                    </table></td>
                  </tr>
                </table></td>
              </tr>
            </table></td>
            </tr>
        </table></td>
      </tr>
      <tr>
        <td ><?php include('../includes/footer.php'); ?></td>
      </tr>
    </table></td>
  </tr>
</table>
</body>
</html>
<?php
mysqli_free_result($rsmenus);
?>