<?php
// DEFINE CONNECTIONS
// ---------------------------------------------------------------

// ---------------------------------------------------------------

include('../activelog.php');

function wrDepts() {
// --------------------------------------------------------------
// Show the donor based on Donor ID
// --------------------------------------------------------------
  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
// --------------------------------------------------------------
  $logSQL = "SELECT department FROM departments ORDER BY department ASC";
// --------------------------------------------------------------
  mysqli_select_db(DB, $CON);
  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve departments - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
// --------------------------------------------------------------
// Write to a js file
// --------------------------------------------------------------
  $file = 'tmp/usr.js';
  $fn = touch($file);
// --------------------------------------------------------------
  $fp = fopen($file, 'w');
// --------------------------------------------------------------
  fwrite($fp,'// departments'."\n"); 
  fwrite($fp,'var dp = new Array();'."\n"); $c = 0; 
  do {
		$str = 'dp['.$c.'] = "'.addslashes($rowuser['department']).'";';
		fwrite($fp,$str."\n");
  		$c++;
  } while($rowuser = mysqli_fetch_assoc($done));
// --------------------------------------------------------------
}

function wrChains() {
// --------------------------------------------------------------
// Show the donor based on Donor ID
// --------------------------------------------------------------
  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
// --------------------------------------------------------------
  $logSQL = "SELECT deptid, chainname FROM authchain ORDER BY chainname ASC";
// --------------------------------------------------------------
  mysqli_select_db(DB, $CON);
  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve department chains - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
// --------------------------------------------------------------
// Write to a js file
// --------------------------------------------------------------
  $file = 'tmp/usr.js';
  $fn = touch($file);
// --------------------------------------------------------------
  $fp = fopen($file, 'a');
// --------------------------------------------------------------
  fwrite($fp,'// AUTH CHAINS'."\n"); 
  fwrite($fp,'var ch = new Array();'."\n"); $c = 0; 
  do {
		$str = 'ch['.$c.'] = "'.addslashes($rowuser['deptid']).'|'.addslashes($rowuser['chainname']).'";';
		fwrite($fp,$str."\n");
  		$c++;
  } while($rowuser = mysqli_fetch_assoc($done));
// --------------------------------------------------------------
}

function wrUsers() {
// --------------------------------------------------------------
// Show the donor based on Donor ID
// --------------------------------------------------------------
  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
// --------------------------------------------------------------
  $logSQL = "SELECT fulname, departmentid FROM users ORDER BY fulname ASC";
// --------------------------------------------------------------
  mysqli_select_db(DB, $CON);
  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve users - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
// --------------------------------------------------------------
// Write to a js file
// --------------------------------------------------------------
  $file = 'tmp/usr.js';
  $fn = touch($file);
// --------------------------------------------------------------
  $fp = fopen($file, 'a');
// --------------------------------------------------------------
  fwrite($fp,'// users'."\n"); 
  fwrite($fp,'var us = new Array();'."\n"); $c = 0; 
  do {
		$str = 'us['.$c.'] = "'.addslashes($rowuser['departmentid']).'|'.addslashes($rowuser['fulname']).'";';
		fwrite($fp,$str."\n");
  		$c++;
  } while($rowuser = mysqli_fetch_assoc($done));
// --------------------------------------------------------------
}

function wrRanks() {
// --------------------------------------------------------------
// Show the donor based on Donor ID
// --------------------------------------------------------------
  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
// --------------------------------------------------------------
  $logSQL = "SELECT costcode, title FROM titletag ORDER BY title ASC";
// --------------------------------------------------------------
  mysqli_select_db(DB, $CON);
  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve titles - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
// --------------------------------------------------------------
// Write to a js file
// --------------------------------------------------------------
  $file = 'tmp/usr.js';
  $fn = touch($file);
// --------------------------------------------------------------
  $fp = fopen($file, 'a');
// --------------------------------------------------------------
  fwrite($fp,'// JOB TITLES'."\n"); 
  fwrite($fp,'var rk = new Array();'."\n"); $c = 0; 
  do {
		$str = 'rk['.$c.'] = "'.addslashes($rowuser['costcode']).'|'.addslashes($rowuser['title']).'";';
		fwrite($fp,$str."\n");
  		$c++;
  } while($rowuser = mysqli_fetch_assoc($done));
// --------------------------------------------------------------
}

function CreateChainsFile() {
	// ---------------------------------
  	$file = 'tmp/usr.js';
	if (file_exists($file)) {
		unlink($file);
	}
	// ---------------------------------
	wrDepts();
	wrChains();
	wrUsers();
	wrRanks();
	// ---------------------------------
}

CreateChainsFile();
// ---------------------------------------------------------------
// End Create 
// ---------------------------------------------------------------
?>

