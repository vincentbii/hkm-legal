<?php require_once('../connections/eProc.php'); 
include('../activelog.php');
// -------------------------------------
$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form1")) {
  
// Generate Necessary Variables
// -----------------------------------------------------------------
$MD5 = md5($_POST['umd5']); // md5 encrypted password
$authcode = createHASH($_POST['fulname']); // Hashed authcode
// -----------------------------------------------------------------
  
  $insertSQL = sprintf("INSERT INTO adminlogin (fulname, username, umd5, authcode) VALUES (%s, %s, %s, %s)",
                       GetSQLValueString($_POST['fulname'], "text"),
                       GetSQLValueString($_POST['username'], "text"),
                       GetSQLValueString($MD5, "text"),
                       GetSQLValueString($authcode, "text"));

  mysqli_select_db($eProc, $database_eProc);
  $Result1 = mysqli_query($eProc, $insertSQL) or die("ERROR CREATING admin USER - ".mysqli_error($eProc));

  $insertGoTo = "ahome.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
    $insertGoTo .= $_SERVER['QUERY_STRING'];
  }
  ?>
	  <script language="JavaScript" type="text/javascript">
			location='<?php echo $insertGoTo  ?>';
	</script>
	<?php
}
?>
<html>
<head>
<title>Management Information System </title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../styles/default.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.style1 {font-weight: bold}
-->
</style>
</head>
<body>
<table width="100%"  border="2" cellpadding="0" cellspacing="3" bordercolor="#0A6EC3" bgcolor="#FFFFFF">
  <tr>
    <td valign="top"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td valign="top"><?php include('../includes/header.php'); ?></td>
      </tr>
      <tr>
        <td valign="top"><img src="../images/template_06.gif" width="100%" height="6"></td>
      </tr>
      <tr>
        <td valign="top"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="100%" class="spacer5"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
              <tr>
               <td><?php include("../includes/admstrip2.php"); ?></td>
              </tr>
              <tr>
                <td colspan="5" class="intspace">
				<table width="100%"  border="0" cellspacing="0" cellpadding="4">
                    <tr valign="top">
                      <td width="170"><?php include('amenu.php'); ?></td>
                      <td><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                          <tr valign="middle">
                            <td width="63%" class="hOne">Add Admin User </td>
                            <td width="37%" align="right" nowrap class="baseline white">last Updated: </td> 
                          </tr>
                        </table>
                          <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td valign="top" class="baseline">Fill in the form below to add a System Administrator to the system.</td>
                            </tr>
                            <tr>
                              <td valign="top" class="spacer5">
                                <form action="<?php echo $editFormAction; ?>" method="post" name="adduser" id="adduser">
                                  <table cellpadding="5" cellspacing="0">
                                    <tr valign="baseline">
                                      <td nowrap align="right"><strong>Full Name:</strong></td>
                                      <td><input name="fulname" type="text" class="forms" value="" size="32"></td>
                                    </tr>
                                    <tr valign="baseline">
                                      <td colspan="2" align="right" nowrap><eProc size="1" class="style1"></td>
                                    </tr>
                                    <tr valign="baseline">
                                      <td nowrap align="right"><strong>User Name:</strong></td>
                                      <td><input name="username" type="text" class="forms" value="" size="32"></td>
                                    </tr>
                                    <tr valign="baseline">
                                      <td nowrap align="right"><strong>Password:</strong></td>
                                      <td><input name="umd5" type="password" class="forms" value="" size="32"></td>
                                    </tr>
                                    <tr valign="baseline">
                                      <td nowrap align="right"><strong>Confirm Password:</strong></td>
                                      <td><input name="umd5" type="password" class="forms" value="" size="32"></td>
                                    </tr>
                                    <tr valign="baseline">
                                      <td colspan="2" align="right" nowrap><eProc size="1"></td>
                                      </tr>
                                    <tr valign="baseline">
                                      <td nowrap align="right">                                        <input type="hidden" name="MM_insert" value="form1"></td>
                                      <td><input type="submit" class="formsBlue" value="Add User">
                                        <input type="button" class="formsorg" value="Cancel" onClick="javascript:location='ahome.php'"></td>
                                    </tr>
                                  </table>
                                    </form>
                                </td>
                            </tr>
                        </table></td>
                    </tr>
                </table></td>
              </tr>
            </table></td>
            </tr>
        </table></td>
      </tr>
      <tr>
        <td ><?php include('../includes/footer.php'); ?></td>
      </tr>
    </table></td>
  </tr>
</table>
</body>
</html>