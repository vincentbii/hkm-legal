<?php require_once('../connections/eProc.php'); 
require_once('../classes-security.php');
?>
<?php

if (isset($_POST['Submit']) && $_POST['Submit']=="Submit") {
	if ($_POST['max']>0) {
		//Save data	
			
		$arr[] = array($_POST['max']);
		$added = 0;
		for ($i = 0; $i<$_POST['max'];$i++) {
			if (isset($_POST[$i]) && $_POST[$i]!="") {
				$groupAccessRights = new _GroupAccessRights;
				$groupAccessRights->SetAccessRightID($_POST[$i]);
				$groupAccessRights->SetGroupID($_POST['group']);
				$arr[$added] = $groupAccessRights;	
				$added++;		
			}
		}
		$groupAccessRights->SaveGroupAccessRights($_POST['group'], $arr);
	}
}
$group="";
if (isset($_POST['group'])) {
	$group=$_POST['group'];
} 
$groupAccessRights = new _GroupAccessRights;
$accessRight = new _AccessRight;
$arrRights =  array();
//echo $group;
$arrRights = $groupAccessRights->GroupAccessRights($group);
$accessCategory="";
?>
include('../activelog.php');
?>
<html>
<head>
<title>Management Information System </title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../styles/default.css" rel="stylesheet" type="text/css">
</head>

<body>
<table width="100%"  border="2" cellpadding="0" cellspacing="3" bordercolor="#0A6EC3" bgcolor="#FFFFFF">
  <tr>
    <td valign="top"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td valign="top"><?php include('../includes/header.php'); ?></td>
      </tr>
      <tr>
        <td valign="top"><img src="../images/template_06.gif" width="100%" height="6"></td>
      </tr>
      <tr>
        <td valign="top"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="100%" class="spacer5"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
               <tr>
                <td><?php include("../includes/admstrip2.php"); ?></td>
              </tr>
              <tr>
                <td colspan="4" class="intspace"><table width="100%"  border="0" cellspacing="0" cellpadding="4">
                  <tr valign="top">
                    <td width="170"><?php include('amenu.php'); ?></td>
                    <td><table width="100%" height="100%"  border="0" cellpadding="0" cellspacing="0">
                        <tr>
                          <td height="19" class="hOne">Group Access Rights </td>
                        </tr>
                        <tr>
                          <td><form name="form1" method="post" action="">
                              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td colspan="4"><span class="menuhdr">Select The Group:</span>
                                  	<select name="group" id="group" onChange="document.form1.submit()">
										<?php echo showGroups($_POST['group'], TRUE); ?>
									</select>
                                  </td>
                                </tr>
                                <tr>
                                  <td colspan="4">Select the access rights accessible by users of this  group  </td>
                                </tr>
                                <?php 
																
								$i=0;
								foreach ($arrRights as $accessRight) {
									if ($accessCategory != $accessRight->AccessCategory()) {
										$accessCategory=$accessRight->AccessCategory(); 
										?>
									 							
									 							<tr>
																						
										<td colspan="3" bgcolor="#CCCCCC"><?php echo $accessRight->AccessCategory() ?></td>
										</tr>
										<?php
									}	
									
									if ($accessRight->GroupAllowed()==true) {
										$allowed="checked=checked";
									} else {
										$allowed="";
									}							
								?>								
								<tr>

									
									
									<td width="4%">&nbsp;</td>
									<td width="36%">
										<input type="checkbox" name="<?php echo $i ?>" id="<?php echo $i ?>"  value="<?php echo $accessRight->accessRightID() ?>" <?php echo $allowed ?>>
										<?php echo $accessRight->AccessName() ?></td>
									<td width="56%"><?php echo $accessRight->AccessDescription() ?></td>
								</tr>
								<?php 
									$i = $i+1;
								} ?>
								  <tr>
								  	<td colspan="4"><p>&nbsp;
								  	  </p>
								  	  <p>
								  	    <input name="Submit" type="submit" class="formsBlue" value="Submit">									
								  	    <input name="Cancel" type="button" class="formsorg" id="Cancel" value="Cancel" onClick="location='ahome.php'">
								  	    <input name="max" type="hidden" id="max" value="<?php echo $i ?>">
								  	  </p></td>
								  </tr>
                              </table>
                          </form></td>
                        </tr>
                    </table></td>
                  </tr>
                </table></td>
              </tr>
            </table></td>
            </tr>
        </table></td>
      </tr>
      <tr>
        <td ><?php include('../includes/footer.php'); ?></td>
      </tr>
    </table></td>
  </tr>
</table>
</body>
</html>
