<?php require_once('../connections/eProc.php'); ?>
<?php
include ('../activelog.php');
// DELETE A company's INFORMATION
// -------------------------------
if (isset($_GET['d']) && $_GET['d'] == 1) {

	  // Retrieve Variables
	  // --------------------------------------------------------------
	  $com_id = $_GET['id'];
	  $delSQL = "DELETE FROM companyinfo WHERE com_id = '".$com_id."'";
	  // --------------------------------------------------------------
	  
	  // Remember to delete all transactional information for this company

	  // Process Deletion
	  // --------------------------------------------------------------
	  mysqli_select_db($eProc, $database_eProc);
	  $Result1 = mysqli_query($eProc, $delSQL) or die("COULD NOT DELETE company INFORMATION - ".mysqli_error());
	  // --------------------------------------------------------------

	  // Redirect Back
	  // --------------------------------------------------------------
	  $insertGoTo = "companylist.php?d";
	  ?>
	  <script language="JavaScript" type="text/javascript">
			location='<?php echo $insertGoTo  ?>';
	</script>
	<?php
	  // --------------------------------------------------------------
	  
} else {
	  // Nothing Chosen --> Return With Nothing
	  // --------------------------------------------
	  $insertGoTo = "companylist.php?d";
	  ?>
	  <script language="JavaScript" type="text/javascript">
			location='<?php echo $insertGoTo  ?>';
	</script>
	<?php
	  // --------------------------------------------
}
// -------------------------------
?>
