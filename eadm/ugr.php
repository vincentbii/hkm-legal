<?php require_once('../connections/eProc.php'); ?>

<?php //include('../activelog.php'); ?>
<?php
$currentPage = $_SERVER["PHP_SELF"];

$maxRows_ugr = 999999;
$pageNum_ugr = 0;
if (isset($_GET['pageNum_ugr'])) {
  $pageNum_ugr = $_GET['pageNum_ugr'];
}
$startRow_ugr = $pageNum_ugr * $maxRows_ugr;

mysqli_select_db($eProc, $database_eProc);
$query_ugr = "SELECT * FROM groups WHERE sys != 1 OR sys != 0 ORDER BY groupname ASC";
$query_limit_ugr = sprintf("%s LIMIT %d, %d", $query_ugr, $startRow_ugr, $maxRows_ugr);
$ugr = mysqli_query($eProc, $query_limit_ugr) or die(mysqli_error($eProc));
$row_ugr = mysqli_fetch_assoc($ugr);

if (isset($_GET['totalRows_ugr'])) {
  $totalRows_ugr = $_GET['totalRows_ugr'];
} else {
  $all_ugr = mysqli_query($eProc,$query_ugr);
  $totalRows_ugr = mysqli_num_rows($all_ugr);
}
$totalPages_ugr = ceil($totalRows_ugr/$maxRows_ugr)-1;

$queryString_ugr = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_ugr") == false && 
        stristr($param, "totalRows_ugr") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_ugr = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_ugr = sprintf("&totalRows_ugr=%d%s", $totalRows_ugr, $queryString_ugr);
?>
<style type="text/css">
<!--
.style1 {
	color: #FF0000;
	font-weight: bold;
}
-->
</style>
<table width="100%" border=" 0" cellpadding="3" cellspacing="0" >
  <tr>
    <td >&nbsp;</td>
	<td >&nbsp;</td>
    <td ><div align="right">
      <table width="123"  border="0" cellspacing="0" cellpadding="3">
          <tr>
            <td width="15"><a href="../newcostcenter.php"><img src="../images/icons/newproject.gif" width="15" height="13" border="0" /></a></td>
            <td width="96" nowrap="nowrap"><a href="newgroup.php">New Group </a></td>
          </tr>
          </table>
    </div></td>
    
  </tr>
  <tr>
    <td width="28%" class="inputdef"><strong>Group Name</strong></td>
    <td width="37%" class="inputdef"><strong>Description</strong></td>
    <td width="35%" class="inputdef">Group type </td>
  </tr>
  <?php if ($totalRows_ugr > 0) { ?>
  <?php do { 
  if ($row_ugr['groupid']=='1') { 
  ?>
  <tr>
    <td class="gridCells"><strong><?php echo $row_ugr['groupname']; ?></strong></td>
    <td class="gridCells"><?php echo $row_ugr['groupdesc']; ?></td>
    <td class="gridCells"><?php echo getgrouptype($row_ugr['grouptypeid']); ?></td>
  </tr>
  <?php } else { ?> 
	 <tr>
    <td class="gridCells"><strong><a href="newgroup.php?token=<?php echo $row_ugr['groupid'] ?>"><?php echo $row_ugr['groupname']; ?></a></strong></td>
    <td class="gridCells"><?php echo $row_ugr['groupdesc']; ?></td>
    <td class="gridCells"><?php echo getgrouptype($row_ugr['grouptypeid']); ?></td>
  </tr>  
 <?php  } } while ($row_ugr = mysqli_fetch_assoc($ugr)); ?>
  <tr>
    <td colspan="3" class="mainbase"><table width="100%"  border="0" cellspacing="0" cellpadding="3">
      <tr align="center">
        <td width="50%">
          <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
              <td width="23%" align="center"><?php if ($pageNum_ugr > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_ugr=%d%s", $currentPage, 0, $queryString_ugr); ?>">First</a>
                <?php } // Show if not first page ?>              </td>
              <td width="31%" align="center"><?php if ($pageNum_ugr > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_ugr=%d%s", $currentPage, max(0, $pageNum_ugr - 1), $queryString_ugr); ?>">Previous</a>
                <?php } // Show if not first page ?>              </td>
              <td width="23%" align="center"><?php if ($pageNum_ugr < $totalPages_ugr) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_ugr=%d%s", $currentPage, min($totalPages_ugr, $pageNum_ugr + 1), $queryString_ugr); ?>">Next</a>
                <?php } // Show if not last page ?>              </td>
              <td width="23%" align="center"><?php if ($pageNum_ugr < $totalPages_ugr) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_ugr=%d%s", $currentPage, $totalPages_ugr, $queryString_ugr); ?>">last</a>
                <?php } // Show if not last page ?>              </td>
            </tr>
          </table></td>
        <td width="50%">&nbsp; Showing <strong><?php echo ($startRow_ugr + 1) ?></strong> to <strong><?php echo min($startRow_ugr + $maxRows_ugr, $totalRows_ugr) ?></strong> of <strong><?php echo $totalRows_ugr ?></strong> </td>
      </tr>
    </table></td>
  </tr>
  <?php } else { ?>
  <tr>
    <td colspan="3" class="mainbase"><span class="style1">No Groups Created! </span></td>
  </tr>
  <?php } ?>
</table>
<?php
mysqli_free_result($ugr);
?>
