<?php require_once('../connections/eProc.php'); ?>
<?php include('../activelog.php'); ?>
<html>
<head>
<title>Management Information System </title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../styles/default.css" rel="stylesheet" type="text/css">
</head>
<script>
<!--
function shiftFrameURL(newpage) {
	var fr = document.getElementById('costcenter');
	fr.src = newpage;
}
-->
</script>
<body>
<table width="100%"  border="2" cellpadding="0" cellspacing="3" bordercolor="#0A6EC3" bgcolor="#FFFFFF">
  <tr>
    <td valign="top"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td valign="top"><?php include('../includes/header.php'); ?></td>
      </tr>
      <tr>
        <td valign="top"><img src="../images/template_06.gif" width="100%" height="6"></td>
      </tr>
      <tr>
        <td valign="top"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="100%" class="spacer5"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
               <tr>
                <td><?php include("../includes/admstrip2.php"); ?></td>
              </tr>
              <tr>
                <td colspan="4" class="intspace"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="100%"><table width="100%"  border="0" cellspacing="0" cellpadding="4">
                      <tr valign="top">
                        <td width="170"><?php include('amenu.php'); ?></td>
                        <td><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td width="85%" height="19" class="hOne">User Groups</td>
                            <td width="15%" align="right" nowrap class="baselinens">&nbsp;</td>
                          </tr>
                          <tr>
                            <td colspan="2"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td valign="top" class="baseline">Use this screen to enable specific user groups for this system </td>
                              </tr>
                              <tr>
                                <td valign="top">
								<?php include("ugr.php"); ?>							
								</td>
                              </tr>
                            </table></td>
                          </tr>
                        </table></td>
                      </tr>
                    </table></td>
                    </tr>
                </table></td>
              </tr>
            </table></td>
            </tr>
        </table></td>
      </tr>
      <tr>
        <td ><?php include('../includes/footer.php'); ?></td>
      </tr>
    </table></td>
  </tr>
</table>
</body>
</html>