<SCRIPT TYPE="text/javascript">
<!--
function popupform(myform, windowname) {
	if (! window.focus)return true;
	window.open('', windowname, 'width=600,left=200,top=200');
	myform.target=windowname;
	return true;
}

function OpenPopup(myform, windowname, height, windth) {
	if (! window.focus)return true;
	window.open('', windowname, 'height=height,width=width,left=400,top=200');
	myform.target=windowname;
	return true;
}
//-->

/**
 * DHTML date validation script. Courtesy of SmartWebby.com (http://www.smartwebby.com/dhtml/)
 */
// Declaring valid date character, minimum year and maximum year
var dtCh= "/";
var minYear=1900;
var maxYear=2100;

function isInteger(s){
	var i;
    for (i = 0; i < s.length; i++){
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    // All characters are numbers.
    return true;
}

function stripCharsInBag(s, bag){
	var i;
    var returnString = "";
    // Search teProcough string's characters one by one.
    // If character is not in bag, append to returnString.
    for (i = 0; i < s.length; i++){
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
}

function daysInFebruary (year){
	// February has 29 days in any year evenly divisible by four,
    // EXCEPT for centurial years which are not also divisible by 400.
    return (((year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28 );
}
function DaysArray(n) {
	for (var i = 1; i <= n; i++) {
		this[i] = 31
		if (i==4 || i==6 || i==9 || i==11) {this[i] = 30}
		if (i==2) {this[i] = 29}
   }
   return this
}

function isDate(dtStr){
	var daysInMonth = DaysArray(12)
	var pos1=dtStr.indexOf(dtCh)
	var pos2=dtStr.indexOf(dtCh,pos1+1)
	var strMonth=dtStr.substring(0,pos1)
	var strDay=dtStr.substring(pos1+1,pos2)
	var strYear=dtStr.substring(pos2+1)
	strYr=strYear
	if (strDay.charAt(0)=="0" && strDay.length>1) strDay=strDay.substring(1)
	if (strMonth.charAt(0)=="0" && strMonth.length>1) strMonth=strMonth.substring(1)
	for (var i = 1; i <= 3; i++) {
		if (strYr.charAt(0)=="0" && strYr.length>1) strYr=strYr.substring(1)
	}
	month=parseInt(strMonth)
	day=parseInt(strDay)
	year=parseInt(strYr)
	if (pos1==-1 || pos2==-1){
		alert("The date format should be : mm/dd/yyyy")
		return false
	}
	if (strMonth.length<1 || month<1 || month>12){
		alert("Please enter a valid month")
		return false
	}
	if (strDay.length<1 || day<1 || day>31 || (month==2 && day>daysInFebruary(year)) || day > daysInMonth[month]){
		alert("Please enter a valid day")
		return false
	}
	if (strYear.length != 4 || year==0 || year<minYear || year>maxYear){
		alert("Please enter a valid 4 digit year between "+minYear+" and "+maxYear)
		return false
	}
	if (dtStr.indexOf(dtCh,pos2+1)!=-1 || isInteger(stripCharsInBag(dtStr, dtCh))==false){
		alert("Please enter a valid date")
		return false
	}
return true
}

function ValidateForm(){
	var dt=document.frmSample.txtDate
	if (isDate(dt.value)==false){
		dt.focus()
		return false
	}
    return true
 }

function round_decimals(original_number, decimals) {
    var result1 = original_number * Math.pow(10, decimals)
    var result2 = Math.round(result1)
    var result3 = result2 / Math.pow(10, decimals)
    return pad_with_zeros(result3, decimals)
}

function pad_with_zeros(rounded_value, decimal_places) {

    // Convert the number to a string
    var value_string = rounded_value.toString()

    // Locate the decimal point
    var decimal_location = value_string.indexOf(".")

    // Is there a decimal point?
    if (decimal_location == -1) {

        // If no, then all decimal places will be padded with 0s
        decimal_part_length = 0

        // If decimal_places is greater than zero, tack on a decimal point
        value_string += decimal_places > 0 ? "." : ""
    }
    else {

        // If yes, then only the extra decimal places will be padded with 0s
        decimal_part_length = value_string.length - decimal_location - 1
    }

    // Calculate the number of decimal places that need to be padded with 0s
    var pad_total = decimal_places - decimal_part_length

    if (pad_total > 0) {

        // Pad the string with 0s
        for (var counter = 1; counter <= pad_total; counter++)
            value_string += "0"
        }
    return value_string
}

</SCRIPT>
<?php
// Report all errors except E_NOTICE
error_reporting(E_ALL ^ E_NOTICE);

// DATABASE CONNECTION
// -----------------------
define("HOSTNAME","localhost","true");
define("DB","doc","true");
define("username","root","true");
define("PWD","","true");
//define("PWD","finlay001","true");

// ------------------------------------------------------------------------------
// AUXILLIARY FUNCTIONS
// ------------------------------------------------------------------------------
// DATE FUNCTION
$dt = getdate();
$date = sprintf("%s-%s-%s", $dt['year'], $dt['mon'], $dt['mday']);
$long_date = sprintf("%s %s, %s", $dt['month'], $dt['mday'], $dt['year']);
$time = sprintf("%s:%s:%s", $dt['hours'], $dt['minutes'], $dt['seconds']);
// ------------------------------------------------------------------------------
// MONTH ARRAY
// ------------------------------------------------------------------------------
		$MONTH = array(1=>'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
		$DAY = array(1=>'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');
// ------------------------------------------------------------------------------
//Includes for functions not on this file but required
//--------------------------------------------------------------------
function log_activity($uid, $action, $section, $act) {
	// --------------------------------------------------------------
	// FUNCTION FOR LOGGING USER ACTIVITY WITHIN THE DONOR DATABASE
	// --------------------------------------------------------------
	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	// --------------------------------------------------------------
	  $dt = getdate(); $ts = time();
	  $date = sprintf("%s-%s-%s", $dt['year'], $dt['mon'], $dt['mday']);
	  $time = sprintf("%s:%s:%s", $dt['hours'], $dt['minutes'], $dt['seconds']);
	  $logSQL = "INSERT INTO activelog (uid, action, adate, atime, section, ACT, TSTAMP) VALUES ('". $uid ."','". $action ."','". $date ."','". $time ."','". $section ."','". $act ."','". $ts ."')";
	// --------------------------------------------------------------
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("LOG ERROR: Trying to save trackable user activity - ".mysqli_error());
	// --------------------------------------------------------------
		return $done;
	}

function evalNull($value, $nullmsg, $msg) {
	if (is_null($value) || $value == "") {
		$message = $nullmsg;
	} else {
		$message = $msg;
	}
	return $message;
}

// This function writes the Javascript url onto the page
// Important when you need PHP generated urls to use!
function addJavascript($url) {
	echo '<script language="javascript" src="'.$url.'"></script>';
}

function assessVAL($val) {
	if ($val == 1) {
		$ret = "YES";
	} else {
		$ret = "NO";
	}
	return $ret;
}

function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
	{
	  $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

	  switch ($theType) {
		case "text":
		  $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
		  break;
		case "long":
		case "int":
		  $theValue = ($theValue != "") ? intval($theValue) : "NULL";
		  break;
		case "double":
		  $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
		  break;
		case "date":
		  $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
		  break;
		case "defined":
		  $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
		  break;
	  }
	  return $theValue;
}

function make_time($day, $mon, $year, $time) {
	// Break time to hours and minutes
	// ------------------------------------
	list($eProc, $min) = explode(':', $time);
	// ------------------------------------
	$timestamp = mktime($eProc, $min, 0 ,$mon, $day, $year);

	return $timestamp;
}
// --------------------------------------------------------------------------------
// Function to process timestamps and return date
// --------------------------------------------------------------------------------
function process_time($ts, $form) {

	if ($ts == '') { $ts = time(); }

	$pdt = getdate($ts);
	$pdate = sprintf("%s-%s-%s", $pdt['year'], $pdt['mon'], $pdt['mday']);
	$pshort_date = sprintf("%s-%s-%s", $pdt['year'], $pdt['month'], $pdt['mday']);
	$pshorter_date = sprintf("%s-%s-%s", $pdt['year'], substr($pdt['month'], 0, 3), $pdt['mday']);
	$plong_date = sprintf("%s %s, %s", $pdt['month'], $pdt['mday'], $pdt['year']);
	$ptime = sprintf("%s:%s", $pdt['hours'], $pdt['minutes']);
	$pdate_time = $pdate."&nbsp;&nbsp; ".$ptime;
	$pdate_time =  date("Y-m-d H:i:s");

	switch ($form) {
		case '1':
		$FORMATTED_DATE = $pdate;
		break;

		case '2':
		$FORMATTED_DATE = $pshort_date;
		break;

		case '3':
		$FORMATTED_DATE = $plong_date;
		break;

		case '4':
		$FORMATTED_DATE = $pdate_time;
		break;

		case '5':
		$FORMATTED_DATE = $pshorter_date;
		break;

		default:
		$FORMATTED_DATE = $pdate;
		break;
	}

	return $FORMATTED_DATE;
}

function recordexists($tablename,$columnname,$value) {
	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
  
	 	 $logSQL = "SELECT ".$columnname." FROM ".$tablename." WHERE ".$columnname." = '".$value."'";
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to check record exists - ".mysqli_error(). $logSQL);
	  //$rowuser = mysqli_fetch_assoc($done);
	  $num_rows = mysqli_num_rows($done); //Get number of rows
	if ($num_rows > 0) {
	   return 'yes';
	} else {
	   return 'no';
	}
}



function getEndValue($rowuser) {
	end($rowuser);
	$value = current($rowuser);
	return $value;
}

function getStartValue($rowuser) {
	reset($rowuser);
	$value = current($rowuser);
	return $value;
}



function remFunnyChars($DIRTY) {
	$CLEAN = str_replace('/','',$DIRTY);
	$CLEAN = str_replace('\\','',$CLEAN);
	$CLEAN = str_replace(' ',',',$CLEAN);
	return $CLEAN;
}

// -------------------------------------------------------------------------
// Create name hash for unique identification of event
// -------------------------------------------------------------------------
function createHASH($name = '') {

	// RANDOM ARRAY FEED
	//-----------------------------------------------------------------
	$alpha = array('a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z');
	$numer = array('0','1','2','3','4','5','6','7','8','9');
	// ---------------------------------------------------------------------------------------------------------------------

	$word = explode(" ",$name);
	$new_sen = "";

	// Happens only when name is not sent

	if ($name == '') {
	// Create Random String
	for ($m = 1; $m <= 10; $m++) {
		$id = mt_rand(1,count($alpha));
		$id2 = mt_rand(1,count($numer));
		// Append to string
		
		$new_sen .= $alpha[$id].$numer[$id2];
			}
	} else {

	$rand = rand(12345, 99999);
		for ($i = 1; $i <= count($word); $i++) {
			if ($i == 1) {
				$new_sen .= $rand;
				$new_sen .= $word[$i];
			} else {
				$new_sen .= $word[$i];
			}
		}
	}
	return strtoupper($new_sen);
	}

function rem_space($name) {
	$word = explode(" ",$name);
	$new_sen = substr_replace($name,"",0,1);
	$new_sen = substr_replace($name,"",strlen($name),-1);
	
	return strtoupper($new_sen);
}

function listShow($divs, $table, $main, $id, $url = '') {

	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT ".$main.",".$id." FROM ".$table;
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve data - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $tot_rows = mysqli_num_rows($done);
	  $dist = floor($tot_rows / $divs);
	  $d_mod = $tot_rows % $divs;
	  $div = $divs;
	  $c = array();
	  // for ($x = 1; $x <= $div; $x++) { $c[$x] = $dist; }
	  for ($x = 1; $x <= $div; $x++) { $c[$x] = $dist; }
	  $mt[1] = 0; $mt[2] = $dist; $mt[3] = $dist * 2; $mt[4] = $dist * 3;
	  if ($d_mod > 0) { $c[1] = $c[1] + $d_mod; $c[2] = $c[2] + $d_mod; }// $div = $divs + 1;
	
		$l = 0;
			do {
				$l++;
				$CAT[$l] = $rowuser[$main];
				$IDS[$l] = $rowuser[$id];
			} while ($rowuser = mysqli_fetch_assoc($done));
			//print_r(array_values($CAT));

		echo "<table width=\"100%\"  border=\"0\" align=\"left\" cellpadding=\"3\" cellspacing=\"0\">
				<tr valign=\"top\">"; $nct = 0;
	
				for ($m = 1; $m <= $div; $m++) {
					echo "<td><table width=\"100%\"  border=\"0\" cellspacing=\"0\" cellpadding=\"3\">";
				
						for ($z = 1; $z <= $c[$m]; $z++) {
							$a = $z + $mt[$m];
							echo "<tr>
								  <td width=\"5%\"><img src=\"bullet.gif\"></td>";
								  if ($url != '') {
									echo "<td width=\"95%\"><a href=\"".$url."?id=".$IDS[$a]."\">".$CAT[$a]."</a></td>";
								  } else {
									echo "<td width=\"95%\">".$CAT[$a]."</td>";
								  }
							echo "</tr>";
						}
					echo "</table></td>";
				}

		echo "</tr>
			 </table>";
	}

function arrayListShow($divs, $commalist) {

	$array = explode(',',$commalist);
	
	$tot_rows = count($array);
	$dist = floor($tot_rows / $divs);
	$d_mod = $tot_rows % $divs;
	$div = $divs;
	$c = array();
	// for ($x = 1; $x <= $div; $x++) { $c[$x] = $dist; }
	for ($x = 1; $x <= $div; $x++) { $c[$x] = $dist; }
	$mt[1] = 0; $mt[2] = $dist; $mt[3] = $dist * 2; $mt[4] = $dist * 3;
	if ($d_mod > 0) { $c[1] = $c[1] + $d_mod; $c[2] = $c[2] + $d_mod; }// $div = $divs + 1;

	echo "<table width=\"100%\"  border=\"0\" align=\"left\" cellpadding=\"3\" cellspacing=\"0\">
			<tr valign=\"top\">"; $nct = 0;
	
			for ($m = 1; $m <= $div; $m++) {
				echo "<td><table width=\"100%\"  border=\"0\" cellspacing=\"0\" cellpadding=\"3\">";
			
					for ($z = 1; $z <= $c[$m]; $z++) {
						$a = $z + $mt[$m];
						if (empty($array[$a])) {
							// do nothing
						} else {
						echo "<tr>
							  <td width=\"5%\"><img src=\"bullet.gif\"></td>";
								echo "<td width=\"95%\">".$array[$a]."</td>";
						echo "</tr>";
						}
					}
				echo "</table></td>";
			}

	echo "</tr>
		 </table>";
	}


function showCompanies($showadmin="",$cc="") {

	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
		if ($_SESSION['USERTYPE']=="") {

		} else {

		}
	  $logSQL = "SELECT * FROM companyinfo ";

	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve companies list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
	
	$selected="n";
		if ($nums > 0) {
		  do {
				if ($cc != '' && $cc == $rowuser['com_id']) {
					echo "<option selected value=\"".$rowuser['com_id']."\">".$rowuser['company']."</option>";
					$selected="y";
				} else {
					if ($rowuser['default']=="Yes") {
						if ($selected=="n") {
							echo "<option selected value=\"".$rowuser['com_id']."\">".$rowuser['company']."</option>";
						}
					} else {
						echo "<option value=\"".$rowuser['com_id']."\">".$rowuser['company']."</option>";
					}
				}

		  } while($rowuser = mysqli_fetch_assoc($done));
		  if ($showadmin == 'SHOWADMIN') {
					echo "<option value='ADM' class='admred'>Administrative Login</option>";
				}
		} else {
				echo "<option value='ADM' class='admred'>Administrative Login</option>";
				echo "<option>No Companies Created</option>";
		}
	}

function showGroups($dp = '', $IncludeCoAdmin=FALSE,$showid="1") {

	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);

		if ($IncludeCoAdmin==FALSE) {
			$logSQL = "SELECT groups.groupid, groups.groupname, groups.groupid, groups.groupdesc, groups.sys
	FROM groups WHERE groups.sys =0 AND groups.groupname <> 'company administrator'   ORDER BY groups.groupname";
		} else {
			  $logSQL = "SELECT groups.groupid, groups.groupname, groups.groupid, groups.groupdesc, groups.sys FROM groups WHERE groups.groupname <> 'company administrator' ORDER BY groups.groupname";
		}


	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve groups list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);

	  $id=0;
	  $nm="";

		if ($nums > 0) {
		echo "<option > </option>";
		  do {
		  
				$id=$rowuser['groupid'];
				$nm=$rowuser['groupname'];
				if ($dp != '' && $dp == $rowuser['groupid']) {
					if ($showid=="1") {
						echo "<option selected value=\"".$id."\">".$nm."</option>";
					} else {
						echo "<option selected value=\"".$nm."\">".$nm."</option>";
					}
				} else {
					if ($showid=="1") {
						echo "<option value=\"".$id."\">".$nm."</option>";
					} else {
						echo "<option value=\"".$nm."\">".$nm."</option>";
					}
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No groups Selected</option>";
		}
	}

function showGrouptypes($selected = '') {
	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	
	$logSQL = "SELECT * FROM grouptypes ";

	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve group types list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);

		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				$id=$rowuser['id'];
				$nm=$rowuser['grouptypename'];
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['grouptypename']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['grouptypename']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No groups types Selected </option>";
		}
}


function getgrouptype($selected) {

	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	
		$logSQL = "SELECT * FROM grouptypes WHERE id = '".$selected."'";

	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve group types list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  
	  return  $rowuser['grouptypename'];
	 
} 


function InsertUserGroups  ($authcode,$groupid) { 
		$CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);

		mysqli_select_db(DB, $CON);
	  //groups 
	  $sql="DELETE FROM user_groups WHERE userid = '".$authcode."'";
		$Result1 = mysqli_query($sql, $CON) or die(mysqli_error());
		
		$sql="INSERT INTO user_groups (userid,groupid) VALUES ('".$authcode."','".$groupid."')";
		//echo $sql; 
		
		$Result1 = mysqli_query($sql, $CON) or die(mysqli_error());
					
		$selectSQL = "SELECT  * FROM groups WHERE groups.groupname <> 'System administrator'  ";
		$done = mysqli_query($selectSQL, $CON) or die("ERROR: Trying to retrieve groups list - ".mysqli_error());
		$groupslist = mysqli_fetch_assoc($done);
		
		if (mysqli_num_rows($done)>0) 
		{ 
			do { 
				if (isset($_POST[$groupslist['groupid']]) && $_POST[$groupslist['groupid']]!='' && $_POST[$groupslist['groupid']]!=$groupid) {
					$sql="INSERT INTO user_groups (userid,groupid) VALUES ('".$authcode."','".$groupslist['groupid']."')";
					//echo $sql; 
					
					$Result1 = mysqli_query($sql, $CON) or die(mysqli_error());
				}
			} while ($groupslist = mysqli_fetch_assoc($done));
		} 
} 



function getcompanyadmingroup() {
	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
		$logSQL = "SELECT groups.groupid, groups.groupname,groups.groupid, groups.groupdesc, groups.sys
	FROM groups WHERE groups.sys = 0 AND groups.groupname = 'Company Administrator'";
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve groups list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
			return $rowuser['groupid'];
		}
		
	}


function getcompany($enum) {
	
	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT com_id, company FROM companyinfo WHERE com_id = '".$enum."'";
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve company name - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  return $rowuser['company'];
	}
	
function gettelno($enum) {

  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
  $logSQL = "SELECT * FROM companyinfo WHERE com_id = '".$enum."'";
  mysqli_select_db(DB, $CON);
  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve company name - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
  return $rowuser['telphone'];
}
function getaddress($enum) {

  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
  $logSQL = "SELECT * FROM companyinfo WHERE com_id = '".$enum."'";
  mysqli_select_db(DB, $CON);
  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve company name - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
  return $rowuser['address'];
}

 function getfilter($enum) {
  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
  $logSQL = "SELECT FilterName  FROM filter WHERE  FilterCode = '".$enum."'";
  mysqli_select_db(DB, $CON);
  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve control - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
  return $rowuser['FilterName'];
}
	

function getcompanyID($enum) {
	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	 trim ($enum); 
	  $logSQL = "SELECT com_id FROM companyinfo WHERE company = '".$enum."'";
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve company name - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);

	  return $rowuser['com_id'];
	}

function getGroup($enum) {

  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
  $logSQL = "SELECT groupname FROM groups WHERE groupid = '".$enum."'";
  mysqli_select_db(DB, $CON);
  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve group name - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
  return $rowuser['groupname'];
}

function getGroupID($enum) {
  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
  $logSQL = "SELECT groupid FROM groups WHERE groupname = '".$enum."'";
  mysqli_select_db(DB, $CON);
  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve group name - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
  return $rowuser['groupid'];
}

function getgrouptypeid($id) 
{ 
  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
  $logSQL = "SELECT grouptypeid FROM groups WHERE groupid = '".$id."'";
  mysqli_select_db(DB, $CON);
  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve group type id - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
  return $rowuser['grouptypeid'];
} 
function scopeValid($valid) {
	// Break Apart
	list($time, $period) = explode(' ', $valid, 2);
	switch ($period) {

		case 'M':
		$per = 'Months';
		break;

		case 'Y':
		$per = 'Years';
		break;

		default:
		$per = 'Months';
		break;
	}

	$ret = $time." ".$per;

	return $ret;
}

// REQ SESSION status
// --------------------------------------------------------------
function showIcon($sessval) {
	switch ($sessval) {
		case 'N':
		$icon = 'grymkr.gif';
		break;

		case 'I':
		$icon = 'orgmkr.gif';
		break;

		case 'C':
		$icon = 'grntck.gif';
		break;

		default:
		$icon = 'grymkr.gif';
		break;
	}

	return $icon;
}


function msgbox($msg) {
	?>
	<script language="JavaScript" type="text/javascript">
		alert('<?php echo $msg ?>')
	</script>

	<?php
}


 function ifrecordexists($tablename,$columnname1,$columnname2,$columnname3,$value1,$value2,$value3) {
 
  $SQL = "SELECT * FROM ".$tablename." WHERE ".$columnname1." = '".$value1."' AND ".$columnname2." = '".$value2."' AND ".$columnname3." = '".$value3."'  ";
  //echo $SQL;
   $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
  $done = mysqli_query($SQL, $CON) or die("ERROR: Trying to check record exists - ".mysqli_error());
  $num_rows = mysqli_num_rows($done); 
	$rTrue = TRUE;
	$rFalse = FALSE;
	if ($num_rows > 0) {
	   return $rTrue;
	} else {
	   return $rFalse;
	}

}  
function getUsername($enum) {

  $eProc = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error($eProc),E_USER_ERROR); 
  $logSQL = "SELECT * FROM users WHERE authcode = '".$enum."'";
  mysqli_select_db($eProc,DB);
  $done = mysqli_query($eProc,$logSQL) or die("ERROR: Trying to retrieve user details - ".mysqli_error($eProc));
  $rowuser = mysqli_fetch_assoc($done);
  return $rowuser['fulname'] ;
}

function getfname($enum) {

  $eProc = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error($eProc),E_USER_ERROR); 
  $logSQL = "SELECT * FROM users WHERE authcode = '".$enum."'";
  mysqli_select_db($eProc,DB);
  $done = mysqli_query($eProc,$logSQL) or die("ERROR: Trying to retrieve user details - ".mysqli_error($eProc));
  $rowuser = mysqli_fetch_assoc($done);
  return $rowuser['username'] ;
}


function getadminEmail() {
  $eProc = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error($eProc),E_USER_ERROR); 
  $logSQL = "SELECT email FROM users WHERE admin ='1'";
  mysqli_select_db($eProc,DB);
  $done = mysqli_query($eProc,$logSQL) or die("ERROR: Trying to retrieve user details - ".mysqli_error($eProc));
  $rowuser = mysqli_fetch_assoc($done);
  return $rowuser['email'] ;
}

function getusergroup($user) {
  $eProc = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error($eProc),E_USER_ERROR); 
  $logSQL = "SELECT  groupid FROM users WHERE  authcode ='".$user."'";
  mysqli_select_db($eProc,DB);
  $done = mysqli_query($eProc,$logSQL) or die("ERROR: Trying to retrieve user details - ".mysqli_error($eProc));
  $rowuser = mysqli_fetch_assoc($done);
  return $rowuser['groupid'] ;
}



function createbarcode($value) { 

	$ean13 = new ean13;
	$ean13->article = $value;   // initial article code
	$ean13->article .= $ean13->generate_checksum();   // add the proper checksum value
	//$ean13->reverse();   // the string is printed backwards
	$value = $ean13->codestring();   // returns a string as input for the truetype font
	return $value ;  // render the image as PNG image

} 

function getcompanyaddress ($enum) {
	 
	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM companyinfo WHERE com_id = '".$enum."'";
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve company details - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  return $rowuser['address'];
}

function getcompanyaddress2 ($enum) {

	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM companyinfo WHERE com_id = '".$enum."'";
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve company details - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  return $rowuser['address2'];

}

function getcompanytown ($enum) {

	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM companyinfo WHERE com_id = '".$enum."'";
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve company details - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  return $rowuser['town'];

}

function getcompanycountry ($enum) {

	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM companyinfo WHERE com_id = '".$enum."'";
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve company details - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  return $rowuser['country'];

}

function getcompanylocation ($enum) {

	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM companyinfo WHERE com_id = '".$enum."'";
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve company details - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  return $rowuser['location'];

} 

function getcompanytel ($enum) {

	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM companyinfo WHERE com_id = '".$enum."'";
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve company details - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  return $rowuser['telphone'];

}  

function getcompanyfax ($enum) {

	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM companyinfo WHERE com_id = '".$enum."'";
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve company details - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  return $rowuser['faxphone'];

} 

function getcompanyemail ($enum) {

	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM companyinfo WHERE com_id = '".$enum."'";
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve company details - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  return $rowuser['email'];

} 

function getcompanywebsite ($enum) {

	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
      $logSQL = "SELECT * FROM companyinfo WHERE com_id = '".$enum."'";
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve company details - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  return $rowuser['website'];

}


function getusertype($enum) {
  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
  $logSQL = "SELECT * FROM users WHERE  authcode = '".$enum."'";
  mysqli_select_db(DB, $CON);
  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve  usertype - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
  return $rowuser['admin'];
}

function getnextcode($field,$table) {
 $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
$logSQL =  "SELECT   max(".$field.") AS num FROM ".$table."   LIMIT 1";
;
  mysqli_select_db(DB, $CON);
	$prq1 = mysqli_query($logSQL, $CON) or die(mysqli_error());
	$row_prq1 = mysqli_fetch_assoc($prq1);
  return  $row_prq1[num]+1;
  }
  
  function ifmyrecordcodeexists($tablename,$col1,$value1) {
	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	$logSQL = "SELECT * FROM ".$tablename." WHERE ".$col1." = '".$value1."' ";
	mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to check record exists - ".mysqli_error(). $logSQL);
	  //$rowuser = mysqli_fetch_assoc($done);
	  $num_rows = mysqli_num_rows($done); //Get number of rows

	if ($num_rows > 0) {
	   return 'yes';
	} else {
	   return 'no';
	}
}

function showclients($selected = '') {
	
	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM clients ORDER BY  fname ASC ";
	  //echo $logSQL;
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve clients list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['fname']." ".$rowuser['lname']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['fname']." ".$rowuser['lname']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No clients Selected </option>";
		}
}
function showcategory($selected = '') {
	
	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM category ORDER BY  name ASC ";
	  //echo $logSQL;
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve category list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No category Selected </option>";
		}
}

function showitemtype($selected = '') {
	
	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM itypes ORDER BY  name ASC ";
	  //echo $logSQL;
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve itemtype list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No itemtype Selected </option>";
		}
}

function showstatus($selected = '') {
	
	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM state ORDER BY  name ASC ";
	  //echo $logSQL;
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve status list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No state Selected </option>";
		}
}

function showloadtype($selected = '') {
	
	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM loadtype ORDER BY  name ASC ";
	  //echo $logSQL;
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve loadtype list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No clients Selected </option>";
		}
}

function getmaxid($field,$table) {
 $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
$logSQL =  "SELECT   max(".$field.") AS num FROM ".$table."   LIMIT 1";
;
  mysqli_select_db(DB, $CON);
	$prq1 = mysqli_query($logSQL, $CON) or die(mysqli_error());
	$row_prq1 = mysqli_fetch_assoc($prq1);
	
  return  $row_prq1['num']+1;
  }


?>
