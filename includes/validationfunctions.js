// commonly used javascript functions
function checkNumbers(evt){
evt = (evt)? evt: window.event
        var charCode = (evt.which)? evt.which: evt.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57)){
status = "this field accepts only numbers only!"
        return false;
}
status = ""
        return true
}


function isEmpty(inputString){

if (inputString == null || inputString == ""){
return true
}
return false
        }
// general purpose function to see if a suspected numeric input
// is a positive integer
function isPosInteger(inputVal) {
inputStr = inputVal.toString()
        for (var i = 0; i < inputStr.length; i++) {
var oneChar = inputStr.charAt(i)
        if (oneChar < "0" || oneChar > "9") {
return false
        }
}
return true
        }

// general purpose function to see if a suspected numeric input
// is a positive or negative integer
function isInteger(inputVal) {
inputStr = inputVal.toString()
        for (var i = 0; i < inputStr.length; i++) {
var oneChar = inputStr.charAt(i)
        if (i == 0 && oneChar == "-") {
continue
        }
if (oneChar < "0" || oneChar > "9") {
return false
        }
}
return true
        }



// general purpose function to see if a suspected numeric input
// is a positive or negative number
function isNumber(inputVal) {
oneDecimal = false
        inputStr = inputVal.toString()
        for (var i = 0; i < inputStr.length; i++) {
var oneChar = inputStr.charAt(i)
        if (i == 0 && oneChar == "-") {
continue
        }
if (oneChar == "." && !oneDecimal) {
oneDecimal = true
        continue
        }
if (oneChar < "0" || oneChar > "9") {
return false
        }
}
return true
        }


// function to determine if value is in acceptable range
// for this application
function inRange(inputStr) {
num = parseInt(inputStr)
        if (num < 10000000 || num > 99999999) {
return false
        }
return true
        }
function isValid(inputStr) {
if (isEmpty(inputStr)) {
alert("Please enter a number into the field before clicking the button.")
        return false
        } else {
if (!isNumber(inputStr)) {
alert("Please make sure entries are numbers only.")
        return false
        } else {
if (!inRange(inputStr)) {
var msg = "Sorry, the number you entered is not part of our database."
        msg += "Try another teProcee-digit number."
        alert(msg)
        return false
        }
}
}
return true
        }