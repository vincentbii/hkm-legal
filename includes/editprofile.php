<?php require_once('../connections/eProc.php'); ?>
<?php


$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
if ($_SESSION['company']=="SYSTEM ADMINISTRATOR") {
	$updateSQL = sprintf("UPDATE adminlogin SET fulname=%s WHERE authcode=%s",
						   GetSQLValueString($_POST['fulname'], "text"),						   
						   GetSQLValueString($_POST['authcode'], "text"));
					  mysqli_select_db($eProc, $database_eProc);
			  $Result1 = mysqli_query($eProc, $updateSQL) or die("ERROR UPDATING USER PROFILE - ".mysqli_error($eProc));
} else {
	  $updateSQL = sprintf("UPDATE users SET fulname=%s,  phone=%s, email=%s WHERE authcode=%s",
						   GetSQLValueString($_POST['fulname'], "text"),						   
						   GetSQLValueString($_POST['phone'], "text"),
						   
						   GetSQLValueString($_POST['email'], "text"),
						   GetSQLValueString($_POST['authcode'], "text"));
					  mysqli_select_db($eProc, $database_eProc);
			  $Result1 = mysqli_query($eProc, $updateSQL) or die("ERROR UPDATING USER PROFILE - ".mysqli_error($eProc));
				   
}


  $updateGoTo = "myprofile.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
    $updateGoTo .= $_SERVER['QUERY_STRING'];
  }
  		//go to index page
			?>
		<SCRIPT LANGUAGE=JAVASCRIPT>
		{
			javascript:history.go(-1);
		}
		</SCRIPT>	<?php 
}

$colname_up = "1";
if (isset($_SESSION['UNQ'])) {
  $colname_up = (get_magic_quotes_gpc()) ? $_SESSION['UNQ'] : addslashes($_SESSION['UNQ']);
}
mysqli_select_db($eProc, $database_eProc);
if ($_SESSION['company']=="SYSTEM ADMINISTRATOR") {
	$query_up = sprintf("SELECT * FROM adminlogin WHERE authcode = '%s'", $colname_up);
} else {
	$query_up = sprintf("SELECT * FROM users WHERE authcode = '%s'", $colname_up);
}
$up = mysqli_query($eProc, $query_up) or die(mysqli_error());
$row_up = mysqli_fetch_assoc($up);
$totalRows_up = mysqli_num_rows($up);
?>

<table width="100%"  border="0" cellspacing="0" cellpadding="4">
                    <tr valign="top"> 
                      <td><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                        <tr valign="middle">
                          <td width="63%" class="hOne">Modify Profile </td>
                          <td width="37%" align="right" nowrap class="baseline white">last Updated: </td>
                        </tr>
                      </table>
                        <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td valign="top" class="baseline">Use this section to modify your profile and login information.</td>
                          </tr>
                          <tr>
                            <td valign="top" class="spacer5">
                              <form method="post" name="form1" action="<?php echo $editFormAction; ?>">
                                <table width="350" cellpadding="5" cellspacing="0" class="tblbdr">
                                  <tr valign="baseline">
                                    <td nowrap align="right">Full name:</td>
                                    <td><input name="fulname" type="text" class="forms" id="fulname" value="<?php echo $row_up['fulname']; ?>"></td>
                                  </tr>
								  
                                  <tr valign="baseline">
                                    <td colspan="2" align="right" nowrap><eProc size="1"></td>
                                  </tr>
                                  <tr valign="baseline">
                                    <td nowrap align="right">Phone:</td>
                                    <td><input name="phone" type="text" class="forms" value="<?php echo $row_up['phone']; ?>" size="32"></td>
                                  </tr>
                               
                               
                                  <tr valign="baseline">
                                    <td nowrap align="right">email:</td>
                                    <td><input name="email" type="text" class="forms" value="<?php echo $row_up['email']; ?>" size="32"></td>
                                  </tr>
                                  <tr valign="baseline">
                                    <td colspan="2" align="right" nowrap><eProc size="1"></td>
                                    </tr>
                                  <tr valign="baseline">
                                    <td nowrap align="right"><input type="hidden" name="MM_update" value="form1">
                                      <input name="authcode" type="hidden" id="authcode" value="<?php echo $row_up['authcode']; ?>"></td>
                                    <td><input type="submit" class="formsBlue" value="Modify Profile">
                                    <input name="button" type="button" class="formsorg" onclick="javascript:location='<?php echo "../".$_SESSION['HOME'] ?>'" value="Cancel" /></td>
                                  </tr>
                                </table>
                              </form>                              </td>
                          </tr>
                        </table></td>
                    </tr>
                </table>

<?php
mysqli_free_result($up);
?>
 </tr>
            </table>