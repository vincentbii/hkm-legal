<?php

function getusermenu() {
	$theValue = "../includes/usermenu.php";
	return $theValue;
}

//This function checks for requisitioner and authoriser group
//and returns 1,2 respectively or 0 if neither
function getGroupTypeIndicator($enum) {

// --------------------------------------------------------------
	if ($_SESSION['USERTYPE'] == 'Requisitioner') {
		return  1;
	} elseif ($_SESSION['USERTYPE'] == 'authoriser') {
		return  2;
	} else {
		return  0;
	}
	
}

function getUser($enum) {
// --------------------------------------------------------------
// Show the donor based on Donor ID
// --------------------------------------------------------------
  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
// --------------------------------------------------------------
  $logSQL = "SELECT * FROM users WHERE authcode = '".$enum."'";
// --------------------------------------------------------------
  mysqli_select_db($CON, DB);
  $done = mysqli_query( $CON,$logSQL) or die("ERROR: Trying to retrieve user details - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
// --------------------------------------------------------------
  return $rowuser['fulname'];
}

function getUserFromID($enum) {
// --------------------------------------------------------------
// Show the donor based on Donor ID
// --------------------------------------------------------------
  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
// --------------------------------------------------------------
  $logSQL = "SELECT * FROM users WHERE uid = '".$enum."'";
// --------------------------------------------------------------
  mysqli_select_db($CON, DB);
  $done = mysqli_query( $CON,$logSQL) or die("ERROR: Trying to retrieve user details - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
// --------------------------------------------------------------
  return $rowuser['fulname'];
}

function getUserLoginName($enum) {
// --------------------------------------------------------------
// Show the donor based on Donor ID
// --------------------------------------------------------------
  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
// --------------------------------------------------------------
  $logSQL = "SELECT * FROM users WHERE uid = '".$enum."'";
// --------------------------------------------------------------
  mysqli_select_db($CON, DB);
  $done = mysqli_query( $CON,$logSQL) or die("ERROR: Trying to retrieve user details - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
// --------------------------------------------------------------
  return $rowuser['username'];
}

function userExists($enum) {
// --------------------------------------------------------------
// Show the donor based on Donor ID
// --------------------------------------------------------------
  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
// --------------------------------------------------------------
  $logSQL = "SELECT * FROM users WHERE username = '".$enum."' AND com_id = '".$_SESSION['company']."' AND uid <> '".$_SESSION['uid']."'";
// --------------------------------------------------------------
  mysqli_select_db($CON, DB);
  $done = mysqli_query( $CON,$logSQL) or die("ERROR: Trying to retrieve user details - ".mysqli_error());
  //$rowuser = mysqli_fetch_assoc($done);
  $num_rows = mysqli_num_rows($done); //Get number of rows
// --------------------------------------------------------------
$rTrue = TRUE;
$rFalse = FALSE;
if ($num_rows > 0) {
   return $rTrue;
} else {
   return $rFalse;
}

}

function usercodeExists($enum) {
// --------------------------------------------------------------
// Show the donor based on Donor ID
// --------------------------------------------------------------
  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
// --------------------------------------------------------------
  $logSQL = "SELECT * FROM users WHERE authcode = '".$enum."'";
// --------------------------------------------------------------
  mysqli_select_db($CON, DB);
  $done = mysqli_query( $CON,$logSQL) or die("ERROR: Trying to retrieve user details - ".mysqli_error());
  //$rowuser = mysqli_fetch_assoc($done);
  $num_rows = mysqli_num_rows($done); //Get number of rows
// --------------------------------------------------------------
$rTrue = TRUE;
$rFalse = FALSE;
if ($num_rows > 0) {
   return $rTrue;
} else {
   return $rFalse;
}

}

function authoriserExists($enum,$chain) {
// --------------------------------------------------------------
// Show the donor based on Donor ID
// --------------------------------------------------------------
  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
// --------------------------------------------------------------
  $logSQL = "SELECT * FROM authorizers WHERE authperson = '".$enum."' AND com_id = '".$_SESSION['company']."' AND chainname = '".$chain."'";
// --------------------------------------------------------------
  mysqli_select_db($CON, DB);
  $done = mysqli_query( $CON,$logSQL) or die("ERROR: Trying to retrieve user details - ".mysqli_error());
  //$rowuser = mysqli_fetch_assoc($done);
  $num_rows = mysqli_num_rows($done); //Get number of rows
// --------------------------------------------------------------
	$rTrue = TRUE;
	$rFalse = FALSE;
	if ($num_rows > 0) {
	   return $rTrue;
	} else {
	   return $rFalse;
	}

}

function getLastUser($enum, $ccid) {
// --------------------------------------------------------------
// Show the donor based on Donor ID
// --------------------------------------------------------------
  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
// --------------------------------------------------------------
  $logSQL = "SELECT uid FROM users WHERE username = '".$enum."' AND costcentercode = '".$cctID."' AND com_id = '".$_SESSION['company']."'";
// --------------------------------------------------------------
  mysqli_select_db($CON, DB);
  $done = mysqli_query( $CON,$logSQL) or die("ERROR: Trying to retrieve user details - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
// --------------------------------------------------------------
  return $rowuser['uid'];
}

function getEmail($enum) {
	// --------------------------------------------------------------
	// Show the donor based on Donor ID
	// --------------------------------------------------------------
	  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
	// --------------------------------------------------------------
	  $logSQL = "SELECT email FROM users WHERE authcode = '".$enum."'";
	// --------------------------------------------------------------
	  mysqli_select_db($CON, DB);
	  $done = mysqli_query( $CON,$logSQL) or die("ERROR: Trying to retrieve user email - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	// --------------------------------------------------------------
	  return $rowuser['email'];
}

function getUserDetails($enum) {
// --------------------------------------------------------------
// Show the donor based on Donor ID
// --------------------------------------------------------------
  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
// --------------------------------------------------------------
  $logSQL = "SELECT users.* , groups.groupname FROM users INNER JOIN groups ON users.groupid = groups.groupid WHERE users.authcode = '".$enum."'";
// --------------------------------------------------------------
  mysqli_select_db($CON, DB);
  $done = mysqli_query($CON,$logSQL) or die("ERROR: Trying to retrieve user details - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
// --------------------------------------------------------------
  return $rowuser;
}

function getCostCenterName($enum) {
// --------------------------------------------------------------
// 
// --------------------------------------------------------------
  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
// --------------------------------------------------------------
  $logSQL = "SELECT DISTINCT costcenter FROM costcenter WHERE costcode = '".$enum."'";
// --------------------------------------------------------------
  mysqli_select_db($CON, DB);
  $done = mysqli_query( $CON,$logSQL) or die("ERROR: Trying to retrieve user details - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
  $rowuser = $rowuser['costcenter'];
// --------------------------------------------------------------
  return $rowuser;
}

function getAdminUser($enum) {
// --------------------------------------------------------------
// Show the donor based on Donor ID
// --------------------------------------------------------------
  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
// --------------------------------------------------------------
  $logSQL = "SELECT * FROM adminlogin WHERE authcode = '".$enum."'";
// --------------------------------------------------------------
  mysqli_select_db($CON, DB);
  $done = mysqli_query( $CON,$logSQL) or die("ERROR: Trying to retrieve admin user details - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);

// --------------------------------------------------------------
  return $rowuser;
}

function showUsers($company, $group='',  $user='') {
// --------------------------------------------------------------
// Show the users from the user database
// --------------------------------------------------------------
  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
// --------------------------------------------------------------

	if ($group!="") {
		$logSQL = "SELECT DISTINCT users.authcode, fulname, users.username FROM users INNER JOIN groups ON users.groupid = groups.groupid  WHERE com_id = '".$company."' AND groups.groupname = '".$group."'";
	} else {
	  $logSQL = "SELECT DISTINCT authcode, fulname, username FROM users  WHERE com_id = '".$company."'";
	}
// --------------------------------------------------------------
  mysqli_select_db($CON, DB);
  $done = mysqli_query( $CON,$logSQL) or die("ERROR: Trying to retrieve donor details - ".mysqli_error());
 // $rowuser = mysqli_fetch_assoc($done);
// --------------------------------------------------------------
	echo "<option value=\""."0"."\">"."None"."</option>";
  while($rowuser = mysqli_fetch_assoc($done)) {	
  		if ($rowuser['authcode']==$user) {
  			echo "<option selected value=\"".$rowuser['authcode']."\">".$rowuser['fulname']." (".$rowuser['username'].")"."</option>";
		} else {
			echo "<option value=\"".$rowuser['authcode']."\">".$rowuser['fulname']." (".$rowuser['username'].")"."</option>";
		}
  } 
}

function showCCUsers($ccCode) {
// --------------------------------------------------------------
// Show the users from the user database
// --------------------------------------------------------------
  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
// --------------------------------------------------------------
  //$logSQL = "SELECT username,uid  FROM users  WHERE com_id = '".$_SESSION['company']."' AND costcentercode = '".$ccCode."' AND sys <> 1";
// ------------------------------------------------
  $logSQL = "SELECT users.username, users.uid, users.fulname  FROM users INNER JOIN user_costcode ON user_costcode.userauthcode = users.uid  WHERE users.com_id = '".$_SESSION['company']."'  AND user_costcode.costcode = '".$ccCode."'";
  //echo  $logSQL;
  mysqli_select_db($CON, DB);
  $done = mysqli_query( $CON,$logSQL) or die("ERROR: Trying to retrieve donor details - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
// --------------------------------------------------------------
  do {
  		echo "<option value=\"".$rowuser['uid']."\">".$rowuser['username']." (".$rowuser['fulname'].")"."</option>";
  } while($rowuser = mysqli_fetch_assoc($done));
}

function showUGRP($UGA) {
// --------------------------------------------------------------
// Show the users from the user database
// --------------------------------------------------------------
  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
// --------------------------------------------------------------
  $logSQL = "SELECT UGroup FROM users WHERE UGA = '".$UGA."'";
// --------------------------------------------------------------
  mysqli_select_db($CON, DB);
  $done = mysqli_query( $CON,$logSQL) or die("ERROR: Trying to retrieve user group - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
// --------------------------------------------------------------
  return $rowuser['UGroup'];
}

// --------------------------------------------------------------------------------
// Function to validate whether a user is logged in or not and redirect as needed
// --------------------------------------------------------------------------------
function isLoggedIn() {
	// Check session values
	if (isset($_SESSION['USER']) && isset($_SESSION['UNQ']) && $_SESSION['USER'] != "") {
		return true;
	} else {
		return false;
	}
	// end validate
}

// --------------------------------------------------------------
// Function to Authenticate user and log them in
// --------------------------------------------------------------
function AuthenticateUser($uname, $password, $company) {
	session_start();
  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
// --------------------------------------------------------------
  if ($company == 'ADM') {
	 $logSQL = "SELECT * FROM adminlogin WHERE username = '".$uname."' AND umd5 = '".md5($password)."'";
	 $page = 'eadm/ahome.php';
  } else {	 
	 $logSQL = "SELECT users.*,users.authcode,users.groupid, groups.groupname FROM users INNER JOIN groups ON users.groupid = groups.groupid WHERE users.username = '".$uname."' AND users.umd5 = '".md5($password)."' AND users.com_id = '".$company."' AND status = 'A'";
	// $page = 'req/index.php';
  }
// --------------------------------------------------------------
  mysqli_select_db($CON,DB);
  $done = mysqli_query( $CON,$logSQL) or die("ERROR: Trying to authenticate user  - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
  $num_rows = mysqli_num_rows($done);
// --------------------------------------------------------------
  $valid = false;
  if ($num_rows > 0) {
  	$valid = true;
}	
  $retval = $valid.";".$rowuser['authcode'].";".$rowuser['groupname'].";".$rowuser['groupid'];
// --------------------------------------------------------------
  return $retval;
}

// --------------------------------------------------------------
// Function to get user group access rights
// --------------------------------------------------------------
function GetGroupAccessRights($uname, $company) {
	session_start();
  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
// --------------------------------------------------------------	 
	 $logSQL = "SELECT users.*, groups.groupname FROM users INNER JOIN groups ON users.groupid = groups.groupid WHERE users.username = '".$uname."' AND users.com_id = '".$company."'";


// --------------------------------------------------------------
  mysqli_select_db($CON, DB);
  $done = mysqli_query( $CON,$logSQL) or die("ERROR: Trying to authenticate user - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
  $num_rows = mysqli_num_rows($done);
// --------------------------------------------------------------
  $valid = false;
  if ($num_rows > 0) {
  	$valid = true;
}	
  $retval = $valid.";".$rowuser['authcode'].";".$rowuser['groupname'];
// --------------------------------------------------------------
  return $retval;
}

//Check if user is authoriser
function UserCanEditRequisition($enum) {
  return 1;
}

function showUserCC($cc = '', $usr='') {
// --------------------------------------------------------------
// Show the users from the user database
// --------------------------------------------------------------
  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
// --------------------------------------------------------------
  $logSQL = "SELECT user_costcode.costcode, costcenter.costcenter FROM user_costcode INNER JOIN costcenter ON  user_costcode.costcode = costcenter.costcode WHERE com_id = '".$_SESSION['company']."' AND userauthcode = '".$usr."'";
// --------------------------------------------------------------
//echo   $logSQL;
  mysqli_select_db($CON, DB);
  $done = mysqli_query( $CON,$logSQL) or die("ERROR: Trying to retrieve departments list - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
  $nums = mysqli_num_rows($done);
// --------------------------------------------------------------
	if ($nums > 0) {
	  do {
				//$cbudget=(getcostcenterbudget($_SESSION['company'],$rowuser['costcentercode'])) - (getcostcodereqamt($rowuser['costcentercode']));
				
				$cbudget=getcostcenterbudget($_SESSION['company'],$rowuser['costcode'],date(m))-getcostcodereqamt($rowuser['costcode'],date(m));
				
				echo "<option selected value=\"".$rowuser['costcode']."\">".$rowuser['costcode']." - ".$rowuser['costcenter']."  [ Remaining Budget Ksh ".$cbudget." ] </option>";

	  } while($rowuser = mysqli_fetch_assoc($done));
	} else {
			echo "<option>No Cost centers Selected</option>";
	}
}

function showUserDepartment($dp = '') {
// --------------------------------------------------------------
// Show the users from the user database
// --------------------------------------------------------------
  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
// --------------------------------------------------------------
  $logSQL = "SELECT * FROM users WHERE com_id = '".$_SESSION['company']."' AND username = '".$_SESSION['USER']."'";
// --------------------------------------------------------------
  mysqli_select_db($CON, DB);
  $done = mysqli_query( $CON,$logSQL) or die("ERROR: Trying to retrieve departments list - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
  $nums = mysqli_num_rows($done);
// --------------------------------------------------------------
	//echo "<option class='greentxt'>Select department</option>";
	if ($nums > 0) {
	  do {

				echo "<option selected value=\"".$rowuser['depid']."\">".$rowuser['departmentid']."</option>";

				//echo "<option value=\"".$rowuser['depid']."\">".$rowuser['departmentid']."</option>";

	  } while($rowuser = mysqli_fetch_assoc($done));
	} else {
			echo "<option>No departments Selected</option>";
	}
}

function showdirectstorereq($company = '',$storekeeperauth='') {
// --------------------------------------------------------------
// Show the users from the user database
// --------------------------------------------------------------
if ($company=="") {
	$company=$_SESSION['company'];
}

  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
// --------------------------------------------------------------
  //$logSQL = "SELECT * FROM directstorerequisitioners WHERE com_id = '".$company."' ORDER BY name";
  
     $logSQL = "SELECT  DISTINCT uid, username, fulname FROM users INNER JOIN warehouse_user_access ON  users.uid = warehouse_user_access.userid WHERE warehouse_user_access.warehouseid in (SELECT  warehouseid FROM warehouse_storekeeper_access WHERE  storekeeperauthcode = '".$storekeeperauth."')"; 

//return $logSQL;
// --------------------------------------------------------------
  mysqli_select_db($CON, DB);
  $done = mysqli_query( $CON,$logSQL) or die("ERROR: Trying to retrieve departments list - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
  $nums = mysqli_num_rows($done);
// --------------------------------------------------------------
	if ($nums > 0) {
		//echo "<option value=\""."0"."\">".""."</option>";
	  do {
		echo "<option value=\"".$rowuser['uid']."\">".$rowuser['username']." (".$rowuser['fulname'].")"."</option>";
	  } while($rowuser = mysqli_fetch_assoc($done));
	} else {
			echo "<option value=\""."0"."\">No direct store requisitioners</option>";
	}
}

function showspecificusers($company = '',$type='') {
// --------------------------------------------------------------
// Show the users from the user database
// --------------------------------------------------------------
if ($company=="") {
	$company=$_SESSION['company'];
}

switch ($type) {
case "str":
   $gr="store_keeper";
   break;
case "req":
   $gr="requisitioners";
   break;
case "salesexec":
    $gr="sales_executive";
   break;
default:
   $gr="requisitioners";
   break;
}

  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
// --------------------------------------------------------------
  //$logSQL = "SELECT * FROM directstorerequisitioners WHERE com_id = '".$company."' ORDER BY name";
  
     $logSQL = "SELECT  users.authcode, users.username, users.fulname FROM users INNER JOIN config_grouptypes ON  users. groupid = config_grouptypes.".$gr." WHERE users.com_id = '".$company."' ORDER BY users.username"; 

//return $logSQL;
// --------------------------------------------------------------
  mysqli_select_db($CON, DB);
  $done = mysqli_query( $CON,$logSQL) or die("ERROR: Trying to retrieve departments list - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
  $nums = mysqli_num_rows($done);
// --------------------------------------------------------------
	if ($nums > 0) {
		echo "<option value=\""."0"."\">".""."</option>";
	  do {
		echo "<option value=\"".$rowuser['authcode']."\">".$rowuser['username']." (".$rowuser['fulname'].")"."</option>";
	  } while($rowuser = mysqli_fetch_assoc($done));
	} else {
			echo "<option>No users found</option>";
	}
}

function validatedirectstrreqcode($directrqner, $code,$company) {

	$CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 

	 $logSQL = "SELECT uid, username, pincode FROM users WHERE uid = ".$directrqner." AND pincode = '".$code."' AND com_id = '".$company."'";

	  mysqli_select_db($CON, DB);
	  $done = mysqli_query( $CON,$logSQL) or die("ERROR: Trying to authenticate user - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $num_rows = mysqli_num_rows($done);
// --------------------------------------------------------------
//return $logSQL;
  	if ($num_rows > 0) {
  		return "y";
	} else {
		return "n";
	}	
 
}

function getstoremanWarehouseID($enum) {
// --------------------------------------------------------------
// Show the donor based on Donor ID
// --------------------------------------------------------------
  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
// --------------------------------------------------------------
  $logSQL = "SELECT  warehouseid FROM warehouse_storekeeper_access WHERE storekeeperauthcode = '".$enum."'";
// --------------------------------------------------------------
  mysqli_select_db($CON, DB);
  $done = mysqli_query( $CON,$logSQL) or die("ERROR: Trying to retrieve warehouse name - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
// --------------------------------------------------------------
  return $rowuser['warehouseid'];
}

function getuserWarehouseID($enum) {
// --------------------------------------------------------------
// Show the donor based on Donor ID
// --------------------------------------------------------------
  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
// --------------------------------------------------------------
  $logSQL = "SELECT distinct catid FROM catalogue_user_access WHERE userauthcode = '".$enum."'";
// --------------------------------------------------------------
  mysqli_select_db($CON, DB);
  $done = mysqli_query( $CON,$logSQL) or die("ERROR: Trying to retrieve warehouse name - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
// --------------------------------------------------------------
  return $rowuser['warehouseid'];
}



function getcatcontactperson($enum) {
// --------------------------------------------------------------
// Show the donor based on Donor ID
// --------------------------------------------------------------
  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
// --------------------------------------------------------------

  $logSQL = "SELECT company_catalogue_prices.salesexecutive, users.fulname FROM company_catalogue_prices INNER JOIN users ON users.authcode = company_catalogue_prices.salesexecutive  WHERE company_catalogue_prices.catalogue_id = '".$enum."'";
// --------------------------------------------------------------
  mysqli_select_db($CON, DB);
  $done = mysqli_query( $CON,$logSQL) or die("ERROR: Trying to retrieve company name - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
// --------------------------------------------------------------
  return $rowuser['fulname'];
}

function wrUsers($file) {
return;
// --------------------------------------------------------------
// Show the donor based on Donor ID
// --------------------------------------------------------------
  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
// --------------------------------------------------------------
  $logSQL = "SELECT costcentercode, fulname FROM users WHERE com_id = '".$_SESSION['company']."' ORDER BY costcentercode ASC";
// --------------------------------------------------------------
  mysqli_select_db($CON, DB);
  $done = mysqli_query( $CON,$logSQL) or die("ERROR: Trying to retrieve users - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
// --------------------------------------------------------------
// Write to a js file
// --------------------------------------------------------------
  // $file = 'tmp/jbs.js';
  $fn = touch($file);
// --------------------------------------------------------------
  $fp = fopen($file, 'a');
// --------------------------------------------------------------
  fwrite($fp,'// users'."\n"); 
  fwrite($fp,'var us = new Array();'."\n"); $c = 0; 
  do {
		$str = 'us['.$c.'] = "'.addslashes($rowuser['costcentercode']).'|'.addslashes($rowuser['fulname']).'";';
		fwrite($fp,$str."\n");
  		$c++;
  } while($rowuser = mysqli_fetch_assoc($done));
// --------------------------------------------------------------
}

function getsupervisor($enum='') {
// --------------------------------------------------------------
// Show the donor based on Donor ID
// --------------------------------------------------------------
  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
// --------------------------------------------------------------
 	if ($enum!='') 
		{ 
  		$logSQL = "SELECT * FROM users WHERE authcode = '".$enum."' AND groupid = '66399SUPERVISOR'";
 		 } else {
  		$logSQL = "SELECT * FROM users WHERE groupid = '66399SUPERVISOR'";
 		 }
// --------------------------------------------------------------
  mysqli_select_db($CON, DB);
  $done = mysqli_query( $CON,$logSQL) or die("ERROR: Trying to retrieve user details - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
// --------------------------------------------------------------
  if ($enum!='') {
	   return $rowuser['fulname'];
  } else {
  		echo "<option></option>";	
  	do {
		echo "<option  value=\"".$rowuser['uid']."\">".$rowuser['fulname']."</option>";	
	} while ($rowuser = mysqli_fetch_assoc($done)); 
  } 
 
}

?>