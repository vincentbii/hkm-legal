<?php

function getrequisitionnotes($enum) {
// --------------------------------------------------------------
// Show the donor based on Donor ID
// --------------------------------------------------------------
  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
// --------------------------------------------------------------
  $logSQL = "SELECT reqnotes FROM requisitions ".$enum;
// --------------------------------------------------------------
  mysqli_select_db(DB, $CON);
  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve user details - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
// --------------------------------------------------------------
  return $rowuser['reqnotes'];
}

function getrequisitionvalue($enum) {
// --------------------------------------------------------------
// Show the donor based on Donor ID
// --------------------------------------------------------------
  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
// --------------------------------------------------------------
  $logSQL = "SELECT reqvalue FROM requisitions WHERE rid = '".$enum."'";
// --------------------------------------------------------------
  mysqli_select_db(DB, $CON);
  $done = mysqli_query($logSQL, $CON) or die("ERROR:  - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
// --------------------------------------------------------------
  return $rowuser['reqvalue'];
}

function getacceptedlposerialnumber($enum) {
// --------------------------------------------------------------
// Show the donor based on Donor ID
// --------------------------------------------------------------
  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
// --------------------------------------------------------------
  $logSQL = "SELECT * FROM acceptedlpo WHERE rqid = '".$enum."'";
// --------------------------------------------------------------
  mysqli_select_db(DB, $CON);
  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve user details - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
// --------------------------------------------------------------
  return $rowuser['serialno'];
}


function finalizeauthorization($reqitemid, $value, $company,$currentauth) {
	// --------------------------------------------------------------
  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
// --------------------------------------------------------------
  $authorisers = "SELECT * FROM authorizers WHERE  com_id = '".$company."' AND upperlimit >= ".$value." OR upperlimit = 0 ORDER BY upperlimit";
// --------------------------------------------------------------
  mysqli_select_db(DB, $CON);
  $authdone = mysqli_query($authorisers, $CON) or die("ERROR:- ".mysqli_error());
  $rowauthorisers = mysqli_fetch_assoc($authdone);
  $countauth=mysqli_num_rows($authdone);
  
  if ($countauth > 0) {
	  do {
		  
		  $authorized = "SELECT DISTINCT reqauthorization.authorizer,users.username FROM reqauthorization INNER JOIN users ON users.authcode=reqauthorization.authorizer WHERE reqitemid = '".$reqitemid."'";
	// --------------------------------------------------------------
		  mysqli_select_db(DB, $CON);
		  $authdone2 = mysqli_query($authorized, $CON) or die("ERROR: Trying to retrieve user details - ".mysqli_error());
		  $rowauthorized = mysqli_fetch_assoc($authdone2);
		  do {
			  if ($rowauthorized['username']==$rowauthorisers['authperson']) {
				return "1";
			  }  
		  } while ($rowauthorized = mysqli_fetch_assoc($authdone2));
	  } while ($rowauthorisers = mysqli_fetch_assoc($authdone));
  }
  //the req has not gone to the final authoriser
  return "0";
}

function getcashpurchasesCount($flag, $usr='',$company='',$type='',$authoriser='') {
// --------------------------------------------------------------
// Show the donor based on Donor ID
// --------------------------------------------------------------
  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
// --------------------------------------------------------------

  switch ($flag) {
  	case 'PDG':
	$status = 'Pending Approval';
	break;
	// ---------------------------
  	case 'PTB':
	$status = 'Posted To buyer';
	break;
	// ---------------------------
  	case 'REJ':
	$status = 'Rejected';
	break;
	// ---------------------------
	case 'ARCH':
	$status = 'Archive';
	break;
	// ---------------------------
  	default:
	$status = 'Pending Approval';
	break;
	// ---------------------------
  }
  
  if ($usr!="") {
  	$cond1 = "  AND authcode = '".$usr."'";
  }
  if ($company!="") {
  	$cond2 = " AND com_id = '".$company."'";
  }
   if ($authoriser!="") {
  	$cond3 = " AND  authoriser = '".$authoriser."'";
  }
 
	  $CC = $_SESSION['costcentercode'];
	  $logSQL = "SELECT COUNT(rid) as NUM FROM cash_requisitions WHERE reqstatus = '".$status."' ".$cond1.$cond2.$cond3; //changed 
	  
 // --------------------------------------------------------------
  mysqli_select_db(DB, $CON);
  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve pending count - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
// --------------------------------------------------------------
  return $rowuser['NUM'];
}

function getauthupperlimit($company, $auth, $chain) {
	// --------------------------------------------------------------
  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
	// --------------------------------------------------------------
  $authorisers = "SELECT authperson, upperlimit FROM authorizers WHERE  com_id = '".$company."' AND  authperson = '".$auth."' AND chainname = '".$chain."'";
	// --------------------------------------------------------------
  mysqli_select_db(DB, $CON);
  $authdone = mysqli_query($authorisers, $CON) or die("ERROR:- ".mysqli_error());
  $rowauthorisers = mysqli_fetch_assoc($authdone);
  $countauth=mysqli_num_rows($authdone);
  return $rowauthorisers['upperlimit'];
}

function getauthlowerlimit($company, $auth, $upperlimit) {
	// --------------------------------------------------------------
  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
	// --------------------------------------------------------------
  $authorisers = "SELECT authperson, upperlimit FROM authorizers WHERE  com_id = '".$company."' AND  upperlimit < '".$upperlimit."' AND authperson <> '".$auth."'  ORDER BY upperlimit DESC";
	// --------------------------------------------------------------
  mysqli_select_db(DB, $CON);
  $authdone = mysqli_query($authorisers, $CON) or die("ERROR:- ".mysqli_error());
  $rowauthorisers = mysqli_fetch_assoc($authdone);
  $countauth=mysqli_num_rows($authdone);
  if ($countauth==0) {
  	return 0;
  } else {
	  if ($rowauthorisers['upperlimit']==0) {
		return 999999999999;
	  } else {
		return $rowauthorisers['upperlimit'];
	  }
	} 
}

function getnextauth($company, $val, $chain) {
	// --------------------------------------------------------------
	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
		// --------------------------------------------------------------
	  $authorisers = "SELECT authperson FROM authorizers WHERE  com_id = '".$company."' AND  upperlimit > '".$val."' AND  chainname = '".$chain."' ORDER BY upperlimit ASC";
	// --------------------------------------------------------------
	  mysqli_select_db(DB, $CON);
	  $authdone = mysqli_query($authorisers, $CON) or die("ERROR:- ".mysqli_error());
	  $rowauthorisers = mysqli_fetch_assoc($authdone);
	  $countauth=mysqli_num_rows($authdone);
  	if ($countauth==0) {
  		return 0;
  	} else {
		return $rowauthorisers['authperson'];
	} 
}

function getNewRequisition($company="") {
	// --------------------------------------------------------------
	// Show the donor based on Donor ID
	// --------------------------------------------------------------
	if ($company=="") {
		$company = $_SESSION['company'];
	}
	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
	// --------------------------------------------------------------
	  $logSQL = "SELECT rid FROM requisitions WHERE authcode = '".$_SESSION['UNQ']."' AND com_id = '".$company."' ORDER BY rid DESC LIMIT 1";
	// --------------------------------------------------------------
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve requisitionID - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	// --------------------------------------------------------------
	  return $rowuser['rid'];
}

function getnewgrnnumber() {
// --------------------------------------------------------------
// Show the donor based on Donor ID
// --------------------------------------------------------------
  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
// --------------------------------------------------------------
  $logSQL = "SELECT  grnno FROM log_itemreceipts ORDER BY grnno DESC LIMIT 1";
// --------------------------------------------------------------
  mysqli_select_db(DB, $CON);
  $done = mysqli_query($logSQL, $CON) or die("ERROR: Generating GRN - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
// --------------------------------------------------------------
	
	return ($rowuser['grnno']+1);	
}

function getlastgrnnumber($userid ) {
// --------------------------------------------------------------
// Show the donor based on Donor ID
// --------------------------------------------------------------
  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
// --------------------------------------------------------------
  $logSQL = "SELECT  recid  FROM log_itemreceipts WHERE userid = '".$userid ."' ORDER BY recid DESC LIMIT 1";
// --------------------------------------------------------------
  mysqli_select_db(DB, $CON);
  $done = mysqli_query($logSQL, $CON) or die("ERROR: Generating GRN - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
// --------------------------------------------------------------
	
	return ($rowuser['recid']);	
}

function getReqDetails($enum) {
	// --------------------------------------------------------------
	// Show the donor based on Donor ID
	// --------------------------------------------------------------
	$CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
	// --------------------------------------------------------------
	$logSQL = "SELECT * FROM requisitions WHERE rid = '".$enum."'"; //changed 17-01-2006
	// --------------------------------------------------------------
	mysqli_select_db(DB, $CON);
	$done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve requisition details - ".mysqli_error());
	$rowuser = mysqli_fetch_assoc($done);
	// --------------------------------------------------------------
	return $rowuser;
}

function getRequisitionItems($enum) {
	// --------------------------------------------------------------
	// Show the donor based on Donor ID
	// --------------------------------------------------------------
	$CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
	// --------------------------------------------------------------
	$logSQL = sprintf("SELECT * FROM reqitems WHERE reqitemid = '%s'", $enum);
	// --------------------------------------------------------------
	mysqli_select_db(DB, $CON);
	$done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve requisitioned items - ".mysqli_error());
	$rowuser = mysqli_fetch_assoc($done);
	// --------------------------------------------------------------
	return $rowuser;
}

function getRequisitionCount($flag, $usr='',$company='',$type='',$authoriser='') {
	// --------------------------------------------------------------
	// Show the donor based on Donor ID
	// --------------------------------------------------------------
	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
	// --------------------------------------------------------------

	switch ($flag) {
	case 'PDG':
	$status = 'Pending Approval';
	break;
	// ---------------------------
	case 'PTB':
	$status = 'Posted To buyer';
	break;
	// ---------------------------
	case 'REJ':
	$status = 'Rejected';
	break;
	// ---------------------------
	case 'ARCH':
	$status = 'Archive';
	break;
	// ---------------------------
	default:
	$status = 'Pending Approval';
	break;
	// ---------------------------
  }
  
  if ($usr!="") {
  	$cond1 = "  AND authcode = '".$usr."'";
  }
  if ($company!="") {
  	$cond2 = " AND com_id = '".$company."'";
  }
   if ($authoriser!="") {
  	$cond3 = " AND  authoriser = '".$authoriser."'";
  }
  
  if ($_SESSION['USERTYPE'] == "Requisitioner") {
  	$where  = " authcode = '".$_SESSION['UNQ']."' AND";
  }
  
  if ($type=="autogenerated") {
	  $logSQL = "SELECT COUNT(rid) as NUM FROM tmp_requisitions WHERE reqstatus = '".$status."'".$cond1.$cond2.$cond3;
  } else {
	  $CC = $_SESSION['costcentercode'];
	  $logSQL = "SELECT COUNT(rid) as NUM FROM requisitions WHERE ".$where."  reqstatus = '".$status."' ".$cond1.$cond2.$cond3; //changed 17-01-2006
  }
  
  //echo $logSQL;
// --------------------------------------------------------------
  mysqli_select_db(DB, $CON);
  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve pending count - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
// --------------------------------------------------------------
  return $rowuser['NUM'];
}

function getauthpendingcount($authoriser, $company, $cashpurchases="" ) {
	$CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
	
	if ($cashpurchases=="") {
		$query_prq = "SELECT requisitions.*,users.fulname FROM requisitions INNER JOIN users ON users.authcode=requisitions.authcode WHERE reqstatus = 'Pending Approval' AND requisitions.com_id = '".$company."' AND requisitions.authoriser= '".$authoriser."'";
	} else {
		$query_prq = "SELECT cash_requisitions.*,users.fulname FROM cash_requisitions INNER JOIN users ON users.authcode=cash_requisitions.authcode WHERE reqstatus = 'Pending Approval' AND cash_requisitions.com_id = '".$company."' AND cash_requisitions.authoriser= '".$authoriser."'";

	}
	mysqli_select_db(DB, $CON);
  	$done = mysqli_query($query_prq, $CON) or die("ERROR: Trying to retrieve pending count - ".mysqli_error());
  	$rowuser = mysqli_fetch_assoc($done);
	$numrows=mysqli_num_rows($done);
// --------------------------------------------------------------
  	return $numrows;
}

function getNewcaseProcequisition() {

// --------------------------------------------------------------
  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
// --------------------------------------------------------------
  $logSQL = "SELECT rid FROM cash_requisitions WHERE authcode = '".$_SESSION['UNQ']."' AND com_id = '".$_SESSION['company']."' ORDER BY rid DESC LIMIT 1";
// --------------------------------------------------------------
  mysqli_select_db(DB, $CON);
  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve requisitionID - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
// --------------------------------------------------------------
  return $rowuser['rid'];
}

function getReqItemsCount($flag) {
// --------------------------------------------------------------
// Show the donor based on Donor ID
// --------------------------------------------------------------
  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
// --------------------------------------------------------------

  switch ($flag) {
  	case 'PDG':
	$status = 'Pending Approval';
	break;
	// ---------------------------
  	case 'PTB':
	$status = 'Posted To buyer';
	break;
	// ---------------------------
  	case 'REJ':
	$status = 'Rejected';
	break;
	// ---------------------------
	case 'lpo':
	$status = 'lpo';
	break;
	// ---------------------------
	case 'PTS':
	$status = 'Posted to Supplier';
	break;
	// ---------------------------
	case 'APPR':
	$status = 'approved lpo';
	break;
	// ---------------------------
	
	case 'REJLPO':
	$status = 'Rejected lpo';
	break;
	// ---------------------------	
	
  	default:
	$status = 'Pending Approval';
	break;
	// ---------------------------
  }
  
  
  if ($_SESSION['USERTYPE'] == "Requisitioner") {
  	$logSQL = "SELECT COUNT(rqid) as NUM FROM `reqitems` INNER JOIN requisitions on requisitions.reqitemid = reqitems.reqitemid WHERE reqitems.itemstatus = '".$status."' AND requisitions.com_id = '".$_SESSION['company']."' AND requisitions.authcode = '".$_SESSION['UNQ']."'";
  } else {
  
  $logSQL = "SELECT COUNT(rqid) as NUM FROM `reqitems` INNER JOIN requisitions on requisitions.reqitemid = reqitems.reqitemid WHERE reqitems.itemstatus = '".$status."' AND requisitions.com_id = '".$_SESSION['company']."'";
  }
// --------------------------------------------------------------
  mysqli_select_db(DB, $CON);
  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve pending count - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
// --------------------------------------------------------------
  return $rowuser['NUM'];
}

function getreqitemcount($supplier,$status) {
// --------------------------------------------------------------
// Show the donor based on Donor ID
// --------------------------------------------------------------
  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
// --------------------------------------------------------------

    $logSQL = "SELECT COUNT(reqitems.rqid) as NUM FROM reqitems WHERE reqitems.itemstatus = '".$status."' AND reqitems.catid = '".$supplier."'";
// --------------------------------------------------------------
  mysqli_select_db(DB, $CON);
  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve pending count - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
// --------------------------------------------------------------
  return $rowuser['NUM'];
}

function getdistinctacceptedlpocount() {
// --------------------------------------------------------------
// Show the donor based on Donor ID
// --------------------------------------------------------------
  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
// --------------------------------------------------------------

    $logSQL = "SELECT COUNT(DISTINCT(serialno)) as NUM FROM acceptedlpo";
// --------------------------------------------------------------
  mysqli_select_db(DB, $CON);
  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve pending count - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
// --------------------------------------------------------------
	$num=$rowuser['NUM'];
	if ($num==0) {
		$num=1;
	} else {
		$num=$num+1;
	}
  return $num;
}

function getsupplierreqitemscount($flag, $com_id="") {
// --------------------------------------------------------------
// Show the donor based on Donor ID
// --------------------------------------------------------------
  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
// --------------------------------------------------------------
list($flag, $com_id) = explode(',',$flag,2);
  switch ($flag) {
  	case 'PDG':
	$status = 'Pending Approval';
	break;
	
  	case 'PTB':
	$status = 'Posted To buyer';
	break;

  	case 'REJ':
	$status = 'Rejected';
	break;

	case 'lpo':
	$status = 'lpo';
	break;

	case 'PTS':
	$status = "Posted to Supplier' OR reqitems.itemstatus = 'Quotation'  OR reqitems.itemstatus = 'Commited stock";
	break;
	
	case 'PENDINGORDER':
	$status = 'Pending order';
	break;

	case 'APPR':
	$status = 'approved lpo';
	break;
	
	case 'REJLPO':
	$status = 'Rejected lpo';
	break;
	
	case 'ACCAPPR':
	$status = "Accounts approval'  OR reqitems.itemstatus = 'Held by accounts' OR reqitems.itemstatus = 'Accounts invoicing";

	break;

	case 'APPRACC':
	$status = 'Approved by accounts';
	break;
	
	case 'REJORD':
	$status = 'Rejected by accounts';
	break;
	
	case 'SALESSUPAPPR':
	$status = 'Order awaiting supervisor approval';
	break;
	
	
  	default:
	$status = '';
	break;
	// ---------------------------
  }
  
  if ($com_id!="") {
  $logSQL = "SELECT  DISTINCT reqitems.rqid as NUM FROM `reqitems` INNER JOIN requisitions on requisitions.reqitemid = reqitems.reqitemid WHERE (reqitems.itemstatus = '".$status."') AND (requisitions.reqsupplierid IN 
  (SELECT supercatalogue.cid FROM supercatalogue WHERE supercatalogue.suppid = '".$_SESSION['company']."') AND requisitions.com_id = '".$com_id."')";
  } else {
  
  $logSQL = "SELECT  DISTINCT reqitems.rqid as NUM FROM `reqitems` INNER JOIN requisitions on requisitions.reqitemid = reqitems.reqitemid WHERE (reqitems.itemstatus = '".$status."') AND (requisitions.reqsupplierid IN 
  (SELECT supercatalogue.cid FROM supercatalogue WHERE supercatalogue.suppid = '".$_SESSION['company']."'))";
  }
// --------------------------------------------------------------

  mysqli_select_db(DB, $CON);
  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve pending count - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
  $count=mysqli_num_rows($done);
// --------------------------------------------------------------
  return $count;//$rowuser['NUM'];
}

function getReqAmount($enum) {
// --------------------------------------------------------------
// Show the donor based on Donor ID
// --------------------------------------------------------------
  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
// --------------------------------------------------------------
  $logSQL = "SELECT SUM(total) as TOT FROM reqitems WHERE reqitemid = '".$enum."'";
// --------------------------------------------------------------
  mysqli_select_db(DB, $CON);
  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve requisition total amount - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
// --------------------------------------------------------------
  return $rowuser['TOT'];
}

function getlposeqnum() {
	//Get lpo number
	$CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	$query_lpo = "SELECT MAX(sequentialno) as rid FROM lpo";
	mysqli_select_db(DB, $CON);		
	$lpoprq = mysqli_query($query_lpo, $CON) or die(mysqli_error());
	$lporow_prq = mysqli_fetch_assoc($lpoprq);
	$lposeqNum = $lporow_prq['rid']	;
	$lposeqNum = $lposeqNum  + 1;
	return $lposeqNum;
}

?>