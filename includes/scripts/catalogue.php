<?php

function getitemqty($enum,$wid='') {
// --------------------------------------------------------------
// Show the donor based on Donor ID
// --------------------------------------------------------------
if ($wid=='') {

	$wid = getstoremanWarehouseID($_SESSION['UNQ']);
	//echo $_SESSION['UNQ'];
	//echo "warehouse id  here"; 
}

  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
// --------------------------------------------------------------
 $logSQL = "SELECT quantity FROM catalogue_warehouse_access WHERE  lineitem_id = ".$enum." AND warehouseid = ".$wid;


  mysqli_select_db(DB, $CON);
  $done = mysqli_query($logSQL, $CON) or die("ERROR: ".$logSQL.mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
// --------------------------------------------------------------

	if ($rowuser['quantity']=="") {
		return 0;
	} else {
 		 return $rowuser['quantity'];
	}
}

function getprodidfrompartno($enum) {
// --------------------------------------------------------------
// Show the donor based on Donor ID
// --------------------------------------------------------------
  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
// --------------------------------------------------------------
  $logSQL = "SELECT prodid FROM catalogue WHERE  partno = '".$enum."'";
// --------------------------------------------------------------
  mysqli_select_db(DB, $CON);
  $done = mysqli_query($logSQL, $CON) or die("ERROR:".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
// --------------------------------------------------------------
  return $rowuser['prodid'];
}

function logstockqtychange($userauthcode, $stockitemid, $oldqty, $newqty, $action, $wid) {
	//log any changes made to stock quantity in store
	$CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	mysqli_select_db(DB, $CON);
	$sqllog = "INSERT INTO stock_adj_log (userauthcode, stockitemid, warehouseid, oldqty, newqty, action) VALUES ('".$userauthcode."', ".$stockitemid.",".$wid.",".$oldqty.",".$newqty.",'".$action."')";
	 $done = mysqli_query($sqllog, $CON) or die("ERROR: Trying to log stock change - ".mysqli_error());
	 return ;
	
}

function pricechanged($partno,$value, $company, $catalogueid) {
	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
	// --------------------------------------------------------------
	  $logSQL = "SELECT price FROM company_catalogue_prices WHERE partno = '".$partno."' AND com_id = '".$company."' AND price = ".$value." AND catalogue_id = '".$catalogueid."'";
	// --------------------------------------------------------------
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die( $logSQL);
	  //$rowuser = mysqli_fetch_assoc($done);
	  $num_rows = mysqli_num_rows($done); //Get number of rows
	// --------------------------------------------------------------
	$rTrue = TRUE;
	$rFalse = FALSE;
	if ($num_rows < 1) {
	   return $rTrue;
	} else {
	   return $rFalse;
	}
}

function addwarehousecatalogueitem($tmpitem="0", $receipts_ref="0", $warehouseid, $catid, $partno, 	$lineitem_id, $qty="0") {
	// --------------------------------------------------------------
  	$CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
	// --------------------------------------------------------------
	$updateSQL = "INSERT INTO catalogue_warehouse_access (tmpitem, receipts_ref, warehouseid, catid, partno, lineitem_id, quantity) VALUES (".$tmpitem.", ".$receipts_ref.", '".$warehouseid."', '".$catid."', '".$partno."', ".$lineitem_id.", ".$qty.")";
	mysqli_select_db(DB, $CON);
  	$done = mysqli_query($updateSQL, $CON) or die("ERROR: ITEM COULD NOT BE ADDED - ".mysqli_error());
	
}

function cleanuptmpwarehousecatalogueitem($wh="") {
	// --------------------------------------------------------------
  	$CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
	// --------------------------------------------------------------
	if ($wh!="") {
		$where = " AND warehouseid = ".$wh;
	}
	$updateSQL = "DELETE from catalogue_warehouse_access WHERE tmpitem =1 AND quantity <= 0 ".$where;
	mysqli_select_db(DB, $CON);
  	$done = mysqli_query($updateSQL, $CON) or die("ERROR: ITEM COULD NOT BE DELETED - ".mysqli_error());
	
}

function getcatalogueprice($catid, $pricetype) {
  	$CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
	// --------------------------------------------------------------

    $sql = "";

	// --------------------------------------------------------------
	mysqli_select_db(DB, $CON);
	$done = mysqli_query($logSQL, $CON) or die(mysqli_error());
	$rowuser = mysqli_fetch_assoc($done);
	$numrows=mysqli_num_rows($done);
	// --------------------------------------------------------------
	return $numrows; 
}

function getcataloguecount($flag) {
  	$CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
	// --------------------------------------------------------------

    $logSQL = "SELECT DISTINCT company_catalogue_prices.com_id, company_catalogue_prices.catalogue_id, supercatalogue.suppid, supercatalogue.cid FROM company_catalogue_prices inner join supercatalogue on supercatalogue.cid = company_catalogue_prices.catalogue_id WHERE  company_catalogue_prices.com_id = '".$_SESSION[company]."'  AND company_catalogue_prices.approved = '".$flag."' AND supercatalogue.internalcat = 0 ORDER BY company_catalogue_prices.com_id";

	// --------------------------------------------------------------
	mysqli_select_db(DB, $CON);
	$done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve type - ".mysqli_error());
	$rowuser = mysqli_fetch_assoc($done);
	$numrows=mysqli_num_rows($done);
	// --------------------------------------------------------------
	return $numrows; 
}

function getcataloguetype($cid) {
  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
// --------------------------------------------------------------

    $logSQL = "SELECT internalcat FROM supercatalogue WHERE cid = ".$cid;
// --------------------------------------------------------------
  mysqli_select_db(DB, $CON);
  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve catalogue type - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
 
// --------------------------------------------------------------
  return $rowuser['internalcat']; //$rowuser['NUM'];
}

function checkpricechanged($catid,$company) {
// --------------------------------------------------------------
// Show the donor based on Donor ID
// --------------------------------------------------------------
  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
// --------------------------------------------------------------

    $logSQL = "SELECT COUNT(partno) as NUM FROM company_catalogue_prices WHERE com_id = '".$company."' AND approved = 'y' AND priceapproved='n' AND  catalogue_id = '".$catid."'";
// --------------------------------------------------------------
  mysqli_select_db(DB, $CON);
  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve price changes - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
// --------------------------------------------------------------
	if ($rowuser['NUM']>0) {
		return TRUE;
	} else {
		return FALSE;
	}
  
}

function checkcatapproved($flag , $catid) {
// --------------------------------------------------------------
// Show the donor based on Donor ID
// --------------------------------------------------------------
  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
// --------------------------------------------------------------

    $logSQL = "SELECT COUNT(approved) as approved FROM company_catalogue_prices WHERE com_id = '".$flag."' AND approved = 'y' AND catalogue_id = '".$catid."'";
// --------------------------------------------------------------
  mysqli_select_db(DB, $CON);
  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to get approved status- ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
  $numrows=mysqli_num_rows($done);
// --------------------------------------------------------------
	if ($rowuser['approved'] > 0) {
		return "y";
	} else {
		return "n";
	}
}

function getProductDesc($enum) {
// --------------------------------------------------------------
// Show the donor based on Donor ID
// --------------------------------------------------------------
  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
// --------------------------------------------------------------
  $logSQL = "SELECT productdesc FROM catalogue WHERE partno = '".$enum."'";
// --------------------------------------------------------------
  mysqli_select_db(DB, $CON);
  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve user details - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
// --------------------------------------------------------------
  return $rowuser['productdesc'];
}

function getProductDetails($prid) {
// --------------------------------------------------------------
  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
// --------------------------------------------------------------
  $logSQL = "SELECT catalogue.*,company_catalogue_prices.price as saleprice FROM catalogue INNER JOIN company_catalogue_prices ON catalogue.catid = company_catalogue_prices.catalogue_id AND catalogue.partno = company_catalogue_prices.partno WHERE catalogue.partno = '".$prid."'";
// --------------------------------------------------------------
  mysqli_select_db(DB, $CON);
  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve product details - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
// --------------------------------------------------------------
  return $rowuser;
}

function getsuppliercatdetails($prid, $company = "") {
// --------------------------------------------------------------
  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
// --------------------------------------------------------------
if ($company!="") {
	$where = "WHERE catalogue.partno = '$prid' AND company_catalogue_prices.com_id = '$company'";
} else {
	$where = "WHERE catalogue.partno = '$prid' AND company_catalogue_prices.com_id = '".$_SESSION['company']."'";
}

  $logSQL = "SELECT catalogue.*, company_catalogue_prices.listprice FROM catalogue INNER JOIN company_catalogue_prices ON company_catalogue_prices.partno = catalogue.partno AND company_catalogue_prices.catalogue_id = catalogue.catid $where";
// --------------------------------------------------------------
  mysqli_select_db(DB, $CON);
  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve product details - ".mysqli_error().$logSQL);
  $rowuser = mysqli_fetch_assoc($done);
// --------------------------------------------------------------
  return $rowuser;
}

function getProductName($prid) {
// --------------------------------------------------------------
  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
// --------------------------------------------------------------
  $logSQL = "SELECT * FROM catalogue WHERE partno = '".$prid."'";
// --------------------------------------------------------------
  mysqli_select_db(DB, $CON);
  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve product name - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
// --------------------------------------------------------------
  return $rowuser['productname'];
}

function getCatName($enum) {
	$CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
	// --------------------------------------------------------------
	$logSQL = "SELECT catname FROM supercatalogue WHERE cid = '".$enum."'";
	// --------------------------------------------------------------
	mysqli_select_db(DB, $CON);
	$done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve catalogue name - ".mysqli_error());
	$rowuser = mysqli_fetch_assoc($done);
	
	return $rowuser['catname'];
}

function getCatSupplierName($enum) {
// --------------------------------------------------------------
// Show the donor based on Donor ID
// --------------------------------------------------------------
  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
// --------------------------------------------------------------
  $logSQL = "SELECT suppid FROM supercatalogue WHERE cid = '".$enum."'";
// --------------------------------------------------------------
  mysqli_select_db(DB, $CON);
  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve catalogue name - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
// --------------------------------------------------------------
  return getcompany($rowuser['suppid']);
}

function getCatSupplierID($enum) {
  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
// --------------------------------------------------------------
  $logSQL = "SELECT suppid FROM supercatalogue WHERE cid = '".$enum."'";
// --------------------------------------------------------------
  mysqli_select_db(DB, $CON);
  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve catalogue name - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
// --------------------------------------------------------------
  return $rowuser['suppid'];
}

function getcatdiscount($customertypeid, $catid, $value) {
	$CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
	// --------------------------------------------------------------
	$logSQL = "SELECT discount FROM customer_discounts WHERE customer_type_id = $customertypeid AND catid = $catid AND discountvalue <= $value";
	// --------------------------------------------------------------
	mysqli_select_db(DB, $CON);
	$done = mysqli_query($logSQL, $CON) or die("Discount calculation error: ".mysqli_error().$logSQL);
	$rowuser = mysqli_fetch_assoc($done);
	// --------------------------------------------------------------
	if (trim($rowuser['discount'])=="") {
		return 0;
	} else {
		return $rowuser['discount'];
	}
}

function getvolumediscount($supplierid, $customertypeid) {
	$CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
	// --------------------------------------------------------------
	$logSQL = "SELECT volumediscount FROM customer_types WHERE comid = '$supplierid' AND customer_type_id = '$customertypeid'";
	// --------------------------------------------------------------
	mysqli_select_db(DB, $CON);
	$done = mysqli_query($logSQL, $CON) or die(mysqli_error());
	$rowuser = mysqli_fetch_assoc($done);
	// --------------------------------------------------------------
	if (trim($rowuser['volumediscount'])=="") {
		return 0;
	} else {
		return $rowuser['volumediscount'];
	}
}

function getProduct($partno) {
// --------------------------------------------------------------
  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
// --------------------------------------------------------------
  $logSQL = "SELECT productname FROM catalogue WHERE partno = '".$partno."'";
// --------------------------------------------------------------
  mysqli_select_db(DB, $CON);
  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve product name - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
// --------------------------------------------------------------
  return $rowuser['productname'];
}

function getProductpartno($lineitemid) {
// --------------------------------------------------------------
  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
// --------------------------------------------------------------
  $logSQL = "SELECT partno FROM catalogue WHERE prodid = ".$lineitemid;
// --------------------------------------------------------------
  mysqli_select_db(DB, $CON);
  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve part no. - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
// --------------------------------------------------------------
  return $rowuser['partno'];
}

function retCatID($comid, $rid) {
// --------------------------------------------------------------
  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
// --------------------------------------------------------------
  $logSQL = "SELECT * FROM supercatalogue WHERE suppid = '".$rid."'";
// --------------------------------------------------------------
  mysqli_select_db(DB, $CON);
  $done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve category ID - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
// --------------------------------------------------------------
  return $rowuser['cid'];
}

function showCatalogs($company="", $internal="") {
	// --------------------------------------------------------------
	// Show the users from the user database
	// --------------------------------------------------------------
	  $CON = mysqli_pconnect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
	  $where="";
	  if ($company!="") {
		$where = " WHERE suppid = '".$company."'";
		if  ($internal!="") {
			$where = $where." AND internalcat = ".$internal;
		}
	  } 
	  
	  $logSQL = "SELECT currencyid FROM currencyadmin";
		mysqli_select_db(DB, $CON);
		$done = mysqli_query($logSQL, $CON) or die("ERROR: Trying to retrieve suppliers list - ".mysqli_error());
		  $rowuser = mysqli_fetch_assoc($done);
		  $nums = mysqli_num_rows($done);
		  if ($nums > 0) {
			  do {
				echo "<option value=\"".$rowuser['currencyid']."\"> All (".$rowuser['currencyid'].")</option>";
			  } while($rowuser = mysqli_fetch_assoc($done));
			}
	  
	// --------------------------------------------------------------
	  $logSQL = "SELECT * FROM supercatalogue".$where;
	// --------------------------------------------------------------
	  mysqli_select_db(DB, $CON);
	  $done = mysqli_query($logSQL, $CON) or die("ERROR trying to retrieve catalogues - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
	// --------------------------------------------------------------
		if ($nums > 0) {
		  do {
				echo "<option value=\"".$rowuser['cid']."\">".$rowuser['catname']."</option>";
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No catalogue Selected</option>";
		}
	}
	
?>
