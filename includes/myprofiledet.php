<?php require_once('../connections/eProc.php'); ?>
<?php

$colname_up = "1";
if (isset($_SESSION['UNQ'])) {
  $colname_up = (get_magic_quotes_gpc()) ? $_SESSION['UNQ'] : addslashes($_SESSION['UNQ']);
}
mysqli_select_db($eProc, $database_eProc);
if ($_SESSION['company']=="SYSTEM ADMINISTRATOR") {
	$query_up = sprintf("SELECT * FROM adminlogin WHERE authcode = '%s'", $colname_up);
} else {
	$query_up = sprintf("SELECT * FROM users WHERE authcode = '%s'", $colname_up);
}

$up = mysqli_query($eProc, $query_up) or die(mysqli_error());
$row_up = mysqli_fetch_assoc($up);
$totalRows_up = mysqli_num_rows($up);
?>

<table width="100%"  border="0" cellspacing="0" cellpadding="4">
                    <tr valign="top"> 
                    
                      <td><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                        <tr valign="middle">
                          <td width="63%" class="hOne">My Profile </td>
                          <td width="37%" align="right" nowrap class="baseline white">last Updated: </td>
                        </tr>
                      </table>
                        <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td valign="top" class="baseline">Use this section to modify your profile and login information.</td>
                          </tr>
                          <tr>
                            <td valign="top" class="spacer5"><table width="350" border="0" cellpadding="5" cellspacing="0" class="tblbdr">
                              <tr valign="top">
                                <td align="right"><strong>Login Name: </strong></td>
                                <td><span class="style2"><?php echo $row_up['username']; ?></span></td>
                              </tr>
                              <tr valign="top">
                                <td width="98" align="right"><strong>Full Name: </strong></td>
                                <td width="230"><span class="style2"><?php echo $row_up['fulname']; ?></span></td>
                              </tr>
                              <tr valign="top">
                                <td colspan="2" align="right"><eProc size="1" class="style1"></td>
                              </tr>
                             
                              <tr valign="top">
                                <td align="right"><strong>Access Role: </strong></td>
                                <td><?php echo getGroup($row_up['groupid']); ?></td>
                              </tr>
                              <tr valign="top">
                                <td colspan="2" align="right"><eProc size="1"></td>
                              </tr>
                              <tr valign="top">
                                <td align="right"><strong>Work Tel:</strong></td>
                                <td><?php echo $row_up['worktel']; ?></td>
                              </tr>
                              <tr valign="top">
                                <td align="right"><strong>Home Tel: </strong></td>
                                <td><?php echo $row_up['hometel']; ?></td>
                              </tr>
                              <tr valign="top">
                                <td align="right"><strong>Mobile:</strong></td>
                                <td><?php echo $row_up['mobtel']; ?></td>
                              </tr>
                              <tr valign="top">
                                <td colspan="2" align="right"><eProc size="1"></td>
                              </tr>
                              <tr valign="top">
                                <td align="right"><strong>email:</strong></td>
                                <td><a href="mailto:<?php echo $row_up['email']; ?>"><?php echo $row_up['email']; ?></a></td>
                              </tr>
                            </table>
                            <p>
                              <input name="button" type="button" class="formsorg" onClick="javascript:location='<?php echo "../".$_SESSION['HOME'] ?>'" value="Cancel">
                            </p></td>
                          </tr>
                        </table></td>
                    </tr>
                </table>