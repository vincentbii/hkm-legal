<?php require_once('../connections/eProc.php');
include('../activelog.php');
?>
<html>
<head>
<title>Management Information System </title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../styles/default.css" rel="stylesheet" type="text/css">
</head>
<body>
<table width="100%"  border="2" cellpadding="0" cellspacing="3" bordercolor="#0A6EC3" bgcolor="#FFFFFF">
  <tr>
    <td valign="top"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td valign="top"><?php include('header.php'); ?></td>
      </tr>
      <tr>
          <td valign="top"><img src="../images/template_06.gif" width="100%" height="6"></td>
      </tr>
      <tr>
        <td valign="top"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="100%" class="spacer5"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
            	<tr>
                    <td width="95%" class="tmenuextra"><div align="left">User Administration</div></td>

                    <td width="5%" class="tmenuextra">&nbsp;</td>
                  </tr>
              <tr>
                <td colspan="4" class="intspace"><table width="100%"  border="0" cellspacing="0" cellpadding="4">
                  <tr valign="top">
                    <td><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                        <tr valign="middle">
                          <td class="hOne">&nbsp;</td>
                            </tr>
                      </table>                      <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                        <tr>
							 <td width="23%" valign="top"><?php include('profmenu.php') ?></td>
                          <td width="77%" valign="top" class="baseline"><?php include('editprofile.php') ?></td>
                            </tr>
                        
                      </table></td>
                    </tr>
                </table></td>
              </tr>
            </table></td>
            </tr>
        </table></td>
      </tr>
      <tr>
        <td ></td>
      </tr>
    </table></td>
  </tr>
</table>
</body>
</html>