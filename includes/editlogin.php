<?php require_once('../connections/eProc.php'); ?>
<?php

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
  if ($_POST['umd5']==$_POST['CUMD5']) {
	  $UNM = $_POST['username'];
	  $PWD = md5($_POST['umd5']);
	  $PIN = md5($_POST['pin']);
	  
	  if ($_SESSION['company']=="SYSTEM ADMINISTRATOR") {
		  $updateSQL = sprintf("UPDATE adminlogin SET  umd5=%s WHERE authcode=%s",
							   GetSQLValueString(md5($_POST['umd5']), "text"),
							  
							   GetSQLValueString($_POST['authcode'], "text"));
	  } else {
		  $updateSQL = sprintf("UPDATE users SET umd5=%s WHERE authcode=%s",
							   GetSQLValueString(md5($_POST['umd5']), "text"),
							 
							   GetSQLValueString($_POST['authcode'], "text"));
	}   
	  mysqli_select_db($eProc, $database_eProc);
	  $Result1 = mysqli_query($eProc, $updateSQL) or die("ERROR UPDATING USER LOGIN CREDENTIALS - ".mysqli_error());
	  
	  // UPDATE SESSION details
	  // -----------------------------------------------------
	  $_SESSION['USER'] = $_POST['username'];
	  // -----------------------------------------------------
	
	  $updateGoTo = "myprofile.php";
	  if (isset($_SERVER['QUERY_STRING'])) {
		$updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
		$updateGoTo .= $_SERVER['QUERY_STRING'];
	  }
				//go to index page
				?>
			<SCRIPT LANGUAGE=JAVASCRIPT>
			{
				javascript:history.go(-1);
			}
			</SCRIPT>	<?php 
	} else { ?>
		<SCRIPT LANGUAGE=JAVASCRIPT>
			{
				alert("Passwords don't match");
			}
			</SCRIPT>	<?php 
	}
}

$colname_up = "1";
if (isset($_SESSION['UNQ'])) {
  $colname_up = (get_magic_quotes_gpc()) ? $_SESSION['UNQ'] : addslashes($_SESSION['UNQ']);
}
mysqli_select_db($eProc, $database_eProc);
if ($_SESSION['company']=="SYSTEM ADMINISTRATOR") {
	$query_up = sprintf("SELECT * FROM adminlogin WHERE authcode = '%s'", $colname_up);
} else {
	$query_up = sprintf("SELECT * FROM users WHERE authcode = '%s'", $colname_up);
}
$up = mysqli_query($eProc, $query_up) or die(mysqli_error());
$row_up = mysqli_fetch_assoc($up);
$totalRows_up = mysqli_num_rows($up);
?>

<table width="100%"  border="0" align="left" cellpadding="4" cellspacing="0">
                    <tr valign="top"> 
                      <td>
                        
                        <div align="left">
                          <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                            <tr valign="middle">
                              <td width="63%" class="hOne">Modify Login details </td>
                              <td width="37%" align="right" nowrap class="baseline white">last Updated: </td>
                            </tr>
                          </table>
                          <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td valign="top" class="baseline">Use this section to modify your profile and login information.</td>
                            </tr>
                            <tr>
                              <td valign="top" class="spacer5">
                                <form method="post" name="form1" action="<?php echo $editFormAction; ?>">
                                  <table width="350" cellpadding="5" cellspacing="0" class="tblbdr">
                                    <tr valign="baseline">
                                      <td nowrap align="right">username:</td>
                                      <td><?php echo $row_up['username']; ?></td>
                                    </tr>
                                    <tr valign="baseline">
                                      <td nowrap align="right">Password:</td>
                                      <td><input name="umd5" type="password" class="forms" value="" size="32"></td>
                                    </tr>
                                    <tr valign="baseline">
                                      <td nowrap align="right">Confirm Password:</td>
										<td><input name="CUMD5" class="forms" type="password" id="CUMD5" size="32" />
                                    </tr>
									
                                    <tr valign="baseline">
                                      <td colspan="2" align="right" nowrap><eProc size="1"></td>
                                    </tr>
                                    <tr valign="baseline">
                                      <td nowrap align="right"><input type="hidden" name="MM_update" value="form1">
                                        <input name="authcode" type="hidden" id="authcode" value="<?php echo $row_up['authcode']; ?>"></td>
                                      <td><input type="submit" class="formsBlue" value="Modify Login details">
                                      <input name="button" type="button" class="formsorg" onClick="javascript:location='<?php echo "../".$_SESSION['HOME'] ?>'" value="Cancel"></td>
                                    </tr>
                                  </table>
                                  <div align="left"></div>
                                </form>                              </td>
                            </tr>
                          </table>
                      </div></td>
                    </tr>
</table>

<?php
mysqli_free_result($up);
?>
