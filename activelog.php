<SCRIPT TYPE="text/javascript">
<!--
function popupform(myform, windowname) {
	if (! window.focus)return true;
	window.open('', windowname, 'width=600,left=200,top=200');
	myform.target=windowname;
	return true;
}

function OpenPopup(myform, windowname, height, windth) {
	if (! window.focus)return true;
	window.open('', windowname, 'height=height,width=width,left=400,top=200');
	myform.target=windowname;
	return true;
}
-->

/**
 * DHTML date validation script. Courtesy of SmartWebby.com (http://www.smartwebby.com/dhtml/)
 */
// Declaring valid date character, minimum year and maximum year
var dtCh= "/";
var minYear=1900;
var maxYear=2100;

function isInteger(s){
	var i;
    for (i = 0; i < s.length; i++){
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    // All characters are numbers.
    return true;
}

function stripCharsInBag(s, bag){
	var i;
    var returnString = "";
    // Search teProcough string's characters one by one.
    // If character is not in bag, append to returnString.
    for (i = 0; i < s.length; i++){
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
}

function daysInFebruary (year){
	// February has 29 days in any year evenly divisible by four,
    // EXCEPT for centurial years which are not also divisible by 400.
    return (((year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28 );
}
function DaysArray(n) {
	for (var i = 1; i <= n; i++) {
		this[i] = 31
		if (i==4 || i==6 || i==9 || i==11) {this[i] = 30}
		if (i==2) {this[i] = 29}
   }
   return this
}

function isDate(dtStr){
	var daysInMonth = DaysArray(12)
	var pos1=dtStr.indexOf(dtCh)
	var pos2=dtStr.indexOf(dtCh,pos1+1)
	var strMonth=dtStr.substring(0,pos1)
	var strDay=dtStr.substring(pos1+1,pos2)
	var strYear=dtStr.substring(pos2+1)
	strYr=strYear
	if (strDay.charAt(0)=="0" && strDay.length>1) strDay=strDay.substring(1)
	if (strMonth.charAt(0)=="0" && strMonth.length>1) strMonth=strMonth.substring(1)
	for (var i = 1; i <= 3; i++) {
		if (strYr.charAt(0)=="0" && strYr.length>1) strYr=strYr.substring(1)
	}
	month=parseInt(strMonth)
	day=parseInt(strDay)
	year=parseInt(strYr)
	if (pos1==-1 || pos2==-1){
		alert("The date format should be : mm/dd/yyyy")
		return false
	}
	if (strMonth.length<1 || month<1 || month>12){
		alert("Please enter a valid month")
		return false
	}
	if (strDay.length<1 || day<1 || day>31 || (month==2 && day>daysInFebruary(year)) || day > daysInMonth[month]){
		alert("Please enter a valid day")
		return false
	}
	if (strYear.length != 4 || year==0 || year<minYear || year>maxYear){
		alert("Please enter a valid 4 digit year between "+minYear+" and "+maxYear)
		return false
	}
	if (dtStr.indexOf(dtCh,pos2+1)!=-1 || isInteger(stripCharsInBag(dtStr, dtCh))==false){
		alert("Please enter a valid date")
		return false
	}
return true
}

function ValidateForm(){
	var dt=document.frmSample.txtDate
	if (isDate(dt.value)==false){
		dt.focus()
		return false
	}
    return true
 }

function round_decimals(original_number, decimals) {
    var result1 = original_number * Math.pow(10, decimals)
    var result2 = Math.round(result1)
    var result3 = result2 / Math.pow(10, decimals)
    return pad_with_zeros(result3, decimals)
}

function pad_with_zeros(rounded_value, decimal_places) {

    // Convert the number to a string
    var value_string = rounded_value.toString()

    // Locate the decimal point
    var decimal_location = value_string.indexOf(".")

    // Is there a decimal point?
    if (decimal_location == -1) {

        // If no, then all decimal places will be padded with 0s
        decimal_part_length = 0

        // If decimal_places is greater than zero, tack on a decimal point
        value_string += decimal_places > 0 ? "." : ""
    }
    else {

        // If yes, then only the extra decimal places will be padded with 0s
        decimal_part_length = value_string.length - decimal_location - 1
    }

    // Calculate the number of decimal places that need to be padded with 0s
    var pad_total = decimal_places - decimal_part_length

    if (pad_total > 0) {

        // Pad the string with 0s
        for (var counter = 1; counter <= pad_total; counter++)
            value_string += "0"
        }
    return value_string
}



</SCRIPT>
<?php
// Report all errors except E_NOTICE
error_reporting(E_ALL ^ E_NOTICE);


// DATABASE CONNECTION
// -----------------------
define("HOSTNAME","localhost");
define("DB","legal");
define("username","root");
define("PWD","b$9bnFCa-1z,");
//define("PWD","finlay001","true");

// ------------------------------------------------------------------------------
// AUXILLIARY FUNCTIONS
// ------------------------------------------------------------------------------
// DATE FUNCTION
$dt = getdate();
$date = sprintf("%s-%s-%s", $dt['year'], $dt['mon'], $dt['mday']);
$long_date = sprintf("%s %s, %s", $dt['month'], $dt['mday'], $dt['year']);
$time = sprintf("%s:%s:%s", $dt['hours'], $dt['minutes'], $dt['seconds']);
// ------------------------------------------------------------------------------
// MONTH ARRAY
// ------------------------------------------------------------------------------
		$MONTH = array(1=>'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
		$DAY = array(1=>'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');
// ------------------------------------------------------------------------------
//Includes for functions not on this file but required
include('includes/scripts/user.php');
include('includes/scripts/requisitions.php');
include('includes/scripts/catalogue.php');
include_once('includes/ean13class.php');
//--------------------------------------------------------------------
function log_activity($uid, $action, $section, $act) {
	// --------------------------------------------------------------
	// FUNCTION FOR LOGGING USER ACTIVITY WITHIN THE DONOR DATABASE
	// --------------------------------------------------------------
	  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	// --------------------------------------------------------------
	  $dt = getdate(); $ts = time();
	  $date = sprintf("%s-%s-%s", $dt['year'], $dt['mon'], $dt['mday']);
	  $time = sprintf("%s:%s:%s", $dt['hours'], $dt['minutes'], $dt['seconds']);
	  $logSQL = "INSERT INTO activelog (uid, action, adate, atime, section, ACT, TSTAMP) VALUES ('". $uid ."','". $action ."','". $date ."','". $time ."','". $section ."','". $act ."','". $ts ."')";
	// --------------------------------------------------------------
	  mysqli_select_db($CON, DB);
	  $done = mysqli_query($CON, $logSQL) or die("LOG ERROR: Trying to save trackable user activity - ".mysqli_error());
	// --------------------------------------------------------------
		return $done;
	}

function evalNull($value, $nullmsg, $msg) {
	if (is_null($value) || $value == "") {
		$message = $nullmsg;
	} else {
		$message = $msg;
	}
	return $message;
}

// This function writes the Javascript url onto the page
// Important when you need PHP generated urls to use!
function addJavascript($url) {
	echo '<script language="javascript" src="'.$url.'"></script>';
}

function assessVAL($val) {
	if ($val == 1) {
		$ret = "YES";
	} else {
		$ret = "NO";
	}
	return $ret;
}

function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
	{
	  $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

	  switch ($theType) {
		case "text":
		  $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
		  break;
		case "long":
		case "int":
		  $theValue = ($theValue != "") ? intval($theValue) : "NULL";
		  break;
		case "double":
		  $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
		  break;
		case "date":
		  $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
		  break;
		case "defined":
		  $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
		  break;
	  }
	  return $theValue;
}

function make_time($day, $mon, $year, $time) {
	// Break time to hours and minutes
	// ------------------------------------
	list($eProc, $min) = explode(':', $time);
	// ------------------------------------
	$timestamp = mktime($eProc, $min, 0 ,$mon, $day, $year);

	return $timestamp;
}
// --------------------------------------------------------------------------------
// Function to process timestamps and return date
// --------------------------------------------------------------------------------
function process_time($ts, $form) {

	if ($ts == '') { $ts = time(); }

	$pdt = getdate($ts);
	$pdate = sprintf("%s-%s-%s", $pdt['year'], $pdt['mon'], $pdt['mday']);
	$pshort_date = sprintf("%s-%s-%s", $pdt['year'], $pdt['month'], $pdt['mday']);
	$pshorter_date = sprintf("%s-%s-%s", $pdt['year'], substr($pdt['month'], 0, 3), $pdt['mday']);
	$plong_date = sprintf("%s %s, %s", $pdt['month'], $pdt['mday'], $pdt['year']);
	$ptime = sprintf("%s:%s", $pdt['hours'], $pdt['minutes']);
	$pdate_time = $pdate."&nbsp;&nbsp; ".$ptime;
	$pdate_time =  date("Y-m-d H:i:s");

	switch ($form) {
		case '1':
		$FORMATTED_DATE = $pdate;
		break;

		case '2':
		$FORMATTED_DATE = $pshort_date;
		break;

		case '3':
		$FORMATTED_DATE = $plong_date;
		break;

		case '4':
		$FORMATTED_DATE = $pdate_time;
		break;

		case '5':
		$FORMATTED_DATE = $pshorter_date;
		break;

		default:
		$FORMATTED_DATE = $pdate;
		break;
	}

	return $FORMATTED_DATE;
}

function recordexists($tablename,$columnname,$value) {
	  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
  
	 	 $logSQL = "SELECT ".$columnname." FROM ".$tablename." WHERE ".$columnname." = '".$value."'";
	  mysqli_select_db($CON, DB);
	  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to check record exists - ".mysqli_error(). $logSQL);
	  //$rowuser = mysqli_fetch_assoc($done);
	  $num_rows = mysqli_num_rows($done); //Get number of rows
	if ($num_rows > 0) {
	   return 'yes';
	} else {
	   return 'no';
	}
}



function getEndValue($rowuser) {
	end($rowuser);
	$value = current($rowuser);
	return $value;
}

function getStartValue($rowuser) {
	reset($rowuser);
	$value = current($rowuser);
	return $value;
}



function remFunnyChars($DIRTY) {
	$CLEAN = str_replace('/','',$DIRTY);
	$CLEAN = str_replace('\\','',$CLEAN);
	$CLEAN = str_replace(' ',',',$CLEAN);
	return $CLEAN;
}

// -------------------------------------------------------------------------
// Create name hash for unique identification of event
// -------------------------------------------------------------------------
function createHASH($name = '') {

	// RANDOM ARRAY FEED
	//-----------------------------------------------------------------
	$alpha = array('a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z');
	$numer = array('0','1','2','3','4','5','6','7','8','9');
	// ---------------------------------------------------------------------------------------------------------------------

	$word = explode(" ",$name);
	$new_sen = "";

	// Happens only when name is not sent

	if ($name == '') {
	// Create Random String
	for ($m = 1; $m <= 10; $m++) {
		$id = mt_rand(1,count($alpha));
		$id2 = mt_rand(1,count($numer));
		// Append to string
		
		$new_sen .= $alpha[$id].$numer[$id2];
			}
	} else {

	$rand = rand(12345, 99999);
		for ($i = 1; $i <= count($word); $i++) {
			if ($i == 1) {
				$new_sen .= $rand;
				$new_sen .= $word[$i];
			} else {
				$new_sen .= $word[$i];
			}
		}
	}
	return strtoupper($new_sen);
	}

function rem_space($name) {
	$word = explode(" ",$name);
	$new_sen = substr_replace($name,"",0,1);
	$new_sen = substr_replace($name,"",strlen($name),-1);
	
	return strtoupper($new_sen);
}

function listShow($divs, $table, $main, $id, $url = '') {

	  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT ".$main.",".$id." FROM ".$table;
	  mysqli_select_db($CON, DB);
	  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve data - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $tot_rows = mysqli_num_rows($done);
	  $dist = floor($tot_rows / $divs);
	  $d_mod = $tot_rows % $divs;
	  $div = $divs;
	  $c = array();
	  // for ($x = 1; $x <= $div; $x++) { $c[$x] = $dist; }
	  for ($x = 1; $x <= $div; $x++) { $c[$x] = $dist; }
	  $mt[1] = 0; $mt[2] = $dist; $mt[3] = $dist * 2; $mt[4] = $dist * 3;
	  if ($d_mod > 0) { $c[1] = $c[1] + $d_mod; $c[2] = $c[2] + $d_mod; }// $div = $divs + 1;
	
		$l = 0;
			do {
				$l++;
				$CAT[$l] = $rowuser[$main];
				$IDS[$l] = $rowuser[$id];
			} while ($rowuser = mysqli_fetch_assoc($done));
			//print_r(array_values($CAT));

		echo "<table width=\"100%\"  border=\"0\" align=\"left\" cellpadding=\"3\" cellspacing=\"0\">
				<tr valign=\"top\">"; $nct = 0;
	
				for ($m = 1; $m <= $div; $m++) {
					echo "<td><table width=\"100%\"  border=\"0\" cellspacing=\"0\" cellpadding=\"3\">";
				
						for ($z = 1; $z <= $c[$m]; $z++) {
							$a = $z + $mt[$m];
							echo "<tr>
								  <td width=\"5%\"><img src=\"bullet.gif\"></td>";
								  if ($url != '') {
									echo "<td width=\"95%\"><a href=\"".$url."?id=".$IDS[$a]."\">".$CAT[$a]."</a></td>";
								  } else {
									echo "<td width=\"95%\">".$CAT[$a]."</td>";
								  }
							echo "</tr>";
						}
					echo "</table></td>";
				}

		echo "</tr>
			 </table>";
	}

function arrayListShow($divs, $commalist) {

	$array = explode(',',$commalist);
	
	$tot_rows = count($array);
	$dist = floor($tot_rows / $divs);
	$d_mod = $tot_rows % $divs;
	$div = $divs;
	$c = array();
	// for ($x = 1; $x <= $div; $x++) { $c[$x] = $dist; }
	for ($x = 1; $x <= $div; $x++) { $c[$x] = $dist; }
	$mt[1] = 0; $mt[2] = $dist; $mt[3] = $dist * 2; $mt[4] = $dist * 3;
	if ($d_mod > 0) { $c[1] = $c[1] + $d_mod; $c[2] = $c[2] + $d_mod; }// $div = $divs + 1;

	echo "<table width=\"100%\"  border=\"0\" align=\"left\" cellpadding=\"3\" cellspacing=\"0\">
			<tr valign=\"top\">"; $nct = 0;
	
			for ($m = 1; $m <= $div; $m++) {
				echo "<td><table width=\"100%\"  border=\"0\" cellspacing=\"0\" cellpadding=\"3\">";
			
					for ($z = 1; $z <= $c[$m]; $z++) {
						$a = $z + $mt[$m];
						if (empty($array[$a])) {
							// do nothing
						} else {
						echo "<tr>
							  <td width=\"5%\"><img src=\"bullet.gif\"></td>";
								echo "<td width=\"95%\">".$array[$a]."</td>";
						echo "</tr>";
						}
					}
				echo "</table></td>";
			}

	echo "</tr>
		 </table>";
	}


function showCompanies($showadmin="",$cc="") {

	  $CON = mysqli_connect(HOSTNAME, username, PWD);
	  if($CON){echo "connected";} else{ echo "error".mysqli_error($CON);}
		if ($_SESSION['USERTYPE']=="") {

		} else {

		}
	  $logSQL = "SELECT * FROM companyinfo ";

	  mysqli_select_db($CON, DB);
	  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve companies list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
	
	$selected="n";
		if ($nums > 0) {
		  do {
				if ($cc != '' && $cc == $rowuser['com_id']) {
					echo "<option selected value=\"".$rowuser['com_id']."\">".$rowuser['company']."</option>";
					$selected="y";
				} else {
					if ($rowuser['default']=="Yes") {
						if ($selected=="n") {
							echo "<option selected value=\"".$rowuser['com_id']."\">".$rowuser['company']."</option>";
						}
					} else {
						echo "<option value=\"".$rowuser['com_id']."\">".$rowuser['company']."</option>";
					}
				}

		  } while($rowuser = mysqli_fetch_assoc($done));
		  if ($showadmin == 'SHOWADMIN') {
					echo "<option value='ADM' class='admred'>Administrative Login</option>";
				}
		} else {
				echo "<option value='ADM' class='admred'>Administrative Login</option>";
				echo "<option>No Companies Created</option>";
		}
	}

function showGroups($dp = '', $IncludeCoAdmin=FALSE,$showid="1") {

	  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);

		if ($IncludeCoAdmin==FALSE) {
			$logSQL = "SELECT groups.groupid, groups.groupname, groups.groupid, groups.groupdesc, groups.sys
	FROM groups WHERE groups.sys =0 AND groups.groupname <> 'company administrator'   ORDER BY groups.groupname";
		} else {
			  $logSQL = "SELECT groups.groupid, groups.groupname, groups.groupid, groups.groupdesc, groups.sys FROM groups WHERE groups.groupname <> 'company administrator' ORDER BY groups.groupname";
		}


	  mysqli_select_db($CON, DB);
	  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve groups list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);

	  $id=0;
	  $nm="";

		if ($nums > 0) {
		echo "<option > </option>";
		  do {
		  
				$id=$rowuser['groupid'];
				$nm=$rowuser['groupname'];
				if ($dp != '' && $dp == $rowuser['groupid']) {
					if ($showid=="1") {
						echo "<option selected value=\"".$id."\">".$nm."</option>";
					} else {
						echo "<option selected value=\"".$nm."\">".$nm."</option>";
					}
				} else {
					if ($showid=="1") {
						echo "<option value=\"".$id."\">".$nm."</option>";
					} else {
						echo "<option value=\"".$nm."\">".$nm."</option>";
					}
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No groups Selected</option>";
		}
	}

function showGrouptypes($selected = '') {
	  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	
	$logSQL = "SELECT * FROM grouptypes ";

	  mysqli_select_db($CON, DB);
	  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve group types list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);

		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				$id=$rowuser['id'];
				$nm=$rowuser['grouptypename'];
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['grouptypename']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['grouptypename']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No groups types Selected </option>";
		}
}


function getgrouptype($selected) {

	  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	
		$logSQL = "SELECT * FROM grouptypes WHERE id = '".$selected."'";

	  mysqli_select_db($CON, DB);
	  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve group types list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  
	  return  $rowuser['grouptypename'];
	 
} 


function InsertUserGroups  ($authcode,$groupid) { 
		$CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);

		mysqli_select_db($CON, DB);
	  //groups 
	  $sql="DELETE FROM user_groups WHERE userid = '".$authcode."'";
		$Result1 = mysqli_query($CON, $sql) or die(mysqli_error());
		
		$sql="INSERT INTO user_groups (userid,groupid) VALUES ('".$authcode."','".$groupid."')";
		//echo $sql; 
		
		$Result1 = mysqli_query($CON, $sql) or die(mysqli_error($CON));
					
		$selectSQL = "SELECT  * FROM groups WHERE groups.groupname <> 'System administrator'  ";
		$done = mysqli_query($CON, $selectSQL) or die("ERROR: Trying to retrieve groups list - ".mysqli_error($CON));
		$groupslist = mysqli_fetch_assoc($done);
		
		if (mysqli_num_rows($done)>0) 
		{ 
			do { 
				if (isset($_POST[$groupslist['groupid']]) && $_POST[$groupslist['groupid']]!='' && $_POST[$groupslist['groupid']]!=$groupid) {
					$sql="INSERT INTO user_groups (userid,groupid) VALUES ('".$authcode."','".$groupslist['groupid']."')";
					//echo $sql; 
					
					$Result1 = mysqli_query($CON, $sql) or die(mysqli_error($CON));
				}
			} while ($groupslist = mysqli_fetch_assoc($done));
		} 
} 



function getcompanyadmingroup() {
	  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
		$logSQL = "SELECT groups.groupid, groups.groupname,groups.groupid, groups.groupdesc, groups.sys
	FROM groups WHERE groups.sys = 0 AND groups.groupname = 'Company Administrator'";
	  mysqli_select_db($CON, DB);
	  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve groups list - ".mysqli_error($CON));
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
			return $rowuser['groupid'];
		}
		
	}


function getcompany($enum) {
	
	  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT com_id, company FROM companyinfo WHERE com_id = '".$enum."'";
	  mysqli_select_db($CON, DB);
	  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve company name - ".mysqli_error($CON));
	  $rowuser = mysqli_fetch_assoc($done);
	  return $rowuser['company'];
	}
	
function gettelno($enum) {

  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
  $logSQL = "SELECT * FROM companyinfo WHERE com_id = '".$enum."'";
  mysqli_select_db($CON, DB);
  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve company name - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
  return $rowuser['telphone'];
}
function getaddress($enum) {

  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
  $logSQL = "SELECT * FROM companyinfo WHERE com_id = '".$enum."'";
  mysqli_select_db($CON, DB);
  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve company name - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
  return $rowuser['address'];
}

 function getfilter($enum) {
  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
  $logSQL = "SELECT FilterName  FROM filter WHERE  FilterCode = '".$enum."'";
  mysqli_select_db($CON, DB);
  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve control - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
  return $rowuser['FilterName'];
}
	

function getcompanyID($enum) {
	  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	 trim ($enum); 
	  $logSQL = "SELECT com_id FROM companyinfo WHERE company = '".$enum."'";
	  mysqli_select_db($CON, DB);
	  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve company name - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);

	  return $rowuser['com_id'];
	}

function getGroup($enum) {

  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
  $logSQL = "SELECT groupname FROM groups WHERE groupid = '".$enum."'";
  mysqli_select_db($CON, DB);
  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve group name - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
  return $rowuser['groupname'];
}

function getGroupID($enum) {
  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
  $logSQL = "SELECT groupid FROM groups WHERE groupname = '".$enum."'";
  mysqli_select_db($CON, DB);
  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve group name - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
  return $rowuser['groupid'];
}

function getgrouptypeid($id) 
{ 
  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
  $logSQL = "SELECT grouptypeid FROM groups WHERE groupid = '".$id."'";
  mysqli_select_db($CON, DB);
  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve group type id - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
  return $rowuser['grouptypeid'];
} 
function scopeValid($valid) {
	// Break Apart
	list($time, $period) = explode(' ', $valid, 2);
	switch ($period) {

		case 'M':
		$per = 'Months';
		break;

		case 'Y':
		$per = 'Years';
		break;

		default:
		$per = 'Months';
		break;
	}

	$ret = $time." ".$per;

	return $ret;
}

// REQ SESSION status
// --------------------------------------------------------------
function showIcon($sessval) {
	switch ($sessval) {
		case 'N':
		$icon = 'grymkr.gif';
		break;

		case 'I':
		$icon = 'orgmkr.gif';
		break;

		case 'C':
		$icon = 'grntck.gif';
		break;

		default:
		$icon = 'grymkr.gif';
		break;
	}

	return $icon;
}





 function ifrecordexists($tablename,$columnname1,$columnname2,$columnname3,$value1,$value2,$value3) {
 
  $SQL = "SELECT * FROM ".$tablename." WHERE ".$columnname1." = '".$value1."' AND ".$columnname2." = '".$value2."' AND ".$columnname3." = '".$value3."'  ";
  //echo $SQL;
   $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
  $done = mysqli_query($SQL, $CON) or die("ERROR: Trying to check record exists - ".mysqli_error());
  $num_rows = mysqli_num_rows($done); 
	$rTrue = TRUE;
	$rFalse = FALSE;
	if ($num_rows > 0) {
	   return $rTrue;
	} else {
	   return $rFalse;
	}

}  
function getUsername($enum) {
$CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
  $logSQL = "SELECT * FROM users WHERE authcode = '".$enum."'";
  mysqli_select_db($CON, DB);
  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve user details - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
 return $rowuser['fulname'] ;

}

function gettheUsername($enum) {


$CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
   $logSQL = "SELECT * FROM employee WHERE id = '".$enum."'";
  mysqli_select_db($CON, DB);
  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve user details - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
 return $rowuser['name'] ;

}

function getclient($enum) {
$CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
   $logSQL ="SELECT * FROM clients WHERE id = '".$enum."'";
  mysqli_select_db($CON, DB);
  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve user details - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
 return $rowuser['fname']." ".$rowuser['lname'] ;

}

function getcat($enum) {
$CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
   $logSQL ="SELECT * FROM category WHERE id = '".$enum."'";
  mysqli_select_db($CON, DB);
  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve user details - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
return $rowuser['name'] ;

 
}

function getfname($enum) {

$CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
   $logSQL ="SELECT * FROM users WHERE authcode = '".$enum."'";
  mysqli_select_db($CON, DB);
  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve user details - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
  return $rowuser['username'] ;

}


function getadminEmail() {
$CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
   $logSQL ="SELECT email FROM users WHERE admin ='1'";
  mysqli_select_db($CON, DB);
  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve user details - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
return $rowuser['email'] ;

}

function getusergroup($user) {

$CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
   $logSQL = "SELECT  groupid FROM users WHERE  authcode ='".$user."'";
  mysqli_select_db($CON, DB);
  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve user details - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
  return $rowuser['groupid'] ;

}



function createbarcode($value) { 

	$ean13 = new ean13;
	$ean13->article = $value;   // initial article code
	$ean13->article .= $ean13->generate_checksum();   // add the proper checksum value
	//$ean13->reverse();   // the string is printed backwards
	$value = $ean13->codestring();   // returns a string as input for the truetype font
	return $value ;  // render the image as PNG image

} 

function getcompanyaddress ($enum) {
	 
	  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM companyinfo WHERE com_id = '".$enum."'";
	  mysqli_select_db($CON, DB);
	  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve company details - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  return $rowuser['address'];
}

function getcompanyaddress2 ($enum) {

	  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM companyinfo WHERE com_id = '".$enum."'";
	  mysqli_select_db($CON, DB);
	  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve company details - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  return $rowuser['address2'];

}

function getcompanytown ($enum) {

	  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM companyinfo WHERE com_id = '".$enum."'";
	  mysqli_select_db($CON, DB);
	  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve company details - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  return $rowuser['town'];

}

function getcompanycountry ($enum) {

	  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM companyinfo WHERE com_id = '".$enum."'";
	  mysqli_select_db($CON, DB);
	  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve company details - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  return $rowuser['country'];

}

function getcompanylocation ($enum) {

	  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM companyinfo WHERE com_id = '".$enum."'";
	  mysqli_select_db($CON, DB);
	  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve company details - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  return $rowuser['location'];

} 

function getcompanytel ($enum) {

	  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM companyinfo WHERE com_id = '".$enum."'";
	  mysqli_select_db($CON, DB);
	  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve company details - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  return $rowuser['telphone'];

}  

function getcompanyfax ($enum) {

	  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM companyinfo WHERE com_id = '".$enum."'";
	  mysqli_select_db($CON, DB);
	  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve company details - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  return $rowuser['faxphone'];

} 

function getcompanyemail ($enum) {

	  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM companyinfo WHERE com_id = '".$enum."'";
	  mysqli_select_db($CON, DB);
	  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve company details - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  return $rowuser['email'];

} 

function getcompanywebsite ($enum) {

	  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
      $logSQL = "SELECT * FROM companyinfo WHERE com_id = '".$enum."'";
	  mysqli_select_db($CON, DB);
	  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve company details - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  return $rowuser['website'];

}


function getusertype($enum) {
  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
  $logSQL = "SELECT * FROM users WHERE  authcode = '".$enum."'";
  mysqli_select_db($CON, DB);
  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve  usertype - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
  return $rowuser['admin'];
}

function getnextcode($field,$table) {
 $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
$logSQL =  "SELECT   max(".$field.") AS num FROM ".$table."   LIMIT 1";
;
  mysqli_select_db($CON, DB);
	$prq1 = mysqli_query($CON, $logSQL) or die(mysqli_error());
	$row_prq1 = mysqli_fetch_assoc($prq1);
  return  $row_prq1[num]+1;
  }
  
  function ifmyrecordcodeexists($tablename,$col1,$value1) {
	  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	$logSQL = "SELECT * FROM ".$tablename." WHERE ".$col1." = '".$value1."' ";
	mysqli_select_db($CON, DB);
	  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to check record exists - ".mysqli_error(). $logSQL);
	  //$rowuser = mysqli_fetch_assoc($done);
	  $num_rows = mysqli_num_rows($done); //Get number of rows

	if ($num_rows > 0) {
	   return 'yes';
	} else {
	   return 'no';
	}
}

function showclients($selected = '') {
	
	  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM clients ORDER BY  fulnames ASC ";
	  //echo $logSQL;
	  mysqli_select_db($CON, DB);
	  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve clients list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['clientno']) 
				{
						echo "<option selected value=\"".$rowuser['clientno']."\">".$rowuser['fulnames']."</option>";
				} else {
						echo "<option value=\"".$rowuser['clientno']."\">".$rowuser['fulnames']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No clients Selected </option>";
		}
}
function showcategory($selected = '') {
	
	  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM category ORDER BY  name ASC ";
	  //echo $logSQL;
	  mysqli_select_db($CON, DB);
	  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve category list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No category Selected </option>";
		}
}

function showitemtype($selected = '') {
	
	  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM itypes ORDER BY  name ASC ";
	  //echo $logSQL;
	  mysqli_select_db($CON, DB);
	  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve itemtype list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No itemtype Selected </option>";
		}
}
function getmaxnum($field,$table,$client) {
 $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
$logSQL =  "SELECT   max(".$field.") AS num FROM ".$table."  WHERE client='".$client."'  LIMIT 1";
;
  mysqli_select_db($CON, DB);
	$prq1 = mysqli_query($CON, $logSQL) or die(mysqli_error());
	$row_prq1 = mysqli_fetch_assoc($prq1);
	if($row_prq1['num']<=0){
		$n=2;
		}else{
		$n=$row_prq1['num']+1;
		}
  return  $n;
  }
  function getlastspecfile($field,$table,$client) {
 $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
$logSQL =  "SELECT   max(".$field.") AS num FROM ".$table."  WHERE client='".$client."'  LIMIT 1";
;
  mysqli_select_db($CON, DB);
	$prq1 = mysqli_query($CON, $logSQL) or die(mysqli_error());
	$row_prq1 = mysqli_fetch_assoc($prq1);
	
	return	$row_prq1['num'];
		
    
  }
  
  
  function getmaxjobno() {
 $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
$logSQL1 =  "SELECT   max(jobno) AS num FROM jobs   LIMIT 1";
;
  mysqli_select_db($CON, DB);
	$prq1 = mysqli_query($CON, $logSQL1) or die(mysqli_error());
	$row_prq1 = mysqli_fetch_assoc($prq1);
	
	$logSQL =  "SELECT   max(jno) AS num FROM specificfile   LIMIT 1";
;
  mysqli_select_db($CON, DB);
	$prq = mysqli_query($CON, $logSQL) or die(mysqli_error());
	$row_prq = mysqli_fetch_assoc($prq);
	
	
	if($row_prq1['num']>$row_prq['num']){
 	 return  $row_prq1['num']+1;
	 }
	if($row_prq['num']>$row_prq1['num']){
 	 return  $row_prq['num']+1;
	 }
	 
	 if($row_prq['num']==$row_prq1['num']){
 	 return  $row_prq['num']+1;
	 }
  }
  
  

function getmaxid($field,$table) {
 $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
$logSQL =  "SELECT   max(".$field.") AS num FROM ".$table."   LIMIT 1";
;
  mysqli_select_db($CON, DB);
	$prq1 = mysqli_query($CON, $logSQL) or die(mysqli_error());
	$row_prq1 = mysqli_fetch_assoc($prq1);
	
  return  $row_prq1['num']+1;
  }
  
  function getlastid($field,$table) {
 $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
$logSQL =  "SELECT   max(".$field.") AS num FROM ".$table."   LIMIT 1";
  mysqli_select_db($CON, DB);
	$prq1 = mysqli_query($CON, $logSQL) or die(mysqli_error());
	$row_prq1 = mysqli_fetch_assoc($prq1);
	
  return  $row_prq1['num'];
  }
  
  
  
/*  
function b() {
 $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
 $k=1;
 while($k<=1520){ 
 mysqli_select_db($CON, DB);
$logSQL =  "INSERT INTO `casenos` (name) VALUES('".$k."')";
$prq1 = mysqli_query($CON, $logSQL) or die(mysqli_error());
$k++;
}
}*/

//------------------------------------------------------------------------------------------------------------------------------------
function showmaritalstatus($selected = '') {
	
	  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM maritalstatus ORDER BY  name ASC ";
	  //echo $logSQL;
	  mysqli_select_db($CON, DB);
	  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve ms list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option>Please Select Marital Status </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No Marital Status Selected </option>";
		}
}

function showtowns($selected = '') {
	
	  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM town ORDER BY  name ASC ";
	  //echo $logSQL;
	  mysqli_select_db($CON, DB);
	  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve town list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option></option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No town Selected </option>";
		}
}
function showtheusers($selected = '') {
	
	  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM employee ORDER BY  name 	 ASC ";
	  //echo $logSQL;
	  mysqli_select_db($CON, DB);
	  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve employee list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option></option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No Employee Selected </option>";
		}
}



/*function showmonth($selected = '') {
	
	  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM months ORDER BY  id ASC ";
	  //echo $logSQL;
	  mysqli_select_db($CON, DB);
	  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve month list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option>Month </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No month Selected </option>";
		}
}

function showday($selected = '') {
	
	  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM days ORDER BY  id ASC ";
	  //echo $logSQL;
	  mysqli_select_db($CON, DB);
	  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve day list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option>Day </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No day Selected </option>";
		}
}

function showyear($selected = '') {
	
	  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM years ORDER BY  id ASC ";
	  //echo $logSQL;
	  mysqli_select_db($CON, DB);
	  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve year list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option>Year </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No year Selected </option>";
		}
}
*/
function showcompanyType($selected = '') {
	
	  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM companytype ORDER BY  name ASC ";
	  //echo $logSQL;
	  mysqli_select_db($CON, DB);
	  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve companytype list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option>Please Select a Company type </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No companytype Selected </option>";
		}
}
function showgender($selected = '') {
	
	  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM gender ORDER BY  name ASC ";
	  //echo $logSQL;
	  mysqli_select_db($CON, DB);
	  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve gender list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option>Please Select a Gender </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No gender Selected </option>";
		}
}



function showdept($selected = '') {
	
	  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM depts ORDER BY  name DESC ";
	  //echo $logSQL;
	  mysqli_select_db($CON, DB);
	  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve dept list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		//echo "<option>Please Select a Department </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['initials']) 
				{
						echo "<option selected value=\"".$rowuser['initials']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['initials']."\">".$rowuser['name']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No dept Selected </option>";
		}
}

function showctype($selected = '') {
	
	  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM clienttype ORDER BY  name ASC ";
	  //echo $logSQL;
	  mysqli_select_db($CON, DB);
	  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve clienttype list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option>Please Select a Client Type </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['initials']) 
				{
						echo "<option selected value=\"".$rowuser['initials']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['initials']."\">".$rowuser['name']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No clienttype Selected </option>";
		}
}

function showimportance($selected = '') {
	
	  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM importance ORDER BY  name ASC ";
	  //echo $logSQL;
	  mysqli_select_db($CON, DB);
	  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve importance list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option>Please Select The client Importance </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['initials']) 
				{
						echo "<option selected value=\"".$rowuser['initials']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['initials']."\">".$rowuser['name']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No importance Selected </option>";
		}
}

function showstatus($selected = '') {
	
	  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM status ORDER BY  name ASC ";
	  //echo $logSQL;
	  mysqli_select_db($CON, DB);
	  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve status list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option>Please Select Status </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No status Selected </option>";
		}
}
function showjobtype($selected = '') {
	
	  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM jobtype ORDER BY  name ASC ";
	  //echo $logSQL;
	  mysqli_select_db($CON, DB);
	  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve jobtype list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option>Please Select a Job Type </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No jobtype Selected </option>";
		}
}

function showpriority($selected = '') {
	
	  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM priority ORDER BY  name ASC ";
	  //echo $logSQL;
	  mysqli_select_db($CON, DB);
	  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve priority list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option>Please Select a Priority </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No priority Selected </option>";
		}
}

 
/*  function getuserid() {
 $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
$logSQL =  "SELECT  *  FROM users  WHERE authcode='".$_SESSION['UNQ']."' LIMIT 1";
//echo $logSQL;
  mysqli_select_db($CON, DB);
	$prq1 = mysqli_query($CON, $logSQL) or die(mysqli_error());
	$row_prq1 = mysqli_fetch_assoc($prq1);
	$nums = mysqli_num_rows($prq1);
	
		return $row_prq1['uid'];
	
  }
 */
   
  function getusernme($enum) {
 $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
$logSQL =  "SELECT  *  FROM employee  WHERE id='".$enum."' LIMIT 1";
//echo $logSQL;
  mysqli_select_db($CON, DB);
	$prq1 = mysqli_query($CON, $logSQL) or die(mysqli_error());
	$row_prq1 = mysqli_fetch_assoc($prq1);
	$nums = mysqli_num_rows($prq1);
	
		return $row_prq1['name'];
	
  }
  
  function getunme($enum) {
 $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
$logSQL =  "SELECT  *  FROM employee  WHERE id='".$enum."' LIMIT 1";
//echo $logSQL;
  mysqli_select_db($CON, DB);
	$prq1 = mysqli_query($CON, $logSQL) or die(mysqli_error());
	$row_prq1 = mysqli_fetch_assoc($prq1);
	$nums = mysqli_num_rows($prq1);
	
		return $row_prq1['name'];
	
  }
  
  
  function get_time_difference( $start, $end )
{
    $uts['start']      =    strtotime( $start,1970 );
    $uts['end']        =    strtotime( $end,1970 );
    if( $uts['start']!==-1 && $uts['end']!==-1 )
    {
        if( $uts['end'] >= $uts['start'] )
        {
            $diff    =    $uts['end'] - $uts['start'];
			$time= $uts['end'] - $uts['start'];
            if( $days=intval((floor($diff/86400))) )
                $diff = $diff % 86400;
            if( $hours=intval((floor($diff/3600))) )
                $diff = $diff % 3600;
            if( $minutes=intval((floor($diff/60))) )
                $diff = $diff % 60;
            $diff    =    intval( $diff ); 
			           
           return( str_pad($hours,2,'0',STR_PAD_LEFT).':'.str_pad($minutes,2,'0',STR_PAD_LEFT).':'.str_pad($diff,2,'0',STR_PAD_LEFT));
		 // return date('H:i:s',mktime((date('H',$time)-3),date('i',$time),date('s',$time),1,1,1));
			
        }
        else
        {
            trigger_error( "Ending date/time is earlier than the start date/time", E_USER_WARNING );
        }
    }
    else
    {
        trigger_error( "Invalid date/time data detected", E_USER_WARNING );
    }
    return( false );
}


  function get_time_sum( $var1,$var2){
	$diff = ($var1+$var2);
	return($diff);
}
 
function calculateDatediff($var1){

		
		$diff = ($var1);
		$eProcs = '00';
			$mins = '00';
			$secs = '00';
			if ($diff>=3600) 
			{ 
				$eProcs = floor($diff/3600); 
				$diff = $diff - (3600 *  floor($diff/3600)); 
				if ($diff>60) 
				{ 
					$mins = floor($diff/60);
					$secs = $diff - (60 * floor($diff/60));
				} 
			} else {
				if ($diff>60) 
				{ 
					$mins = floor($diff/60);
					$secs = $diff - (60 * floor($diff/60));
				} 
			}
			$eProcs = str_pad($eProcs,2,'0',STR_PAD_LEFT);
			$mins = str_pad($mins,2,'0',STR_PAD_LEFT);
			$secs = str_pad($secs,2,'0',STR_PAD_LEFT);
			
			$diff =$eProcs.':'.$mins.':'.$secs;
			return($diff); 

	}
      
function showjob($selected='',$enum) {
	
        $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
        $logSQL = "SELECT * FROM jobs  WHERE client ='".$enum."' ORDER BY  jobno ASC ";
        //echo $logSQL;
        mysqli_select_db($CON, DB);
        $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve job list - ".mysqli_error());
        $rowuser = mysqli_fetch_assoc($done);
        $nums = mysqli_num_rows($done);

        if($nums>0){
              echo "<option></option>";
          do {
              if( $rowuser['ftype']=='G'){
                  $logSQL1 = "SELECT filename, dept FROM generalfile  WHERE client ='".$enum."' ";
                 //echo $logSQL;
                 mysqli_select_db($CON, DB);
                 $done1 = mysqli_query( $CON,$logSQL1) or die("ERROR: Trying to retrieve job list - ".mysqli_error());
                 $rowuser1 = mysqli_fetch_assoc($done1);
                 do{

                     if ($selected != '' && $selected == $rowuser['id']) {
                         echo "<option selected value=\"".$rowuser['id']."\">".$rowuser1['filename'].'-'.$rowuser1['dept'].str_pad($rowuser['jobno'],3,"0",STR_PAD_LEFT)."</option>";
//                                     echo "<option selected value=\"".$rowuser['id']."\">".$rowuser1['filename'].'-'.$rowuser1['dept'].$rowuser['jobno']."</option>";
                     } else {
//                                     echo "<option value=\"".$rowuser['id']."\">".$rowuser1['filename'].'-'.$rowuser1['dept'].$rowuser['jobno']."</option>";
                                     echo "<option value=\"".$rowuser['id']."\">".$rowuser1['filename'].'-'.$rowuser1['dept'].str_pad($rowuser['jobno'],3,"0",STR_PAD_LEFT)."</option>";
                     }

                 }while($rowuser1 = mysqli_fetch_assoc($done1));

              }

              if( $rowuser['ftype']=='S'){
                  $logSQL2 = "SELECT filename, dept FROM specificfile  WHERE client ='".$enum."' AND id='".$rowuser['fileno']."'  ";
                  //echo $logSQL;
                  mysqli_select_db($CON, DB);
                  $done2 = mysqli_query($CON, $logSQL2) or die("ERROR: Trying to retrieve job list - ".mysqli_error());
                  $rowuser2 = mysqli_fetch_assoc($done2);
                  do{
                      if ($selected != '' && $selected == $rowuser['id']) {
                                      echo "<option selected value=\"".$rowuser['id']."\">".$rowuser2['filename'].'-'.$rowuser2['dept'].str_pad($rowuser['jobno'],3,"0",STR_PAD_LEFT)."</option>";
                      } else {
                                      echo "<option value=\"".$rowuser['id']."\">".$rowuser2['filename'].'-'.$rowuser2['dept'].str_pad($rowuser['jobno'],3,"0",STR_PAD_LEFT)."</option>";
                      }
                  }while($rowuser2 = mysqli_fetch_assoc($done2));

              }

          } while($rowuser = mysqli_fetch_assoc($done));
        } else {
           echo "<option>No job Selected </option>";
        }
}

function showrtype($selected = '') {
	
	  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM requesttype ORDER BY  name ASC ";
	  //echo $logSQL;
	  mysqli_select_db($CON, DB);
	  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve request type list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option>Please Select a request type </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No request type Selected </option>";
		}
}

function showclientsno($selected = '') {
	
	  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM clientnos WHERE status='N' ORDER BY  name ASC ";
	  //echo $logSQL;
	  mysqli_select_db($CON, DB);
	  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve client No list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option>Please Select Client No </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['name']) 
				{
						echo "<option selected value=\"".$rowuser['name']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['name']."\">".$rowuser['name']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No Client No Selected </option>";
		}
}

function showcasesno($selected = '') {
	
	  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM casenos WHERE status='N' ORDER BY  name ASC ";
	  //echo $logSQL;
	  mysqli_select_db($CON, DB);
	  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve client No list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option>Please Select Job No </option>"; // Please Select Client No
		  do {
				
				if ($selected != '' && $selected == $rowuser['name']) 
				{
						echo "<option selected value=\"".$rowuser['name']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['name']."\">".$rowuser['name']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No Client No Selected </option>";
		}
}

//-----------------------------------------------------------------------------------------------
//                 HR FUNCTIONS
//-----------------------------------------------------------------------------------------------
/*
function getusername($enum) {
$CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
  $logSQL = "SELECT * FROM users WHERE authcode = '".$enum."'";
  mysqli_select_db($CON, DB);
  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve user details - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
 return $rowuser['fulname'] ;

}*/

function getauthoriser($enum) {
$CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
    $logSQL = "SELECT * FROM employee WHERE id = '".$enum."'";
  mysqli_select_db($CON, DB);
  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve employee details - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
 return $rowuser['name'] ;
}

function getdept($enum) {
$CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
    $logSQL = "SELECT * FROM employee WHERE id = '".$enum."'";
  mysqli_select_db($CON, DB);
  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve department details - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
 return $rowuser['department'] ;
}
function getuserdept($user) {
  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
  $logSQL = "SELECT department.name FROM department INNER JOIN users ON department.id=users.department WHERE  users.authcode ='".$user."'";
  mysqli_select_db($CON, DB);
  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to user details - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
  return $rowuser['name'] ;
}


function gettheuserdept($enum) {
$CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
    $logSQL = "SELECT * FROM department WHERE id = '".$enum."'";
  mysqli_select_db($CON, DB);
  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve dept details - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
 return $rowuser['name'] ;
}



function showbranches($selected = '') {	
	  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM companybranch ORDER BY name ASC ";
	  //echo $logSQL;
	  mysqli_select_db($CON, DB);
	  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve branch - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No branch Selected </option>";
		}
}
function showprobations ($selected = '') {	
	  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM probationtype ORDER BY name ASC ";
	  //echo $logSQL;
	  mysqli_select_db($CON, DB);
	  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve probationtype - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No probationtype Selected </option>";
		}
}

function showdocuments($selected = '') {	
	  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM doc ORDER BY name ASC ";
	  //echo $logSQL;
	  mysqli_select_db($CON, DB);
	  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve doc - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No doc Selected </option>";
		}
}


function showbanks($selected = '') {	
	  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM bank ORDER BY name ASC ";
	  //echo $logSQL;
	  mysqli_select_db($CON, DB);
	  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve Bank - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No Bank Selected </option>";
		}
}

function showbankbranches($bank,$selected = '') {	
	  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM bankbranch  WHERE bank='".$bank."'ORDER BY name ASC ";
	  //echo $logSQL;
	  mysqli_select_db($CON, DB);
	  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve Bank branch - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No Bank branch Selected </option>";
		}
}




function shownationality($selected = '') {	
	  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM nationality ORDER BY  name ASC ";
	  //echo $logSQL;
	  mysqli_select_db($CON, DB);
	  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve nationality list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No Nationality Selected </option>";
		}
}

function showdeptemployees($selected) {	
	  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM employee WHERE department='".$selected."' ORDER BY  name ASC ";
	  //echo $logSQL;
	  mysqli_select_db($CON, DB);
	  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve employee list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."   (".$rowuser['empcode'].")"."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']."   (".$rowuser['empcode'].")"."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No EmployeeSelected </option>";
		}
}

function showpretaxdeductions($selected = '') {	
	  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM pretaxdeduction ORDER BY  name ASC ";
	  //echo $logSQL;
	  mysqli_select_db($CON, DB);
	  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve pretaxdeductions list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']." </option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No pretaxdeductions Selected </option>";
		}
}

function showallowances($selected = '') {	
	  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM allowance ORDER BY  name ASC ";
	  //echo $logSQL;
	  mysqli_select_db($CON, DB);
	  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve allowance list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']." </option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No allowance Selected </option>";
		}
}

function showleavetypes($selected = '') {	
	  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM leavetype ORDER BY  name ASC ";
	  //echo $logSQL;
	  mysqli_select_db($CON, DB);
	  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve leave type list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']." </option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No leave type Selected </option>";
		}
}
function showempleaves($selected = '',$emp) {	
	  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT  leavetype.* FROM empleave INNER JOIN leavetype ON empleave.empleave=leavetype.id WHERE  empleave.employee='".$emp."'  ORDER BY leavetype.name ASC ";
	 // echo $logSQL;
	  mysqli_select_db($CON, DB);
	  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve leave type list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']." </option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No leave type Selected </option>";
		}
}



function showlevels($selected = '',$leave) {	
	  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT level FROM leavetype  WHERE id='".$leave."'ORDER BY  name ASC ";
	  //echo $logSQL;
	  mysqli_select_db($CON, DB);
	  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve leave type list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		
		  for($i=1;$i<=$rowuser['level'];$i++){
				
				if ($selected != '' && $selected == $i) 
				{
						echo "<option selected value=\"".$i."\">".$i."</option>";
				} else {
						echo "<option value=\"".$i."\">".$i." </option>";
				}
		  } 
		} else {
				echo "<option>No leave level defined  </option>";
		}
}


function showdeductions($selected = '') {	
	  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM pretaxdeduction ORDER BY  name ASC ";
	  //echo $logSQL;
	  mysqli_select_db($CON, DB);
	  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve deduction list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']." </option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No deduction Selected </option>";
		}
}

function showpostdeductions($selected = '') {	
	  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM posttaxdeduction ORDER BY  name ASC ";
	  //echo $logSQL;
	  mysqli_select_db($CON, DB);
	  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve deduction list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']." </option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No deduction Selected </option>";
		}
}


function showpositions($selected = '') {	
	  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM jobposition ORDER BY  name ASC ";
	  //echo $logSQL;
	  mysqli_select_db($CON, DB);
	  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve job position list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No Job Position Selected </option>";
		}
}

function showacademiccourses($selected = '') {	
	  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM academiccourse ORDER BY  name ASC ";
	  //echo $logSQL;
	  mysqli_select_db($CON, DB);
	  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve Academic Course list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No Academic Course Selected </option>";
		}
}

function showproffesionalcourses($selected = '') {	
	  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM proffesionalcourse ORDER BY  name ASC ";
	  //echo $logSQL;
	  mysqli_select_db($CON, DB);
	  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve proffesional Course list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No proffesional Course Selected </option>";
		}
}


function showacademicawards($selected = '') {	
	  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM academicaward ORDER BY  name ASC ";
	  //echo $logSQL;
	  mysqli_select_db($CON, DB);
	  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve Academic award list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No Academic award Selected </option>";
		}
}

function showproffesionalawards($selected = '') {	
	  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM proffesionalaward ORDER BY  name ASC ";
	  //echo $logSQL;
	  mysqli_select_db($CON, DB);
	  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve proffesional award list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No proffesional award Selected </option>";
		}
}



function showempterms($selected = '') {	
	  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM employmentterm ORDER BY  name ASC ";
	  //echo $logSQL;
	  mysqli_select_db($CON, DB);
	  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve employement term list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No Employment term  Selected </option>";
		}
}

function showdepartments($selected = '') {	
	  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM department ORDER BY  name ASC ";
	  //echo $logSQL;
	  mysqli_select_db($CON, DB);
	  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve department list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No department Selected </option>";
		}
}
function showempcategory($selected = '') {	
	  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM  employeecategory ORDER BY  name ASC ";
	  //echo $logSQL;
	  mysqli_select_db($CON, DB);
	  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve staffcategory list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No employee category Selected </option>";
		}
}

function showreligion($selected = '') {	
	  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM religion ORDER BY  name ASC ";
	  //echo $logSQL;
	  mysqli_select_db($CON, DB);
	  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve religions list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No religion Selected </option>";
		}
}




 

  function getcourse($field) {
 $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
$logSQL =  "SELECT  * FROM academiccourse  WHERE id='".$field."' LIMIT 1";
//echo $logSQL;
  mysqli_select_db($CON, DB);
	$prq1 = mysqli_query($CON, $logSQL) or die(mysqli_error());
	$row_prq1 = mysqli_fetch_assoc($prq1);
	$nums = mysqli_num_rows($prq1);	
		return $row_prq1['name'];	
  }
  
  function getpcourse($field) {
 $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
$logSQL =  "SELECT  * FROM proffesionalcourse  WHERE id='".$field."' LIMIT 1";
//echo $logSQL;
  mysqli_select_db($CON, DB);
	$prq1 = mysqli_query($CON, $logSQL) or die(mysqli_error());
	$row_prq1 = mysqli_fetch_assoc($prq1);
	$nums = mysqli_num_rows($prq1);	
		return $row_prq1['name'];
	
  }
  
  function getaward($field) {
 $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
$logSQL =  "SELECT  * FROM academicaward  WHERE id='".$field."' LIMIT 1";
//echo $logSQL;
  mysqli_select_db($CON, DB);
	$prq1 = mysqli_query($CON, $logSQL) or die(mysqli_error());
	$row_prq1 = mysqli_fetch_assoc($prq1);
	$nums = mysqli_num_rows($prq1);	
		return $row_prq1['name'];	
  }
  
   function getpaward($field) {
 $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
$logSQL =  "SELECT  * FROM proffesionalaward  WHERE id='".$field."' LIMIT 1";
//echo $logSQL;
  mysqli_select_db($CON, DB);
	$prq1 = mysqli_query($CON, $logSQL) or die(mysqli_error());
	$row_prq1 = mysqli_fetch_assoc($prq1);
	$nums = mysqli_num_rows($prq1);	
		return $row_prq1['name'];	
  }
  
  function getuserid() {
 $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
$logSQL =  "SELECT  employee  FROM users  WHERE authcode='".$_SESSION['UNQ']."' LIMIT 1";
//echo $logSQL;
  mysqli_select_db($CON, DB);
	$prq1 = mysqli_query($CON, $logSQL) or die(mysqli_error());
	$row_prq1 = mysqli_fetch_assoc($prq1);
	$nums = mysqli_num_rows($prq1);	
		return $row_prq1['employee'];	
  } 
    
  function getusrdept() {
 $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
$logSQL =  "SELECT  department  FROM employee  WHERE id='".getuserid()."' LIMIT 1";
//echo $logSQL;
  mysqli_select_db($CON, DB);
	$prq1 = mysqli_query($CON, $logSQL) or die(mysqli_error());
	$row_prq1 = mysqli_fetch_assoc($prq1);
	$nums = mysqli_num_rows($prq1);
	
		return $row_prq1['department'];
	
  }
  
   function getuserapplevel($employee) {
 $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
$logSQL1 =  "SELECT  * FROM  approvers  WHERE employee='".$employee."'";
//echo $logSQL;
  mysqli_select_db($CON, DB);
	$prq21 = mysqli_query($CON, $logSQL1) or die(mysqli_error());
	$row_prq21 = mysqli_fetch_assoc($prq21);
	$nums21 = mysqli_num_rows($prq21);	
		return $row_prq21['applevel'];	
  }

  function getdepartment($field) {
 $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
$logSQL =  "SELECT  * FROM  department  WHERE id='".$field."' LIMIT 1";
//echo $logSQL;
  mysqli_select_db($CON, DB);
	$prq1 = mysqli_query($CON, $logSQL) or die(mysqli_error());
	$row_prq1 = mysqli_fetch_assoc($prq1);
	$nums = mysqli_num_rows($prq1);	
		return $row_prq1['name'];	
  }
  
  function getdeduction($field) {
 $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
$logSQL =  "SELECT  * FROM pretaxdeduction  WHERE id='".$field."' LIMIT 1";
//echo $logSQL;
  mysqli_select_db($CON, DB);
	$prq1 = mysqli_query($CON, $logSQL) or die(mysqli_error());
	$row_prq1 = mysqli_fetch_assoc($prq1);
	$nums = mysqli_num_rows($prq1);	
		return $row_prq1['name'];	
  }
  
   function getjobposition($field) {
 $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
$logSQL =  "SELECT  * FROM jobposition  WHERE id='".$field."' LIMIT 1";
//echo $logSQL;
  mysqli_select_db($CON, DB);
	$prq1 = mysqli_query($CON, $logSQL) or die(mysqli_error());
	$row_prq1 = mysqli_fetch_assoc($prq1);
	$nums = mysqli_num_rows($prq1);	
		return $row_prq1['name'];	
  }
  
   function getjobpos($field) {
 $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
$logSQL =  "SELECT jobposition FROM employee  WHERE id='".$field."' LIMIT 1";
//echo $logSQL;
  mysqli_select_db($CON, DB);
	$prq1 = mysqli_query($CON, $logSQL) or die(mysqli_error());
	$row_prq1 = mysqli_fetch_assoc($prq1);
	$nums = mysqli_num_rows($prq1);	
		return getjobposition($row_prq1['jobposition']);	
  }
  
   function getallowance($field) {
 $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
$logSQL =  "SELECT  * FROM allowance  WHERE id='".$field."' LIMIT 1";
//echo $logSQL;
  mysqli_select_db($CON, DB);
	$prq1 = mysqli_query($CON, $logSQL) or die(mysqli_error());
	$row_prq1 = mysqli_fetch_assoc($prq1);
	$nums = mysqli_num_rows($prq1);	
		return $row_prq1['name'];	
  }
   function getleave($field) {
 $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
$logSQL =  "SELECT  * FROM leavetype  WHERE id='".$field."' LIMIT 1";
//echo $logSQL;
  mysqli_select_db($CON, DB);
	$prq1 = mysqli_query($CON, $logSQL) or die(mysqli_error());
	$row_prq1 = mysqli_fetch_assoc($prq1);
	$nums = mysqli_num_rows($prq1);	
		return $row_prq1['name'];	
  }  
  


   function getempid() {
 $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
$logSQL =  "SELECT  * FROM users WHERE authcode='".$_SESSION['UNQ']."' LIMIT 1";
//echo $logSQL;
  mysqli_select_db($CON, DB);
	$prq1 = mysqli_query($CON, $logSQL) or die(mysqli_error());
	$row_prq1 = mysqli_fetch_assoc($prq1);
	$nums = mysqli_num_rows($prq1);	
		return $row_prq1['uid'];	
  }
   function getmyempid() {
 $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
$logSQL =  "SELECT  * FROM users WHERE authcode='".$_SESSION['UNQ']."' LIMIT 1";
//echo $logSQL;
  mysqli_select_db($CON, DB);
	$prq1 = mysqli_query($CON, $logSQL) or die(mysqli_error());
	$row_prq1 = mysqli_fetch_assoc($prq1);
	$nums = mysqli_num_rows($prq1);	
		return $row_prq1['uid'];	
  }

  
  function getemployee($enum) {
  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
  $logSQL = "SELECT * FROM employee WHERE  id = '".$enum."'";
  mysqli_select_db($CON, DB);
  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve  employee - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
  $result="EMP CODE : ".$rowuser['empcode']."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Name : ".$rowuser['name'];
 // echo $result
  return  $result;
}

function getempl($enum) {
  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
  $logSQL = "SELECT * FROM employee WHERE  id = '".$enum."'";
  mysqli_select_db($CON, DB);
  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve  employee - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
  $result= $rowuser['name'];
 // echo $result
  return  $result;
}


function getuseremail($enum) {
  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
  $logSQL = "SELECT * FROM employee WHERE  id = '".$enum."'";
  mysqli_select_db($CON, DB);
  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve  teacher - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
  return $rowuser['email'];
}

function getusertel($enum) {
  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
  $logSQL = "SELECT * FROM employee WHERE  id = '".$enum."'";
  mysqli_select_db($CON, DB);
  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve  teacher - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
  return $rowuser['hometel'];
}
function getauthusername($enum) {
  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
  $logSQL = "SELECT * FROM employee WHERE  id = '".$enum."'";
  mysqli_select_db($CON, DB);
  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve  teacher - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
  return $rowuser['name'];
}


function create_temppayroll () { 	
	drop_temppayroll ();	
	$CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
   	mysqli_select_db($CON, DB);	
	$_SESSION['temppayroll'] = "temppayroll".$_SESSION['UNQ'];
	  $sql = 'CREATE TABLE `'.$_SESSION['temppayroll'].'` ('
		. ' `employee` INT(10) NOT NULL ,'
		. ' `pretax` DECIMAL(10,2) NOT NULL  DEFAULT "0.00",  '
		. ' `posttax` DECIMAL(10,2)  NOT NULL  DEFAULT "0.00",  '
		. ' `allowance` DECIMAL(10,2)  NOT NULL  DEFAULT "0.00",  '
		. ' `grosspay` DECIMAL(10,2)  NOT NULL  DEFAULT "0.00", '
		. ' `taxableamount` DECIMAL(10,2)  NOT NULL  DEFAULT "0.00" , '
		. ' `Payee` DECIMAL(10,2)  NOT NULL  DEFAULT "0.00" ,'
		. ' `deduction` DECIMAL(10,2)  NOT NULL  DEFAULT "0.00" , '
		. ' `tdeduction` DECIMAL(10,2)  NOT NULL  DEFAULT "0.00" ,'
		. ' `netpay` DECIMAL(10,2)  NOT NULL  DEFAULT "0.00" '		
								
		. ' )'
		. ' ENGINE = innodb;';

	$done = mysqli_query($sql, $CON) or die("ERROR: Trying to create temppayroll  table- ".mysqli_error());
	
} 


function drop_temppayroll () { 	
	$CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
   	mysqli_select_db($CON, DB);
	
	$_SESSION['temppayroll'] = "temppayroll".$_SESSION['UNQ'];		  
	$deleteSQL = "DROP TABLE IF EXISTS ".$_SESSION['temppayroll']."";
	$done = mysqli_query($deleteSQL, $CON) or die("ERROR: Trying to delete temppayroll table - ".mysqli_error());
	
}

function insertrecords ($table,$field1,$val1) { 	
	$CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
   	mysqli_select_db($CON, DB);	
		  $sql = "INSERT INTO  ".$table." (".$field1.") VALUES(".$val1.")";
		$done = mysqli_query($sql, $CON) or die("ERROR: Trying to save record- ".mysqli_error());
	
} 

function deleterecords () { 	
	$CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
   	mysqli_select_db($CON, DB);	
		  $sql1 = "DELETE FROM  ".$_SESSION['temppayroll']." ";
		  $done1 = mysqli_query($sql1, $CON) or die("ERROR: Trying to delete record- ".mysqli_error());	  
		 	
} 

function updaterecords ($table,$field1,$val1,$emp) { 	
	$CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
   	mysqli_select_db($CON, DB);	
		  $sql = "UPDATE  ".$table." SET ".$field1."=".$val1." WHERE employee=".$emp."";
		  //echo  $sql."<BR>";
		$done = mysqli_query($sql, $CON) or die("ERROR: Trying to update record- ".mysqli_error());
	
} 

function create_temppayee  () { 	
	drop_temppayee ();	
	$CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
   	mysqli_select_db($CON, DB);	
	$_SESSION['temppayee'] = "temppayee".$_SESSION['UNQ'];
	  $sql = 'CREATE TABLE `'.$_SESSION['temppayee'].'` ('
		. ' `employee` INT(10)  NULL ,'
		. ' `total` DECIMAL(10,2)  NULL  '
								
		. ' )'
		. ' ENGINE = innodb;';

	$done = mysqli_query($sql, $CON) or die("ERROR: Trying to create temppayee   table- ".mysqli_error());
	
} 


function drop_temppayee  () { 	
	$CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
   	mysqli_select_db($CON, DB);	
	$_SESSION['temppayee'] = "temppayee".$_SESSION['UNQ'];		  
	$deleteSQL = "DROP TABLE IF EXISTS ".$_SESSION['temppayee']."";
	//echo $deleteSQL;
	$done = mysqli_query($deleteSQL, $CON) or die("ERROR: Trying to delete temppayee  table - ".mysqli_error());
	
}


function showempls($selected = '') {	
	  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM users ORDER BY  fulname ASC ";
	  //echo $logSQL;
	  mysqli_select_db($CON, DB);
	  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve user list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['uid']) 
				{
						echo "<option selected value=\"".$rowuser['uid']."\">".$rowuser['fulname']." (".$rowuser['authcode'].")</option>";
				} else {
						echo "<option value=\"".$rowuser['uid']."\">".$rowuser['fulname']." (".$rowuser['authcode'].")</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No user Selected </option>";
		}
}

  function getdesignation($enum) {
  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
  $logSQL = "SELECT * FROM users WHERE  authcode = '".$enum."'";
  mysqli_select_db($CON, DB);
  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve  usertype - ".mysqli_error());
  $rowuser = mysqli_fetch_assoc($done);
  return $rowuser['designation'];
}
 

function showemployees($selected = '') {
	
	  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM employee ORDER BY  name ASC ";
	  //echo $logSQL;
	  mysqli_select_db($CON, DB);
	  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve employee list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No employee Selected </option>";
		}
}


function showweekno($selected = '') {	
	  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM weekno ORDER BY  name ASC ";
	  //echo $logSQL;
	  mysqli_select_db($CON, DB);
	  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve weeks list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option> </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['name']) 
				{
						echo "<option selected value=\"".$rowuser['name']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['name']."\">".$rowuser['name']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No Week Selected </option>";
		}
}
function recordexists3cols($tablename,$col1,$col2,$val1,$val2) {
	  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);  
	 	 $logSQL = "SELECT * FROM ".$tablename." WHERE ".$col1." = '".$val1."' AND ".$col2." = '".$val2."' ";
	  mysqli_select_db($CON, DB);
	  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to check record exists  - ".mysqli_error(). $logSQL);
	  //$rowuser = mysqli_fetch_assoc($done);
	  $num_rows = mysqli_num_rows($done); //Get number of rows
	if ($num_rows > 0) {
	   return TRUE;
	} else {
	   return FALSE;
	}
}



function datediff($interval, $datefrom, $dateto, $using_timestamps = false) {
		/* $interval can be: yyyy - Number of full years
		q - Number of full quarters
		m - Number of full months 
		y - Difference between day numbers (eg 1st Jan 2004 is "1", the first day. 2nd Feb 2003 is "33". The datediff is "-32".) 
		d - Number of full days
		w - Number of full weekdays
		ww - Number of full weeks
		h - Number of full hours 
		n - Number of full minutes 
		s - Number of full seconds (default) */ 
		 if (!$using_timestamps) { 
		 $datefrom = strtotime($datefrom, 0); 
		 $dateto = strtotime($dateto, 0);
		  } 
		  $difference = $dateto - $datefrom; // Difference in seconds 
		  switch($interval)  {
		   case 'yyyy': // Number of full years 
		  $years_difference = floor($difference / 31536000); 
		  if (mktime(date("H", $datefrom), date("i", $datefrom), date("s", $datefrom), date("n", $datefrom), date("j", $datefrom), date("Y", $datefrom)+$years_difference) > $dateto)  {
		   $years_difference--; } if (mktime(date("H", $dateto), date("i", $dateto), date("s", $dateto), date("n", $dateto), date("j", $dateto), date("Y", $dateto)-($years_difference+1)) > $datefrom) { $years_difference++; } $datediff = $years_difference; break; case "q": // Number of full quarters 
		   $quarters_difference = floor($difference / 8035200); while (mktime(date("H", $datefrom), date("i", $datefrom), date("s", $datefrom), date("n", $datefrom)+($quarters_difference*3), date("j", $dateto), date("Y", $datefrom)) < $dateto) { $months_difference++; } $quarters_difference--; $datediff = $quarters_difference; break; case "m": // Number of full months 
		   $months_difference = floor($difference / 2678400); while (mktime(date("H", $datefrom), date("i", $datefrom), date("s", $datefrom), date("n", $datefrom)+($months_difference), date("j", $dateto), date("Y", $datefrom)) < $dateto) { $months_difference++; } $months_difference--; $datediff = $months_difference; break; case 'y': // Difference between day numbers 
		   $datediff = date("z", $dateto) - date("z", $datefrom); break; case "d": // Number of full days
		    $datediff = floor($difference / 86400); break; case "w": // Number of full weekdays
			 $days_difference = floor($difference / 86400); $weeks_difference = floor($days_difference / 7); // Complete weeks 
			 $first_day = date("w", $datefrom); $days_remainder = floor($days_difference % 7); $odd_days = $first_day + $days_remainder; // Do we have a Saturday or Sunday in the remainder?
			  if ($odd_days > 7) { // Sunday
			   $days_remainder--; } if ($odd_days > 6) { // Saturday 
			   $days_remainder--; } $datediff = ($weeks_difference * 5) + $days_remainder; break; case "ww": // Number of full weeks 
			   $datediff = floor($difference / 604800); break; case "h": // Number of full hours 
			   $datediff = floor($difference / 3600); break; case "n": // Number of full minutes
			    $datediff = floor($difference / 60); break; default: // Number of full seconds (default) 
				$datediff = $difference; break; } 
				return $datediff; 
				} 

function create_temp_rq () { 	
	drop_temp_rq ();	
	$CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
   	mysqli_select_db($CON, DB);	
	$_SESSION['temp_rq'] = "temp_rq".$_SESSION['UNQ'];
	  $sql = 'CREATE TABLE `'.$_SESSION['temp_rq'].'` ('
	  . ' `id` INT( 10 ) NOT NULL AUTO_INCREMENT ,'
		. ' `acc` VARCHAR(50) NULL ,'
		. ' `debit` VARCHAR(50) NULL ,'
		. ' `credit` VARCHAR(50)  NULL , '
		. ' PRIMARY KEY ( `id` ) '
							
		. ' )'
		. ' ENGINE = innodb;';

	$done = mysqli_query($CON, $sql) or die("ERROR: Trying to create temp table - ".mysqli_error());
	
} 


function drop_temp_rq () { 	
	$CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
   	mysqli_select_db($CON, DB);
	
	$_SESSION['temp_rq'] = "temp_rq".$_SESSION['UNQ'];		  
	$deleteSQL = "DROP TABLE IF EXISTS ".$_SESSION['temp_rq']."";
	$done = mysqli_query($CON,$deleteSQL) or die("ERROR: Trying to delete temp table - ".mysqli_error());
	
}

function create_temp_req () { 	
	drop_temp_req ();	
	$CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
   	mysqli_select_db($CON, DB);	
	$_SESSION['temp_req'] = "temp_req".$_SESSION['UNQ'];
	  $sql = 'CREATE TABLE `'.$_SESSION['temp_req'].'` ('
		. ' `item` int(10) NULL ,'
		. ' `qty` int(10) NULL ,'
		. ' `price` DECIMAL(10,2) NULL ,'
		. ' `supplier` int(10) NULL ,'
		. ' `product` int(10) NULL '							
		. ' )'
		. ' ENGINE = innodb;';

	$done = mysqli_query($CON, $sql) or die("ERROR: Trying to create temp table - ".mysqli_error());
	
} 

function drop_temp_req () { 	
	$CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR); 
   	mysqli_select_db($CON, DB);
	
	$_SESSION['temp_req'] = "temp_req".$_SESSION['UNQ'];		  
	$deleteSQL = "DROP TABLE IF EXISTS ".$_SESSION['temp_req']."";
	$done = mysqli_query($CON, $deleteSQL) or die("ERROR: Trying to delete temp table - ".mysqli_error());
	
}

function showunit($selected = '' ,$enum) {
	
	  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM units WHERE projectno='".$enum."' AND state='Y' ORDER BY  name ASC ";
	  //echo $logSQL;
	  mysqli_select_db($CON, DB);
	  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve unit list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option>Please Select a unit </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No unit Selected </option>";
		}
}

function showprojunit($selected = '' ,$enum) {
	
	  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM units WHERE projectno='".$enum."'  ORDER BY  name ASC ";
	  //echo $logSQL;
	  mysqli_select_db($CON, DB);
	  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve unit list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option>Please Select a unit </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['id']) 
				{
						echo "<option selected value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['id']."\">".$rowuser['name']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No unit Selected </option>";
		}
}


function showunits($selected = '' ,$enum) {
	
	  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM units WHERE projectno='".$enum."'  ORDER BY  name ASC ";
	  //echo $logSQL;
	  mysqli_select_db($CON, DB);
	  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve unit list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		echo "<option>Please Select a unit </option>";
		  do {
				
				if ($selected != '' && $selected == $rowuser['name']) 
				{
						echo "<option selected value=\"".$rowuser['name']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['name']."\">".$rowuser['name']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} else {
				echo "<option>No unit Selected </option>";
		}
}

function showcprojects($selected = '' ,$enum) {
	
	  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT * FROM specificfile  WHERE client='".$enum."'  ORDER BY  name ASC ";
	 // echo $logSQL;
	  mysqli_select_db($CON, DB);
	  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve unit list - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  $nums = mysqli_num_rows($done);
		if ($nums > 0) {
		
		  do {
				
				if ($selected != '' && $selected == $rowuser['jno']) 
				{
						echo "<option selected value=\"".$rowuser['jno']."\">".$rowuser['name']."</option>";
				} else {
						echo "<option value=\"".$rowuser['jno']."\">".$rowuser['name']."</option>";
				}
		  } while($rowuser = mysqli_fetch_assoc($done));
		} 
}


function getsignature($enum) {
	
	  $CON = mysqli_connect(HOSTNAME, username, PWD) or trigger_error(mysqli_error(),E_USER_ERROR);
	  $logSQL = "SELECT empsig FROM employee  WHERE id='".$enum."'";
	  mysqli_select_db($CON, DB);
	  $done = mysqli_query($CON, $logSQL) or die("ERROR: Trying to retrieve employee signature - ".mysqli_error());
	  $rowuser = mysqli_fetch_assoc($done);
	  return $rowuser['empsig'];
	}

function msgbox($msg) {
	?>
	<script language="JavaScript" type="text/javascript">
		alert('<?php echo $msg ?>')
	</script>

	<?php
}

?>
