<?php require_once('../connections/eProc.php'); 

 require_once('../activelog.php'); ?>
<?php
$currentPage = $_SERVER["PHP_SELF"];

$maxRows_employmentterm = 30;
$pageNum_employmentterm = 0;
if (isset($_GET['pageNum_employmentterm'])) {
  $pageNum_employmentterm = $_GET['pageNum_employmentterm'];
}
$startRow_employmentterm = $pageNum_employmentterm * $maxRows_employmentterm;

mysqli_select_db($eProc, $database_eProc);
$query_employmentterm = "SELECT * FROM employmentterm ";
$query_limit_employmentterm = sprintf("%s LIMIT %d, %d", $query_employmentterm, $startRow_employmentterm, $maxRows_employmentterm);
$employmentterm = mysqli_query($eProc,$query_limit_employmentterm) or die(mysqli_error());
$row_employmentterm = mysqli_fetch_assoc($employmentterm);

if (isset($_GET['totalRows_employmentterm'])) {
  $totalRows_employmentterm = $_GET['totalRows_employmentterm'];
} else {
  $all_employmentterm = mysqli_query($eProc,$query_employmentterm);
  $totalRows_employmentterm = mysqli_num_rows($all_employmentterm);
}
$totalPages_employmentterm = ceil($totalRows_employmentterm/$maxRows_employmentterm)-1;

$queryString_employmentterm = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_employmentterm") == false && 
        stristr($param, "totalRows_employmentterm") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_employmentterm = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_employmentterm = sprintf("&totalRows_employmentterm=%d%s", $totalRows_employmentterm, $queryString_employmentterm);
?>

<html>
<head>
<title>LEGAL  MANAGEMENT INFORMATION  SYSTEM</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../styles/default.css" rel="stylesheet" type="text/css">

<style type="text/css">
<!--
.style1 {
	color: #FF0000;
	font-weight: bold;
}
-->
</style>
</head>
<body>

<table width="100%" border="0" cellpadding="4" cellspacing="0" >

  <tr class="inputdeft">
   <td width="36%"   class="inputdeft" style="font-weight: bold">Employemt Terms</td>
   
	<td width="64%"   class="inputdeft" style="font-weight: bold" ><div align="right">
	  <table width="154"  border="0" cellspacing="0" cellpadding="3">
	    <tr class="inputdeft">
	      <td width="19"  ><img src="../images/icons/newproject.gif" width="15" height="13" border="0" /></td>
                
			<td width="123"   ><a href="../adm/newemploymentterm.php">New Employemt Term </a></td>
          </tr>
	    </table>
	  </div></td>
  </tr>
  <?php if ($totalRows_employmentterm > 0) { ?>
  <?php do { ?>
  <tr>
  <td ><?php echo $row_employmentterm['name']?> </td>
  
  <td ><a href="../adm/newemploymentterm.php?id=<?php echo $row_employmentterm['id'] ?>">Edit</a></td>
  </tr>
  <?php } while ($row_employmentterm = mysqli_fetch_assoc($employmentterm)); ?>
  <tr>
    <td colspan="12" class="mainbase"><table width="97%"  border="0" cellspacing="0" cellpadding="3">
      <tr align="center">
        <td width="45%">
          <table width="78%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
              <td width="23%" align="center"><?php if ($pageNum_employmentterm > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_employmentterm=%d%s", $currentPage, 0, $queryString_employmentterm); ?>">First</a>
                <?php } // Show if not first page ?>              </td>
              <td width="31%" align="center"><?php if ($pageNum_employmentterm > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_employmentterm=%d%s", $currentPage, max(0, $pageNum_employmentterm - 1), $queryString_employmentterm); ?>">Previous</a>
                <?php } // Show if not first page ?>              </td>
              <td width="23%" align="center"><?php if ($pageNum_employmentterm < $totalPages_employmentterm) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_employmentterm=%d%s", $currentPage, min($totalPages_employmentterm, $pageNum_employmentterm + 1), $queryString_employmentterm); ?>">Next</a>
                <?php } // Show if not last page ?>              </td>
              <td width="23%" align="center"><?php if ($pageNum_employmentterm < $totalPages_employmentterm) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_employmentterm=%d%s", $currentPage, $totalPages_employmentterm, $queryString_employmentterm); ?>">last</a>
                <?php } // Show if not last page ?>              </td>
            </tr>
          </table></td>
        <td width="55%">&nbsp; Showing <strong><?php echo ($startRow_employmentterm + 1) ?></strong> to <strong><?php echo min($startRow_employmentterm + $maxRows_employmentterm, $totalRows_employmentterm) ?></strong> of <strong><?php echo $totalRows_employmentterm ?></strong> </td>
      </tr>
    </table></td>
  </tr>
  <?php } else { ?>
  <tr>
    <td colspan="12" class="mainbase"><span class="style1">No employmentterms Created! </span></td>
  </tr>
  <?php } ?>
</table>
</body>
</html>
<?php
mysqli_free_result($employmentterm);
?>

