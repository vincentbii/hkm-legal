<?php require_once('../connections/eProc.php'); 

 require_once('../activelog.php'); ?>
<?php
$currentPage = $_SERVER["PHP_SELF"];

$maxRows_nationality = 30;
$pageNum_nationality = 0;
if (isset($_GET['pageNum_nationality'])) {
  $pageNum_nationality = $_GET['pageNum_nationality'];
}
$startRow_nationality = $pageNum_nationality * $maxRows_nationality;

mysqli_select_db($eProc, $database_eProc);
$query_nationality = "SELECT * FROM nationality ";
$query_limit_nationality = sprintf("%s LIMIT %d, %d", $query_nationality, $startRow_nationality, $maxRows_nationality);
$nationality = mysqli_query($eProc,$query_limit_nationality) or die(mysqli_error($eProc));
$row_nationality = mysqli_fetch_assoc($nationality);

if (isset($_GET['totalRows_nationality'])) {
  $totalRows_nationality = $_GET['totalRows_nationality'];
} else {
  $all_nationality = mysqli_query($eProc,$query_nationality);
  $totalRows_nationality = mysqli_num_rows($all_nationality);
}
$totalPages_nationality = ceil($totalRows_nationality/$maxRows_nationality)-1;

$queryString_nationality = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_nationality") == false && 
        stristr($param, "totalRows_nationality") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_nationality = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_nationality = sprintf("&totalRows_nationality=%d%s", $totalRows_nationality, $queryString_nationality);
?>

<html>
<head>
<title>LEGAL  MANAGEMENT INFORMATION  SYSTEM</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../styles/default.css" rel="stylesheet" type="text/css">

<style type="text/css">
<!--
.style1 {
	color: #FF0000;
	font-weight: bold;
}
-->
</style>
</head>
<body>

<table width="100%" border="0" cellpadding="4" cellspacing="0" >

  <tr>
   <td width="36%"   class="inputdeft" style="font-weight: bold">Nationalies</td>
   
	<td width="64%"   class="inputdeft" style="font-weight: bold" ><div align="right">
	  <table width="154"  border="0" cellspacing="0" cellpadding="3">
	    <tr class="inputdeft">
	      <td width="19"  ><img src="../images/icons/newproject.gif" width="15" height="13" border="0" /></td>
                
			<td width="123"   ><a href="../adm/newnationality.php">New Nationality </a></td>
            </tr>
	    </table>
	  </div></td>
  </tr>
  <?php if ($totalRows_nationality > 0) { ?>
  <?php do { ?>
  <tr>
  <td ><?php echo $row_nationality['name']?> </td>
  
  <td ><a href="../adm/newnationality.php?id=<?php echo $row_nationality['id'] ?>">Edit</a></td>
  </tr>
  <?php } while ($row_nationality = mysqli_fetch_assoc($nationality)); ?>
  <tr>
    <td colspan="12" class="mainbase"><table width="97%"  border="0" cellspacing="0" cellpadding="3">
      <tr align="center">
        <td width="45%">
          <table width="78%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
              <td width="23%" align="center"><?php if ($pageNum_nationality > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_nationality=%d%s", $currentPage, 0, $queryString_nationality); ?>">First</a>
                <?php } // Show if not first page ?>              </td>
              <td width="31%" align="center"><?php if ($pageNum_nationality > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_nationality=%d%s", $currentPage, max(0, $pageNum_nationality - 1), $queryString_nationality); ?>">Previous</a>
                <?php } // Show if not first page ?>              </td>
              <td width="23%" align="center"><?php if ($pageNum_nationality < $totalPages_nationality) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_nationality=%d%s", $currentPage, min($totalPages_nationality, $pageNum_nationality + 1), $queryString_nationality); ?>">Next</a>
                <?php } // Show if not last page ?>              </td>
              <td width="23%" align="center"><?php if ($pageNum_nationality < $totalPages_nationality) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_nationality=%d%s", $currentPage, $totalPages_nationality, $queryString_nationality); ?>">last</a>
                <?php } // Show if not last page ?>              </td>
            </tr>
          </table></td>
        <td width="55%">&nbsp; Showing <strong><?php echo ($startRow_nationality + 1) ?></strong> to <strong><?php echo min($startRow_nationality + $maxRows_nationality, $totalRows_nationality) ?></strong> of <strong><?php echo $totalRows_nationality ?></strong> </td>
      </tr>
    </table></td>
  </tr>
  <?php } else { ?>
  <tr>
    <td colspan="12" class="mainbase"><span class="style1">No nationalitys Created! </span></td>
  </tr>
  <?php } ?>
</table>
</body>
</html>
<?php
mysqli_free_result($nationality);
?>

