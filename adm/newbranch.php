<?php
require_once('../connections/eProc.php');
include('../activelog.php');
mysqli_select_db($eProc, $database_eProc);
unset($_SESSION['companybranch']);
if (isset($_GET['id']) && $_GET['id'] != '') {
    $_SESSION['companybranch'] = $_GET['id'];
}


if ($_POST['update'] != "" && $_POST['update'] == "Update") {

    $updateSQL = "UPDATE companybranch SET  name='" . $_POST['name'] . "',description='" . $_POST['desc'] . "'  WHERE  id ='" . $_SESSION['companybranch'] . "'  AND company='" . $_SESSION['company'] . "'  ";
    //echo $updateSQL; 
    $Result1 = mysqli_query($eProc, $updateSQL) or die(mysqli_error($eProc));
    ?> 
    <script language="JavaScript" type="text/javascript">
        location = 'branchslist.php';
    </script>
    <?php
}

if ($_POST['save'] != "" && $_POST['save'] == "Save") {
    $exist = recordexists("companybranch", "name", $_POST['name']);
    if ($exist == 'no') {

        $insertSQL = sprintf("INSERT INTO companybranch (name,description,company) VALUES (%s,%s,%s)", GetSQLValueString($_POST['name'], "text"), GetSQLValueString($_POST['desc'], "text"), GetSQLValueString($_SESSION['company'], "text"));


        //echo $insertSQL;
        $Result2 = mysqli_query($eProc, $insertSQL) or die(mysqli_error($eProc));
        ?> 
        <script language="JavaScript" type="text/javascript">
            location = 'branchslist.php';
        </script>
        <?php
    } else {
        ?> 
        <script language="JavaScript" type="text/javascript">
            alert("A  companybranch  with <?php echo $_POST['name'] ?>  name exists.");
            location = 'newcompanybranch.php';
        </script>
        <?php
    }
}

if (isset($_GET['del']) && $_GET['del'] != "") {

    $DeleteSQL = "DELETE FROM companybranch WHERE  id ='" . $_GET['del'] . "'";
    //echo $DeleteSQL;
    if (!$Result3 = mysqli_query($eProc, $DeleteSQL)) {
        ?>

        <script language="JavaScript" type="text/javascript">
            alert("The companybranch has transactions.Please delete the transactions first");
            location = 'branchslist.php';

        </script>
    <?php } else { ?>

        <script language="JavaScript" type="text/javascript">
            location = 'branchslist.php';
        </script>
        <?php
    }
}


$selectSQL = "SELECT * FROM companybranch WHERE id = '" . $_SESSION['companybranch'] . "'";
//echo $selectSQL;	
$sql_select = mysqli_query($eProc, $selectSQL) or die(mysqli_error($eProc));
$row_sql = mysqli_fetch_assoc($sql_select);
?>
<html>
    <head>
        <title>LEGAL  MANAGEMENT INFORMATION  SYSTEM</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="../styles/<?php echo $_SESSION['Theme']; ?>/default.css" REL="stylesheet" type="text/css">
        <script language="javascript" type="text/javascript">
            function MM_valIDateForm() {
                if (document.newcompanybranch.name.value == '') {
                    alert('You must enter the Name to continue');
                    document.newcompanybranch.name.focus();
                    return false;
                }
            }

            function valIDatechars(evt)
            {
                // Prevents the entry of special characters and spaces into a field
                evt = (evt) ? evt : window.event
                var charCode = (evt.which) ? evt.which : evt.keyCode
                if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                    if (charCode < 64 || charCode > 90) {
                        if ((charCode < 95 || charCode > 122) && charCode != 8) {
                            if (charCode != 127 && charCode != 46 && charCode != 45) {
                                if ((charCode < 37 || charCode > 40) && charCode != 32) {
                                    alert('This field accepts only 0-9, a-z, A_Z, space, backspace & del characters !');
                                    status = 'This field accepts only 0-9, a-z, A_Z, space & del characters !'
                                    return false;
                                }
                            }
                        }
                    }
                }
                status = ''
                return true
            }
            function confirmdelete(name, id) {
                if (confirm('Are you sure you want to delete  companybranch ' + name + '?')) {
                    location.href = 'newbranch.php?del=' + id
                }
            }
        </script>
    </head>
    <body>
        <table width="100%" cellpadding="5">
            <tr align="companybranch">
                <td width="50%" valign="top">
                    <fieldset>
                        <legend>Company Branch Details</legend>
                        <form onSubmit='return MM_valIDateForm()' action="" method="post" name="newcompanybranch" ID="newcompanybranch">
                            <input name="ID" type="hidden" value="<?php echo $row_sql['id'] ?>" />
                            <table width="100%" border="0" cellspacing="0" cellpadding="4">
                                <tr valign="baseline">
                                    <td align='right' nowrap><strong><font color=red size=+1>* </font>Name:</strong></td>
                                    <td>
                                        <input size="40" maxlength="100" name="name" ID="name"  onFocus="this.style.backgroundColor = '#ffff00'"  onBlur="this.style.backgroundColor = '#F2FAFF'"  class='forms'   onkeypress='return valIDatechars(event)' value="<?php echo $row_sql['name']; ?>" >
                                    </td>
                                </tr>
                                <tr valign="baseline">
                                    <td align='right' nowrap><strong>Description:</strong></td>
                                    <td>
                                        <textarea cols='40' rows='3' name="desc"  onFocus="this.style.backgroundColor = '#ffff00'"  onBlur="this.style.backgroundColor = '#F2FAFF'" onChange='this.value = this.value.toUpperCase()' class='forms'  > <?php echo $row_sql['description']; ?></textarea>
                                    </td>
                                </tr>
                                <tr valign='companybranch'>
                                    <td colspan='2' ><div align='center'>
<?php if (isset($_SESSION['companybranch'])) { ?>
                                                <input name="update" type="submit" class="formsBlue" id="update" value="Update" />
                                                <a href="newcompanybranch.php?id=<?php $row_sql['id']; ?>"> <a onclick='javascript:confirmdelete("<?php echo $row_sql['name'] ?>", "<?php echo $row_sql['id'] ?> ")' ><input name='delete' type='button' class='formsRed' value='Delete'  > </a> </a>
<?php } else { ?>
                                                <input name='save' type='submit' class='formsBlue' value='Save'>
                                            <?php } ?>
                                            <input name='cancel' type='button' onClick="location = 'branchslist.php'" class='formsorg' value='Cancel'>
                                        </div></td>
                                </tr>
                            </table>
                        </form>
                    </fieldset
                    ></table>
    </body>
</html>
