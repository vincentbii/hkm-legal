<?php require_once('../connections/eProc.php'); 

 require_once('../activelog.php'); ?>
<?php
$currentPage = $_SERVER["PHP_SELF"];

$maxRows_bank = 30;
$pageNum_bank = 0;
if (isset($_GET['pageNum_bank'])) {
  $pageNum_bank = $_GET['pageNum_bank'];
}
$startRow_bank = $pageNum_bank * $maxRows_bank;

mysqli_select_db($eProc, $database_eProc);
$query_bank = "SELECT * FROM bank ";
$query_limit_bank = sprintf("%s LIMIT %d, %d", $query_bank, $startRow_bank, $maxRows_bank);
$bank = mysqli_query($eProc, $query_limit_bank) or die(mysqli_error($eProc));
$row_bank = mysqli_fetch_assoc($bank);

if (isset($_GET['totalRows_bank'])) {
  $totalRows_bank = $_GET['totalRows_bank'];
} else {
  $all_bank = mysqli_query($eProc, $query_bank);
  $totalRows_bank = mysqli_num_rows($all_bank);
}
$totalPages_bank = ceil($totalRows_bank/$maxRows_bank)-1;

$queryString_bank = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_bank") == false && 
        stristr($param, "totalRows_bank") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_bank = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_bank = sprintf("&totalRows_bank=%d%s", $totalRows_bank, $queryString_bank);
?>

<html>
<head>
<title>LEGAL  MANAGEMENT INFORMATION  SYSTEM</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../styles/default.css" rel="stylesheet" type="text/css">

<style type="text/css">
<!--
.style1 {
	color: #FF0000;
	font-weight: bold;
}
-->
</style>
</head>
<body>

<table width="100%" border="0" cellpadding="4" cellspacing="0" >

  <tr>
   <td width="36%"   class="inputdeft" style="font-weight: bold"> Bank</td>
   
	<td width="64%"   class="inputdeft" style="font-weight: bold" ><div align="right">
	  <table width="154"  border="0" cellspacing="0" cellpadding="3">
	    <tr class="inputdeft">
	      <td width="19"  ><img src="../images/icons/newproject.gif" width="15" height="13" border="0" /></td>
                
			<td width="123"   ><a href="../adm/newbank.php">New Bank  </a></td>
          </tr>
	    </table>
	  </div></td>
  </tr>
  <?php if ($totalRows_bank > 0) { ?>
  <?php do { ?>
  <tr>
  <td ><?php echo $row_bank['name']?> </td>
  
  <td ><a href="../adm/newbank.php?id=<?php echo $row_bank['id'] ?>">Edit</a></td>
  </tr>
  <?php } while ($row_bank = mysqli_fetch_assoc($bank)); ?>
  <tr>
    <td colspan="12" class="mainbase"><table width="97%"  border="0" cellspacing="0" cellpadding="3">
      <tr align="center">
        <td width="45%">
          <table width="78%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
              <td width="23%" align="center"><?php if ($pageNum_bank > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_bank=%d%s", $currentPage, 0, $queryString_bank); ?>">First</a>
                <?php } // Show if not first page ?>              </td>
              <td width="31%" align="center"><?php if ($pageNum_bank > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_bank=%d%s", $currentPage, max(0, $pageNum_bank - 1), $queryString_bank); ?>">Previous</a>
                <?php } // Show if not first page ?>              </td>
              <td width="23%" align="center"><?php if ($pageNum_bank < $totalPages_bank) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_bank=%d%s", $currentPage, min($totalPages_bank, $pageNum_bank + 1), $queryString_bank); ?>">Next</a>
                <?php } // Show if not last page ?>              </td>
              <td width="23%" align="center"><?php if ($pageNum_bank < $totalPages_bank) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_bank=%d%s", $currentPage, $totalPages_bank, $queryString_bank); ?>">last</a>
                <?php } // Show if not last page ?>              </td>
            </tr>
          </table></td>
        <td width="55%">&nbsp; Showing <strong><?php echo ($startRow_bank + 1) ?></strong> to <strong><?php echo min($startRow_bank + $maxRows_bank, $totalRows_bank) ?></strong> of <strong><?php echo $totalRows_bank ?></strong> </td>
      </tr>
    </table></td>
  </tr>
  <?php } else { ?>
  <tr>
    <td colspan="12" class="mainbase"><span class="style1">No banks Created! </span></td>
  </tr>
  <?php } ?>
</table>
</body>
</html>
<?php
mysqli_free_result($bank);
?>

