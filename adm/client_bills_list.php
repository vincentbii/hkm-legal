<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require('../connections/eProc.php');
include('../activelog.php');
?>

<?php
if (($_GET['action'] == 'delete') && isset($_GET['id'])) { // if delete was requested AND an id is present...
    mysqli_select_db($eProc, $database_eProc);
    $sqlClient = "select client_id from bills where id = '" . $_GET['id'] . "'";
    $queryClient = mysqli_query($eProc, $sqlClient) or die(mysqli_error($eProc));
    $clientRow = mysqli_fetch_array($queryClient);
    $clientid = $clientRow['client_id'];

    
    $sql = mysqli_query($eProc, "DELETE FROM bills WHERE id = '" . $_GET['id'] . "'") or die(mysqli_error($eProc));

    if ($sql) {

        echo 'Record deleted!';
        ?>
        <script language='javascript'>
            location = 'client_bills_list.php?cid=<?php echo $clientid; ?>'
        </script>
        <?php
    }
}



if (isset($_GET["del"])) {
    $id = $_GET["del"];

    $querySql = "DELETE FROM bills WHERE id='" . $_GET['del'] . "'";
    $resultSql = mysqli_query($eProc, $querySql) or die(mysqli_error($eProc));
    if ($resultSql) {
        header('Location: client_bills_list.php');
    } else {
        echo "Failed to delete nill.";
    }
}
?>

<?php
$currentPage = $_SERVER["PHP_SELF"];

$maxRows_bills = 20;
$pageNum_bills = 0;

if (isset($_GET['pageNum_bills'])) {
    $pageNum_bills = $_GET['pageNum_bills'];
}
$startRow_bills = $pageNum_bills * $maxRows_bills;

mysqli_select_db($eProc, $database_eProc);


$query_bills = "SELECT * FROM bills WHERE client_id = '" . $_GET['cid'] . "' AND status = 1 ORDER BY id ASC ";
$query_limit_bills = sprintf("%s LIMIT %d, %d", $query_bills, $startRow_bills, $maxRows_bills);
$bills = mysqli_query($eProc, $query_limit_bills) or die(mysqli_error());
$row_bills = mysqli_fetch_assoc($bills);

if (isset($_GET['totalRows_bills'])) {
    $totalRows_bills = $_GET['totalRows_bills'];
} else {
    $all_bills = mysqli_query($eProc, $query_bills);
    $totalRows_bills = mysqli_num_rows($all_bills);
}
$totalPages_bills = ceil($totalRows_bills / $maxRows_bills) - 1;

$queryString_bills = "";
if (!empty($_SERVER['QUERY_STRING'])) {
    $params = explode("&", $_SERVER['QUERY_STRING']);
    $newParams = array();
    foreach ($params as $param) {
        if (stristr($param, "pageNum_bills") == false &&
                stristr($param, "totalRows_bills") == false) {
            array_push($newParams, $param);
        }
    }
    if (count($newParams) != 0) {
        $queryString_bills = "&" . htmlentities(implode("&", $newParams));
    }
}
$queryString_bills = sprintf("&totalRows_bills=%d%s", $totalRows_bills, $queryString_bills);
?>

<html>
    <head>
        <title>LEGAL MANAGEMENT SYSTEM</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="../styles/default.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <style type="text/css">
            <!--
            .style1 {
                color: #FF0000;
                font-weight: bold;
            }
            -->
        </style>
    </head>
    <body>
        <table class="table-condensed" width="70%" border="0" cellpadding="4" cellspacing="0" >

            <tr>
                <td width="25%"   class="inputdef" style="font-weight: bold">Bill ID</td>
                <td width="25%"   class="inputdef" style="font-weight: bold">Bill Date</td>
                <td width="25%"   class="inputdef" style="font-weight: bold">Bill Name</td>
                <td width="25%"   class="inputdef" style="font-weight: bold">Amount</td>
                <td width="25%"   class="inputdef" style="font-weight: bold" >
                    Action
                </td>
            </tr>



            <?php
            $total_amount = 0;
            if ($totalRows_bills > 0) {
                $k = 0;
                do {
                    $bill_id = $row_bills['id'];
                    ?>
                    <?php
                    if ($k == 1) {
                        echo '<tr class="EvenTableRows">';
                        $k = 0;
                    } else {
                        echo '<tr class="OddTableRows">';
                        $k++;
                    }
                    ?>
                    <td><?php echo $row_bills['id']; ?></td>
                    <td><?php echo $row_bills['created_at']; ?></td>
                    <td><?php echo $row_bills['service_name']; ?></td>
                    <td><?php echo number_format($row_bills['amount']); ?></td>
                    <td>
                        <a class="btn btn-danger btn-xs" href="client_bills_list.php?action=delete&id=<?php echo $bill_id; ?>"><span class="glyphicon glyphicon-remove"></span></a>
                        <!--<a href='client_bills_list.php?b_id=<?PHP //echo $row_bills['id'];   ?>' class='btn btn-danger btn-xs'></a>-->
                    </td>
                </tr>
                <?php
                $total_amount = $total_amount + $row_bills['amount'];
            } while ($row_bills = mysqli_fetch_assoc($bills));
        }
        ?>
        <tr>
            <td colspan="3" width="9%" style="font-weight: bold">Total Amount</td>
            <td width="9%" style="font-weight: bold">
                <?php echo number_format($total_amount); ?>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <div align='right'>
                    <table width=""  border="0" cellspacing="0" cellpadding="3">
                        <tr class="inputdef">
                            <td width="123"><a class=" btn-xs" href="generate_bill.php?cid=<?php echo $_GET['cid']; ?>">Generate Invoice</a></td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>

</body>
</html>