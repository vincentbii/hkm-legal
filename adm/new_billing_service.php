<?php
require_once('../connections/eProc.php');
include('../activelog.php');
mysqli_select_db($eProc, $database_eProc);
?>

<?php
if (isset($_POST['servicename']) && $_POST['servicename'] != '') {
    $_SESSION['servicename'] = $_POST['servicename'];
}
if (isset($_POST['servicename']) && $_POST['servicename'] == '') {
    unset($_SESSION['servicename']);
}

if (isset($_POST['servicecost']) && $_POST['servicecost'] != '') {
    $_SESSION['servicecost'] = $_POST['servicecost'];
}
if (isset($_POST['servicecost']) && $_POST['servicecost'] == '') {
    unset($_SESSION['servicecost']);
}

if (isset($_GET['id']) && $_GET['id'] !== '') {
    $selectSQL = "SELECT * FROM services  WHERE id=" . $_GET['id'] . "";
    //echo $selectSQL;	
    $sql_select = mysqli_query($eProc, $selectSQL) or die(mysqli_error());
    $row_sql = mysqli_fetch_assoc($sql_select);
    $nums = mysqli_num_rows($sql_select);
    $_SESSION['name'] = $row_sql['name'];
    $_SESSION['amount'] = $row_sql['amount'];
    $_SESSION['description'] = $row_sql['description'];
}

if (isset($_POST['update']) && $_POST['update'] == 'Update') {
    $updateSQL = " UPDATE `services` SET `name` =  '" . $_POST['servicename'] . "',
    `amount` = '" . $_POST['servicecost'] . "',
    `description` = '" . $_POST['description'] . "'
      WHERE  id='" . $_GET['id'] . "'";

    //echo $updateSQL;
    $update = mysqli_query($eProc, $updateSQL) or die(mysqli_error($eProc));
    ?>
    <script language='javascript'>
        location = 'billing_services.php'
    </script>
    <?php
}

if (isset($_GET['del']) && $_GET['del'] != "") {
    $DeleteSQL = "DELETE FROM services WHERE  id ='" . $_GET['del'] . "'";
    //echo $DeleteSQL;
    $Result3 = mysqli_query($eProc, $DeleteSQL);
    ?>      
    <script language='javascript'>location = 'billing_services.php'</script>
        <?php
}

if (isset($_POST['save']) && $_POST['save'] == 'Save') {
    $insertSQL = sprintf("INSERT INTO `services` (`name` ,
`amount`, description)

    VALUES (%s,%s, %s)", GetSQLValueString($_POST['servicename'], "text"), GetSQLValueString($_POST['servicecost'], "text"), GetSQLValueString($_POST['description'], "text"));

    $Result2 = mysqli_query($eProc, $insertSQL) or die(mysqli_error($eProc));
    ?>
    <script language='javascript'>
        location = 'billing_services.php'
    </script>
    <?php
}
?>

<html>
    <head>
        <title>LEGAL MANAGEMENT SYSTEM</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="default.css" REL="stylesheet" type="text/css">
        <LINK REL=StyleSheet  href="calendar.css"  TYPE="text/css">
        <script LANGUAGE="JavaScript"  src="weeklycalendar.js"></script>
        <link href="../styles/default.css" rel="stylesheet" type="text/css">

        <script language="JavaScript" type="text/javascript">

        buildWeeklyCalendar(1);

        function confirmdelete(name, id) {
            if (confirm('Are you sure you want to delete  service ' + name + '?')) {
                location.href = 'new_billing_service.php?del=' + id
            }
        }

        </script>

    </head>
    <body>
        <fieldset>
            <legend>Billing Service Details</legend>
            <form id="newservice" name="newservice" method="POST" onSubmit='return MM_valIDateForm()'  enctype="multipart/form-data">
                <table width="100%" border="0" cellspacing="0" cellpadding="4">
                    <tr valign='center'>
                        <td width="37%" align='right'>Service Name:</td>
                        <td width="63%">
                            <input type="text"  class="forms" name="servicename" value="<?php echo $_SESSION['name']; ?>" required />
                        </td>
                    </tr>
                    <tr valign='center'>
                        <td width="37%" align='right'>Cost:</td>
                        <td width="63%">
                            <input type="text"  class="forms" name="servicecost" value="<?php echo $_SESSION['amount']; ?>" required />
                        </td>
                    </tr>
                    <tr valign='center'>
                        <td width="37%" align='right'>Description:</td>
                        <td width="63%">
                            <input type="text"  class="forms" name="description" value="<?php echo $_SESSION['description']; ?>" required />
                        </td>
                    </tr>

                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="4">
                    <tr valign='center'>
                        <td colspan='2' ><div align='center'>
                                <?php if ($_GET['id'] != '') { ?>
                                    <input name='update' type='submit' class='formsBlue' value='Update'>
                                    <a href="new_billing_service.php?id=<?php $_GET['did']; ?>"> <a onclick='javascript:confirmdelete("<?php echo $row_sql['name'] ?>", "<?php echo $_GET['id'] ?> ")' ><input name='delete' type='button' class='formsRed' value='Delete'  > </a> </a>
                                <?php } else { ?>
                                    <input name='save' type='submit' class='formsBlue' value='Save'>
                                    <?php
                                }
                                ?>
                                <input name='cancel' type='button' onClick="parent.location = 'billing.php'" class='formsorg' value='Cancel'>
                            </div></td>
                    </tr>
                </table>
            </form>
        </fieldset>
    </body>
</html>

