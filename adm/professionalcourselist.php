<?php require_once('../connections/eProc.php'); 

 require_once('../activelog.php'); ?>
<?php
$currentPage = $_SERVER["PHP_SELF"];

$maxRows_professionalcourse = 30;
$pageNum_professionalcourse = 0;
if (isset($_GET['pageNum_professionalcourse'])) {
  $pageNum_professionalcourse = $_GET['pageNum_professionalcourse'];
}
$startRow_professionalcourse = $pageNum_professionalcourse * $maxRows_professionalcourse;

mysqli_select_db($eProc, $database_eProc);
$query_professionalcourse = "SELECT * FROM proffesionalcourse ";
$query_limit_professionalcourse = sprintf("%s LIMIT %d, %d", $query_professionalcourse, $startRow_professionalcourse, $maxRows_professionalcourse);
$professionalcourse = mysqli_query($eProc, $query_limit_professionalcourse) or die(mysqli_error($eProc));
$row_professionalcourse = mysqli_fetch_assoc($professionalcourse);

if (isset($_GET['totalRows_professionalcourse'])) {
  $totalRows_professionalcourse = $_GET['totalRows_professionalcourse'];
} else {
  $all_professionalcourse = mysqli_query($eProc, $query_professionalcourse);
  $totalRows_professionalcourse = mysqli_num_rows($all_professionalcourse);
}
$totalPages_professionalcourse = ceil($totalRows_professionalcourse/$maxRows_professionalcourse)-1;

$queryString_professionalcourse = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_professionalcourse") == false && 
        stristr($param, "totalRows_professionalcourse") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_professionalcourse = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_professionalcourse = sprintf("&totalRows_professionalcourse=%d%s", $totalRows_professionalcourse, $queryString_professionalcourse);
?>

<html>
<head>
<title>LEGAL  MANAGEMENT INFORMATION  SYSTEM</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../styles/default.css" rel="stylesheet" type="text/css">

<style type="text/css">
<!--
.style1 {
	color: #FF0000;
	font-weight: bold;
}
-->
</style>
</head>
<body>

<table width="100%" border="0" cellpadding="4" cellspacing="0" >

  <tr>
   <td width="50%"   class="inputdeft" style="font-weight: bold"> Proffesional Course</td>
   
	<td width="50%"   class="inputdeft" style="font-weight: bold" ><div align="right">
	  <table width="154"  border="0" cellspacing="0" cellpadding="3">
	    <tr class="inputdeft">
	      <td width="15"  ><img src="../images/icons/newproject.gif" width="15" height="13" border="0" /></td>
                
			<td width="127"   ><a href="../adm/newprofessionalcourse.php">New  Course  </a></td>
          </tr>
	    </table>
	  </div></td>
  </tr>
  <?php if ($totalRows_professionalcourse > 0) { ?>
  <?php do { ?>
  <tr>
  <td ><?php echo $row_professionalcourse['name']?> </td>
  
  <td ><a href="../adm/newprofessionalcourse.php?id=<?php echo $row_professionalcourse['id'] ?>">Edit</a></td>
  </tr>
  <?php } while ($row_professionalcourse = mysqli_fetch_assoc($professionalcourse)); ?>
  <tr>
    <td colspan="12" class="mainbase"><table width="97%"  border="0" cellspacing="0" cellpadding="3">
      <tr align="center">
        <td width="45%">
          <table width="78%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
              <td width="23%" align="center"><?php if ($pageNum_professionalcourse > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_professionalcourse=%d%s", $currentPage, 0, $queryString_professionalcourse); ?>">First</a>
                <?php } // Show if not first page ?>              </td>
              <td width="31%" align="center"><?php if ($pageNum_professionalcourse > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_professionalcourse=%d%s", $currentPage, max(0, $pageNum_professionalcourse - 1), $queryString_professionalcourse); ?>">Previous</a>
                <?php } // Show if not first page ?>              </td>
              <td width="23%" align="center"><?php if ($pageNum_professionalcourse < $totalPages_professionalcourse) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_professionalcourse=%d%s", $currentPage, min($totalPages_professionalcourse, $pageNum_professionalcourse + 1), $queryString_professionalcourse); ?>">Next</a>
                <?php } // Show if not last page ?>              </td>
              <td width="23%" align="center"><?php if ($pageNum_professionalcourse < $totalPages_professionalcourse) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_professionalcourse=%d%s", $currentPage, $totalPages_professionalcourse, $queryString_professionalcourse); ?>">last</a>
                <?php } // Show if not last page ?>              </td>
            </tr>
          </table></td>
        <td width="55%">&nbsp; Showing <strong><?php echo ($startRow_professionalcourse + 1) ?></strong> to <strong><?php echo min($startRow_professionalcourse + $maxRows_professionalcourse, $totalRows_professionalcourse) ?></strong> of <strong><?php echo $totalRows_professionalcourse ?></strong> </td>
      </tr>
    </table></td>
  </tr>
  <?php } else { ?>
  <tr>
    <td colspan="12" class="mainbase"><span class="style1">No professionalcourses Created! </span></td>
  </tr>
  <?php } ?>
</table>
</body>
</html>
<?php
mysqli_free_result($professionalcourse);
?>

