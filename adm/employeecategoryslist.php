<?php require_once('../connections/eProc.php'); 

 require_once('../activelog.php'); ?>
<?php
$currentPage = $_SERVER["PHP_SELF"];

$maxRows_employeecategory = 30;
$pageNum_employeecategory = 0;
if (isset($_GET['pageNum_employeecategory'])) {
  $pageNum_employeecategory = $_GET['pageNum_employeecategory'];
}
$startRow_employeecategory = $pageNum_employeecategory * $maxRows_employeecategory;

mysqli_select_db($eProc, $database_eProc);
$query_employeecategory = "SELECT * FROM employeecategory ";
$query_limit_employeecategory = sprintf("%s LIMIT %d, %d", $query_employeecategory, $startRow_employeecategory, $maxRows_employeecategory);
$employeecategory = mysqli_query($eProc,$query_limit_employeecategory) or die(mysqli_error($eProc));
$row_employeecategory = mysqli_fetch_assoc($employeecategory);

if (isset($_GET['totalRows_employeecategory'])) {
  $totalRows_employeecategory = $_GET['totalRows_employeecategory'];
} else {
  $all_employeecategory = mysqli_query($eProc,$query_employeecategory);
  $totalRows_employeecategory = mysqli_num_rows($all_employeecategory);
}
$totalPages_employeecategory = ceil($totalRows_employeecategory/$maxRows_employeecategory)-1;

$queryString_employeecategory = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_employeecategory") == false && 
        stristr($param, "totalRows_employeecategory") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_employeecategory = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_employeecategory = sprintf("&totalRows_employeecategory=%d%s", $totalRows_employeecategory, $queryString_employeecategory);
?>

<html>
<head>
<title>LEGAL  MANAGEMENT INFORMATION  SYSTEM</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../styles/default.css" rel="stylesheet" type="text/css">

<style type="text/css">
<!--
.style1 {
	color: #FF0000;
	font-weight: bold;
}
-->
</style>
</head>
<body>

<table width="100%" border="0" cellpadding="4" cellspacing="0" >

  <tr>
   <td width="15%"   class="inputdeft" >Job Groups</td>
   <td width="17%"   class="inputdeft" >Lowest Salary</td>
   <td width="28%"   class="inputdeft" >Highest Salary</td>
   
	<td width="40%"   class="inputdeft"  ><div align="right">
	  <table width="154"  border="0" cellspacing="0" cellpadding="3">
	    <tr class="inputdeft">
	      <td width="15"  ><img src="../images/icons/newproject.gif" width="15" height="13" border="0" /></td>
                
			<td width="127"   ><a href="../adm/newemployeecategory.php">New Job Group </a></td>
          </tr>
	    </table>
	  </div></td>
  </tr>
  <?php if ($totalRows_employeecategory > 0) { ?>
  <?php do { ?>
  <tr>
  <td ><?php echo $row_employeecategory['name']?> </td>
   <td ><?php echo $row_employeecategory['lowestsalary']?> </td>
    <td ><?php echo $row_employeecategory['highestsalary']?> </td>
  
  <td ><a href="../adm/newemployeecategory.php?id=<?php echo $row_employeecategory['id'] ?>">Edit</a></td>
  </tr>
  <?php } while ($row_employeecategory = mysqli_fetch_assoc($employeecategory)); ?>
  <tr>
    <td colspan="12" class="mainbase"><table width="97%"  border="0" cellspacing="0" cellpadding="3">
      <tr align="center">
        <td width="45%">
          <table width="78%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
              <td width="23%" align="center"><?php if ($pageNum_employeecategory > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_employeecategory=%d%s", $currentPage, 0, $queryString_employeecategory); ?>">First</a>
                <?php } // Show if not first page ?>              </td>
              <td width="31%" align="center"><?php if ($pageNum_employeecategory > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_employeecategory=%d%s", $currentPage, max(0, $pageNum_employeecategory - 1), $queryString_employeecategory); ?>">Previous</a>
                <?php } // Show if not first page ?>              </td>
              <td width="23%" align="center"><?php if ($pageNum_employeecategory < $totalPages_employeecategory) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_employeecategory=%d%s", $currentPage, min($totalPages_employeecategory, $pageNum_employeecategory + 1), $queryString_employeecategory); ?>">Next</a>
                <?php } // Show if not last page ?>              </td>
              <td width="23%" align="center"><?php if ($pageNum_employeecategory < $totalPages_employeecategory) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_employeecategory=%d%s", $currentPage, $totalPages_employeecategory, $queryString_employeecategory); ?>">last</a>
                <?php } // Show if not last page ?>              </td>
            </tr>
          </table></td>
        <td width="55%">&nbsp; Showing <strong><?php echo ($startRow_employeecategory + 1) ?></strong> to <strong><?php echo min($startRow_employeecategory + $maxRows_employeecategory, $totalRows_employeecategory) ?></strong> of <strong><?php echo $totalRows_employeecategory ?></strong> </td>
      </tr>
    </table></td>
  </tr>
  <?php } else { ?>
  <tr>
    <td colspan="12" class="mainbase"><span class="style1">No employeecategorys Created! </span></td>
  </tr>
  <?php } ?>
</table>
</body>
</html>
<?php
mysqli_free_result($employeecategory);
?>

