<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once('../connections/eProc.php');
include('../activelog.php');
mysqli_select_db($eProc, $database_eProc);

$client_id = $_GET['cid'];

$currentPage = $_SERVER["PHP_SELF"];

$maxRows_payments = 20;
$pageNum_payments = 0;


unset($_SESSION['itype']);


if (isset($_GET['pageNum_payments'])) {
    $pageNum_payments = $_GET['pageNum_payments'];
}
$startRow_payments = $pageNum_payments * $maxRows_payments;

mysqli_select_db($eProc, $database_eProc);

$query_payments = "SELECT payment_method.payment_name, payments.* FROM payments inner join "
        . "invoices on invoices.id = payments.invoice_id "
        . "inner join payment_method on payments.payment_method = payment_method.id"
        . " WHERE invoices.client_id = $client_id "
        . "order by payments.id ASC";
$query_limit_payments = sprintf("%s LIMIT %d, %d", $query_payments, $startRow_payments, $maxRows_payments);
$payments = mysqli_query($eProc, $query_limit_payments) or die(mysqli_error());
$row_payments = mysqli_fetch_assoc($payments);

if (isset($_GET['totalRows_payments'])) {
    $totalRows_payments = $_GET['totalRows_payments'];
} else {
    $all_payments = mysqli_query($eProc, $query_payments);
    $totalRows_payments = mysqli_num_rows($all_payments);
}
$totalPages_payments = ceil($totalRows_payments / $maxRows_payments) - 1;

$queryString_payments = "";
if (!empty($_SERVER['QUERY_STRING'])) {
    $params = explode("&", $_SERVER['QUERY_STRING']);
    $newParams = array();
    foreach ($params as $param) {
        if (stristr($param, "pageNum_payments") == false &&
                stristr($param, "totalRows_payments") == false) {
            array_push($newParams, $param);
        }
    }
    if (count($newParams) != 0) {
        $queryString_payments = "&" . htmlentities(implode("&", $newParams));
    }
}
$queryString_payments = sprintf("&totalRows_payments=%d%s", $totalRows_payments, $queryString_payments);
?>
<html>
    <head>
        <title>LEGAL MANAGEMENT SYSTEM</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="../styles/default.css" rel="stylesheet" type="text/css">
        <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="../font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css">

    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-9 col-sm-offset-1">
                    <table class="table" border="0" width="100%">
                        <thead>
                            <tr>
                                <th>Action</th>
                                <th>Payment</th>
                                <th>Invoice</th>
                                <th>Amount</th>
                                <th>Note</th>
                                <th>Payment Method</th>
                                <th>Date Of Payment</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if ($totalRows_payments > 0) {
                                $k = 0
                                ?>
                                <?php
                                do {
                                    if ($k == 1) {
                                        echo '<tr class="EvenTableRows">';
                                        $k = 0;
                                    } else {
                                        echo '<tr class="OddTableRows">';
                                        $k++;
                                    }
                                    ?>

                                <td></td>
                                <td ><?php echo $row_payments['id']; ?></td>
                                <td ><?php echo $row_payments['invoice_id']; ?></td>
                                <td ><?php echo number_format($row_payments['amount']); ?></td>
                                <td ><?php echo $row_payments['note']; ?> </td>
                                <td ><?php echo $row_payments['payment_name']; ?> </td>
                                <td ><?php echo $row_payments['created_at']; ?> </td>
                                </tr>
                            <?php } while ($row_payments = mysqli_fetch_assoc($payments)); ?>
                            <tr>
                                <td colspan="6" class="mainbase"><table width="97%"  border="0" cellspacing="0" cellpadding="3">
                                        <tr align="center">
                                            <td width="45%">
                                                <table width="78%" border="0" align="center" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td width="23%" align="center"><?php if ($pageNum_payments > 0) { // Show if not first page      ?>
                                                                <a href="<?php printf("%s?pageNum_payments=%d%s", $currentPage, 0, $queryString_payments); ?>">First</a>
                                                            <?php } // Show if not first page     ?>              </td>
                                                        <td width="31%" align="center"><?php if ($pageNum_payments > 0) { // Show if not first page     ?>
                                                                <a href="<?php printf("%s?pageNum_payments=%d%s", $currentPage, max(0, $pageNum_payments - 1), $queryString_payments); ?>">Previous</a>
                                                            <?php } // Show if not first page     ?>              </td>
                                                        <td width="23%" align="center"><?php if ($pageNum_payments < $totalPages_payments) { // Show if not last page     ?>
                                                                <a href="<?php printf("%s?pageNum_payments=%d%s", $currentPage, min($totalPages_payments, $pageNum_payments + 1), $queryString_payments); ?>">Next</a>
                                                            <?php } // Show if not last page     ?>              </td>
                                                        <td width="23%" align="center"><?php if ($pageNum_payments < $totalPages_payments) { // Show if not last page     ?>
                                                                <a href="<?php printf("%s?pageNum_payments=%d%s", $currentPage, $totalPages_payments, $queryString_payments); ?>">last</a>
                                                            <?php } // Show if not last page     ?>              </td>
                                                    </tr>
                                                </table></td>
                                            <td width="55%">&nbsp; Showing <strong><?php echo ($startRow_payments + 1) ?></strong> to <strong><?php echo min($startRow_payments + $maxRows_payments, $totalRows_payments) ?></strong> of <strong><?php echo $totalRows_payments ?></strong> </td>
                                        </tr>
                                    </table></td>
                            </tr>
                        <?php } else { ?>
                            <tr>
                                <td colspan="6" class="mainbase"><span class="style1">No payments Created! </span></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </body>
</html>
