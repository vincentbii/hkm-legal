<?php
require('../fpdf17/fpdf.php');
require_once('../connections/eProc.php');
//require_once('../activelog.php');
mysqli_select_db($eProc, $database_eProc);

//Select the Products you want to show in your PDF file
$query_invoices = "SELECT * from invoices";
$invoices = mysqli_query($eProc, $query_invoices) or die(mysqli_error());

//Initialize the 3 columns and the total
$column_code = "";
$column_name = "";
$column_price = "";
$total = 0;

//For each row, add the field to the corresponding column
while($row = $invoices->fetch_assoc())
{
	$code = $row["id"];
	$name = substr($row["client_id"],0,20);
	$real_price = $row["total_amount"];
	$price_to_show = number_format($row["paid"]);

	$column_code = $column_code.$code."\n";
	$column_name = $column_name.$name."\n";
	$column_price = $column_price.$price_to_show."\n";

	//Sum all the Prices (TOTAL)
	$total = $total+$real_price;
}


//Convert the Total Price to a number with (.) for thousands, and (,) for decimals.
$total = number_format($total);

//Create a new PDF file
$pdf=new FPDF();
$pdf->AddPage();

//Fields Name position
$Y_Fields_Name_position = 20;
//Table position, under Fields Name
$Y_Table_Position = 26;

//First create each Field Name
//Gray color filling each Field Name box
$pdf->SetFillColor(232,232,232);
//Bold Font for Field Name
$pdf->SetFont('Arial','B',12);
$pdf->SetY($Y_Fields_Name_position);
$pdf->SetX(45);
$pdf->Cell(20,6,'id',1,0,'L',1);
$pdf->SetX(65);
$pdf->Cell(100,6,'client_id',1,0,'L',1);
$pdf->SetX(135);
$pdf->Cell(30,6,'total_amount',1,0,'R',1);
$pdf->Ln();

//Now show the 3 columns
$pdf->SetFont('Arial','',12);
$pdf->SetY($Y_Table_Position);
$pdf->SetX(45);
$pdf->MultiCell(20,6,$column_code,1);
$pdf->SetY($Y_Table_Position);
$pdf->SetX(65);
$pdf->MultiCell(100,6,$column_name,1);
$pdf->SetY($Y_Table_Position);
$pdf->SetX(135);
$pdf->MultiCell(30,6,$column_price,1,'R');
$pdf->SetX(135);
$pdf->MultiCell(30,6,'$ '.$total,1,'R');

//Create lines (boxes) for each ROW (Product)
//If you don't use the following code, you don't create the lines separating each row
$i = 0;
$pdf->SetY($Y_Table_Position);

mysqli_close($eProc);
$pdf->Output();
?>
