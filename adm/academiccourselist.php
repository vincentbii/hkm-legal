<?php require_once('../connections/eProc.php'); 

 require_once('../activelog.php'); ?>
<?php
$currentPage = $_SERVER["PHP_SELF"];

$maxRows_academiccourse = 30;
$pageNum_academiccourse = 0;
if (isset($_GET['pageNum_academiccourse'])) {
  $pageNum_academiccourse = $_GET['pageNum_academiccourse'];
}
$startRow_academiccourse = $pageNum_academiccourse * $maxRows_academiccourse;

mysqli_select_db($eProc, $database_eProc);
$query_academiccourse = "SELECT * FROM academiccourse ";
$query_limit_academiccourse = sprintf("%s LIMIT %d, %d", $query_academiccourse, $startRow_academiccourse, $maxRows_academiccourse);
$academiccourse = mysqli_query($eProc, $query_limit_academiccourse) or die(mysqli_error($eProc));
$row_academiccourse = mysqli_fetch_assoc($academiccourse);

if (isset($_GET['totalRows_academiccourse'])) {
  $totalRows_academiccourse = $_GET['totalRows_academiccourse'];
} else {
  $all_academiccourse = mysqli_query($eProc, $query_academiccourse);
  $totalRows_academiccourse = mysqli_num_rows($all_academiccourse);
}
$totalPages_academiccourse = ceil($totalRows_academiccourse/$maxRows_academiccourse)-1;

$queryString_academiccourse = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_academiccourse") == false && 
        stristr($param, "totalRows_academiccourse") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_academiccourse = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_academiccourse = sprintf("&totalRows_academiccourse=%d%s", $totalRows_academiccourse, $queryString_academiccourse);
?>

<html>
<head>
<title>BMA MANAGEMENT INFORMATION  SYSTEM</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../styles/default.css" rel="stylesheet" type="text/css">

<style type="text/css">
<!--
.style1 {
	color: #FF0000;
	font-weight: bold;
}
-->
</style>
</head>
<body>

<table width="100%" border="0" cellpadding="4" cellspacing="0" >

  <tr class="inputdeft">
   <td width="36%"   class="inputdeft" style="font-weight: bold"> Academic Course</td>
   
	<td width="64%"   class="inputdeft" style="font-weight: bold" ><div align="right">
	  <table width="154"  border="0" cellspacing="0" cellpadding="3">
	    <tr class="inputdeft">
	      <td width="15"  ><img src="../images/icons/newproject.gif" width="15" height="13" border="0" /></td>
                
			<td width="127"   ><a href="../adm/newacademiccourse.php">New Academic Course  </a></td>
          </tr>
	    </table>
	  </div></td>
  </tr>
  <?php if ($totalRows_academiccourse > 0) { ?>
  <?php do { ?>
  <tr>
  <td ><?php echo $row_academiccourse['name']?> </td>
  
  <td ><a href="../adm/newacademiccourse.php?id=<?php echo $row_academiccourse['id'] ?>">Edit</a></td>
  </tr>
  <?php } while ($row_academiccourse = mysqli_fetch_assoc($academiccourse)); ?>
  <tr>
    <td colspan="12" class="mainbase"><table width="97%"  border="0" cellspacing="0" cellpadding="3">
      <tr align="center">
        <td width="45%">
          <table width="78%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
              <td width="23%" align="center"><?php if ($pageNum_academiccourse > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_academiccourse=%d%s", $currentPage, 0, $queryString_academiccourse); ?>">First</a>
                <?php } // Show if not first page ?>              </td>
              <td width="31%" align="center"><?php if ($pageNum_academiccourse > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_academiccourse=%d%s", $currentPage, max(0, $pageNum_academiccourse - 1), $queryString_academiccourse); ?>">Previous</a>
                <?php } // Show if not first page ?>              </td>
              <td width="23%" align="center"><?php if ($pageNum_academiccourse < $totalPages_academiccourse) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_academiccourse=%d%s", $currentPage, min($totalPages_academiccourse, $pageNum_academiccourse + 1), $queryString_academiccourse); ?>">Next</a>
                <?php } // Show if not last page ?>              </td>
              <td width="23%" align="center"><?php if ($pageNum_academiccourse < $totalPages_academiccourse) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_academiccourse=%d%s", $currentPage, $totalPages_academiccourse, $queryString_academiccourse); ?>">last</a>
                <?php } // Show if not last page ?>              </td>
            </tr>
          </table></td>
        <td width="55%">&nbsp; Showing <strong><?php echo ($startRow_academiccourse + 1) ?></strong> to <strong><?php echo min($startRow_academiccourse + $maxRows_academiccourse, $totalRows_academiccourse) ?></strong> of <strong><?php echo $totalRows_academiccourse ?></strong> </td>
      </tr>
    </table></td>
  </tr>
  <?php } else { ?>
  <tr>
    <td colspan="12" class="mainbase"><span class="style1">No academiccourses Created! </span></td>
  </tr>
  <?php } ?>
</table>
</body>
</html>
<?php
mysqli_free_result($academiccourse);
?>

