<?php
require_once('../connections/eProc.php');
include('../activelog.php');
mysqli_select_db($eProc, $database_eProc);
?>

<html>
    <head>
        <title>LEGAL MANAGEMENT SYSTEM</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="../styles/default.css" rel="stylesheet" type="text/css">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    </head>
    <body>
        <div class="container">
            <h2>Client Invoices</h2>
            <p></p>
            <?php
            $currentPage = $_SERVER["PHP_SELF"];

            $maxRows_invoices = 20;
            $pageNum_invoices = 0;

            if (isset($_GET['pageNum_invoices'])) {
                $pageNum_invoices = $_GET['pageNum_invoices'];
            }
            $startRow_invoices = $pageNum_invoices * $maxRows_invoices;

            $client_id = $_GET['cid'];
            ?>
            <ul class="nav nav-tabs">
                <li class="active"><a href="#all">All Invoices</a></li>
                <li><a href="#paid">Paid</a></li>
                <li><a href="#unpaid">Unpaid</a></li>
            </ul>

            <div class="tab-content">
                <div id="all" class="tab-pane fade in active">
                    <?php
                    $query_invoices = "SELECT invoices.*, clients.fulnames"
                            . "  FROM "
                            . "invoices inner join clients on invoices.client_id = clients.id"
                            . " WHERE client_id = $client_id ";

                    $query_limit_invoices = sprintf("%s LIMIT %d, %d", $query_invoices, $startRow_invoices, $maxRows_invoices);
                    $invoices = mysqli_query($eProc, $query_limit_invoices) or die(mysqli_error());


                    if (isset($_GET['totalRows_invoices'])) {
                        $totalRows_invoices = $_GET['totalRows_invoices'];
                    } else {
                        $all_invoices = mysqli_query($eProc, $query_invoices);
                        $totalRows_invoices = mysqli_num_rows($all_invoices);
                    }
                    $totalPages_invoices = ceil($totalRows_invoices / $maxRows_invoices) - 1;

                    $queryString_invoices = "";
                    if (!empty($_SERVER['QUERY_STRING'])) {
                        $params = explode("&", $_SERVER['QUERY_STRING']);
                        $newParams = array();
                        foreach ($params as $param) {
                            if (stristr($param, "pageNum_invoices") == false &&
                                    stristr($param, "totalRows_invoices") == false) {
                                array_push($newParams, $param);
                            }
                        }
                        if (count($newParams) != 0) {
                            $queryString_invoices = "&" . htmlentities(implode("&", $newParams));
                        }
                    }
                    $queryString_invoices = sprintf("&totalRows_invoices=%d%s", $totalRows_invoices, $queryString_invoices);
                    ?>

                    <?php
                    if ($totalRows_invoices !== NULL) {
                        ?>
                        <form class="form-group">
                            <table class="table table-condensed">
                                <thead class="tableheader">
                                    <tr>
                                        <th>Action</th>
                                        <th>ID</th>
                                        <th>Customer</th>
                                        <th>Date</th>
                                        <th>Total</th>
                                        <th>Owing</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    while ($row = $invoices->fetch_assoc()) {
                                        ?>

                                        <tr>
                                            <td>
                                                <a href="view_invoice.php?iid=<?php echo $row['id']; ?>" class="glyphicon glyphicon-plus">View</a>
                                                |
                                                <a href="generate_bill.php?iid=<?php echo $row['id']; ?>" class="glyphicon glyphicon-pencil"></a>
                                                |
                                                <a href="invoice_pdf.php?cid=<?php echo $row['id']; ?>" class="fa fa-file-pdf-o">pdf</a>

                                            </td>
                                            <td><?php echo 'Invoice ' . $row['id']; ?></td>
                                            <td><?php echo $row['fulnames']; ?></td>
                                            <td><?php echo $row['created_at']; ?></td>
                                            <td><?php echo 'KES ' . number_format($row['invoice_amount']); ?></td>
                                            <td><?php echo $row['amount_due']; ?></td>
                                        </tr>

                                        <?php
                                    }
                                    ?>
                                        <tr>
                                        <td colspan="6" class="mainbase"><table width="97%"  border="0" cellspacing="0" cellpadding="3">
                                                <tr align="center">
                                                    <td width="45%">
                                                        <table width="78%" border="0" align="center" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td width="23%" align="center"><?php if ($pageNum_invoices > 0) { // Show if not first page         ?>
                                                                        <a href="<?php printf("%s?pageNum_invoices=%d%s", $currentPage, 0, $queryString_invoices); ?>">First</a>
                                                                    <?php } // Show if not first page     ?>              </td>
                                                                <td width="31%" align="center"><?php if ($pageNum_invoices > 0) { // Show if not first page        ?>
                                                                        <a href="<?php printf("%s?pageNum_invoices=%d%s", $currentPage, max(0, $pageNum_invoices - 1), $queryString_invoices); ?>">Previous</a>
                                                                    <?php } // Show if not first page     ?>              </td>
                                                                <td width="23%" align="center"><?php if ($pageNum_invoices < $totalPages_invoices) { // Show if not last page        ?>
                                                                        <a href="<?php printf("%s?pageNum_invoices=%d%s", $currentPage, min($totalPages_invoices, $pageNum_invoices + 1), $queryString_invoices); ?>">Next</a>
                                                                    <?php } // Show if not last page     ?>              </td>
                                                                <td width="23%" align="center"><?php if ($pageNum_invoices < $totalPages_invoices) { // Show if not last page        ?>
                                                                        <a href="<?php printf("%s?pageNum_invoices=%d%s", $currentPage, $totalPages_invoices, $queryString_invoices); ?>">last</a>
                                                                    <?php } // Show if not last page     ?>              </td>
                                                            </tr>
                                                        </table></td>
                                                    <td width="55%">&nbsp; Showing <strong><?php echo ($startRow_invoices + 1) ?></strong> to <strong><?php echo min($startRow_invoices + $maxRows_invoices, $totalRows_invoices) ?></strong> of <strong><?php echo $totalRows_invoices ?></strong> </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </form>
                        <?php
                    }
                    ?> 
                </div>
                <div id="paid" class="tab-pane fade">
                    <?php
                    $query_invoices = "SELECT invoices.*, clients.fulnames"
                            . "  FROM "
                            . "invoices inner join clients on invoices.client_id = clients.id"
                            . " WHERE client_id = $client_id "
                            . "and paid = 1";

                    $query_limit_invoices = sprintf("%s LIMIT %d, %d", $query_invoices, $startRow_invoices, $maxRows_invoices);
                    $invoices = mysqli_query($eProc, $query_limit_invoices) or die(mysqli_error());


                    if (isset($_GET['totalRows_invoices'])) {
                        $totalRows_invoices = $_GET['totalRows_invoices'];
                    } else {
                        $all_invoices = mysqli_query($eProc, $query_invoices);
                        $totalRows_invoices = mysqli_num_rows($all_invoices);
                    }
                    $totalPages_invoices = ceil($totalRows_invoices / $maxRows_invoices) - 1;

                    $queryString_invoices = "";
                    if (!empty($_SERVER['QUERY_STRING'])) {
                        $params = explode("&", $_SERVER['QUERY_STRING']);
                        $newParams = array();
                        foreach ($params as $param) {
                            if (stristr($param, "pageNum_invoices") == false &&
                                    stristr($param, "totalRows_invoices") == false) {
                                array_push($newParams, $param);
                            }
                        }
                        if (count($newParams) != 0) {
                            $queryString_invoices = "&" . htmlentities(implode("&", $newParams));
                        }
                    }
                    $queryString_invoices = sprintf("&totalRows_invoices=%d%s", $totalRows_invoices, $queryString_invoices);
                    ?>

                    <?php
                    if ($totalRows_invoices !== NULL) {
                        ?>
                        <form class="form-group">
                            <table class="table table-condensed">
                                <thead class="tableheader">
                                    <tr>
                                        <th>Action</th>
                                        <th>ID</th>
                                        <th>Customer</th>
                                        <th>Date</th>
                                        <th>Total</th>
                                        <th>Owing</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    while ($row = $invoices->fetch_assoc()) {
                                        ?>

                                        <tr>
                                            <td>
                                                <a href="view_invoice.php?iid=<?php echo $row['id']; ?>" class="glyphicon glyphicon-plus">View</a>
                                                |
                                                <a href="generate_bill.php?iid=<?php echo $row['id']; ?>" class="glyphicon glyphicon-pencil"></a>
                                                |
                                                <a href="invoice_pdf.php?cid=<?php echo $row['id']; ?>" class="fa fa-file-pdf-o">pdf</a>

                                            </td>
                                            <td><?php echo 'Invoice ' . $row['id']; ?></td>
                                            <td><?php echo $row['fulnames']; ?></td>
                                            <td><?php echo $row['created_at']; ?></td>
                                            <td><?php echo 'KES ' . number_format($row['invoice_amount']); ?></td>
                                            <td><?php echo $row['amount_due']; ?></td>
                                        </tr>

                                        <?php
                                    }
                                    ?>
                                        <tr>
                                        <td colspan="6" class="mainbase"><table width="97%"  border="0" cellspacing="0" cellpadding="3">
                                                <tr align="center">
                                                    <td width="45%">
                                                        <table width="78%" border="0" align="center" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td width="23%" align="center"><?php if ($pageNum_invoices > 0) { // Show if not first page         ?>
                                                                        <a href="<?php printf("%s?pageNum_invoices=%d%s", $currentPage, 0, $queryString_invoices); ?>">First</a>
                                                                    <?php } // Show if not first page     ?>              </td>
                                                                <td width="31%" align="center"><?php if ($pageNum_invoices > 0) { // Show if not first page        ?>
                                                                        <a href="<?php printf("%s?pageNum_invoices=%d%s", $currentPage, max(0, $pageNum_invoices - 1), $queryString_invoices); ?>">Previous</a>
                                                                    <?php } // Show if not first page     ?>              </td>
                                                                <td width="23%" align="center"><?php if ($pageNum_invoices < $totalPages_invoices) { // Show if not last page        ?>
                                                                        <a href="<?php printf("%s?pageNum_invoices=%d%s", $currentPage, min($totalPages_invoices, $pageNum_invoices + 1), $queryString_invoices); ?>">Next</a>
                                                                    <?php } // Show if not last page     ?>              </td>
                                                                <td width="23%" align="center"><?php if ($pageNum_invoices < $totalPages_invoices) { // Show if not last page        ?>
                                                                        <a href="<?php printf("%s?pageNum_invoices=%d%s", $currentPage, $totalPages_invoices, $queryString_invoices); ?>">last</a>
                                                                    <?php } // Show if not last page     ?>              </td>
                                                            </tr>
                                                        </table></td>
                                                    <td width="55%">&nbsp; Showing <strong><?php echo ($startRow_invoices + 1) ?></strong> to <strong><?php echo min($startRow_invoices + $maxRows_invoices, $totalRows_invoices) ?></strong> of <strong><?php echo $totalRows_invoices ?></strong> </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </form>
                        <?php
                    }
                    ?>
                </div>
                <div id="unpaid" class="tab-pane fade">
                    <?php
                    $query_invoices = "SELECT invoices.*, clients.fulnames"
                            . "  FROM "
                            . "invoices inner join clients on invoices.client_id = clients.id"
                            . " WHERE client_id = $client_id "
                            . "and paid = 0";

                    $query_limit_invoices = sprintf("%s LIMIT %d, %d", $query_invoices, $startRow_invoices, $maxRows_invoices);
                    $invoices = mysqli_query($eProc, $query_limit_invoices) or die(mysqli_error());


                    if (isset($_GET['totalRows_invoices'])) {
                        $totalRows_invoices = $_GET['totalRows_invoices'];
                    } else {
                        $all_invoices = mysqli_query($eProc, $query_invoices);
                        $totalRows_invoices = mysqli_num_rows($all_invoices);
                    }
                    $totalPages_invoices = ceil($totalRows_invoices / $maxRows_invoices) - 1;

                    $queryString_invoices = "";
                    if (!empty($_SERVER['QUERY_STRING'])) {
                        $params = explode("&", $_SERVER['QUERY_STRING']);
                        $newParams = array();
                        foreach ($params as $param) {
                            if (stristr($param, "pageNum_invoices") == false &&
                                    stristr($param, "totalRows_invoices") == false) {
                                array_push($newParams, $param);
                            }
                        }
                        if (count($newParams) != 0) {
                            $queryString_invoices = "&" . htmlentities(implode("&", $newParams));
                        }
                    }
                    $queryString_invoices = sprintf("&totalRows_invoices=%d%s", $totalRows_invoices, $queryString_invoices);
                    ?>

                    <?php
                    if ($totalRows_invoices !== NULL) {
                        ?>
                        <form class="form-group">
                            <table class="table table-condensed">
                                <thead class="tableheader">
                                    <tr>
                                        <th>Action</th>
                                        <th>ID</th>
                                        <th>Customer</th>
                                        <th>Date</th>
                                        <th>Total</th>
                                        <th>Owing</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    while ($row = $invoices->fetch_assoc()) {
                                        ?>

                                        <tr>
                                            <td>
                                                <a href="view_invoice.php?iid=<?php echo $row['id']; ?>" class="glyphicon glyphicon-plus">View</a>
                                                |
                                                <a href="generate_bill.php?iid=<?php echo $row['id']; ?>" class="glyphicon glyphicon-pencil"></a>
                                                |
                                                <a href="invoice_pdf.php?cid=<?php echo $row['id']; ?>" class="fa fa-file-pdf-o">pdf</a>

                                            </td>
                                            <td><?php echo 'Invoice ' . $row['id']; ?></td>
                                            <td><?php echo $row['fulnames']; ?></td>
                                            <td><?php echo $row['created_at']; ?></td>
                                            <td><?php echo 'KES ' . number_format($row['invoice_amount']); ?></td>
                                            <td><?php echo $row['amount_due']; ?></td>
                                        </tr>

                                        <?php
                                    }
                                    ?>

                                    <tr>
                                        <td colspan="6" class="mainbase"><table width="97%"  border="0" cellspacing="0" cellpadding="3">
                                                <tr align="center">
                                                    <td width="45%">
                                                        <table width="78%" border="0" align="center" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td width="23%" align="center"><?php if ($pageNum_invoices > 0) { // Show if not first page         ?>
                                                                        <a href="<?php printf("%s?pageNum_invoices=%d%s", $currentPage, 0, $queryString_invoices); ?>">First</a>
                                                                    <?php } // Show if not first page     ?>              </td>
                                                                <td width="31%" align="center"><?php if ($pageNum_invoices > 0) { // Show if not first page        ?>
                                                                        <a href="<?php printf("%s?pageNum_invoices=%d%s", $currentPage, max(0, $pageNum_invoices - 1), $queryString_invoices); ?>">Previous</a>
                                                                    <?php } // Show if not first page     ?>              </td>
                                                                <td width="23%" align="center"><?php if ($pageNum_invoices < $totalPages_invoices) { // Show if not last page        ?>
                                                                        <a href="<?php printf("%s?pageNum_invoices=%d%s", $currentPage, min($totalPages_invoices, $pageNum_invoices + 1), $queryString_invoices); ?>">Next</a>
                                                                    <?php } // Show if not last page     ?>              </td>
                                                                <td width="23%" align="center"><?php if ($pageNum_invoices < $totalPages_invoices) { // Show if not last page        ?>
                                                                        <a href="<?php printf("%s?pageNum_invoices=%d%s", $currentPage, $totalPages_invoices, $queryString_invoices); ?>">last</a>
                                                                    <?php } // Show if not last page     ?>              </td>
                                                            </tr>
                                                        </table></td>
                                                    <td width="55%">&nbsp; Showing <strong><?php echo ($startRow_invoices + 1) ?></strong> to <strong><?php echo min($startRow_invoices + $maxRows_invoices, $totalRows_invoices) ?></strong> of <strong><?php echo $totalRows_invoices ?></strong> </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </form>
                        <?php
                    }
                    ?>
                </div>
                <?php
                ?>
            </div>
        </div>

        <script>
            $(document).ready(function () {
                $(".nav-tabs a").click(function () {
                    $(this).tab('show');
                });
                $('.nav-tabs a').on('shown.bs.tab', function (event) {
                    var x = $(event.target).text();         // active tab
                    var y = $(event.relatedTarget).text();  // previous tab
                    $(".act span").text(x);
                    $(".prev span").text(y);
                });
            });
        </script>

    </body>
</body>
</html>