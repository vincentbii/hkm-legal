<?php require_once('../connections/eProc.php'); 

 require_once('../activelog.php'); ?>
<?php
$currentPage = $_SERVER["PHP_SELF"];

$maxRows_companybranch = 30;
$pageNum_companybranch = 0;
if (isset($_GET['pageNum_companybranch'])) {
  $pageNum_companybranch = $_GET['pageNum_companybranch'];
}
$startRow_companybranch = $pageNum_companybranch * $maxRows_companybranch;

mysqli_select_db($eProc, $database_eProc);
$query_companybranch = "SELECT * FROM companybranch ";
$query_limit_companybranch = sprintf("%s LIMIT %d, %d", $query_companybranch, $startRow_companybranch, $maxRows_companybranch);
$companybranch = mysqli_query($eProc,$query_limit_companybranch) or die(mysqli_error($eProc));
$row_companybranch = mysqli_fetch_assoc($companybranch);

if (isset($_GET['totalRows_companybranch'])) {
  $totalRows_companybranch = $_GET['totalRows_companybranch'];
} else {
  $all_companybranch = mysqli_query($eProc,$query_companybranch);
  $totalRows_companybranch = mysqli_num_rows($all_companybranch);
}
$totalPages_companybranch = ceil($totalRows_companybranch/$maxRows_companybranch)-1;

$queryString_companybranch = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_companybranch") == false && 
        stristr($param, "totalRows_companybranch") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_companybranch = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_companybranch = sprintf("&totalRows_companybranch=%d%s", $totalRows_companybranch, $queryString_companybranch);
?>

<html>
<head>
<title>LEGAL  MANAGEMENT INFORMATION  SYSTEM</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../styles/default.css" rel="stylesheet" type="text/css">

<style type="text/css">
<!--
.style1 {
	color: #FF0000;
	font-weight: bold;
}
-->
</style>
</head>
<body>

<table width="100%" border="0" cellpadding="4" cellspacing="0" >

  <tr class="inputdeft">
   <td width="36%"   class="inputdeft" style="font-weight: bold">Branches</td>
   
	<td width="64%"   class="inputdeft" style="font-weight: bold" ><div align="right">
	  <table width="154"  border="0" cellspacing="0" cellpadding="3">
	    <tr class="inputdeft">
	      <td width="19"  ><img src="../images/icons/newproject.gif" width="15" height="13" border="0" /></td>
                
			<td width="123"   ><a href="../adm/newbranch.php">New Branch </a></td>
            </tr>
	    </table>
	  </div></td>
  </tr>
  <?php if ($totalRows_companybranch > 0) { ?>
  <?php do { ?>
  <tr>
  <td ><?php echo $row_companybranch['name']?> </td>
  
  <td ><a href="../adm/newbranch.php?id=<?php echo $row_companybranch['id'] ?>">Edit</a></td>
  </tr>
  <?php } while ($row_companybranch = mysqli_fetch_assoc($companybranch)); ?>
  <tr>
    <td colspan="12" class="mainbase"><table width="97%"  border="0" cellspacing="0" cellpadding="3">
      <tr align="center">
        <td width="45%">
          <table width="78%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
              <td width="23%" align="center"><?php if ($pageNum_companybranch > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_companybranch=%d%s", $currentPage, 0, $queryString_companybranch); ?>">First</a>
                <?php } // Show if not first page ?>              </td>
              <td width="31%" align="center"><?php if ($pageNum_companybranch > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_companybranch=%d%s", $currentPage, max(0, $pageNum_companybranch - 1), $queryString_companybranch); ?>">Previous</a>
                <?php } // Show if not first page ?>              </td>
              <td width="23%" align="center"><?php if ($pageNum_companybranch < $totalPages_companybranch) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_companybranch=%d%s", $currentPage, min($totalPages_companybranch, $pageNum_companybranch + 1), $queryString_companybranch); ?>">Next</a>
                <?php } // Show if not last page ?>              </td>
              <td width="23%" align="center"><?php if ($pageNum_companybranch < $totalPages_companybranch) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_companybranch=%d%s", $currentPage, $totalPages_companybranch, $queryString_companybranch); ?>">last</a>
                <?php } // Show if not last page ?>              </td>
            </tr>
          </table></td>
        <td width="55%">&nbsp; Showing <strong><?php echo ($startRow_companybranch + 1) ?></strong> to <strong><?php echo min($startRow_companybranch + $maxRows_companybranch, $totalRows_companybranch) ?></strong> of <strong><?php echo $totalRows_companybranch ?></strong> </td>
      </tr>
    </table></td>
  </tr>
  <?php } else { ?>
  <tr>
    <td colspan="12" class="mainbase"><span class="style1">No companybranchs Created! </span></td>
  </tr>
  <?php } ?>
</table>
</body>
</html>
<?php
mysqli_free_result($companybranch);
?>

