<?php require_once('../connections/eProc.php'); 
include('../activelog.php');
unset($_SESSION['bank']);
mysqli_select_db($eProc, $database_eProc);

if(isset($_GET['id']) && $_GET['id']!=''){ 
	$_SESSION['bankbranch']=$_GET['id'];
	
	$selectSQL = "SELECT * FROM bankbranch WHERE id = '".$_SESSION['bankbranch']."'";
	//echo $selectSQL;	
	$sql_select = mysqli_query($eProc, $selectSQL) or die(mysqli_error($eProc));
	$row_sql = mysqli_fetch_assoc($sql_select);	
	$_SESSION['bank']=$row_sql['bank'];
	
	}
	
if(isset($_POST['bank']) && $_POST['bank']!=''){ 
	$_SESSION['bank']=$_POST['bank'];
}
if(isset($_POST['bank']) && $_POST['bank']==''){
	unset($_SESSION['bank']);
}


	if ($_POST['update']!="" && $_POST['update']=="Update" ) {
		
		$updateSQL = "UPDATE bankbranch SET  
					name='".$_POST['name']."',
						description='".$_POST['description']."',
					physicaladdress='".$_POST['physicaladdress']."' ,
					postaladdress='".$_POST['postaladdress']."' ,
					telephone='".$_POST['telephone']."' ,
					cellphone='".$_POST['cellphone']."' ,
					email='".$_POST['email']."' ,
					fax='".$_POST['fax']."' ,
					contactperson='".$_POST['contactperson']."' 
					bank='".$_SESSION['bank']."'					
					WHERE  id ='".$_SESSION['bankbranch']."' AND company='".$_SESSION['company']."' ";
			//echo $updateSQL; 
			$Result1 = mysqli_query($eProc, $updateSQL) or die(mysqli_error($eProc));
			
			?> 
			<script language="JavaScript" type="text/javascript">
				location='bankbranchslist.php';
			</script>
			<?php  
	} 
		
	if ($_POST['save']!="" && $_POST['save']=="Save" ) {
	$exist=recordexists("bankbranch","name",$_POST['name']) ;		
		if($exist=='no'){
					
			$insertSQL =sprintf( "INSERT INTO bankbranch (name,description,physicaladdress,postaladdress,telephone,cellphone,email,fax,contactperson,bank,company) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",
			 GetSQLValueString($_POST['name'], "text"),
			 GetSQLValueString($_POST['desc'], "text"),
			 GetSQLValueString($_POST['physicaladdress'], "text"),
			 GetSQLValueString($_POST['postaladdress'], "text"),
			 GetSQLValueString($_POST['telephone'], "text"),
			 GetSQLValueString($_POST['cellphone'], "text"),
			 GetSQLValueString($_POST['email'], "text"),
			 GetSQLValueString($_POST['fax'], "text"),
			 GetSQLValueString($_POST['contactperson'], "text"),
			 GetSQLValueString($_POST['bank'], "text"),			 
			 GetSQLValueString($_SESSION['company'], "text"));	
			
						
			//echo $insertSQL;
		$Result2 = mysqli_query($eProc, $insertSQL) or die(mysqli_error($eProc));
					 
			?> 
			<script language="JavaScript" type="text/javascript">
				location='bankbranchslist.php';
			</script>
			<?php
			
			}else{
	?> 
			<script language="JavaScript" type="text/javascript">
				alert("A  bankbranch  with <?php echo $_POST['name']?>  name exists.");
				location='newbankbranch.php';
			</script>
			<?php
			}
	}
	
	if ( isset($_GET['del']) && $_GET['del']!="" ) {
	
		$DeleteSQL = "DELETE FROM bankbranch WHERE  id ='".$_GET['del']."'";
		//echo $DeleteSQL;
		if(!$Result3 = mysqli_query($eProc, $DeleteSQL)){?>
         
		<script language="JavaScript" type="text/javascript">
			alert("The bankbranch has transactions.Please delete the transactions first");
			location='bankbranchslist.php';
		
		</script>
		<?php }else{?>
	
		<script language="JavaScript" type="text/javascript">
			location='bankbranchslist.php';
		</script>
		<?php
	}
	}
	

	

?>
<html>
<head>
<title>LEGAL  MANAGEMENT INFORMATION  SYSTEM</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../styles/<?php echo $_SESSION['Theme'] ; ?>/default.css" REL="stylesheet" type="text/css">
<script language="javascript" type="text/javascript">
   function MM_valIDateForm() { 
 	  if(document.newbankbranch.name.value=='' ){
 		alert('You must enter the Name to continue');
 		document.newbankbranch.name.focus();
 		return false ;
 		} 
 	} 

	function valIDatechars(evt)
		{
			// Prevents the entry of special characters and spaces into a field
			evt=(evt)? evt: window.event
			var charCode= (evt.which)? evt.which: evt.keyCode
			if( charCode>31 && (charCode<48 || charCode>57)) {
				if (charCode<64 || charCode>90) {
					if ((charCode<95 || charCode>122) && charCode!=8 ) {
						if (charCode!=127 && charCode!=46 && charCode!=45 ) {
							if ((charCode<37 || charCode>40) && charCode!=32) {
								alert('This field accepts only 0-9, a-z, A_Z, space, backspace & del characters !');
								status='This field accepts only 0-9, a-z, A_Z, space & del characters !'
								return false;
							}
						}
					}
				}
			}
			status=''
			return true
		}
 		function confirmdelete(name,id){
 		    	if (confirm('Are you sure you want to delete  bankbranch ' +name+ '?')) {
 					location.href = 'newbankbranch.php?del='+id 
 					} 
  				}
</script>
</head>
<body>
<table width="100%" cellpadding="5">
<tr align="bankbranch">
<td width="50%" valign="top">
<fieldset>
<legend> bankbranch Details</legend>
<form onSubmit='return MM_valIDateForm()' action="" method="post" name="newbankbranch" ID="newbankbranch">
<input name="ID" type="hidden" value="<?php echo $row_sql['id'] ?>" />
<table width="100%" border="0" cellspacing="0" cellpadding="4">

<tr valign="baseline">
<td align='right' nowrap><strong>Bank:</strong></td>
<td>
<select name="bank" id="bank"  onFocus="this.style.backgroundColor='#ffff00'"  onBlur="this.style.backgroundColor='#F2FAFF'" onChange="submit()" class='forms' ><?php 
 	showbanks($_SESSION['bank']); ?>
</select></td>
</tr>
<?php if (isset($_SESSION['bank'])&& $_SESSION['bank']!=''){?>
<tr valign="baseline">
<td width="38%" align='right' nowrap><strong><font color=red size=+1>* </font>Name:</strong></td>
<td width="62%">
<input size="20" maxlength="100" name="name" ID="name"  onFocus="this.style.backgroundColor='#ffff00'"  onBlur="this.style.backgroundColor='#F2FAFF'"  class='forms'   onkeypress='return valIDatechars(event)' value="<?php echo $row_sql['name']; ?>" >
</td>
</tr>
<tr valign="baseline">
<td align='right' nowrap><strong>Description:</strong></td>
<td>
<textarea cols='30' rows='3' name="desc"  onFocus="this.style.backgroundColor='#ffff00'"  onBlur="this.style.backgroundColor='#F2FAFF'" onChange='this.value=this.value.toUpperCase()' class='forms'  > <?php echo $row_sql['description']; ?></textarea>
</td>
</tr>

<tr valign="baseline">
<td width="38%" align='right' nowrap><strong>Physical Address</strong></td>
<td width="62%">
<input size="20" maxlength="100" name="physicaladdress" ID="physicaladdress"  onFocus="this.style.backgroundColor='#ffff00'"  onBlur="this.style.backgroundColor='#F2FAFF'"  class='forms'   onkeypress='return valIDatechars(event)' value="<?php echo $row_sql['physicaladdress']; ?>" >
</td>
</tr>
<tr valign="baseline">
<td width="38%" align='right' nowrap><strong>Postal Address</strong></td>
<td width="62%">
<input size="20" maxlength="100" name="postaladdress" ID="postaladdress"  onFocus="this.style.backgroundColor='#ffff00'"  onBlur="this.style.backgroundColor='#F2FAFF'"  class='forms'   onkeypress='return valIDatechars(event)' value="<?php echo $row_sql['postaladdress']; ?>" >
</td>
</tr>
<tr valign="baseline">
<td width="38%" align='right' nowrap><strong>Telephone</strong></td>
<td width="62%">
<input size="20" maxlength="100" name="telephone" ID="telephone"  onFocus="this.style.backgroundColor='#ffff00'"  onBlur="this.style.backgroundColor='#F2FAFF'"  class='forms'   onkeypress='return valIDatechars(event)' value="<?php echo $row_sql['telephone']; ?>" >
</td>
</tr>
<tr valign="baseline">
<td width="38%" align='right' nowrap><strong>Cellphone</strong></td>
<td width="62%">
<input size="20" maxlength="100" name="cellphone" ID="cellphone"  onFocus="this.style.backgroundColor='#ffff00'"  onBlur="this.style.backgroundColor='#F2FAFF'"  class='forms'   onkeypress='return valIDatechars(event)' value="<?php echo $row_sql['cellphone']; ?>" >
</td>
</tr>
<tr valign="baseline">
<td width="38%" align='right' nowrap><strong>Email</strong></td>
<td width="62%">
<input size="20" maxlength="100" name="email" ID="email"  onFocus="this.style.backgroundColor='#ffff00'"  onBlur="this.style.backgroundColor='#F2FAFF'"  class='forms'   onkeypress='return valIDatechars(event)' value="<?php echo $row_sql['email']; ?>" >
</td>
</tr>
<tr valign="baseline">
<td width="38%" align='right' nowrap><strong>Fax</strong></td>
<td width="62%">
<input size="20" maxlength="100" name="fax" ID="fax"  onFocus="this.style.backgroundColor='#ffff00'"  onBlur="this.style.backgroundColor='#F2FAFF'"  class='forms'   onkeypress='return valIDatechars(event)' value="<?php echo $row_sql['fax']; ?>" >
</td>
</tr>
<tr valign="baseline">
<td width="38%" align='right' nowrap><strong>Contact Person</strong></td>
<td width="62%">
<input size="20" maxlength="100" name="contactperson" ID="contactperson"  onFocus="this.style.backgroundColor='#ffff00'"  onBlur="this.style.backgroundColor='#F2FAFF'"  class='forms'   onkeypress='return valIDatechars(event)' value="<?php echo $row_sql['contactperson']; ?>" >
</td>
</tr>



<tr >
<td colspan='2' ><div align='center'>
 <?php if (isset($_SESSION['bankbranch'])){?>
 <input name="update" type="submit" class="formsBlue" id="update" value="Update" />
<a href="newbankbranch.php?id=<?php $row_sql['id']; ?>"> <a onclick='javascript:confirmdelete("<?php  echo $row_sql['name']?>","<?php echo $row_sql['id']?> ")' ><input name='delete' type='button' class='formsRed' value='Delete'  > </a> </a>
<?php }else{?>
<input name='save' type='submit' class='formsBlue' value='Save'>
<?php }?>
<input name='cancel' type='button' onClick="location='bankbranchslist.php'" class='formsorg' value='Cancel'>
</div></td>
</tr>
<?php }?>
</table>
</form>
</fieldset
></table>
</body>
</html>
