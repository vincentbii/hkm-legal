<?php require_once('../connections/eProc.php'); ?>

<?php

$currentPage = $_SERVER["PHP_SELF"];

$maxRows_town = 30;
$pageNum_town = 0;


  unset($_SESSION['itype']);


if (isset($_GET['pageNum_town'])) {
  $pageNum_town = $_GET['pageNum_town'];
}
$startRow_town = $pageNum_town * $maxRows_town;

mysqli_select_db($eProc, $database_eProc);
$query_town = "SELECT * FROM town   ";

$query_limit_town = sprintf("%s LIMIT %d, %d", $query_town, $startRow_town, $maxRows_town);
$town = mysqli_query($eProc, $query_limit_town) or die(mysqli_error());
$row_town = mysqli_fetch_assoc($town);

if (isset($_GET['totalRows_town'])) {
  $totalRows_town = $_GET['totalRows_town'];
} else {
  $all_town = mysqli_query($eProc,$query_town);
  $totalRows_town = mysqli_num_rows($all_town);
}
$totalPages_town = ceil($totalRows_town/$maxRows_town)-1;

$queryString_town = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_town") == false && 
        stristr($param, "totalRows_town") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_town = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_town = sprintf("&totalRows_town=%d%s", $totalRows_town, $queryString_town);
?>

<html>
<head>
<title>LEGAL MANAGEMENT SYSTEM</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../styles/default.css" rel="stylesheet" type="text/css">

<style type="text/css">
<!--
.style1 {
	color: #FF0000;
	font-weight: bold;
}
-->
</style>
</head>
<body>
<fieldset>
<table width="100%" border="0" cellpadding="4" cellspacing="0" >

  <tr>
  <td width="26%"   class="inputdef" style="font-weight: bold">Town Name</td>
  <td width="33%"   class="inputdef" style="font-weight: bold">Initials</td>
   <td width="41%"   class="inputdef" style="font-weight: bold" ><div align="right">
	  <table width="154"  border="0" cellspacing="0" cellpadding="3">
	    <tr class="inputdef">
	      <td width="19"  ><img src="../images/icons/newproject.gif" width="15" height="13" border="0" /></td>
                
			<td width="123"    ><a href="newtown.php">New town</a></td>
            </tr>
	    </table>
	  </div></td>
  </tr>
  <?php if ($totalRows_town > 0) { ?>
  <?php do { ?>
  <tr>
   <td > <?php echo $row_town['name']?> </td>
   <td > <?php echo $row_town['initials']?> </td>
  <td ><a href="newtown.php?id=<?php echo $row_town['id'] ?>">Edit</a></td>
  </tr>
  <?php } while ($row_town = mysqli_fetch_assoc($town)); ?>
  <tr>
    <td colspan="5" class="mainbase"><table width="97%"  border="0" cellspacing="0" cellpadding="3">
      <tr align="center">
        <td width="45%">
          <table width="78%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
              <td width="23%" align="center"><?php if ($pageNum_town > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_town=%d%s", $currentPage, 0, $queryString_town); ?>">First</a>
                <?php } // Show if not first page ?>              </td>
              <td width="31%" align="center"><?php if ($pageNum_town > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_town=%d%s", $currentPage, max(0, $pageNum_town - 1), $queryString_town); ?>">Previous</a>
                <?php } // Show if not first page ?>              </td>
              <td width="23%" align="center"><?php if ($pageNum_town < $totalPages_town) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_town=%d%s", $currentPage, min($totalPages_town, $pageNum_town + 1), $queryString_town); ?>">Next</a>
                <?php } // Show if not last page ?>              </td>
              <td width="23%" align="center"><?php if ($pageNum_town < $totalPages_town) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_town=%d%s", $currentPage, $totalPages_town, $queryString_town); ?>">last</a>
                <?php } // Show if not last page ?>              </td>
            </tr>
          </table></td>
        <td width="55%">&nbsp; Showing <strong><?php echo ($startRow_town + 1) ?></strong> to <strong><?php echo min($startRow_town + $maxRows_town, $totalRows_town) ?></strong> of <strong><?php echo $totalRows_town ?></strong> </td>
      </tr>
    </table></td>
  </tr>
  <?php } else { ?>
  <tr>
    <td colspan="5" class="mainbase"><span class="style1">No town Created! </span></td>
  </tr>
  <?php } ?>
</table>
</fieldset>
</body>
</html>
<?php
mysqli_free_result($town);
?>

