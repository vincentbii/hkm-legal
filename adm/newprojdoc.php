<?php
require_once('../connections/eProc.php');
include('../activelog.php');
mysqli_select_db($eProc, $database_eProc);
$_SESSION['perc'] = 'd';
unset($_SESSION['document']);
if (isset($_POST['client']) && $_POST['client'] != '') {
    $_SESSION['client'] = $_POST['client'];
    unset($_SESSION['document']);
    unset($_SESSION['category']);
}

if (isset($_POST['perc']) && $_POST['perc'] != '') {
    $_SESSION['perc'] = $_POST['perc'];
}
if (isset($_POST['perc']) && $_POST['perc'] == '') {
    unset($_SESSION['perc']);
}

if (isset($_POST['proj']) && $_POST['proj'] != '') {
    $_SESSION['proj'] = $_POST['proj'];
}
if (isset($_POST['proj']) && $_POST['proj'] == '') {
    unset($_SESSION['proj']);
}

if (isset($_POST['unit']) && $_POST['unit'] != '') {
    $_SESSION['unit'] = $_POST['unit'];
}
if (isset($_POST['unit']) && $_POST['unit'] == '') {
    unset($_SESSION['unit']);
}


if (isset($_POST['client']) && $_POST['client'] == '') {
    unset($_SESSION['client']);
}
if (isset($_POST['document']) && $_POST['document'] != '') {
    $_SESSION['document'] = $_POST['document'];
}
if (isset($_POST['document']) && $_POST['document'] == '') {
    unset($_SESSION['document']);
}
if (isset($_POST['category']) && $_POST['category'] != '') {
    $_SESSION['category'] = $_POST['category'];
}
if (isset($_POST['category']) && $_POST['category'] == '') {
    unset($_SESSION['category']);
}


$logSQL = "SELECT MAX(displayorder) AS D FROM projdocs WHERE    client='" . $_SESSION['client'] . "' AND category='" . $_SESSION['category'] . "' AND  project='" . $_SESSION['proj'] . "' AND unit='" . $_SESSION['unit'] . "' limit 1";
//echo $logSQL;
$prq1 = mysqli_query($eProc, $logSQL) or die(mysqli_error($eProc));
$row_prq1 = mysqli_fetch_assoc($prq1);
$snums = mysqli_num_rows($prq1);


if (isset($_POST['save']) && $_POST['save'] == 'Save') {
    if (isset($_POST['disp']) && $_POST['disp'] != '' && isset($_POST['desc']) && $_POST['desc'] != '' && isset($_POST['client']) && $_POST['client'] != '' && isset($_POST['category']) && $_POST['category'] != '') {

        $selectSQL2 = "SELECT * FROM projdocs  WHERE  client='" . $_SESSION['client'] . "' AND category='" . $_SESSION['category'] . "' AND  project='" . $_SESSION['proj'] . "' AND unit='" . $_SESSION['unit'] . "' ORDER BY displayorder ASC";
        //echo $selectSQL;	
        $sql_select2 = mysqli_query($eProc, $selectSQL2) or die(mysqli_error($eProc));
        $row_sql2 = mysqli_fetch_assoc($sql_select2);
        $nums = mysqli_num_rows($sql_select2);

        $path = "../pdocs/";

        $path = $path . $_FILES["docimage"]["name"];
        if (((
                $_FILES["docimage"]["type"] == "image/gif")// valid file types
                || ($_FILES["docimage"]["type"] == "image/jpeg")
                || ($_FILES["docimage"]["type"] == "image/png")
                || ($_FILES["docimage"]["type"] == "application/msword")
                || ($_FILES["docimage"]["type"] == "application/pdf")
                || ($_FILES["docimage"]["type"] == "application/vnd.openxmlformats-officedocument.wordprocessingml.document") || ($_FILES["docimage"]["type"] == "application/doc") || ($_FILES["docimage"]["type"] == "application/vnd.ms-excel")
                ) && ($_FILES["docimage"]["size"] < 5000000)) {

            /* if ((($_FILES["docimage"]["type"] == "image/gif")// valid file types
              || ($_FILES["docimage"]["type"] == "image/jpeg")
              || ($_FILES["docimage"]["type"] == "image/pjpeg")
              || ($_FILES["docimage"]["type"] == "image/pdf")
              || ($_FILES["docimage"]["type"] == "image/doc"))
              && ($_FILES["docimage"]["size"] < 2000000)) {//specify maximum size 2MB
             */
            //echo "one";

            if ($_FILES["docimage"]["error"] > 0) { //if no error found
                echo "Error: " . $_FILES["docimage"]["error"] . "<br />";
            } else {


                if (file_exists($path)) {
                    // echo "teProcee";
                    echo "Warning:: " . $path . " already exists<br/>";
                } else {
                    // echo "four";
                    move_uploaded_file($_FILES["docimage"]["tmp_name"], $path);
                    //echo "Stored in: " . $path."<br/>";
                }
            }
            //display the image
//echo'<img src="'.$path.'" alt="uploaded photo"  width="400" height="400"/><br/>'; 
        } else {
            //echo "five";
            echo "<p>Invalid file</p>";
        }

        if (isset($_POST['disp']) && $_POST['disp'] != '') {
            $disporder = $_POST['disp'];
        } else {
            $disporder = $row_prq1['D'] + 1;
        }
        $maxn = $row_prq1['D'];
        $startval = $_POST['disp'];


        for ($j = $maxn; $j >= $startval; $j--) {
            $i = $j;
            $update = "UPDATE  projdocs 
	 SET displayorder=displayorder+1
	 WHERE  displayorder='" . $i . "' AND  client='" . $_SESSION['client'] . "' AND  project='" . $_SESSION['proj'] . "' AND unit='" . $_SESSION['unit'] . "' AND category='" . $_SESSION['category'] . "'";
            //echo $update;
            $update = mysqli_query($eProc, $update) or die(mysqli_error($eProc));
        }



        $insertSQL = sprintf("INSERT INTO projdocs (description,displayorder,category,client,project,unit,adverseparty,imageurl) 
	 VALUES (%s,%s,%s,%s,%s,%s,%s,%s)", GetSQLValueString($_POST['desc'], "text"), GetSQLValueString($disporder, "text"), GetSQLValueString($_POST['category'], "text"), GetSQLValueString($_POST['client'], "text"), GetSQLValueString($_POST['proj'], "text"), GetSQLValueString($_POST['unit'], "text"), GetSQLValueString($_POST['adverseparty'], "text"), GetSQLValueString($path, "text"));
        //echo $insertSQL;
        $Result2 = mysqli_query($eProc, $insertSQL) or die(mysqli_error($eProc));


        $action = getUsername($_SESSION['UNQ']) . "  " . " added image  " . $_POST['desc'] . "  for Client " . getclient($_POST['client']) . " " . getcat($_POST['category']) . " document category";
        $insertSQL = sprintf("INSERT INTO audittrail (action) 
	 VALUES (%s)", GetSQLValueString($action, "text"));
        //echo $insertSQL;
        $Result2 = mysqli_query($eProc, $insertSQL) or die(mysqli_error($eProc));
        ?> <script language='javascript'>
            location = 'newprojdoc.php'
        </script><?php
    } else {
        ?> <script language='javascript'>
            alert("You must fill all required fields");
        </script><?php
    }
}



if (isset($_POST['update']) && $_POST['update'] == 'Update') {

    //echo "the posted".$_FILES["docimage"]["name"];
    if ($_FILES["docimage"]["name"] != '') {
        $path = '';
        $path = "../docs/";

        $path = $path . $_FILES["docimage"]["name"];



        if ((($_FILES["docimage"]["type"] == "image/gif")// valid file types
                || ($_FILES["docimage"]["type"] == "image/jpeg") || ($_FILES["docimage"]["type"] == "image/pjpeg") || ($_FILES["docimage"]["type"] == "image/pdf") || ($_FILES["docimage"]["type"] == "image/doc")) && ($_FILES["docimage"]["size"] < 2000000)) {//specify maximum size 2MB
            //echo "one";
            if ($_FILES["docimage"]["error"] > 0) { //if no error found
                echo "Error: " . $_FILES["docimage"]["error"] . "<br />";
            } else {


                if (file_exists($path)) {
                    // echo "teProcee";
                    echo "Warning:: " . $path . " already exists<br/>";
                } else {
                    // echo "four";
                    move_uploaded_file($_FILES["docimage"]["tmp_name"], $path);
                    //echo "Stored in: " . $path."<br/>";
                }
            }
            //display the image
//echo'<img src="'.$path.'" alt="uploaded photo"  width="400" height="400"/><br/>'; 
        } else {
            //echo "five";
            echo "<p>Invalid file</p>";
        }
    }if ($_FILES["docimage"]["name"] == '') {
        $path = $_SESSION['imagepath'];
    }

    $maxn = $row_prq1['D'];
    $startval = $_POST['disp'];
    if ($_POST['disp'] > $_SESSION['dp']) {
        $pos = $_POST['disp'];
        $mov = $_SESSION['dp'];

        for ($j = $mov + 1; $j <= $pos; $j++) {
            $i = $j;
            $val = $i - 1;
            $update1 = "UPDATE  projdocs 
	 SET displayorder='" . $val . "'
	 WHERE  displayorder='" . $i . "' 
	 AND  client='" . $_SESSION['client'] . "'
	  AND category='" . $_SESSION['category'] . "' 
	  AND project='" . $_SESSION['proj'] . "' 
	  AND unit='" . $_SESSION['unit'] . "'";
            //echo $update1."val=".$val."<br>";
            $update = mysqli_query($eProc, $update1) or die(mysqli_error($eProc));
        }
    }

    if ($_POST['disp'] < $_SESSION['dp']) {
        $pos = $_POST['disp'];
        $mov = $_SESSION['dp'];

        for ($j = $mov - 1; $j >= $pos; $j--) {
            $i = $j;
            $val = $i + 1;
            $update2 = "UPDATE  projdocs 
	 SET displayorder='" . $val . "'
	 WHERE  displayorder='" . $i . "' 
	 AND   client='" . $_SESSION['client'] . "'
	 AND category='" . $_SESSION['category'] . "'
	 AND project='" . $_SESSION['proj'] . "' 
	 AND unit='" . $_SESSION['unit'] . "'";
// echo $update2."val=".$val."<br>";
            $updtes = mysqli_query($eProc, $update2) or die(mysqli_error($eProc));
        }
    }

    $updateSQL = "UPDATE  projdocs 
	 SET description='" . $_POST['desc'] . "',imageurl='" . $path . "' ,displayorder='" . $_POST['disp'] . "',category='" . $_POST['category'] . "',client='" . $_POST['client'] . "',adverseparty='" . $_POST['adverseparty'] . "', project='" . $_SESSION['proj'] . "' ,
	  unit='" . $_SESSION['unit'] . "'
	 WHERE  id='" . $_GET['did'] . "'";
    //echo $updateSQL;
    $update = mysqli_query($eProc, $updateSQL) or die(mysqli_error($eProc));
    unset($_SESSION['imagepath']);
    unset($_SESSION['dp']);

    $action = getUsername($_SESSION['UNQ']) . "  " . "Updated document  " . $_POST['desc'] . "  for Client " . getclient($_POST['client']) . " " . getcat($_POST['category']) . " document category";
    $insertSQL = sprintf("INSERT INTO audittrail (action) 
	 VALUES (%s)", GetSQLValueString($action, "text"));
    //echo $insertSQL;
    $Result2 = mysqli_query($eProc, $insertSQL) or die(mysqli_error($eProc));
    ?> <script language='javascript'>
        location = 'newprojdoc.php'
    </script><?php
}






$selectSQL2 = "SELECT * FROM projdocs WHERE client='" . $_SESSION['client'] . "' AND category='" . $_SESSION['category'] . "' AND  project='" . $_SESSION['proj'] . "' AND unit='" . $_SESSION['unit'] . "' ORDER BY displayorder ASC";
//echo $selectSQL2;	
$sql_select2 = mysqli_query($eProc, $selectSQL2) or die(mysqli_error($eProc));
$row_sql2 = mysqli_fetch_assoc($sql_select2);
$nums2 = mysqli_num_rows($sql_select2);
$url = $row_sql['imageurl'];


if (isset($_POST['view' . $rowsql['item']]) && $_POST['view' . $rowsql['item']] == 'View Supplier Price') {
    ?>
    <script>
        window.open("supplierprices.php?item=<?php echo $_POST['item' . $rowsql['id']]; ?>", "-blank", "height=400,width=450,left=200,top=200,scrollbars=no");
    </script>
    <?php
}


if (isset($_GET['did']) && $_GET['did'] !== '') {
    $selectSQL = "SELECT * FROM projdocs  WHERE id=" . $_GET['did'] . " AND  client='" . $_SESSION['client'] . "' AND category='" . $_SESSION['category'] . "'  AND project='" . $_SESSION['proj'] . "'  AND	  unit='" . $_SESSION['unit'] . "'";
    //echo $selectSQL;	
    $sql_select = mysqli_query($eProc, $selectSQL) or die(mysqli_error($eProc));
    $row_sql = mysqli_fetch_assoc($sql_select);
    $nums = mysqli_num_rows($sql_select);
    $_SESSION['imagepath'] = $row_sql['imageurl'];
    $_SESSION['dp'] = $row_sql['displayorder'];
}

if (isset($_GET['del']) && $_GET['del'] != "") {
    $del = "SELECT * FROM projdocs where id ='" . $_GET['del'] . "'  AND  client='" . $_SESSION['client'] . "' AND category='" . $_SESSION['category'] . " AND project='" . $_SESSION['proj'] . "'  AND
	  unit='" . $_SESSION['unit'] . "'";
    //echo $selectSQL;	
    $sql_del = mysqli_query($eProc, $del) or die(mysqli_error($eProc));
    $row_del = mysqli_fetch_assoc($sql_del);
    $delnums = mysqli_num_rows($sql_del);

    $del1 = "SELECT * FROM projdocs where  client='" . $_SESSION['client'] . "'
	  AND category='" . $_SESSION['category'] . "' 
	   AND project='" . $_SESSION['proj'] . "' 
	    AND	  unit='" . $_SESSION['unit'] . "'
		AND displayorder >'" . $row_del['displayorder'] . "'";
    //echo $selectSQL;	
    $sql_del1 = mysqli_query($eProc, $del1) or die(mysqli_error($eProc));
    $row_del1 = mysqli_fetch_assoc($sql_del1);
    $delnum1 = mysqli_num_rows($sql_del1);



    $action = getUsername($_SESSION['UNQ']) . "  " . "Deleted document  " . $row_del['description'] . "  for Client " . getclient($row_del['client']) . " " . getcat($row_del['category']) . " document category";
    $insertSQL = sprintf("INSERT INTO audittrail (action) 
	 VALUES (%s)", GetSQLValueString($action, "text"));
    //echo $insertSQL;
    $Result2 = mysqli_query($eProc, $insertSQL) or die(mysqli_error($eProc));


    $DeleteSQL = "DELETE FROM projdocs WHERE  id ='" . $_GET['del'] . "'";
    //echo $DeleteSQL;
    $Result3 = mysqli_query($eProc, $DeleteSQL);

    if ($delnum1 > 0) {
        do {
            $update3 = "UPDATE  projdocs 
	 SET displayorder=displayorder-1
	 WHERE  id='" . $row_del1['id'] . "' 
	 AND  client='" . $_SESSION['client'] . "' 
	 AND category='" . $_SESSION['category'] . "'
	 AND project='" . $_SESSION['proj'] . "' 
	 AND	  unit='" . $_SESSION['unit'] . "'";
            //echo $update3."val=".$val."<br>";
            $updtes3 = mysqli_query($eProc, $update3) or die(mysqli_error($eProc));
        } while ($row_del1 = mysqli_fetch_assoc($sql_del1));
    }
    ?>      
    <script language='javascript'>location = 'newprojdoc.php'</script> <?php
}
?>  

<html>
    <head>
        <title>LEGAL MANAGEMENT SYSTEM</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="default.css" REL="stylesheet" type="text/css">
        <LINK REL=StyleSheet  href="calendar.css"  TYPE="text/css">
        <script LANGUAGE="JavaScript"  src="weeklycalendar.js"></script>
        <script language="JavaScript" type="text/javascript">

        buildWeeklyCalendar(1);

        function confirmdelete(name, id) {
            if (confirm('Are you sure you want to delete  Document ' + name + '?')) {
                location.href = 'newprojdoc.php?del=' + id
            }
        }

        </script>

        <script language="JavaScript" type="text/javascript">
            function MM_valIDateForm() {
                if (document.newdocument.disp.value == '') {
                    alert('You must specify the display order to continue');
                    document.newdocument.disp.focus();
                    return false;
                }
            }
        </script>

        <link href="../styles/default.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <form onSubmit='return MM_validateForm()' action="" method="post" name="newdocument" id="newdocument" enctype="multipart/form-data">
            <input type="hidden" name="MAX_FILE_SIZE" value="1000000" />

            <fieldset>
                <legend>Document Details</legend>

                <input name="id" type="hidden" value="<?php echo $id; ?>" />
                <table width="100%" border="0" cellspacing="0" cellpadding="4">
                    <tr>
                        <td width="16%" align='right' nowrap><strong>Client :</strong></td>
                        <td width="29%">
                            <select name="client" id="client"  onFocus="this.style.backgroundColor = '#ffff00'"  onBlur="this.style.backgroundColor = '#F2FAFF'"  onChange="submit()" class='forms' ><?php showclients($_SESSION['client']); ?>
                            </select></td>
                        <td width="20%" align='right' nowrap><strong>Document Category :</strong></td>
                        <td width="35%">
                            <select name="category" id="category"  onFocus="this.style.backgroundColor = '#ffff00'"  onBlur="this.style.backgroundColor = '#F2FAFF'"  onChange="submit()" class='forms' ><?php showcategory($_SESSION['category']); ?>
                            </select></td>
                    </tr>
                    <tr></tr>
                    <tr valign="baseline">
                        <td align='right' nowrap><strong>Project :</strong></td>
                        <td><select name="proj" id="proj"  onFocus="this.style.backgroundColor = '#ffff00'"  onBlur="this.style.backgroundColor = '#F2FAFF'"  onChange="submit()" class='forms' ><?php showcprojects($_SESSION['proj'], $_SESSION['client']); ?>
                            </select></td>
                        <td align='right' nowrap><strong>Document Description:</strong></td>
                        <td><input name="desc"   type="text"  class="forms"  size="50" value="<?php echo $row_sql['description'] ?>" /></td>
                    </tr>
                    <tr valign="baseline"></tr>
                    <tr valign="baseline">
                        <td align='right' nowrap ><strong>Unit :</strong></td>
                        <td ><select name="unit" class="forms" onChange="submit()" >
                                <?php showprojunit($_SESSION['unit'], $_SESSION['proj']); ?>
                            </select></td>
                        <td align='right' nowrap><strong>Upload Document:</strong></td>
                        <td><input name="docimage"   type="file"  class="forms"  size="25" /></td>
                    </tr>

                    <tr valign="baseline">
                        <td align='right' nowrap><strong>Display Order:</strong></td>
                        <td><input name="disp" id="disp"   type="text"  class="forms"  size="5" value="<?php echo $row_sql['displayorder'] ?>" />
                            &nbsp;<strong> Next <?php echo $row_prq1['D'] + 1; ?></strong></td>
                        <td align='right' nowrap><strong>Adverse Party</strong></td>
                        <td><input name="adverseparty"   type="text"  class="forms"  size="50" value="<?php echo $row_sql['adverseparty'] ?>" /></td>
                    </tr>
                    <tr valign='center'>
                        <td colspan='4' ><div align='center'>

                                <?php if ($_GET['did'] != '') { ?>
                                    <input name='update' type='submit' class='formsBlue' value='Update'>
                                    <a href="newprojdoc.php?id=<?php $_GET['did']; ?>"> <a onclick='javascript:confirmdelete("<?php echo $row_sql['description'] ?>", "<?php echo $_GET['did'] ?> ")' ><input name='delete' type='button' class='formsRed' value='Delete'  > </a> </a>
                                <?php } else { ?>
                                    <input name='save' type='submit' class='formsBlue' value='Save'>
                                    <?php
                                }
                                ?>
                                <input name='cancel' type='button' onClick="location = 'newprojdoc.php'" class='formsorg' value='Cancel'>
                            </div></td>
                    </tr>
                </table>

            </fieldset>

            <br>
            <?php if (isset($_SESSION['client']) && $_SESSION['client'] != '' && isset($_SESSION['category']) && $_SESSION['category'] != '' && isset($_SESSION['proj']) && $_SESSION['proj'] != '') {
                ?>
                <table  width="41%"cellspacing="0" cellpadding="4" border="1" >
                    <tr valign="baseline"  >

                        <td colspan="2"  valign="top" class="inputdef"><strong>Default 
                                <input    name="perc" type="radio" value="d" onClick="submit()" <?php if ($_SESSION['perc'] == 'd') { ?> checked="checked"<?php } ?>>&nbsp;
                                View 50% <strong>
                                    <input    name="perc" type="radio" value="y" onClick="submit()" <?php if ($_SESSION['perc'] == 'y') { ?> checked="checked"<?php } ?>>
                                </strong>&nbsp;
                                View 100% 
                                <input   name="perc" type="radio" value="f" onClick="submit()" <?php if ($_SESSION['perc'] == 'f') { ?> checked="checked"<?php } ?>>
                            </strong></td>
                    </tr>
                </table>

                <table  width="57%"cellspacing="0" cellpadding="4" border="1" height="104">


                    <?php
                    if ($_SESSION['perc'] == 'y') {
                        do {

                            $length = strlen($row_sql2['imageurl']);
                            $start = $length - 3;
                            $st = substr($row_sql2['imageurl'], $start, 3);
//echo "the doc type".$st;
                            if ($st == 'doc' or $st == 'pdf' or $st == 'xls') {
                                $filename = $row_sql2['imageurl'];
                                ?>
                                <tr valign="baseline" bordercolor="#333333">
                                    <td ><?php if ($row_sql2['imageurl'] != '') { ?> <a href="<?php echo $filename ?>" >Download file</a><?php } ?></td>
                                    <td width="45%"  valign="top" nowrap><strong><?php echo $row_sql2['description'] ?><p><a href="newprojdoc.php?did=<?php echo $row_sql2['id'] ?>">Edit</a></p><?php echo $row_sql2['displayorder'] ?></strong></td>
                                </tr>
                            <?php } else {
                                ?>


                                <tr valign="baseline" bordercolor="#333333">
                                    <td width="55%"  valign="top"> <input  type="image"  src="<?php echo $row_sql2['imageurl']; ?>"  width="450"  height="600"  onClick="window.open('viewimage.php?id=<?php echo $row_sql2['id']; ?>', '-blank', 'height=800,width=800,left=200,top=100,scrollbars=yes')"></td>
                                    <td width="45%"  valign="top" nowrap><strong><?php echo $row_sql2['description'] ?><p><a href="newprojdoc.php?did=<?php echo $row_sql2['id'] ?>">Edit</a></p><?php echo $row_sql2['displayorder'] ?></strong></td>
                                </tr>
                            <?php
                            }
                        } while ($row_sql2 = mysqli_fetch_assoc($sql_select2));
                    }
                    ?>
                </table>

                <table  width="100%"cellspacing="0" cellpadding="4" border="1" height="104">       
                    <?php
                    if ($_SESSION['perc'] == 'f') {
                        do {
                            $length = strlen($row_sql2['imageurl']);
                            $start = $length - 3;
                            $st = substr($row_sql2['imageurl'], $start, 3);
//echo "the doc type".$st;
                            if ($st == 'doc' or $st == 'pdf' or $st == 'xls') {
                                $filename = $row_sql2['imageurl'];
                                ?>
                                <tr valign="baseline" bordercolor="#333333">
                                    <td ><?php if ($row_sql2['imageurl'] != '') { ?> <a href="<?php echo $filename ?>" >Download file</a><?php } ?></td>
                                    <td width="45%"  valign="top" nowrap><strong><?php echo $row_sql2['description'] ?><p><a href="newprojdoc.php?did=<?php echo $row_sql2['id'] ?>">Edit</a></p><?php echo $row_sql2['displayorder'] ?></strong></td>
                                </tr>
                            <?php } else {
                                ?>
                                <tr valign="baseline" bordercolor="#333333">
                                    <td  valign="top"> <input  type="image"  src="<?php echo $row_sql2['imageurl']; ?>"  width="900"  height="800"  onClick="window.open('viewimage.php?id=<?php echo $row_sql2['id']; ?>', '-blank', 'height=800,width=800,left=200,top=100,scrollbars=yes')"></td>
                                    <td  valign="top"><strong><?php echo $row_sql2['description'] ?><p><a href="newprojdoc.php?did=<?php echo $row_sql2['id'] ?>">Edit</a></p><?php echo $row_sql2['displayorder'] ?></strong></td>
                                </tr>
                            <?php
                            }
                        } while ($row_sql2 = mysqli_fetch_assoc($sql_select2));
                    }
                    ?>
                </table>

                <table  width="41%"cellspacing="0" cellpadding="4" border="1" >  
                    <?php
                    if ($_SESSION['perc'] == 'd') {
                        do {
                            $length = strlen($row_sql2['imageurl']);
                            $start = $length - 3;
                            $st = substr($row_sql2['imageurl'], $start, 3);
//echo "the doc type".$st;
                            $doc_name = pathinfo($row_sql2['imageurl']);
                            if ($doc_name['extension'] == 'doc'or $doc_name['extension'] == 'docx' or $doc_name['extension'] == 'pdf' or $doc_name['extension'] == 'xls') {
                                $filename = $row_sql2['imageurl'];
                                ?>
                                <tr valign="baseline" bordercolor="#333333">
                                    <td ><?php if ($row_sql2['imageurl'] != '') { ?> <a href="<?php echo $filename ?>" >Download file</a><?php } ?></td>
                                    <td width="45%"  valign="top" nowrap><strong><?php echo $row_sql2['description'] ?><p><a href="newprojdoc.php?did=<?php echo $row_sql2['id'] ?>">Edit</a></p><?php echo $row_sql2['displayorder'] ?></strong></td>
                                </tr>
                                <?php
                            } else {
                                ?>

                                <tr valign="baseline" bordercolor="#333333">
                                    <td width="49%"  valign="top"> <input  type="image"  src="<?php echo $row_sql2['imageurl']; ?>"  width="190"  height="150"  onClick="window.open('viewimage.php?id=<?php echo $row_sql2['id']; ?>', '-blank', 'height=800,width=800,left=200,top=100,scrollbars=yes')"></td>
                                    <td width="51%"  valign="top"><strong><?php echo $row_sql2['description'] ?><p><a href="newprojdoc.php?did=<?php echo $row_sql2['id'] ?>">Edit</a></p><?php echo $row_sql2['displayorder'] ?></strong></td>
                                </tr>

                            <?php
                            }
                        } while ($row_sql2 = mysqli_fetch_assoc($sql_select2));
                    }
                    ?>
                </table>
<?php } ?>
        </form>
    </body>
</html>
