<?php require_once('../connections/eProc.php'); ?>

<?php

$currentPage = $_SERVER["PHP_SELF"];

$maxRows_requesttype = 30;
$pageNum_requesttype = 0;


  unset($_SESSION['itype']);


if (isset($_GET['pageNum_requesttype'])) {
  $pageNum_requesttype = $_GET['pageNum_requesttype'];
}
$startRow_requesttype = $pageNum_requesttype * $maxRows_requesttype;

mysqli_select_db($eProc, $database_eProc);
$query_requesttype = "SELECT * FROM requesttype   ";

$query_limit_requesttype = sprintf("%s LIMIT %d, %d", $query_requesttype, $startRow_requesttype, $maxRows_requesttype);
$requesttype = mysqli_query( $eProc, $query_limit_requesttype) or die(mysqli_error());
$row_requesttype = mysqli_fetch_assoc($requesttype);

if (isset($_GET['totalRows_requesttype'])) {
  $totalRows_requesttype = $_GET['totalRows_requesttype'];
} else {
  $all_requesttype = mysqli_query($eProc,$query_requesttype);
  $totalRows_requesttype = mysqli_num_rows($all_requesttype);
}
$totalPages_requesttype = ceil($totalRows_requesttype/$maxRows_requesttype)-1;

$queryString_requesttype = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_requesttype") == false && 
        stristr($param, "totalRows_requesttype") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_requesttype = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_requesttype = sprintf("&totalRows_requesttype=%d%s", $totalRows_requesttype, $queryString_requesttype);
?>

<html>
<head>
<title>LEGAL MANAGEMENT SYSTEM</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../styles/default.css" rel="stylesheet" type="text/css">

<style type="text/css">
<!--
.style1 {
	color: #FF0000;
	font-weight: bold;
}
-->
</style>
</head>
<body>
<fieldset>
<table width="100%" border="0" cellpadding="4" cellspacing="0" >

  <tr>
  <td width="30%"   class="inputdef" style="font-weight: bold">Reques Type Name</td>
  <td width="33%"   class="inputdef" style="font-weight: bold">Cost</td>
  <td width="37%"   class="inputdef" style="font-weight: bold" ><div align="right">
	  <table width="154"  border="0" cellspacing="0" cellpadding="3">
	    <tr class="inputdef">
	      <td width="19"  ><img src="../images/icons/newproject.gif" width="15" height="13" border="0" /></td>
                
			<td width="123"    ><a href="newrequesttype.php">New Request Type</a></td>
            </tr>
	    </table>
	  </div></td>
  </tr>
  <?php if ($totalRows_requesttype > 0) { ?>
  <?php do { ?>
  <tr>
   <td > <?php echo $row_requesttype['name']?> </td>
   <td ><?php echo $row_requesttype['cost']?></td>
   <td ><a href="newrequesttype.php?id=<?php echo $row_requesttype['id'] ?>">Edit</a></td>
  </tr>
  <?php } while ($row_requesttype = mysqli_fetch_assoc($requesttype)); ?>
  <tr>
    <td colspan="5" class="mainbase"><table width="97%"  border="0" cellspacing="0" cellpadding="3">
      <tr align="center">
        <td width="45%">
          <table width="78%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
              <td width="23%" align="center"><?php if ($pageNum_requesttype > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_requesttype=%d%s", $currentPage, 0, $queryString_requesttype); ?>">First</a>
                <?php } // Show if not first page ?>              </td>
              <td width="31%" align="center"><?php if ($pageNum_requesttype > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_requesttype=%d%s", $currentPage, max(0, $pageNum_requesttype - 1), $queryString_requesttype); ?>">Previous</a>
                <?php } // Show if not first page ?>              </td>
              <td width="23%" align="center"><?php if ($pageNum_requesttype < $totalPages_requesttype) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_requesttype=%d%s", $currentPage, min($totalPages_requesttype, $pageNum_requesttype + 1), $queryString_requesttype); ?>">Next</a>
                <?php } // Show if not last page ?>              </td>
              <td width="23%" align="center"><?php if ($pageNum_requesttype < $totalPages_requesttype) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_requesttype=%d%s", $currentPage, $totalPages_requesttype, $queryString_requesttype); ?>">last</a>
                <?php } // Show if not last page ?>              </td>
            </tr>
          </table></td>
        <td width="55%">&nbsp; Showing <strong><?php echo ($startRow_requesttype + 1) ?></strong> to <strong><?php echo min($startRow_requesttype + $maxRows_requesttype, $totalRows_requesttype) ?></strong> of <strong><?php echo $totalRows_requesttype ?></strong> </td>
      </tr>
    </table></td>
  </tr>
  <?php } else { ?>
  <tr>
    <td colspan="5" class="mainbase"><span class="style1">No requesttype Created! </span></td>
  </tr>
  <?php } ?>
</table>
</fieldset>
</body>
</html>
<?php
mysqli_free_result($requesttype);
?>

