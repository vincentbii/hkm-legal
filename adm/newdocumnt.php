<?php
require_once('../connections/eProc.php');
include('../activelog.php');
mysqli_select_db($eProc, $database_eProc);
unset($_SESSION['document']);

if (isset($_POST['document']) && $_POST['document'] != '') {
     $_SESSION['document'] = $_POST['document'];
}
if (isset($_POST['document']) && $_POST['document'] == '') {
     unset($_SESSION['document']);
}

if (isset($_POST['save']) && $_POST['save'] == 'Save') {

     $path = "../docimages/";

     $path = $path . $_FILES["docimage"]["name"];



     if (((
             $_FILES["docimage"]["type"] == "image/gif")// valid file types
             || ($_FILES["docimage"]["type"] == "image/jpeg")
             || ($_FILES["docimage"]["type"] == "image/pjpeg"))
             && ($_FILES["docimage"]["size"] < 2000000)) {//specify maximum size 2MB
          //echo "one";
          if ($_FILES["docimage"]["error"] > 0) { //if no error found
               echo "Error: " . $_FILES["docimage"]["error"] . "<br />";
          } else {


               if (file_exists($path)) {
                    // echo "teProcee";
                    echo "Warning:: " . $path . " already exists<br/>";
               } else {
                    // echo "four";
                    move_uploaded_file($_FILES["docimage"]["tmp_name"], $path);
                    //echo "Stored in: " . $path."<br/>";
               }
          }
          //display the image
//echo'<img src="'.$path.'" alt="uploaded photo"  width="400" height="400"/><br/>'; 
     } else {
          //echo "five";
          echo "<p>Invalid file</p>";
     }
     $insertSQL = sprintf("INSERT INTO empdocs (employee,doc, imageurl,company) 
	 VALUES (%s,%s,%s,%s)", GetSQLValueString($_SESSION['employee'], "text"), GetSQLValueString($_POST['document'], "text"), GetSQLValueString($path, "text"), GetSQLValueString($_SESSION['company'], "text"));
     //echo $insertSQL;
     $Result2 = mysqli_query($eProc, $insertSQL) or die(mysqli_error($eProc));
     ?> <script language='javascript'>
                          location = 'newdocumnt.php'
     </script><?php
}


if (isset($_POST['update']) && $_POST['update'] == 'Update') {

     $selectSQL2 = "SELECT * FROM empdocs WHERE employee = '" . $_SESSION['employee'] . "' AND id='" . $_GET['id'] . "'";
     //echo $selectSQL;	
     $sql_select2 = mysqli_query($eProc, $selectSQL2) or die(mysqli_error($eProc));
     $row_sql2 = mysqli_fetch_assoc($sql_select2);
     $nums2 = mysqli_num_rows($sql_select2);
     $_SESSION['document'] = $row_sql2['doc'];
     $url = $row_sql2['imageurl'];


     $path = "../docimages/";

     $path = $path . $_FILES["docimage"]["name"];



     if (((
             $_FILES["docimage"]["type"] == "image/gif")// valid file types
             || ($_FILES["docimage"]["type"] == "image/jpeg")
             || ($_FILES["docimage"]["type"] == "image/pjpeg"))
             && ($_FILES["docimage"]["size"] < 2000000)) {//specify maximum size 2MB
          //echo "one";
          if ($_FILES["docimage"]["error"] > 0) { //if no error found
               echo "Error: " . $_FILES["docimage"]["error"] . "<br />";
          } else {


               if (file_exists($path)) {
                    // echo "teProcee";
                    echo "Warning:: " . $path . " already exists<br/>";
               } else {
                    // echo "four";
                    move_uploaded_file($_FILES["docimage"]["tmp_name"], $path);
                    //echo "Stored in: " . $path."<br/>";
               }
          }
          //display the image
//echo'<img src="'.$path.'" alt="uploaded photo"  width="400" height="400"/><br/>'; 
     } else {
          //echo "five";
          echo "<p>Invalid file</p>";
     }


     if ($_POST['docimage'] == "") {
          $url = $url;
     } else {
          $url = $path;
     }

     $updateSQL = "UPDATE empdocs SET  doc='" . $_POST['document'] . "' ,imageurl='" . $url . "' WHERE  id ='" . $_GET['id'] . "'  ";

     $updtResult = mysqli_query($eProc, $updateSQL) or die(mysqli_error($eProc));
     ?> <script language='javascript'>
                          location = 'newdocumnt.php'
     </script><?php
}


if (isset($_GET['delete']) && $_GET['delete'] == 'Delete') {
     $DeleteSQL = "DELETE FROM empdocs WHERE  id ='" . $_GET['id'] . "'";
     //echo $DeleteSQL;
     $Result3 = mysqli_query($eProc, $DeleteSQL)
     ?>      
     <script language='javascript'>location = 'newdocumnt.php'</script> <?php
}


$selectSQL = "SELECT empdocs.*,doc.name AS documnt FROM empdocs INNER JOIN doc ON empdocs.doc=doc.id WHERE employee = '" . $_SESSION['employee'] . "'";
//echo $selectSQL;	
$sql_select = mysqli_query($eProc, $selectSQL) or die(mysqli_error($eProc));
$row_sql = mysqli_fetch_assoc($sql_select);
$nums = mysqli_num_rows($sql_select);


$selectSQL2 = "SELECT * FROM empdocs WHERE employee = '" . $_SESSION['employee'] . "' AND id='" . $_GET['id'] . "'";
//echo $selectSQL;	
$sql_select2 = mysqli_query($eProc, $selectSQL2) or die(mysqli_error($eProc));
$row_sql2 = mysqli_fetch_assoc($sql_select2);
$nums2 = mysqli_num_rows($sql_select2);
$_SESSION['document'] = $row_sql2['doc'];
$url = $row_sql['imageurl'];



if (isset($_POST['view' . $rowsql['item']]) && $_POST['view' . $rowsql['item']] == 'View Supplier Price') {
     ?>
     <script>
          window.open("supplierprices.php?item=<?php echo $_POST['item' . $rowsql['id']]; ?>", "-blank", "height=400,width=450,left=200,top=200,scrollbars=no");
     </script>
     <?php
}
?>  

<html>
     <head>
          <title>NBNET  MANAGEMENT INFORMATION  SYSTEM</title>
          <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
          <link href="../styles/default.css" REL="stylesheet" type="text/css">
          <LINK REL=StyleSheet  href="calendar.css"  TYPE="text/css">
          <script LANGUAGE="JavaScript"  src="weeklycalendar.js"></script>
          <script language="JavaScript" type="text/javascript">

               buildWeeklyCalendar(1);
          </script>
          <script language="javascript" type="text/javascript">
               function MM_validateForm() {
                    if (document.newdoc.EmployeeCode.value == '') {
                         alert('You must enter the EmployeeCode to continue');
                         document.newdoc.EmployeeCode.focus();
                         return false;
                    }
                    if (document.newdoc.EmployerName.value == '') {
                         alert('You must enter the EmployerName to continue');
                         document.newdoc.EmployerName.focus();
                         return false;
                    }
                    if (document.newdoc.Address.value == '') {
                         alert('You must enter the Address to continue');
                         document.newdoc.Address.focus();
                         return false;
                    }
                    if (document.newdoc.JobpositionCode.value == '') {
                         alert('You must enter the JobpositionCode to continue');
                         document.newdoc.JobpositionCode.focus();
                         return false;
                    }
               }

               function checkNumbers(evt) {
                    evt = (evt) ? evt : window.event
                    var charCode = (evt.which) ? evt.which : evt.keyCode
                    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                         if (charCode < 37 || charCode > 40) {
                              alert('This field accepts only numeric characters (0-9) !');
                              status = 'this field accepts only numbers only!'
                              return false;
                         }
                    }
                    status = ''
                    return true
               }
               function validatechars(evt)
               {
                    // Prevents the entry of special characters and spaces into a field
                    evt = (evt) ? evt : window.event
                    var charCode = (evt.which) ? evt.which : evt.keyCode
                    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                         if (charCode < 64 || charCode > 90) {
                              if ((charCode < 95 || charCode > 122) && charCode != 8) {
                                   if (charCode != 127 && charCode != 46 && charCode != 45) {
                                        if ((charCode < 37 || charCode > 40) && charCode != 32) {
                                             alert('This field accepts only 0-9, a-z, A_Z, space, backspace & del characters !');
                                             status = 'This field accepts only 0-9, a-z, A_Z, space & del characters !'
                                             return false;
                                        }
                                   }
                              }
                         }
                    }
                    status = ''
                    return true
               }
               function confirmdelete(delname, delid) {
                    if (confirm('Are you sure you want to delete  ' + delname + '?')) {
                         location.href = 'newdocumnt.php?delete=Delete&id=' + delid
                    }
               }
          </script>
          <link href="../styles/default.css" rel="stylesheet" type="text/css">
     </head>
     <body>
          <form onSubmit='return MM_validateForm()' action="" method="post" name="newdoc" id="newdoc" enctype="multipart/form-data">
               <input type="hidden" name="MAX_FILE_SIZE" value="1000000" />
               <table width="100%" height="183">

                    <tr>
                         <td width="47%" valign="top">    


                              <table width="91%" border="0" cellspacing="0" cellpadding="4">
                                   <tr valign="baseline">
                                        <td align='right'  class="inputdef"><strong>Document</strong></td>
                                   </tr>
<?php if ($nums > 0) {
     do {
          ?>
                                             <tr valign="baseline">
                                                  <td ><a href="newdocumnt.php?id=<?php echo $row_sql['id'] ?>"><?php echo $row_sql['documnt'] ?></a></td>
                                             </tr>
     <?php } while ($row_sql = mysqli_fetch_assoc($sql_select));
}
?>
                              </table>

                         </td>
                         <td width="53%" valign="top">   
                              <table width="100%" cellpadding="5">
                                   <tr align="center">
                                        <td width="50%" valign="top">
                                             <fieldset>
                                                  <legend>Document Details</legend>

                                                  <input name="id" type="hidden" value="<?php echo $id; ?>" />
                                                  <table width="100%" border="0" cellspacing="0" cellpadding="4">
                                                       <tr valign="baseline">
                                                            <td align='right' nowrap><strong>Document type:</strong></td>
                                                            <td>
                                                                 <select name="document" id="document"  onFocus="this.style.backgroundColor = '#ffff00'"  onBlur="this.style.backgroundColor = '#F2FAFF'"  class='forms' ><?php showdocuments($_SESSION['document']); ?>
                                                                 </select></td>
                                                       </tr>
                                                       <tr valign="baseline">
                                                            <td align='right' nowrap><strong>Upload Document:</strong></td>
                                                            <td><input name="docimage"   type="file"  class="forms"  size="25" /></td>
                                                       </tr>
                                                       <tr valign='center'>
                                                            <td colspan='2' ><div align='center'>
<?php if ($_GET['id'] != '') { ?>
                                                                           <input name='update' type='submit' class='formsBlue' value='Update'>
                                                                           <a href="newworkexperience.php?id=<?php $_GET['id']; ?>"> <a onclick='javascript:confirmdelete("<?php echo $row_sql2['employer'] ?>", "<?php echo $_GET['id'] ?> ")' ><input name='delete' type='button' class='formsRed' value='Delete'  > </a> </a>
<?php } else { ?>
                                                                           <input name='save' type='submit' class='formsBlue' value='Save'>
     <?php
}
?>
                                                                      <input name='cancel' type='button' onClick="location = 'newdocumnt.php'" class='formsorg' value='Cancel'>
                                                                 </div></td>
                                                       </tr>
                                                  </table>

                                             </fieldset
                                             ></table>
<?php if (isset($_GET['id']) && $_GET['id'] != '') { ?>
                                   <br>
                                   <div align="center">
                                        <table  width="29%"cellspacing="0" cellpadding="4" border="1" height="104">
                                             <tr valign="baseline" bordercolor="#333333">
                                                  <td  valign="top"> <input  type="image"  src="<?php echo $row_sql2['imageurl']; ?>"  width="400"  height="450"  onClick="window.open('viewimage.php?id=<?php echo $_GET['id']; ?>', '-blank', 'height=800,width=800,left=200,top=100,scrollbars=yes')"></td>
                                             </tr>
                                        </table></div>
<?php } ?>
                         </td>
                    </tr>
               </table></form>
     </body>
</html>
