<?php 
session_name('assetsystem');
@session_start();
$hostname_stock = "localhost";
$database_stock = "legal";
$username_stock = "root";
$password_stock = "";

$stock = mysqli_connect($hostname_stock, $username_stock, $password_stock) or trigger_error(mysqli_error(),E_USER_ERROR);

$datet=date("Y-m-d H:i:s");
require ('../pdf/fpdf.php');
mysqli_select_db($database_stock, $stock);

$PDF = new FPDF('L', 'mm', 'A4');$PDF->SetMargins('20','10','10');
$PDF->AliasNbPages();
$PDF->AddPage();
$PDF->SetFont('Arial', 'B', 8);
$PDF->Cell(0, 3, 'REQUISITION PAYMENT VOUCHER ','0','1','C');
$PDF->ln();
$PDF->Cell(0, 3, 'AS AT '.$datet.$_SESSION['UNQ'],'0','1','C');
$PDF->ln();
$PDF->Cell(0, 3, 'GENERATED  BY '.$_SESSION['USERNAME'],'0','1','C');
$PDF->ln();
$PDF->SetFont('Arial', 'B', 8);
$PDF->Cell(60, 7, 'Requested By','0','0');
$PDF->Cell(55, 7, 'Owner','0','0');
$PDF->Cell(25, 7, 'Location','0','0');
$PDF->Cell(20, 7, 'Status','0','0');
$PDF->Cell(30, 7, 'Tel','0','0');
$PDF->Cell(40, 7, 'Contact person','0','0');
$PDF->Cell(20, 7, 'Reg Date','0','0');  
$PDF->Cell(20, 7, 'Expiry Date','0','0'); 	  	  	  	

$PDF->ln();
$PDF->SetFont('Arial', '', 7);

$selectSQL = "SELECT * FROM requisition ";
	//echo $selectSQL;	
$sql_select = mysqli_query($selectSQL, $stock) or die(mysqli_error());
$row_sql = mysqli_fetch_assoc($sql_select);	
$x=1;
do{
$PDF->Cell(60, 5,$x.".  ". $row_sql['name'],'0','0');
$PDF->Cell(55,5 ,  $row_sql['owner'],'0','0');
$PDF->Cell(25, 5, $row_sql['country'],'0','0');
$PDF->Cell(20, 5, $row_sql['status'],'0','0');
$PDF->Cell(30, 5, $row_sql['tel'],'0','0');
$PDF->Cell(40, 5, $row_sql['contactperson'],'0','0');
$PDF->Cell(20, 5, $row_sql['regdate'],'0','0');
$PDF->Cell(20, 5, $row_sql['expiry'],'0','0');
$PDF->ln();
$x++;
  }while($row_sql = mysqli_fetch_assoc($sql_select)); //funga while

//$PDF->Output('myFile.pdf','I');
$PDF->Output('Domains.pdf','D'); 
//$PDF->Output('myFile.pdf','S');
//$PDF->Output('pdf/tickets-summary'.$newno.'.pdf','F');  //save file in the server

 //include('../includes/footer.php');
exit();
?>
