<?php require_once('../connections/eProc.php'); 
include('../activelog.php');
mysqli_select_db($eProc, $database_eProc);
unset($_SESSION['department']);
if(isset($_GET['id']) && $_GET['id']!=''){ 
	$_SESSION['department']=$_GET['id'];
	}


	if ($_POST['update']!="" && $_POST['update']=="Update" ) {
		
		$updateSQL = "UPDATE department SET  name='".$_POST['name']."',description='".$_POST['desc']."',telephone='".$_POST['tel']."',email='".$_POST['email']."',fax='".$_POST['fax']."' WHERE  id ='".$_SESSION['department']."' AND company='".$_SESSION['company']."'  ";
			//echo $updateSQL; 
			$Result1 = mysqli_query($eProc, $updateSQL) or die(mysqli_error($eProc));
			
			?> 
			<script language="JavaScript" type="text/javascript">
				location='departmentslist.php';
			</script>
			<?php  
	} 
		
	if ($_POST['save']!="" && $_POST['save']=="Save" ) {
	$exist=recordexists("department","name",$_POST['name']) ;		
		if($exist=='no'){					
			$insertSQL =sprintf( "INSERT INTO department (name,description,telephone,email,fax,company) VALUES (%s,%s,%s,%s,%s,%s)",
			 GetSQLValueString($_POST['name'], "text"),
			 GetSQLValueString($_POST['desc'], "text"),
			 GetSQLValueString($_POST['tel'], "text"),
			 GetSQLValueString($_POST['email'], "text"),
			 GetSQLValueString($_POST['fax'], "text"),
			  GetSQLValueString($_SESSION['company'], "text"));			
						
			//echo $insertSQL;
		$Result2 = mysqli_query($eProc, $insertSQL) or die(mysqli_error($eProc));
					 
			?> 
			<script language="JavaScript" type="text/javascript">
				location='departmentslist.php';
			</script>
			<?php
			
			}else{
	?> 
			<script language="JavaScript" type="text/javascript">
				alert("A  department  with <?php echo $_POST['name']?>  name exists.");
				location='newdepartment.php';
			</script>
			<?php
			}
	}
	
	if ( isset($_GET['del']) && $_GET['del']!="" ) {
	
		$DeleteSQL = "DELETE FROM department WHERE  id ='".$_GET['del']."'";
		//echo $DeleteSQL;
		if(!$Result3 = mysqli_query($eProc, $DeleteSQL)){?>
         
		<script language="JavaScript" type="text/javascript">
			alert("The department has transactions.Please delete the transactions first");
			location='departmentslist.php';
		
		</script>
		<?php }else{?>
	
		<script language="JavaScript" type="text/javascript">
			location='departmentslist.php';
		</script>
		<?php
	}
	}
	

	$selectSQL = "SELECT * FROM department WHERE id = '".$_SESSION['department']."'";
	//echo $selectSQL;	
	$sql_select = mysqli_query($eProc, $selectSQL) or die(mysqli_error());
	$row_sql = mysqli_fetch_assoc($sql_select);	

?>
<html>
<head>
<title>LEGAL  MANAGEMENT INFORMATION  SYSTEM</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../styles/default.css" REL="stylesheet" type="text/css">
<script language="javascript" type="text/javascript">
   function MM_valIDateForm() { 
 	  if(document.newdepartment.name.value=='' ){
 		alert('You must enter the Name to continue');
 		document.newdepartment.name.focus();
 		return false ;
 		} 
 	} 

	function valIDatechars(evt)
		{
			// Prevents the entry of special characters and spaces into a field
			evt=(evt)? evt: window.event
			var charCode= (evt.which)? evt.which: evt.keyCode
			if( charCode>31 && (charCode<48 || charCode>57)) {
				if (charCode<64 || charCode>90) {
					if ((charCode<95 || charCode>122) && charCode!=8 ) {
						if (charCode!=127 && charCode!=46 && charCode!=45 ) {
							if ((charCode<37 || charCode>40) && charCode!=32) {
								alert('This field accepts only 0-9, a-z, A_Z, space, backspace & del characters !');
								status='This field accepts only 0-9, a-z, A_Z, space & del characters !'
								return false;
							}
						}
					}
				}
			}
			status=''
			return true
		}
 		function confirmdelete(name,id){
 		    	if (confirm('Are you sure you want to delete  department ' +name+ '?')) {
 					location.href = 'newdepartment.php?del='+id 
 					} 
  				}
</script>
</head>
<body>
<table width="100%" cellpadding="5">
<tr align="department">
<td width="50%" valign="top">
<fieldset>
<legend>Department Details</legend>
<form onSubmit='return MM_valIDateForm()' action="" method="post" name="newdepartment" ID="newdepartment">
<input name="ID" type="hidden" value="<?php echo $row_sql['id'] ?>" />
<table width="100%" border="0" cellspacing="0" cellpadding="4">
<tr valign="baseline">
<td align='right' nowrap class=""><strong><font color=red size=+1>* </font>Name:</strong></td>
<td>
<input size="40" maxlength="100" name="name" ID="name"  onFocus="this.style.backgroundColor='#ffff00'"  onBlur="this.style.backgroundColor='#F2FAFF'"  class='forms'   onkeypress='return valIDatechars(event)' value="<?php echo $row_sql['name']; ?>" >
</td>
</tr>
<tr valign="baseline">
<td align='right' nowrap><strong>Description:</strong></td>
<td>
<textarea cols='40' rows='3' name="desc"  onFocus="this.style.backgroundColor='#ffff00'"  onBlur="this.style.backgroundColor='#F2FAFF'" onChange='this.value=this.value.toUpperCase()' class='forms'  > <?php echo $row_sql['description']; ?></textarea>
</td>
</tr>
<tr valign="baseline">
<td align='right' nowrap><strong>Telephone:</strong></td>
<td>
<input size="40" maxlength="100" name="tel" ID="tel"  onFocus="this.style.backgroundColor='#ffff00'"  onBlur="this.style.backgroundColor='#F2FAFF'"  class='forms'   onkeypress='return valIDatechars(event)' value="<?php echo $row_sql['telephone']; ?>" >
</td>
</tr>
<tr valign="baseline">
<td align='right' nowrap><strong>Email:</strong></td>
<td>
<input size="40" maxlength="100" name="email" ID="email"  onFocus="this.style.backgroundColor='#ffff00'"  onBlur="this.style.backgroundColor='#F2FAFF'"  class='forms'   onkeypress='return valIDatechars(event)' value="<?php echo $row_sql['email']; ?>" >
</td>
</tr>
<tr valign="baseline">
<td align='right' nowrap><strong>Fax:</strong></td>
<td>
<input size="40" maxlength="100" name="fax" ID="fax"  onFocus="this.style.backgroundColor='#ffff00'"  onBlur="this.style.backgroundColor='#F2FAFF'"  class='forms'   onkeypress='return valIDatechars(event)' value="<?php echo $row_sql['fax']; ?>" >
</td>
</tr>
<tr valign='department'>
<td colspan='2' ><div align='center'>
 <?php if (isset($_SESSION['department'])){?>
 <input name="update" type="submit" class="formsBlue" id="update" value="Update" />
<a href="newdepartment.php?id=<?php $row_sql['id']; ?>"> <a onclick='javascript:confirmdelete("<?php  echo $row_sql['name']?>","<?php echo $row_sql['id']?> ")' ><input name='delete' type='button' class='formsRed' value='Delete'  > </a> </a>
<?php }else{?>
<input name='save' type='submit' class='formsBlue' value='Save'>
<?php }?>
<input name='cancel' type='button' onClick="location='departmentslist.php'" class='formsorg' value='Cancel'>
</div></td>
</tr>
</table>
</form>
</fieldset
></table>
</body>
</html>
