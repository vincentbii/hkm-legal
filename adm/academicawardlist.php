<?php require_once('../connections/eProc.php'); 

 require_once('../activelog.php'); ?>
<?php
$currentPage = $_SERVER["PHP_SELF"];

$maxRows_academicaward = 30;
$pageNum_academicaward = 0;
if (isset($_GET['pageNum_academicaward'])) {
  $pageNum_academicaward = $_GET['pageNum_academicaward'];
}
$startRow_academicaward = $pageNum_academicaward * $maxRows_academicaward;

mysqli_select_db($eProc, $database_eProc);
$query_academicaward = "SELECT * FROM academicaward ";
$query_limit_academicaward = sprintf("%s LIMIT %d, %d", $query_academicaward, $startRow_academicaward, $maxRows_academicaward);
$academicaward = mysqli_query($eProc, $query_limit_academicaward) or die(mysqli_error($eProc));
$row_academicaward = mysqli_fetch_assoc($academicaward);

if (isset($_GET['totalRows_academicaward'])) {
  $totalRows_academicaward = $_GET['totalRows_academicaward'];
} else {
  $all_academicaward = mysqli_query($eProc, $query_academicaward);
  $totalRows_academicaward = mysqli_num_rows($all_academicaward);
}
$totalPages_academicaward = ceil($totalRows_academicaward/$maxRows_academicaward)-1;

$queryString_academicaward = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_academicaward") == false && 
        stristr($param, "totalRows_academicaward") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_academicaward = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_academicaward = sprintf("&totalRows_academicaward=%d%s", $totalRows_academicaward, $queryString_academicaward);
?>

<html>
<head>
<title>BMA  MANAGEMENT INFORMATION  SYSTEM</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../styles/default.css" rel="stylesheet" type="text/css">

<style type="text/css">
<!--
.style1 {
	color: #FF0000;
	font-weight: bold;
}
-->
</style>
</head>
<body>

<table width="100%" border="0" cellpadding="4" cellspacing="0" >

  <tr >
   <td width="36%"   class="inputdeft" style="font-weight: bold"> Academic Awards</td>
   
	<td width="64%"   class="inputdeft" style="font-weight: bold" ><div align="right">
	  <table width="154"  border="0" cellspacing="0" cellpadding="3">
	    <tr class="inputdeft">
	      <td width="15"  ><img src="../images/icons/newproject.gif" width="15" height="13" border="0" /></td>
                
			<td width="127"   ><a href="../adm/newacademicaward.php">New Academic Award  </a></td>
          </tr>
	    </table>
	  </div></td>
  </tr>
  <?php if ($totalRows_academicaward > 0) { ?>
  <?php do { ?>
  <tr>
  <td ><?php echo $row_academicaward['name']?> </td>
  
  <td ><a href="../adm/newacademicaward.php?id=<?php echo $row_academicaward['id'] ?>">Edit</a></td>
  </tr>
  <?php } while ($row_academicaward = mysqli_fetch_assoc($academicaward)); ?>
  <tr>
    <td colspan="12" class="mainbase"><table width="97%"  border="0" cellspacing="0" cellpadding="3">
      <tr align="center">
        <td width="45%">
          <table width="78%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
              <td width="23%" align="center"><?php if ($pageNum_academicaward > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_academicaward=%d%s", $currentPage, 0, $queryString_academicaward); ?>">First</a>
                <?php } // Show if not first page ?>              </td>
              <td width="31%" align="center"><?php if ($pageNum_academicaward > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_academicaward=%d%s", $currentPage, max(0, $pageNum_academicaward - 1), $queryString_academicaward); ?>">Previous</a>
                <?php } // Show if not first page ?>              </td>
              <td width="23%" align="center"><?php if ($pageNum_academicaward < $totalPages_academicaward) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_academicaward=%d%s", $currentPage, min($totalPages_academicaward, $pageNum_academicaward + 1), $queryString_academicaward); ?>">Next</a>
                <?php } // Show if not last page ?>              </td>
              <td width="23%" align="center"><?php if ($pageNum_academicaward < $totalPages_academicaward) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_academicaward=%d%s", $currentPage, $totalPages_academicaward, $queryString_academicaward); ?>">last</a>
                <?php } // Show if not last page ?>              </td>
            </tr>
          </table></td>
        <td width="55%">&nbsp; Showing <strong><?php echo ($startRow_academicaward + 1) ?></strong> to <strong><?php echo min($startRow_academicaward + $maxRows_academicaward, $totalRows_academicaward) ?></strong> of <strong><?php echo $totalRows_academicaward ?></strong> </td>
      </tr>
    </table></td>
  </tr>
  <?php } else { ?>
  <tr>
    <td colspan="12" class="mainbase"><span class="style1">No academicawards Created! </span></td>
  </tr>
  <?php } ?>
</table>
</body>
</html>
<?php
mysqli_free_result($academicaward);
?>

