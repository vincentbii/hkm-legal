<?php require_once('../connections/eProc.php');
include('../activelog.php');
	$employee=getuserid();
$_SESSION['emp']=$employee;
unset($_SESSION['start']);	
unset($_SESSION['query']);
$query='';


if(isset($_POST['submit'])){

		
		if(isset($_POST['val']) && $_POST['val']!=''){
			$_SESSION['val']=$_POST['val'];
			
			$query=" AND (clients.fulnames LIKE '%".$_SESSION['val']."%' OR specificfile.jno='".intval($_SESSION['val'])."') ";
		}
		if(isset($_POST['val']) && $_POST['val']==''){
			unset($_SESSION['val']);
		}
		
$_SESSION['query']=$query;
}
$currentPage = $_SERVER["PHP_SELF"];

$maxRows_clients = 30;
$pageNum_clients = 0;


  unset($_SESSION['itype']);


if (isset($_GET['pageNum_clients'])) {
  $pageNum_clients = $_GET['pageNum_clients'];
}
$startRow_clients = $pageNum_clients * $maxRows_clients;

mysqli_select_db($eProc, $database_eProc);

$query_clients = "
SELECT * FROM userprojects
  INNER JOIN units ON userprojects.unit=units.id
  INNER JOIN projects ON projects.unit=units.id
  WHERE projects.state='A'
ORDER BY units.id ASC
 ";
//echo $query_clients;
$query_limit_clients = sprintf("%s LIMIT %d, %d", $query_clients, $startRow_clients, $maxRows_clients);
$clients = mysqli_query($eProc,$query_limit_clients) or die(mysqli_error($eProc));
$row_clients = mysqli_fetch_assoc($clients);

if (isset($_GET['totalRows_clients'])) {
  $totalRows_clients = $_GET['totalRows_clients'];
} else {
  $all_clients = mysqli_query($eProc,$query_clients);
  $totalRows_clients = mysqli_num_rows($all_clients);
}
$totalPages_clients = ceil($totalRows_clients/$maxRows_clients)-1;

$queryString_clients = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_clients") == false && 
        stristr($param, "totalRows_clients") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_clients = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_clients = sprintf("&totalRows_clients=%d%s", $totalRows_clients, $queryString_clients);

?>

<html>
<head>
<title>LEGAL MANAGEMENT SYSTEM</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../styles/default.css" rel="stylesheet" type="text/css">

<style type="text/css">
<!--
.style1 {
	color: #FF0000;
	font-weight: bold;
}
-->
</style>
</head>
<body>
<form action="" name="clientslist" method="post">
<fieldset>
<table width="100%" border="0" cellpadding="4" cellspacing="0" >

  <tr valign="baseline"  class="inputdef">
  <td width="37%"   class="inputdef" style="font-weight: bold" valign="top">  Enter Client Name or Client No:&nbsp;
      
    <input size="20" maxlength="20" name="val" ID="val"  onFocus="this.style.backgroundColor='#ffff00'"  onBlur="this.style.backgroundColor='#F2FAFF'"  class='forms'    value="<?php echo $_SESSION['val'];?>" >&nbsp;</td>
<td width="63%" class="inputdef"> <input type="submit" name="submit" id="submit" value="Execute"  class="formsblue" align="center" ></td>
</tr>
  </table>
  <table width="100%" border="0" cellpadding="4" cellspacing="0" >
  <tr>
  <td width="26%"   class="inputdef" style="font-weight: bold">Job Ref </td>
  <td width="20%"   class="inputdef" style="font-weight: bold">Client Name</td>
   <td width="15%"   class="inputdef" style="font-weight: bold">Subject</td>
   <td width="14%"   class="inputdef" style="font-weight: bold"> Adverse Party</td>
    <td width="25%"   class="inputdef" style="font-weight: bold"> Status</td>
    </tr>
  <?php if ($totalRows_clients > 0) { ?>
  <?php do {
  
 
  $selectf= "SELECT clients.*,specificfile.filename,specificfile.id AS fid,specificfile.dateopened,units.name as Un ,status.name as s,priority.name as p,projects.aparty,projects.details
 FROM clients
 INNER JOIN specificfile ON clients.clientno=specificfile.client
 INNER JOIN projects ON specificfile.filename=projects.fileno
 INNER JOIN units ON projects.unit=units.id
  INNER JOIN status ON projects.status=status.id
 INNER JOIN priority ON projects.priority=priority.id
 AND projects.unit='".$row_clients['unit']."'
  ".$_SESSION['query']."
 ORDER BY specificfile.dateopened DESC ";
//echo  $selectf ;
  
  $sql_f = mysqli_query($eProc,$selectf) or die(mysqli_error($eProc));
$row_f = mysqli_fetch_assoc($sql_f );
  
  
  $jb=$row_f['filename'].'/'.$row_f['Un'];
   ?>
  <tr>
   <td ><a href="newpsummary.php?jid=<?php echo $row_f['filename']?>&unit=<?php echo $row_clients['unit'] ?>"><?php echo $jb;?></a></td>
   <td ><?php echo $row_f['fulnames'] ;?> </td>
  <td > <?php echo $row_f['details'] ;?></td>
  <td ><?php echo $row_f['aparty'] ;?> </td>
   <td ><?php echo $row_f['s'] ;?> </td>
    </tr>
  <?php } while ($row_clients = mysqli_fetch_assoc($clients)); ?>
  <tr>
    <td colspan="5" class="mainbase"><table width="97%"  border="0" cellspacing="0" cellpadding="3">
      <tr align="center">
        <td width="45%">
          <table width="78%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
              <td width="23%" align="center"><?php if ($pageNum_clients > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_clients=%d%s", $currentPage, 0, $queryString_clients); ?>">First</a>
                <?php } // Show if not first page ?>              </td>
              <td width="31%" align="center"><?php if ($pageNum_clients > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_clients=%d%s", $currentPage, max(0, $pageNum_clients - 1), $queryString_clients); ?>">Previous</a>
                <?php } // Show if not first page ?>              </td>
              <td width="23%" align="center"><?php if ($pageNum_clients < $totalPages_clients) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_clients=%d%s", $currentPage, min($totalPages_clients, $pageNum_clients + 1), $queryString_clients); ?>">Next</a>
                <?php } // Show if not last page ?>              </td>
              <td width="23%" align="center"><?php if ($pageNum_clients < $totalPages_clients) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_clients=%d%s", $currentPage, $totalPages_clients, $queryString_clients); ?>">last</a>
                <?php } // Show if not last page ?>              </td>
            </tr>
          </table></td>
        <td width="55%">&nbsp; Showing <strong><?php echo ($startRow_clients + 1) ?></strong> to <strong><?php echo min($startRow_clients + $maxRows_clients, $totalRows_clients) ?></strong> of <strong><?php echo $totalRows_clients ?></strong> </td>
      </tr>
    </table></td>
  </tr>
  <?php } else { ?>
  <tr>
    <td colspan="5" class="mainbase"><span class="style1">No files Created! </span></td>
  </tr>
  <?php } ?>
</table>
</fieldset>
</form>
</body>
</html>
<?php
//mysqli_free_result($clients);
?>

