
<?php

require('../fpdf17/fpdf.php');
require_once('../connections/eProc.php');
mysqli_select_db($eProc, $database_eProc);

//Select the Products you want to show in your PDF file
$invoice_id = $_GET['cid'];
//$sql_query = "select invoices.*, town.name, clients.pcode, clients.fulnames,clients.address, clients.town, tax.tax, bills.*"
//        . " from "
//        . "invoices inner join bills "
//        . "on invoices.id = bills.invoice_no "
//        . "inner join tax on bills.tax_id = tax.id "
//        . "inner join clients on "
//        . "invoices.client_id = clients.id "
//        . "inner join town on clients.town = town.id "
//        . "where invoices.id = $invoice_id "
//        . " order by invoices.created_at ASC";
//
//$result = mysqli_query($eProc, $sql_query) or die(mysqli_error($eProc));
//
//$sql_row = mysqli_num_rows($result);
//
//$row_file = mysqli_fetch_assoc($result);

$query_invoices = "select invoices.*, town.name, clients.pcode, clients.fulnames,clients.address, clients.town, tax.tax, bills.*"
        . " from "
        . "invoices inner join bills "
        . "on invoices.id = bills.invoice_no "
        . "inner join tax on bills.tax_id = tax.id "
        . "inner join clients on "
        . "invoices.client_id = clients.id "
        . "inner join town on clients.town = town.id "
        . "where invoices.id = $invoice_id "
        . " order by invoices.created_at ASC";
$result = mysqli_query($eProc, $query_invoices) or die(mysqli_error());

$invoice = "select invoices.*, town.name, clients.pcode, clients.fulnames,clients.address, clients.town, tax.tax, bills.*"
        . " from "
        . "invoices inner join bills "
        . "on invoices.id = bills.invoice_no "
        . "inner join tax on bills.tax_id = tax.id "
        . "inner join clients on "
        . "invoices.client_id = clients.id "
        . "inner join town on clients.town = town.id "
        . "where invoices.id = $invoice_id "
        . " order by invoices.created_at ASC";
$invoice_result = mysqli_query($eProc, $invoice) or die(mysql_error());
$row_file = mysqli_fetch_assoc($invoice_result);



//Initialize the 3 columns and the total
$invoice_id = "";
$service_name = "";
$service_amount = '';
$total_amount = "";
$tax_tax = "";

//For each row, add the field to the corresponding column



while ($row = mysqli_fetch_array($result)) {
    $id = $row["id"];
    $name = substr($row["service_name"], 0, 20);
    $amount = $row["amount"];
    $t_amount = $row["total_amount"];
    $tax = $row['tax'];

    $invoice_id = $invoice_id . $id . "\n";
    $service_name = $service_name . $name . "\n";
    $service_amount = $service_amount . number_format($amount) . "\n";
    $total_amount = $total_amount . number_format($t_amount) . "\n";
    $tax_tax = $tax_tax . $tax . "%\n";
}
mysqli_close($eProc);

$date = date('F d, Y h:mA', strtotime($row_file['created_at']));

class PDF extends FPDF {

// Page header
    function Header() {
        // Logo
        $this->Image('../img/hkm.jpg', 10, 6, 30);
        // Arial bold 15
        $this->SetFont('Arial', 'B', 15);
        // Move to the right
        $this->Cell(80);
        // Title
        $this->Cell(30, 10, 'HKM Associates, Advocates', 0, 0, 'C');
        // Line break
        $this->Ln(20);
    }

// Page footer
    function Footer() {
        // Position at 1.5 cm from bottom
        $this->SetY(-15);
        // Arial italic 8
        $this->SetFont('Arial', 'I', 8);
        // Page number
        $this->Cell(0, 10, 'Page ' . $this->PageNo() . '/{nb}', 0, 0, 'C');
    }

}



// Instanciation of inherited class
$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();


$pdf->SetFillColor(232, 232, 232);
$pdf->Cell(100, 20, 'Invoice #' . $row_file['id']);
$pdf->Ln(10);
$pdf->Cell(100, 20, 'Invoice Date:' . $date);
//$pdf->Ln(10);
//$pdf->Cell(100, 20, 'Invoice Due Date:' . $row_file['due_date']);

$pdf->Ln();
$pdf->Cell(100, 20, "Invoiced to:");
$pdf->Ln(10);
$pdf->Cell(100, 20, $row_file['fulnames']);
$pdf->Ln(5);
$pdf->Cell(100, 20, 'P.O Box ' . $row_file['address'] . ' - ' . $row_file['pcode']);
$pdf->Ln(5);
$pdf->Cell(100, 20, $row_file['name']);


//Fields Name position
$Y_Fields_Name_position = 130;
//Table position, under Fields Name
$Y_Table_Position = 136;

//First create each Field Name
//Gray color filling each Field Name box
$pdf->SetFillColor(232, 232, 232);
//Bold Font for Field Name
$pdf->SetFont('Arial', 'B', 12);
$pdf->SetY($Y_Fields_Name_position);
$pdf->SetX(20);
$pdf->Cell(50, 6, 'Service Name', 1, 0, 'L', 1);
$pdf->SetX(70);
$pdf->Cell(30, 6, 'Amount', 1, 0, 'R', 1);
$pdf->Cell(30, 6, 'Tax', 1, 0, 'R', 1);
$pdf->Cell(50, 6, 'Total Amount', 1, 0, 'R', 1);
$pdf->Ln();

//Now show the 3 columns
$pdf->SetFont('Arial', '', 12);
$pdf->SetY($Y_Table_Position);
$pdf->SetX(20);
$pdf->MultiCell(50, 6, $service_name, 1);
$pdf->SetY($Y_Table_Position);
$pdf->SetX(70);
$pdf->MultiCell(30, 6, $service_amount, 1, 'R');
$pdf->SetY($Y_Table_Position);
$pdf->SetX(100);
$pdf->MultiCell(30, 6, $tax_tax, 1, 'R');
$pdf->SetY($Y_Table_Position);
$pdf->SetX(130);
$pdf->MultiCell(50, 6, $total_amount, 1, 'R');

$pdf->SetFillColor(232, 232, 232);
$pdf->SetX(120);
$pdf->Cell(100, 20, 'Invoice Amount KES ' . number_format($row_file['invoice_amount']));

$pdf->SetFont('Times', '', 12);
$pdf->Output();
