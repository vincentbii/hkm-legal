<?php require_once('../connections/eProc.php'); ?>

<?php

$currentPage = $_SERVER["PHP_SELF"];

$maxRows_jobtype = 30;
$pageNum_jobtype = 0;


  unset($_SESSION['itype']);


if (isset($_GET['pageNum_jobtype'])) {
  $pageNum_jobtype = $_GET['pageNum_jobtype'];
}
$startRow_jobtype = $pageNum_jobtype * $maxRows_jobtype;

mysqli_select_db($eProc, $database_eProc);
$query_jobtype = "SELECT * FROM jobtype   ";

$query_limit_jobtype = sprintf("%s LIMIT %d, %d", $query_jobtype, $startRow_jobtype, $maxRows_jobtype);
$jobtype = mysqli_query($eProc, $query_limit_jobtype) or die(mysqli_error());
$row_jobtype = mysqli_fetch_assoc($jobtype);

if (isset($_GET['totalRows_jobtype'])) {
  $totalRows_jobtype = $_GET['totalRows_jobtype'];
} else {
  $all_jobtype = mysqli_query($eProc, $query_jobtype);
  $totalRows_jobtype = mysqli_num_rows($all_jobtype);
}
$totalPages_jobtype = ceil($totalRows_jobtype/$maxRows_jobtype)-1;

$queryString_jobtype = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_jobtype") == false && 
        stristr($param, "totalRows_jobtype") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_jobtype = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_jobtype = sprintf("&totalRows_jobtype=%d%s", $totalRows_jobtype, $queryString_jobtype);
?>

<html>
<head>
<title>LEGAL MANAGEMENT SYSTEM</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../styles/default.css" rel="stylesheet" type="text/css">

<style type="text/css">
<!--
.style1 {
	color: #FF0000;
	font-weight: bold;
}
-->
</style>
</head>
<body>
<fieldset>
<table width="100%" border="0" cellpadding="4" cellspacing="0" >

  <tr>
  <td width="26%"   class="inputdef" style="font-weight: bold">jobtype Name</td>
  <td width="41%"   class="inputdef" style="font-weight: bold" ><div align="right">
	  <table width="154"  border="0" cellspacing="0" cellpadding="3">
	    <tr class="inputdef">
	      <td width="19"  ><img src="../images/icons/newproject.gif" width="15" height="13" border="0" /></td>
                
			<td width="123"    ><a href="newjobtype.php">New jobtype</a></td>
            </tr>
	    </table>
	  </div></td>
  </tr>
  <?php if ($totalRows_jobtype > 0) { ?>
  <?php do { ?>
  <tr>
   <td > <?php echo $row_jobtype['name']?> </td>
   <td ><a href="newjobtype.php?id=<?php echo $row_jobtype['id'] ?>">Edit</a></td>
  </tr>
  <?php } while ($row_jobtype = mysqli_fetch_assoc($jobtype)); ?>
  <tr>
    <td colspan="4" class="mainbase"><table width="97%"  border="0" cellspacing="0" cellpadding="3">
      <tr align="center">
        <td width="45%">
          <table width="78%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
              <td width="23%" align="center"><?php if ($pageNum_jobtype > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_jobtype=%d%s", $currentPage, 0, $queryString_jobtype); ?>">First</a>
                <?php } // Show if not first page ?>              </td>
              <td width="31%" align="center"><?php if ($pageNum_jobtype > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_jobtype=%d%s", $currentPage, max(0, $pageNum_jobtype - 1), $queryString_jobtype); ?>">Previous</a>
                <?php } // Show if not first page ?>              </td>
              <td width="23%" align="center"><?php if ($pageNum_jobtype < $totalPages_jobtype) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_jobtype=%d%s", $currentPage, min($totalPages_jobtype, $pageNum_jobtype + 1), $queryString_jobtype); ?>">Next</a>
                <?php } // Show if not last page ?>              </td>
              <td width="23%" align="center"><?php if ($pageNum_jobtype < $totalPages_jobtype) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_jobtype=%d%s", $currentPage, $totalPages_jobtype, $queryString_jobtype); ?>">last</a>
                <?php } // Show if not last page ?>              </td>
            </tr>
          </table></td>
        <td width="55%">&nbsp; Showing <strong><?php echo ($startRow_jobtype + 1) ?></strong> to <strong><?php echo min($startRow_jobtype + $maxRows_jobtype, $totalRows_jobtype) ?></strong> of <strong><?php echo $totalRows_jobtype ?></strong> </td>
      </tr>
    </table></td>
  </tr>
  <?php } else { ?>
  <tr>
    <td colspan="4" class="mainbase"><span class="style1">No jobtype Created! </span></td>
  </tr>
  <?php } ?>
</table>
</fieldset>
</body>
</html>
<?php
mysqli_free_result($jobtype);
?>

