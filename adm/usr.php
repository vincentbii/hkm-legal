<?php require_once('../connections/eProc.php'); ?>
<?php include('../activelog.php'); ?>
<?php

$currentPage = $_SERVER["PHP_SELF"];

$maxRows_usr = 999999;
$pageNum_usr = 0;
if (isset($_GET['pageNum_usr'])) {
  $pageNum_usr = $_GET['pageNum_usr'];
}
$startRow_usr = $pageNum_usr * $maxRows_usr;
unset($_SESSION['disable']);
mysqli_select_db($eProc, $database_eProc);
/* HANDLE SELECTED department, IF SELECTED
---------------------------------------------------*/
if (isset($_POST['group']) && $_POST['group']!="All") {
	$where=" groupid = '".$_POST['group']."'";
}

if ($where!="") {
	$where=$where." AND  com_id = '".$_SESSION['company']."'";
} else {
	$where=" com_id = '".$_SESSION['company']."'";
}


//$query_usr = "SELECT * FROM users WHERE com_id = '".$_SESSION['company']."' AND  admin = 0 ORDER BY fulname ASC";
//if (isset($_POST['DPT']) && $_POST['DPT'] != 'All') {
	$query_usr = "SELECT * FROM users WHERE ".$where." AND status = 'A' ORDER BY username ASC";
//}
// ---------------------------------------------------------------------------------------------------------------------
//echo $query_usr;
$query_limit_usr = sprintf("%s LIMIT %d, %d", $query_usr, $startRow_usr, $maxRows_usr);
$usr = mysqli_query($eProc,$query_limit_usr) or die(mysqli_error($eProc));
$row_usr = mysqli_fetch_assoc($usr);

if (isset($_GET['totalRows_usr'])) {
  $totalRows_usr = $_GET['totalRows_usr'];
} else {
  $all_usr = mysqli_query($eProc,$query_usr);
  $totalRows_usr = mysqli_num_rows($all_usr);
}
$totalPages_usr = ceil($totalRows_usr/$maxRows_usr)-1;

$queryString_usr = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_usr") == false && 
        stristr($param, "totalRows_usr") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_usr = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_usr = sprintf("&totalRows_usr=%d%s", $totalRows_usr, $queryString_usr);



?>
<link href="../styles/default.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.style1 {font-weight: bold}
.style2 {
	color: #FF0000;
	font-weight: bold;
}
.style3 {
	color: #009900;
	font-weight: bold;
}
-->
</style>

<script language="JavaScript" type="text/JavaScript">
<!--
function submitform() {
  document.form1.submit();
}
//-->
</script>
<style type="text/css">
<!--
.style1 {
	color: #009933;
	font-weight: bold;
}
-->
</style>

<table width="100%"  border="0" cellspacing="0" cellpadding="4">
  <tr><form method="post" name="form1">
    <td width="37%" valign="middle" class="baseline">&nbsp;</td>
    <td width="38%" valign="middle" class="baseline"><span class="style1">Group:</span>
		<select name="group" class="forms" id="group" onchange="submitform()">
			<option >All</option>
			<?php showGroups($_POST['group'], FALSE,"1") ?>
		</select>    </td>
    <td width="25%" align="right" valign="middle" class="baseline">
      <a href="usr.php">View All</a>   </td>
  </form>
  </tr>
  <tr>
    <td colspan="3"><table width="100%" border="0" cellpadding="4" cellspacing="0" >
      <tr>
        <td width="16%" class="inputdef" style="font-weight: bold">User Names </td>
        <td width="16%" class="inputdef" style="font-weight: bold">Department</td>
        <td width="16%" class="inputdef"><strong>Email</strong></td>
        <td width="16%" class="inputdef" style="font-weight: bold">Group</td>
        
        </tr>
      <?php if ($totalRows_usr > 0) { ?>
      <?php do {
	  
	   $selectstud = "SELECT employee.*,department.name AS dept FROM employee INNER JOIN department ON employee.department=department.id   WHERE employee.id='".$row_usr['employee']."'";
 //echo $selectstud;
	$sql_stud = mysqli_query($eProc,$selectstud) or die(mysqli_error($eProc));
	$row_stud = mysqli_fetch_assoc($sql_stud );
	$studnum=mysqli_num_rows($sql_stud);
	  
	   
	  if ($row_usr['groupid']=='1') { 
	  ?>
      <tr valign="top">
        <td ><strong><?php echo $row_usr['username']." (".$row_stud['name'].")"; ?></strong><br /></td>
         <td ><?php echo $row_stud['dept']; ?></td>
        <td ><a href="mailto:<?php echo $row_stud['email']; ?>"><?php echo $row_stud['email']; ?></a></td>
        <td ><?php echo getGroup($row_usr['groupid']); ?></td>
        </tr>
		
		<?php } else { 
		
		
		?>
		<tr valign="top">
        <td ><strong><a href="edituser.php?uid=<?php echo $row_usr['uid']; ?>"><?php echo $row_usr['username']." (".$row_stud['name'].")"; ?></a></strong><br /></td>
        <td ><?php echo $row_stud['dept']; ?></td>
        <td ><a href="mailto:<?php echo $row_stud['email']; ?>"><?php echo $row_stud['email']; ?></a>
          <table width="100%" border="0" align="center" cellpadding="2" cellspacing="0">
            <tr>
              <td width="23%" align="center"><?php if ($pageNum_usr > 0) { // Show if not first page ?>
                  <a href="<?php printf("%s?pageNum_usr=%d%s", $currentPage, 0, $queryString_usr); ?>">First</a>
                  <?php } // Show if not first page ?>              </td>
              <td width="31%" align="center"><?php if ($pageNum_usr > 0) { // Show if not first page ?>
                  <a href="<?php printf("%s?pageNum_usr=%d%s", $currentPage, max(0, $pageNum_usr - 1), $queryString_usr); ?>">Previous</a>
                  <?php } // Show if not first page ?>              </td>
              <td width="23%" align="center"><?php if ($pageNum_usr < $totalPages_usr) { // Show if not last page ?>
                  <a href="<?php printf("%s?pageNum_usr=%d%s", $currentPage, min($totalPages_usr, $pageNum_usr + 1), $queryString_usr); ?>">Next</a>
                  <?php } // Show if not last page ?>              </td>
              <td width="23%" align="center"><?php if ($pageNum_usr < $totalPages_usr) { // Show if not last page ?>
                  <a href="<?php printf("%s?pageNum_usr=%d%s", $currentPage, $totalPages_usr, $queryString_usr); ?>">last</a>
                  <?php } // Show if not last page ?>              </td>
            </tr>
          </table></td>
        <td ><?php echo getGroup($row_usr['groupid']); ?></td>
        </tr>
		
      <?php } } while ($row_usr = mysqli_fetch_assoc($usr)); ?>
      <tr>
        <td colspan="6" class="mainbase"><table width="100%"  border="0" cellspacing="0" cellpadding="3">
            <tr align="center">
              <td width="50%">&nbsp;</td>
              <td width="50%" align="right">&nbsp; Showing <strong><?php echo ($startRow_usr + 1) ?></strong> to <strong><?php echo min($startRow_usr + $maxRows_usr, $totalRows_usr) ?></strong> of <strong><?php echo $totalRows_usr ?></strong> </td>
            </tr>
        </table></td>
      </tr>
      <?php } else { ?>
      <tr>
        <td colspan="6" class="mainbase"><span class="style2">NO USERS IN THIS GROUP! </span></td>
      </tr>
      <?php } ?>
    </table></td>
  </tr>
</table>

<?php
mysqli_free_result($usr);
?>
