<?php

require_once('../connections/eProc.php');
include('../activelog.php');
mysqli_select_db($eProc, $database_eProc);

$invoice_id = $_GET['iid'];

$query_bills = "SELECT tax.tax, bills.*, clients.fulnames FROM bills "
        . "inner join clients"
        . " on bills.client_id = clients.id "
        . "inner join tax "
        . "on bills.tax_id = tax.id"
        . " WHERE bills.invoice_no = $invoice_id ORDER BY id ASC ";
$bills = mysqli_query($eProc, $query_bills) or die(mysqli_error($eProc));
$row_bills = mysqli_fetch_assoc($bills);
$totalRows_bills = mysqli_num_rows($bills);

$query_invoice = "SELECT invoices.*, clients.fulnames FROM invoices "
        . "inner join clients"
        . " on invoices.client_id = clients.id"
        . " WHERE invoices.id = $invoice_id ORDER BY id ASC ";
$invoice = mysqli_query($eProc, $query_invoice) or die(mysqli_error($eProc));
$row_invoices = mysqli_fetch_assoc($invoice);

?>

<html>
    <head>
        <title>LEGAL MANAGEMENT SYSTEM</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="../styles/default.css" rel="stylesheet" type="text/css">
        <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="../font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css">

        <style type="text/css">
            <!--
            .style1 {
                color: #FF0000;
                font-weight: bold;
            }
            -->
        </style>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-offset-1">
                    <table class="" width="70%" border="0" cellspacing="0" cellpadding="4">
                        <tr>
                            <td>Invoice NO:</td>
                            <td><?php echo $row_invoices['id']; ?></td>
                        </tr>
                        <tr>
                            <td>Invoice Date:</td>
                            <td><?php echo $row_invoices['created_at']; ?></td>
                        </tr>
                        <tr>
                            <td>Client:</td>
                            <td><?php echo $row_invoices['fulnames']; ?></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <table  class="table table-responsive">
                                    <tr class="tableheader">
                                        <td>Service</td>
                                        <td>Unit Price</td>
                                        <td>Tax</td>
                                        <td>Total Price</td>
                                    </tr>
                                    <?php
                    if ($totalRows_bills !== NULL) {
                       while ($row = $bills->fetch_assoc()) {
                            ?>
                            <tr>
                                <td><?php echo $row['service_name']; ?></td>
                                <td><?php echo 'KES ' . number_format($row['amount']); ?></td>
                                <td><?php echo $row['tax'].'%'; ?></td>
                                <td><?php echo 'KES ' . number_format($row['total_amount']); ?></td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <table width="100%" class="table table-condensed">
                                    <tr>
                                        <td>Total Invoice Amount</td>
                                        <td>Amount Paid</td>
                                        <td>Amount Due</td>
                                    </tr>
                                    <tr>
                                        <td><?php echo 'KES '.number_format($row_invoices['invoice_amount']); ?></td>
                                        <td><?php echo 'KES '.number_format($row_invoices['amount_paid']); ?></td>
                                        <td><?php echo 'KES '.number_format($row_invoices['amount_due']); ?></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        
                    </table>
                </div>
            </div>
        </div>
    </body>
</html>