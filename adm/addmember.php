<?php require_once('../connections/eProc.php'); 
include('../activelog.php');
// ----------------------------------------------------------------------
$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "createcustomer")) {
  	 mysqli_select_db($eProc, $database_eProc);
	// Generate Necessary Variables
	// -----------------------------------------------------------------
	$selectSQL = "SELECT MAX(id) as id FROM  members"; 
	$result = mysqli_query($eProc, $selectSQL);
	$prq_id = mysqli_fetch_assoc($result);
	if (mysqli_num_rows($result)>0) 
		{
			$id = intval($prq_id['id'])++; 
			$id= str_pad($id,5,0,STR_PAD_LEFT);
		} else { 
			$id= '00001';
		} 
	
	$com_id = strtoupper(substr($customers['customer'],0,3)).$id; // Create authcode
	// -----------------------------------------------------------------
	  
  $insertSQL = sprintf("INSERT INTO members(com_id, customer, address, telphone, faxphone, mobphone, email, location, contactperson,customercode) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s,%s)",
                       GetSQLValueString($com_id, "text"),
                       GetSQLValueString($_POST['customer'], "text"),
                       GetSQLValueString($_POST['address'], "text"),
                       GetSQLValueString($_POST['telphone'], "text"),
                       GetSQLValueString($_POST['faxphone'], "text"),
                       GetSQLValueString($_POST['mobphone'], "text"),
                       GetSQLValueString($_POST['email'], "text"),
                       GetSQLValueString($_POST['location'], "text"),
					   GetSQLValueString($_POST['contactperson'], "text"),
					   GetSQLValueString($_POST['customercode'], "text"));

 
  $Result1 = mysqli_query($eProc, $insertSQL) or die("ERROR ADDING CUSTOMER INFORMATION - ".mysqli_error($eProc));
  
  // Generate The System customer Folder - Used to store customer specific data
  // ------------------------------------------------------------------------
  //$path = "../eadtmp/".$com_id;
 // mkdir($path, 700);
  // ------------------------------------------------------------------------

  $insertGoTo = "customerlist.php";
  ?>
	  <script language="JavaScript" type="text/javascript">
			location='<?php echo $insertGoTo  ?>';
	</script>
	<?php
}
?>
<html>
<head>
<title>BMA  MANAGEMENT SYSTEM</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../styles/default.css" rel="stylesheet" type="text/css">
</head>
<body>
<table width="100%"  border="2" cellpadding="0" cellspacing="3" bordercolor="#0A6EC3" bgcolor="#FFFFFF">
  <tr>
    <td valign="top"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td valign="top"><?php include('../includes/header.php'); ?></td>
      </tr>
      <tr>
          <td valign="top"><img src="../images/template_06.gif" width="100%" height="6"></td>
      </tr>
      <tr>
        <td valign="top"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="100%" class="spacer5"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td><?php //include("../includes/cadmstrip.php"); ?></td>
              </tr>
              <tr>
                <td colspan="5" class="intspace">
				<table width="100%"  border="0" cellspacing="0" cellpadding="4">
                    <tr valign="top">
                      <td width="170"><?php include(getusermenu()); ?></td> 
                      <td><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                          <tr valign="middle">
                            <td width="63%" class="hOne">New Customer</td>
                            <td width="37%" align="right" nowrap class="baseline white">last Updated: </td> 
                          </tr>
                        </table>
                          <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td valign="top" class="baseline">Add a member using the form below.</td>
                            </tr>
                            <tr>
                              <td valign="top" class="spacer5">
                                <form action="<?php echo $editFormAction; ?>" method="POST" name="createcustomer" id="createcustomer">
                                  <fieldset>
                                  <legend>Customer Details</legend>
                                  <table width="350" border="0" cellspacing="0" cellpadding="5">
                                    <tr valign="baseline">
                                      <td align="right" nowrap>Customer:</td>
                                      <td width="271"><input name="customer" type="text" class="forms" size="50"></td>
                                    </tr>
                                    <tr valign="baseline">
                                      <td align="right" nowrap>Customer code: </td>
                                      <td><input name="customercode" id="customercode" type="text" class="forms" size="50"></td>
                                    </tr>
                                    <tr valign="baseline">
                                      <td nowrap align="right" valign="top">Address:</td>
                                      <td><textarea name="address" cols="50" rows="4" class="forms"></textarea>                                      </td>
                                    </tr>
                                  </table>
                                  </fieldset>
                                  <br>
                                  <fieldset>
                                  <legend>Customer Contacts</legend>
                                  <table width="350" border="0" cellspacing="0" cellpadding="5">
                                    <tr valign="baseline">
                                      <td nowrap align="right">Contact Person:</td>
                                      <td><input name="contactperson" type="text" class="forms" id="contactperson" size="50"></td>
                                    </tr>
                                    <tr valign="baseline">
                                      <td nowrap align="right">Telephone:</td>
                                      <td><input name="telphone" type="text" class="forms" size="32"></td>
                                    </tr>
                                    <tr valign="baseline">
                                      <td nowrap align="right">Fax:</td>
                                      <td><input name="faxphone" type="text" class="forms" size="32"></td>
                                    </tr>
                                    <tr valign="baseline">
                                      <td nowrap align="right">Mobile:</td>
                                      <td><input name="mobphone" type="text" class="forms" size="32"></td>
                                    </tr>
                                    <tr valign="baseline">
                                      <td nowrap align="right">Email:</td>
                                      <td><input name="email" type="text" class="forms" size="50"></td>
                                    </tr>
                                  </table>
                                  </fieldset>
                                  <br>
                                  <fieldset>
                                  <legend>Customer Contacts</legend>
                                  <table width="350" cellpadding="5" cellspacing="0">
                                    <tr valign="baseline">
                                      <td width="57" align="right" valign="top" nowrap>Location:</td>
                                      <td width="271"><textarea name="location" cols="50" rows="4" class="forms"></textarea>
                                      </td>
                                    </tr>
                                    <tr valign="baseline">
                                      <td colspan="2" align="right" nowrap><hr size="1"></td>
                                    </tr>
                                    <tr align="center" valign="baseline">
                                      <td colspan="2" nowrap><input name="MM_insert" type="hidden" id="MM_insert" value="createcustomer">
                                          <input type="submit" class="formsBlue" value="Create New Customer">
                                          <input type="button" class="formsorg" value="Cancel" onClick="javascript:location='customerlist.php'"></td>
                                    </tr>
                                  </table>
                                  </fieldset>
                                </form></td>
                            </tr>
                        </table></td>
                    </tr>
                </table></td>
              </tr>
            </table></td>
            </tr>
        </table></td>
      </tr>
      <tr>
        <td ><?php include('../includes/footer.php'); ?></td>
      </tr>
    </table></td>
  </tr>
</table>
</body>
</html>