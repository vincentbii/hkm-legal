<?php require_once('../connections/eProc.php'); ?>
<?php include('../activelog.php'); ?>

<html>
<head>
<title>Management Information System </title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../styles/default.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../js/funcs.js"></script>

<style type="text/css">
<!--
#Layer1 {
	position:absolute;
	width:451px;
	height:115px;
	z-index:1;
	left: 6px;
	top: 88px;
}
-->
</style>
</head>
<body onLoad="initialize(dp, ut);">

<?php
$gr=$_POST['grpdet'];
$contdet=$_POST['contactdet'];
$secdet=$_POST['secdet'];

if (isset($_POST['authlimit'])) {
	$Alimit=$_POST['authlimit'];
} else {
	$Alimit=0;
}


$Group=$_POST['groupid'];
$costcentercode=$_POST['costcentercode'];
$names=$_POST['fulname'];
$worktel=$_POST['worktel'];
$hometel=$_POST['hometel'];
$mobtel=$_POST['mobtel'];
$emailaddr=$_POST['email'];
$uname=$_POST['username'];
$password=$_POST['umd5'];
$secdet=$_POST['CUMD5'];


// Generate Necessary Variables
// -----------------------------------------------------------------
$MD5 = md5($password); // md5 encrypted password
$authcode = createHASH($names); // Hashed authcode
// -----------------------------------------------------------------

//Check if user exists for this company
if (userExists($names)) {
	?>
	<SCRIPT LANGUAGE=JAVASCRIPT>
	{
	    alert ("The 'User name' you entered already exists. Enter a unique username");
	}
	</SCRIPT>
	<?php
} Else {
	
	 $insertSQL = sprintf("INSERT INTO users (fulname, username, umd5, worktel, hometel, mobtel, email, authlimit, uregdate, authcode, groupid, com_id, costcentercode) VALUES (%s, %s, %s, %s, %s, %s, %s,  %s, %s, %s, %s, %s, %s)",
                       GetSQLValueString($names, "text"),
                       GetSQLValueString($uname, "text"),
                       GetSQLValueString($MD5, "text"),
                       GetSQLValueString($worktel, "text"),
                       GetSQLValueString($hometel, "text"),
                       GetSQLValueString($mobtel, "text"),
                       GetSQLValueString($emailaddr, "text"),
					   GetSQLValueString($Alimit, "float"), 
                       GetSQLValueString(time(), "int"),
                       GetSQLValueString($authcode, "text"),
                       GetSQLValueString($Group, "text"),
					   GetSQLValueString($_SESSION['company'], "text"),
					   GetSQLValueString($costcentercode, "text"));

  mysqli_select_db($eProc, $database_eProc);
  $Result1 = mysqli_query($eProc, $insertSQL) or die('ERROR CREATING USER: '.mysqli_error($eProc));
  
  //Save catalog access details
   $arrCat = $_REQUEST['Catalogs']; //Get selected catalogs
   //echo  $uname.",".$costcentercode;
   $uid=getlastuser($uname,$costcentercode);
   //echo  "---".$uid."----";
   //echo $uid;
   	

   //echo implode(",", $arrCat);

  $insertGoTo = "usr.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
    $insertGoTo .= $_SERVER['QUERY_STRING'];
  }

}

?>
<input name="button" type="button" class="formsGrey" onClick="javascript:location='usr.php'" value="Done">
</body>
</html>