<?php require_once('../connections/eProc.php'); 

 require_once('../activelog.php'); ?>
<?php
$currentPage = $_SERVER["PHP_SELF"];

$maxRows_doc = 30;
$pageNum_doc = 0;
if (isset($_GET['pageNum_doc'])) {
  $pageNum_doc = $_GET['pageNum_doc'];
}
$startRow_doc = $pageNum_doc * $maxRows_doc;

mysqli_select_db($eProc, $database_eProc);
$query_doc = "SELECT * FROM doc ";
$query_limit_doc = sprintf("%s LIMIT %d, %d", $query_doc, $startRow_doc, $maxRows_doc);
$doc = mysqli_query($eProc, $query_limit_doc) or die(mysqli_error());
$row_doc = mysqli_fetch_assoc($doc);

if (isset($_GET['totalRows_doc'])) {
  $totalRows_doc = $_GET['totalRows_doc'];
} else {
  $all_doc = mysqli_query($eProc, $query_doc);
  $totalRows_doc = mysqli_num_rows($all_doc);
}
$totalPages_doc = ceil($totalRows_doc/$maxRows_doc)-1;

$queryString_doc = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_doc") == false && 
        stristr($param, "totalRows_doc") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_doc = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_doc = sprintf("&totalRows_doc=%d%s", $totalRows_doc, $queryString_doc);
?>

<html>
<head>
<title>LEGAL  MANAGEMENT INFORMATION  SYSTEM</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../styles/default.css" rel="stylesheet" type="text/css">

<style type="text/css">
<!--
.style1 {
	color: #FF0000;
	font-weight: bold;
}
-->
</style>
</head>
<body>

<table width="100%" border="0" cellpadding="4" cellspacing="0" >

  <tr>
   <td width="36%"   class="inputdeft" style="font-weight: bold">Document Type</td>
   
	<td width="64%"   class="inputdeft" style="font-weight: bold" ><div align="right">
	  <table width="154"  border="0" cellspacing="0" cellpadding="3">
	    <tr class="inputdeft">
	      <td width="19"  ><img src="../images/icons/newproject.gif" width="15" height="13" border="0" /></td>
                
			<td width="123"   ><a href="../adm/newdoc.php">New Document </a></td>
            </tr>
	    </table>
	  </div></td>
  </tr>
  <?php if ($totalRows_doc > 0) { ?>
  <?php do { ?>
  <tr>
  <td ><?php echo $row_doc['name']?> </td>
  
  <td ><a href="../adm/newdoc.php?id=<?php echo $row_doc['id'] ?>">Edit</a></td>
  </tr>
  <?php } while ($row_doc = mysqli_fetch_assoc($doc)); ?>
  <tr>
    <td colspan="12" class="mainbase"><table width="97%"  border="0" cellspacing="0" cellpadding="3">
      <tr align="center">
        <td width="45%">
          <table width="78%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
              <td width="23%" align="center"><?php if ($pageNum_doc > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_doc=%d%s", $currentPage, 0, $queryString_doc); ?>">First</a>
                <?php } // Show if not first page ?>              </td>
              <td width="31%" align="center"><?php if ($pageNum_doc > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_doc=%d%s", $currentPage, max(0, $pageNum_doc - 1), $queryString_doc); ?>">Previous</a>
                <?php } // Show if not first page ?>              </td>
              <td width="23%" align="center"><?php if ($pageNum_doc < $totalPages_doc) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_doc=%d%s", $currentPage, min($totalPages_doc, $pageNum_doc + 1), $queryString_doc); ?>">Next</a>
                <?php } // Show if not last page ?>              </td>
              <td width="23%" align="center"><?php if ($pageNum_doc < $totalPages_doc) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_doc=%d%s", $currentPage, $totalPages_doc, $queryString_doc); ?>">last</a>
                <?php } // Show if not last page ?>              </td>
            </tr>
          </table></td>
        <td width="55%">&nbsp; Showing <strong><?php echo ($startRow_doc + 1) ?></strong> to <strong><?php echo min($startRow_doc + $maxRows_doc, $totalRows_doc) ?></strong> of <strong><?php echo $totalRows_doc ?></strong> </td>
      </tr>
    </table></td>
  </tr>
  <?php } else { ?>
  <tr>
    <td colspan="12" class="mainbase"><span class="style1">No docs Created! </span></td>
  </tr>
  <?php } ?>
</table>
</body>
</html>
<?php
mysqli_free_result($doc);
?>

