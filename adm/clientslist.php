<?php
require_once('../connections/eProc.php');

unset($_SESSION['town']);
unset($_SESSION['maritalstatus']);
unset($_SESSION['gender']);
unset($_SESSION['status']);
unset($_SESSION['religion']);
unset($_SESSION['jobtype']);
unset($_SESSION['priority']);
unset($_SESSION['importance']);
unset($_SESSION['ctype']);
unset($_SESSION['dept']);
unset($_SESSION['companytype']);
unset($_SESSION['cid']);
unset($_SESSION['job']);
unset($_SESSION['caseno']);

unset($_SESSION['query']);
$_SESSION['client'] = null;
$_SESSION['val'] = null;
$query = '';


if (isset($_POST['submit'])) {

    if (isset($_POST['client']) && $_POST['client'] != '') {
        $_SESSION['client'] = $_POST['client'];
    }
    if (isset($_POST['client']) && $_POST['client'] == '') {
        unset($_SESSION['client']);
    }


    if (isset($_POST['val']) && $_POST['val'] != '') {
        $_SESSION['val'] = $_POST['val'];

        $query = " AND (fulnames LIKE '%" . $_SESSION['val'] . "%' OR clientno='" . intval($_SESSION['val']) . "') ";
    }
    if (isset($_POST['val']) && $_POST['val'] == '') {
        unset($_SESSION['val']);
    }

    $_SESSION['query'] = $query;
} else {
    $_SESSION['client'] = 'i';
    $_SESSION['val'] = 'i';
    $_SESSION['query'] = '';
}
?>

<?php
$currentPage = $_SERVER["PHP_SELF"];

$maxRows_clients = 20;
$pageNum_clients = 0;


unset($_SESSION['itype']);


if (isset($_GET['pageNum_clients'])) {
    $pageNum_clients = $_GET['pageNum_clients'];
}
$startRow_clients = $pageNum_clients * $maxRows_clients;

mysqli_select_db($eProc, $database_eProc);

if ($_SESSION['client'] == 'i') {
    $query_clients = "SELECT * FROM clients WHERE category='i' " . $_SESSION['query'] . "  ORDER BY clientno ASC ";
    $query_limit_clients = sprintf("%s LIMIT %d, %d", $query_clients, $startRow_clients, $maxRows_clients);
    $clients = mysqli_query($eProc, $query_limit_clients) or die(mysqli_error());
    $row_clients = mysqli_fetch_assoc($clients);

    if (isset($_GET['totalRows_clients'])) {
        $totalRows_clients = $_GET['totalRows_clients'];
    } else {
        $all_clients = mysqli_query($eProc, $query_clients);
        $totalRows_clients = mysqli_num_rows($all_clients);
    }
    $totalPages_clients = ceil($totalRows_clients / $maxRows_clients) - 1;

    $queryString_clients = "";
    if (!empty($_SERVER['QUERY_STRING'])) {
        $params = explode("&", $_SERVER['QUERY_STRING']);
        $newParams = array();
        foreach ($params as $param) {
            if (stristr($param, "pageNum_clients") == false &&
                    stristr($param, "totalRows_clients") == false) {
                array_push($newParams, $param);
            }
        }
        if (count($newParams) != 0) {
            $queryString_clients = "&" . htmlentities(implode("&", $newParams));
        }
    }
    $queryString_clients = sprintf("&totalRows_clients=%d%s", $totalRows_clients, $queryString_clients);
}
if ($_SESSION['client'] == 'c') {
    $query_clients = "SELECT * FROM clients WHERE category='c'  " . $_SESSION['query'] . " ORDER BY id ASC ";
    $query_limit_clients = sprintf("%s LIMIT %d, %d", $query_clients, $startRow_clients, $maxRows_clients);
    $clients = mysqli_query($eProc, $query_limit_clients) or die(mysqli_error());
    $row_clients = mysqli_fetch_assoc($clients);

    if (isset($_GET['totalRows_clients'])) {
        $totalRows_clients = $_GET['totalRows_clients'];
    } else {
        $all_clients = mysqli_query($eProc, $query_clients);
        $totalRows_clients = mysqli_num_rows($all_clients);
    }
    $totalPages_clients = ceil($totalRows_clients / $maxRows_clients) - 1;

    $queryString_clients = "";
    if (!empty($_SERVER['QUERY_STRING'])) {
        $params = explode("&", $_SERVER['QUERY_STRING']);
        $newParams = array();
        foreach ($params as $param) {
            if (stristr($param, "pageNum_clients") == false &&
                    stristr($param, "totalRows_clients") == false) {
                array_push($newParams, $param);
            }
        }
        if (count($newParams) != 0) {
            $queryString_clients = "&" . htmlentities(implode("&", $newParams));
        }
    }
    $queryString_clients = sprintf("&totalRows_clients=%d%s", $totalRows_clients, $queryString_clients);
}
if ($_SESSION['client'] == 'p') {
    $query_clients = "SELECT * FROM prospective WHERE category='p' " . $_SESSION['query'] . " ";
    $query_limit_clients = sprintf("%s LIMIT %d, %d", $query_clients, $startRow_clients, $maxRows_clients);
    $clients = mysqli_query($eProc, $query_limit_clients) or die(mysqli_error());
    $row_clients = mysqli_fetch_assoc($clients);

    if (isset($_GET['totalRows_clients'])) {
        $totalRows_clients = $_GET['totalRows_clients'];
    } else {
        $all_clients = mysqli_query($eProc, $query_clients);
        $totalRows_clients = mysqli_num_rows($all_clients);
    }
    $totalPages_clients = ceil($totalRows_clients / $maxRows_clients) - 1;

    $queryString_clients = "";
    if (!empty($_SERVER['QUERY_STRING'])) {
        $params = explode("&", $_SERVER['QUERY_STRING']);
        $newParams = array();
        foreach ($params as $param) {
            if (stristr($param, "pageNum_clients") == false &&
                    stristr($param, "totalRows_clients") == false) {
                array_push($newParams, $param);
            }
        }
        if (count($newParams) != 0) {
            $queryString_clients = "&" . htmlentities(implode("&", $newParams));
        }
    }
    $queryString_clients = sprintf("&totalRows_clients=%d%s", $totalRows_clients, $queryString_clients);
}
?>

<html>
    <head>
        <title>LEGAL MANAGEMENT SYSTEM</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="../styles/default.css" rel="stylesheet" type="text/css">
        <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">

        <style type="text/css">
            <!--
            .style1 {
                color: #FF0000;
                font-weight: bold;
            }
            -->
        </style>
    </head>
    <body>
        <form action="" name="clientslist" method="post">
            <table width="100%" >
                <tr>
                    <td>
                        <strong>Individual :</strong><input name="client" type="radio"   <?php if ($_SESSION['client'] == 'i') { ?> checked="checked"<?php } ?> value="i"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong> Company:</strong> <input name="client" type="radio"  <?php if ($_SESSION['client'] == 'c') { ?> checked="checked"<?php } ?> value="c">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Prospective :</strong><input name="client" type="radio"  <?php if ($_SESSION['client'] == 'p') { ?> checked="checked"<?php } ?> value="p">
                    </td>
                </tr>
                <tr valign="baseline"  class="inputdef">
                    <td width="30%"   class="inputdef" style="font-weight: bold" valign="top">  Enter Client Name or Client No:&nbsp;

                        <input size="20" maxlength="20" name="val" ID="val"  onFocus="this.style.backgroundColor = '#ffff00'"  onBlur="this.style.backgroundColor = '#F2FAFF'"  class='forms'  >&nbsp;</td>
                    <td width="35%"> <input type="submit" name="submit" id="submit" value="Execute"  class="formsblue" align="center" ></td>
                </tr>
            </table>

            <?php if (isset($_SESSION['client']) && $_SESSION['client'] != '') { ?>
                <?php if ($_SESSION['client'] == 'i') { ?>
                    <table width="100%" border="0" cellpadding="4" cellspacing="0" >

                        <tr>
                            <td width="9%"   class="inputdef" style="font-weight: bold">Client ID</td>

                            <td width="11%"   class="inputdef" style="font-weight: bold"> Name</td>
                            <td width="9%"   class="inputdef" style="font-weight: bold"> Email</td>
                            <td width="11%"   class="inputdef" style="font-weight: bold">Contact </td>
                            <td width="50%"   class="inputdef" style="font-weight: bold" ><div align="right">
                                    <table width="154"  border="0" cellspacing="0" cellpadding="3">
                                        <tr class="inputdef">
                                            <td width="19"  ><img src="../images/icons/newproject.gif" width="15" height="13" border="0" /></td>

                                            <td width="123"    ><a href="newclient.php">New client</a></td>
                                        </tr>
                                    </table>
                                </div></td>
                        </tr>
                        <?php
                        if ($totalRows_clients > 0) {
                            $k = 0
                            ?>
                            <?php
                            do {
                                if ($k == 1) {
                                    echo '<tr class="EvenTableRows">';
                                    $k = 0;
                                } else {
                                    echo '<tr class="OddTableRows">';
                                    $k++;
                                }
                                ?>

                                <td > <?php echo str_pad($row_clients['id'], 3, "0", STR_PAD_LEFT) ?>  </td>
                                <td ><?php echo $row_clients['fulnames'] ?> </td>
                                <td ><a href="mailto:<?php echo $row_clients['email'] ?>"><?php echo $row_clients['email'] ?></a> </td>
                                <td ><?php echo $row_clients['phone'] ?> </td>
                                <td>
                                    <table>
                                        <tr>
                                            <td><a href="newclient.php?id=<?php echo $row_clients['id'] ?>">Edit Client Details</a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="editgnfile.php?cid=<?php echo $row_clients['id'] ?>">General File</a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="newspfile.php?cid=<?php echo $row_clients['id'] ?>">Specific File</a>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="newproject.php?cid=<?php echo $row_clients['id'] ?>">Project File</a>  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="genfileassing.php?cid=<?php echo $row_clients['id'] ?>">Assign Job</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <a href="client_billing.php?cid=<?php echo $row_clients['id'] ?>">Bill this client</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <a href="client_bills_list.php?cid=<?php echo $row_clients['id']; ?>">View Bill</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <a href="all_invoices.php?cid=<?php echo $row_clients['id']; ?>">View Invoices</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <a href="process_payment.php?cid=<?php echo $row_clients['id']; ?>">Process Payment</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <a href="client_payments.php?cid=<?php echo $row_clients['id']; ?>">Client Payments</a>

                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                </tr>
            <?php } while ($row_clients = mysqli_fetch_assoc($clients)); ?>
                            <tr>
                                <td colspan="6" class="mainbase"><table width="97%"  border="0" cellspacing="0" cellpadding="3">
                                        <tr align="center">
                                            <td width="45%">
                                                <table width="78%" border="0" align="center" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td width="23%" align="center"><?php if ($pageNum_clients > 0) { // Show if not first page    ?>
                                                                <a href="<?php printf("%s?pageNum_clients=%d%s", $currentPage, 0, $queryString_clients); ?>">First</a>
            <?php } // Show if not first page    ?>              </td>
                                                        <td width="31%" align="center"><?php if ($pageNum_clients > 0) { // Show if not first page    ?>
                                                                <a href="<?php printf("%s?pageNum_clients=%d%s", $currentPage, max(0, $pageNum_clients - 1), $queryString_clients); ?>">Previous</a>
            <?php } // Show if not first page    ?>              </td>
                                                        <td width="23%" align="center"><?php if ($pageNum_clients < $totalPages_clients) { // Show if not last page    ?>
                                                                <a href="<?php printf("%s?pageNum_clients=%d%s", $currentPage, min($totalPages_clients, $pageNum_clients + 1), $queryString_clients); ?>">Next</a>
            <?php } // Show if not last page    ?>              </td>
                                                        <td width="23%" align="center"><?php if ($pageNum_clients < $totalPages_clients) { // Show if not last page    ?>
                                                                <a href="<?php printf("%s?pageNum_clients=%d%s", $currentPage, $totalPages_clients, $queryString_clients); ?>">last</a>
            <?php } // Show if not last page    ?>              </td>
                                                    </tr>
                                                </table></td>
                                            <td width="55%">&nbsp; Showing <strong><?php echo ($startRow_clients + 1) ?></strong> to <strong><?php echo min($startRow_clients + $maxRows_clients, $totalRows_clients) ?></strong> of <strong><?php echo $totalRows_clients ?></strong> </td>
                                        </tr>
                                    </table></td>
                            </tr>
        <?php } else { ?>
                            <tr>
                                <td colspan="6" class="mainbase"><span class="style1">No clients Created! </span></td>
                            </tr>
                    <?php } ?>
                    </table>
                <?php } ?>
    <?php if ($_SESSION['client'] == 'c') { ?>
                    <table width="100%" border="0" cellpadding="4" cellspacing="0" >

                        <tr>
                            <td width="14%"   class="inputdef" style="font-weight: bold">Company ID</td>
                            <td width="15%"   class="inputdef" style="font-weight: bold"> Name</td>
                            <td width="12%"   class="inputdef" style="font-weight: bold"> Email</td>
                            <td width="10%"   class="inputdef" style="font-weight: bold"> Contact</td>
                            <td width="49%"   class="inputdef" style="font-weight: bold" ><div align="right">
                                    <table width="154"  border="0" cellspacing="0" cellpadding="3">
                                        <tr class="inputdef">
                                            <td width="19"  ><img src="../images/icons/newproject.gif" width="15" height="13" border="0" /></td>

                                            <td width="123"    ><a href="newcompany.php">New Company</a></td>
                                        </tr>
                                    </table>
                                </div></td>
                        </tr>
                        <?php
                        if ($totalRows_clients > 0) {
                            $k = 0;
                            ?>
                            <?php
                            do {
                                if ($k == 1) {
                                    echo '<tr class="EvenTableRows">';
                                    $k = 0;
                                } else {
                                    echo '<tr class="OddTableRows">';
                                    $k++;
                                }
                                ?>

                                <td ><?php echo str_pad($row_clients['clientno'], 3, "0", STR_PAD_LEFT) ?></td>
                                <td ><?php echo $row_clients['fulnames'] ?> </td>
                                <td ><a href="mailto:<?php echo $row_clients['email'] ?>"><?php echo $row_clients['email'] ?></a> </td>
                                <td ><?php echo $row_clients['phone'] ?> </td>
                                <td >
                                    <table>
                                        <tr>
                                            <td>
                                                <a href="newcompany.php?id=<?php echo $row_clients['id'] ?>">Edit </a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="editgnfile.php?cid=<?php echo $row_clients['id'] ?>">General File</a> &nbsp;&nbsp;&nbsp;<a href="newspfile.php?cid=<?php echo $row_clients['id'] ?>">Specific File</a>&nbsp;&nbsp;&nbsp;<a href="newproject.php?cid=<?php echo $row_clients['id'] ?>">Project File</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="genfileassing.php?cid=<?php echo $row_clients['id'] ?>">Assign Job</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="newdirectors.php?cid=<?php echo $row_clients['id'] ?>">Add Directors</a>&nbsp;&nbsp;&nbsp;
                                                <a href="client_billing.php?cid=<?php echo $row_clients['id'] ?>">Bill this client</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <a href="client_billing.php?cid=<?php echo $row_clients['id'] ?>">Bill this client</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <a href="client_bills_list.php?cid=<?php echo $row_clients['id']; ?>">View Bill</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <a href="all_invoices.php?cid=<?php echo $row_clients['id']; ?>">View Invoices</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <a href="process_payment.php?cid=<?php echo $row_clients['id']; ?>">Process Payment</a>

                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                </tr>
            <?php } while ($row_clients = mysqli_fetch_assoc($clients)); ?>
                            <tr>
                                <td colspan="5" class="mainbase"><table width="97%"  border="0" cellspacing="0" cellpadding="3">
                                        <tr align="center">
                                            <td width="45%">
                                                <table width="78%" border="0" align="center" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td width="23%" align="center"><?php if ($pageNum_clients > 0) { // Show if not first page  ?>
                                                                <a href="<?php printf("%s?pageNum_clients=%d%s", $currentPage, 0, $queryString_clients); ?>">First</a>
                                                            <?php } // Show if not first page    ?>              </td>
                                                        <td width="31%" align="center"><?php if ($pageNum_clients > 0) { // Show if not first page  ?>
                                                                <a href="<?php printf("%s?pageNum_clients=%d%s", $currentPage, max(0, $pageNum_clients - 1), $queryString_clients); ?>">Previous</a>
                                                            <?php } // Show if not first page    ?>              </td>
                                                        <td width="23%" align="center"><?php if ($pageNum_clients < $totalPages_clients) { // Show if not last page  ?>
                                                                <a href="<?php printf("%s?pageNum_clients=%d%s", $currentPage, min($totalPages_clients, $pageNum_clients + 1), $queryString_clients); ?>">Next</a>
                                                            <?php } // Show if not last page    ?>              </td>
                                                        <td width="23%" align="center"><?php if ($pageNum_clients < $totalPages_clients) { // Show if not last page  ?>
                                                                <a href="<?php printf("%s?pageNum_clients=%d%s", $currentPage, $totalPages_clients, $queryString_clients); ?>">last</a>
            <?php } // Show if not last page    ?>              </td>
                                                    </tr>
                                                </table></td>
                                            <td width="55%">&nbsp; Showing <strong><?php echo ($startRow_clients + 1) ?></strong> to <strong><?php echo min($startRow_clients + $maxRows_clients, $totalRows_clients) ?></strong> of <strong><?php echo $totalRows_clients ?></strong> </td>
                                        </tr>
                                    </table></td>
                            </tr>
        <?php } else { ?>
                            <tr>
                                <td colspan="5" class="mainbase"><span class="style1">No clients Created! </span></td>
                            </tr>
                    <?php } ?>
                    </table>
    <?php } ?>
    <?php if ($_SESSION['client'] == 'p') { ?>
                    <table width="100%" border="0" cellpadding="4" cellspacing="0" >

                        <tr>
                            <td width="12%"   class="inputdef" style="font-weight: bold"> Name</td>
                            <td width="17%"   class="inputdef" style="font-weight: bold"> Contact</td>
                            <td width="15%"   class="inputdef" style="font-weight: bold"> Place of work</td>
                            <td width="35%"   class="inputdef" style="font-weight: bold"> Email</td>
                            <td width="21%"   class="inputdef" style="font-weight: bold" ><div align="right">
                                    <table width="154"  border="0" cellspacing="0" cellpadding="3">
                                        <tr class="inputdef">
                                            <td width="19"  ><img src="../images/icons/newproject.gif" width="15" height="13" border="0" /></td>

                                            <td width="123"    ><a href="newprospective.php">New client</a></td>
                                        </tr>
                                    </table>
                                </div></td>
                        </tr>
                        <?php
                        if ($totalRows_clients > 0) {
                            $k = 0;
                            ?>
                            <?php
                            do {
                                if ($k == 1) {
                                    echo '<tr class="EvenTableRows">';
                                    $k = 0;
                                } else {
                                    echo '<tr class="OddTableRows">';
                                    $k++;
                                }
                                ?>

                                <td ><?php echo $row_clients['fulnames'] ?> </td>
                                <td ><?php echo $row_clients['phone'] ?> </td>
                                <td ><?php echo $row_clients['workplace'] ?> </td>
                                <td ><a href="mailto:<?php echo $row_clients['email'] ?>"><?php echo $row_clients['email'] ?></a> </td>

                                <td ><a href="newprospective.php?id=<?php echo $row_clients['id'] ?>">Edit</a></td>
                                </tr>
            <?php } while ($row_clients = mysqli_fetch_assoc($clients)); ?>
                            <tr>
                                <td colspan="5" class="mainbase"><table width="97%"  border="0" cellspacing="0" cellpadding="3">
                                        <tr align="center">
                                            <td width="45%">
                                                <table width="78%" border="0" align="center" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td width="23%" align="center"><?php if ($pageNum_clients > 0) { // Show if not first page   ?>
                                                                <a href="<?php printf("%s?pageNum_clients=%d%s", $currentPage, 0, $queryString_clients); ?>">First</a>
                                                            <?php } // Show if not first page  ?>              </td>
                                                        <td width="31%" align="center"><?php if ($pageNum_clients > 0) { // Show if not first page   ?>
                                                                <a href="<?php printf("%s?pageNum_clients=%d%s", $currentPage, max(0, $pageNum_clients - 1), $queryString_clients); ?>">Previous</a>
                                                            <?php } // Show if not first page  ?>              </td>
                                                        <td width="23%" align="center"><?php if ($pageNum_clients < $totalPages_clients) { // Show if not last page    ?>
                                                                <a href="<?php printf("%s?pageNum_clients=%d%s", $currentPage, min($totalPages_clients, $pageNum_clients + 1), $queryString_clients); ?>">Next</a>
                                                            <?php } // Show if not last page  ?>              </td>
                                                        <td width="23%" align="center"><?php if ($pageNum_clients < $totalPages_clients) { // Show if not last page    ?>
                                                                <a href="<?php printf("%s?pageNum_clients=%d%s", $currentPage, $totalPages_clients, $queryString_clients); ?>">last</a>
            <?php } // Show if not last page    ?>              </td>
                                                    </tr>
                                                </table></td>
                                            <td width="55%">&nbsp; Showing <strong><?php echo ($startRow_clients + 1) ?></strong> to <strong><?php echo min($startRow_clients + $maxRows_clients, $totalRows_clients) ?></strong> of <strong><?php echo $totalRows_clients ?></strong> </td>
                                        </tr>
                                    </table></td>
                            </tr>
                        <?php } else { ?>
                            <tr>
                                <td colspan="5" class="mainbase"><span class="style1">No clients Created! </span></td>
                            </tr>
                    <?php } ?>
                    </table>
    <?php } ?>
<?php } ?>
        </form>
    </body>
</html>
<?php
//mysqli_free_result($clients);
?>

