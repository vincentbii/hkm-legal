<?php require_once('../connections/eProc.php'); ?>

<?php

$currentPage = $_SERVER["PHP_SELF"];

$maxRows_status = 30;
$pageNum_status = 0;


  unset($_SESSION['itype']);


if (isset($_GET['pageNum_status'])) {
  $pageNum_status = $_GET['pageNum_status'];
}
$startRow_status = $pageNum_status * $maxRows_status;

mysqli_select_db($eProc, $database_eProc);
$query_status = "SELECT * FROM status   ";

$query_limit_status = sprintf("%s LIMIT %d, %d", $query_status, $startRow_status, $maxRows_status);
$status = mysqli_query( $eProc,$query_limit_status) or die(mysqli_error());
$row_status = mysqli_fetch_assoc($status);

if (isset($_GET['totalRows_status'])) {
  $totalRows_status = $_GET['totalRows_status'];
} else {
  $all_status = mysqli_query( $eProc, $query_status);
  $totalRows_status = mysqli_num_rows($all_status);
}
$totalPages_status = ceil($totalRows_status/$maxRows_status)-1;

$queryString_status = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_status") == false && 
        stristr($param, "totalRows_status") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_status = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_status = sprintf("&totalRows_status=%d%s", $totalRows_status, $queryString_status);
?>

<html>
<head>
<title>LEGAL MANAGEMENT SYSTEM</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../styles/default.css" rel="stylesheet" type="text/css">

<style type="text/css">
<!--
.style1 {
	color: #FF0000;
	font-weight: bold;
}
-->
</style>
</head>
<body>
<fieldset>
<table width="100%" border="0" cellpadding="4" cellspacing="0" >

  <tr>
  <td width="26%"   class="inputdef" style="font-weight: bold">status Name</td>
  <td width="41%"   class="inputdef" style="font-weight: bold" ><div align="right">
	  <table width="154"  border="0" cellspacing="0" cellpadding="3">
	    <tr class="inputdef">
	      <td width="19"  ><img src="../images/icons/newproject.gif" width="15" height="13" border="0" /></td>
                
			<td width="123"    ><a href="newstatus.php">New status</a></td>
            </tr>
	    </table>
	  </div></td>
  </tr>
  <?php if ($totalRows_status > 0) { ?>
  <?php do { ?>
  <tr>
   <td > <?php echo $row_status['name']?> </td>
   <td ><a href="newstatus.php?id=<?php echo $row_status['id'] ?>">Edit</a></td>
  </tr>
  <?php } while ($row_status = mysqli_fetch_assoc($status)); ?>
  <tr>
    <td colspan="4" class="mainbase"><table width="97%"  border="0" cellspacing="0" cellpadding="3">
      <tr align="center">
        <td width="45%">
          <table width="78%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
              <td width="23%" align="center"><?php if ($pageNum_status > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_status=%d%s", $currentPage, 0, $queryString_status); ?>">First</a>
                <?php } // Show if not first page ?>              </td>
              <td width="31%" align="center"><?php if ($pageNum_status > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_status=%d%s", $currentPage, max(0, $pageNum_status - 1), $queryString_status); ?>">Previous</a>
                <?php } // Show if not first page ?>              </td>
              <td width="23%" align="center"><?php if ($pageNum_status < $totalPages_status) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_status=%d%s", $currentPage, min($totalPages_status, $pageNum_status + 1), $queryString_status); ?>">Next</a>
                <?php } // Show if not last page ?>              </td>
              <td width="23%" align="center"><?php if ($pageNum_status < $totalPages_status) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_status=%d%s", $currentPage, $totalPages_status, $queryString_status); ?>">last</a>
                <?php } // Show if not last page ?>              </td>
            </tr>
          </table></td>
        <td width="55%">&nbsp; Showing <strong><?php echo ($startRow_status + 1) ?></strong> to <strong><?php echo min($startRow_status + $maxRows_status, $totalRows_status) ?></strong> of <strong><?php echo $totalRows_status ?></strong> </td>
      </tr>
    </table></td>
  </tr>
  <?php } else { ?>
  <tr>
    <td colspan="4" class="mainbase"><span class="style1">No status Created! </span></td>
  </tr>
  <?php } ?>
</table>
</fieldset>
</body>
</html>
<?php
mysqli_free_result($status);
?>

