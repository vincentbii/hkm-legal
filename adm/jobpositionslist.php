<?php require_once('../connections/eProc.php'); 

 require_once('../activelog.php'); ?>
<?php
//echo"the company". $_SESSION['company'];
$currentPage = $_SERVER["PHP_SELF"];

$maxRows_jobposition = 30;
$pageNum_jobposition = 0;
if (isset($_GET['pageNum_jobposition'])) {
  $pageNum_jobposition = $_GET['pageNum_jobposition'];
}
$startRow_jobposition = $pageNum_jobposition * $maxRows_jobposition;

mysqli_select_db($eProc, $database_eProc);
$query_jobposition = "SELECT * FROM jobposition ";
$query_limit_jobposition = sprintf("%s LIMIT %d, %d", $query_jobposition, $startRow_jobposition, $maxRows_jobposition);
$jobposition = mysqli_query($eProc,$query_limit_jobposition) or die(mysqli_error($eProc));
$row_jobposition = mysqli_fetch_assoc($jobposition);

if (isset($_GET['totalRows_jobposition'])) {
  $totalRows_jobposition = $_GET['totalRows_jobposition'];
} else {
  $all_jobposition = mysqli_query($eProc,$query_jobposition);
  $totalRows_jobposition = mysqli_num_rows($all_jobposition);
}
$totalPages_jobposition = ceil($totalRows_jobposition/$maxRows_jobposition)-1;

$queryString_jobposition = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_jobposition") == false && 
        stristr($param, "totalRows_jobposition") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_jobposition = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_jobposition = sprintf("&totalRows_jobposition=%d%s", $totalRows_jobposition, $queryString_jobposition);
?>

<html>
<head>
<title>LEGAL  MANAGEMENT INFORMATION  SYSTEM</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../styles/default.css" rel="stylesheet" type="text/css">

<style type="text/css">
<!--
.style1 {
	color: #FF0000;
	font-weight: bold;
}
-->
</style>
</head>
<body>

<table width="100%" border="0" cellpadding="4" cellspacing="0" >

  <tr>
   <td width="36%"   class="inputdeft" style="font-weight: bold">Job Positions</td>
   
	<td width="64%"   class="inputdeft" style="font-weight: bold" ><div align="right">
	  <table width="154"  border="0" cellspacing="0" cellpadding="3">
	    <tr class="inputdeft">
	      <td width="19"  ><img src="../images/icons/newproject.gif" width="15" height="13" border="0" /></td>
                
			<td width="123"   ><a href="../adm/newjobposition.php">New Job Position </a></td>
            </tr>
	    </table>
	  </div></td>
  </tr>
  <?php if ($totalRows_jobposition > 0) { ?>
  <?php do { ?>
  <tr>
  <td ><?php echo $row_jobposition['name']?> </td>
  
  <td ><a href="../adm/newjobposition.php?id=<?php echo $row_jobposition['id'] ?>">Edit</a></td>
  </tr>
  <?php } while ($row_jobposition = mysqli_fetch_assoc($jobposition)); ?>
  <tr>
    <td colspan="12" class="mainbase"><table width="97%"  border="0" cellspacing="0" cellpadding="3">
      <tr align="center">
        <td width="45%">
          <table width="78%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
              <td width="23%" align="center"><?php if ($pageNum_jobposition > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_jobposition=%d%s", $currentPage, 0, $queryString_jobposition); ?>">First</a>
                <?php } // Show if not first page ?>              </td>
              <td width="31%" align="center"><?php if ($pageNum_jobposition > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_jobposition=%d%s", $currentPage, max(0, $pageNum_jobposition - 1), $queryString_jobposition); ?>">Previous</a>
                <?php } // Show if not first page ?>              </td>
              <td width="23%" align="center"><?php if ($pageNum_jobposition < $totalPages_jobposition) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_jobposition=%d%s", $currentPage, min($totalPages_jobposition, $pageNum_jobposition + 1), $queryString_jobposition); ?>">Next</a>
                <?php } // Show if not last page ?>              </td>
              <td width="23%" align="center"><?php if ($pageNum_jobposition < $totalPages_jobposition) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_jobposition=%d%s", $currentPage, $totalPages_jobposition, $queryString_jobposition); ?>">last</a>
                <?php } // Show if not last page ?>              </td>
            </tr>
          </table></td>
        <td width="55%">&nbsp; Showing <strong><?php echo ($startRow_jobposition + 1) ?></strong> to <strong><?php echo min($startRow_jobposition + $maxRows_jobposition, $totalRows_jobposition) ?></strong> of <strong><?php echo $totalRows_jobposition ?></strong> </td>
      </tr>
    </table></td>
  </tr>
  <?php } else { ?>
  <tr>
    <td colspan="12" class="mainbase"><span class="style1">No jobpositions Created! </span></td>
  </tr>
  <?php } ?>
</table>
</body>
</html>
<?php
mysqli_free_result($jobposition);
?>

