<?php
require_once('../connections/eProc.php');

$currentPage = $_SERVER["PHP_SELF"];

$maxRows_services = 20;
$pageNum_services = 0;

if (isset($_GET['pageNum_services'])) {
    $pageNum_services = $_GET['pageNum_services'];
}

$startRow_services = $pageNum_services * $maxRows_services;

mysqli_select_db($eProc, $database_eProc);

$query_services = "SELECT * FROM services WHERE status='1' ORDER BY id ASC ";
$query_limit_services = sprintf("%s LIMIT %d, %d", $query_services, $startRow_services, $maxRows_services);
$services = mysqli_query($eProc, $query_limit_services) or die(mysqli_error());
$row_services = mysqli_fetch_assoc($services);

if (isset($_GET['totalRows_services'])) {
    $totalRows_services = $_GET['totalRows_services'];
} else {
    $all_services = mysqli_query($eProc, $query_services);
    $totalRows_services = mysqli_num_rows($all_services);
}
$totalPages_services = ceil($totalRows_services / $maxRows_services) - 1;

$queryString_services = "";
if (!empty($_SERVER['QUERY_STRING'])) {
    $params = explode("&", $_SERVER['QUERY_STRING']);
    $newParams = array();
    foreach ($params as $param) {
        if (stristr($param, "pageNum_services") == false &&
                stristr($param, "totalRows_services") == false) {
            array_push($newParams, $param);
        }
    }
    if (count($newParams) != 0) {
        $queryString_services = "&" . htmlentities(implode("&", $newParams));
    }
}
$queryString_services = sprintf("&totalRows_services=%d%s", $totalRows_services, $queryString_services);
?>
<html>
    <head>
        <link href="../styles/default.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <form name="billing_services" action="" method="POST">
            <table width="100%" border="0" cellpadding="4" cellspacing="0" >

                <tr>
                    <th>Name</th>
                    <th>Amount</th>
                    <th>Description</th>
                    <th>
                <table width="154"  border="0" cellspacing="0" cellpadding="3">
                    <tr class="inputdef">
                        <td width="19"  ><img src="../images/icons/newproject.gif" width="15" height="13" border="0" /></td>

                        <td width="123"    ><a href="new_billing_service.php">New Service</a></td>
                    </tr>
                </table>
                </th>
                </tr>

                <?php
                if ($totalRows_services > 0) {
                    do {
                        ?>

                        <tr>
                            <td ><?php echo $row_services['name'] ?></td>
                            <td ><?php echo number_format($row_services['amount']) ?> </td>
                            <td ><?php echo $row_services['description'] ?> </td>
                            <td ><a href="new_billing_service.php?id=<?php echo $row_services['id'] ?>">Edit</a></td>
                        </tr>

                        <?php
                    } while ($row_services = mysqli_fetch_assoc($services));
                }
                ?>

                <tr>
                    <td colspan="5" class="mainbase"><table width="97%"  border="0" cellspacing="0" cellpadding="3">
                            <tr align="center">
                                <td width="45%">
                                    <table width="78%" border="0" align="center" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td width="23%" align="center"><?php if ($pageNum_services > 0) { // Show if not first page      ?>
                                                    <a href="<?php printf("%s?pageNum_services=%d%s", $currentPage, 0, $queryString_services); ?>">First</a>
                                                <?php } // Show if not first page  ?>              </td>
                                            <td width="31%" align="center"><?php if ($pageNum_services > 0) { // Show if not first page      ?>
                                                    <a href="<?php printf("%s?pageNum_services=%d%s", $currentPage, max(0, $pageNum_services - 1), $queryString_services); ?>">Previous</a>
                                                <?php } // Show if not first page  ?>              </td>
                                            <td width="23%" align="center"><?php if ($pageNum_services < $totalPages_services) { // Show if not last page      ?>
                                                    <a href="<?php printf("%s?pageNum_services=%d%s", $currentPage, min($totalPages_services, $pageNum_services + 1), $queryString_services); ?>">Next</a>
                                                <?php } // Show if not last page  ?>              </td>
                                            <td width="23%" align="center"><?php if ($pageNum_services < $totalPages_services) { // Show if not last page      ?>
                                                    <a href="<?php printf("%s?pageNum_services=%d%s", $currentPage, $totalPages_services, $queryString_services); ?>">last</a>
                                                <?php } // Show if not last page  ?>              </td>
                                        </tr>
                                    </table></td>
                                <td width="55%">&nbsp; Showing <strong><?php echo ($startRow_services + 1) ?></strong> to <strong><?php echo min($startRow_services + $maxRows_services, $totalRows_services) ?></strong> of <strong><?php echo $totalRows_services ?></strong> </td>
                            </tr>
                        </table></td>
                </tr>

            </table>

        </form>
    </body>
</html>