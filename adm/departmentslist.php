<?php require_once('../connections/eProc.php'); 

 require_once('../activelog.php'); ?>
<?php
$currentPage = $_SERVER["PHP_SELF"];

$maxRows_department = 30;
$pageNum_department = 0;
if (isset($_GET['pageNum_department'])) {
  $pageNum_department = $_GET['pageNum_department'];
}
$startRow_department = $pageNum_department * $maxRows_department;

mysqli_select_db($eProc, $database_eProc);
$query_department = "SELECT * FROM department ";
$query_limit_department = sprintf("%s LIMIT %d, %d", $query_department, $startRow_department, $maxRows_department);
$department = mysqli_query($eProc,$query_limit_department) or die(mysqli_error());
$row_department = mysqli_fetch_assoc($department);

if (isset($_GET['totalRows_department'])) {
  $totalRows_department = $_GET['totalRows_department'];
} else {
  $all_department = mysqli_query($eProc,$query_department);
  $totalRows_department = mysqli_num_rows($all_department);
}
$totalPages_department = ceil($totalRows_department/$maxRows_department)-1;

$queryString_department = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_department") == false && 
        stristr($param, "totalRows_department") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_department = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_department = sprintf("&totalRows_department=%d%s", $totalRows_department, $queryString_department);
?>

<html>
<head>
<title>LEGAL  MANAGEMENT INFORMATION  SYSTEM</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../styles/default.css" rel="stylesheet" type="text/css">

<style type="text/css">
<!--
.style1 {
	color: #FF0000;
	font-weight: bold;
}
-->
</style>
</head>
<body>

<table width="100%" border="0" cellpadding="4" cellspacing="0" >

  <tr class="inputdeft">
   <td width="36%"   class="inputdeft" >Departments</td>
   
	<td width="64%"   class="inputdeft"  ><div align="right">
	  <table width="154"  border="0" cellspacing="0" cellpadding="3">
	    <tr class="inputdeft">
	      <td width="19"  ><img src="../images/icons/newproject.gif" width="15" height="13" border="0" /></td>
                
			<td width="123"   ><a href="../adm/newdepartment.php">New Department </a></td>
            </tr>
	    </table>
	  </div></td>
  </tr>
  <?php if ($totalRows_department > 0) { ?>
  <?php do { ?>
  <tr>
  <td ><?php echo $row_department['name']?> </td>
  
  <td ><a href="../adm/newdepartment.php?id=<?php echo $row_department['id'] ?>">Edit</a></td>
  </tr>
  <?php } while ($row_department = mysqli_fetch_assoc($department)); ?>
  <tr>
    <td colspan="12" class="mainbase"><table width="97%"  border="0" cellspacing="0" cellpadding="3">
      <tr align="center">
        <td width="45%">
          <table width="78%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
              <td width="23%" align="center"><?php if ($pageNum_department > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_department=%d%s", $currentPage, 0, $queryString_department); ?>">First</a>
                <?php } // Show if not first page ?>              </td>
              <td width="31%" align="center"><?php if ($pageNum_department > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_department=%d%s", $currentPage, max(0, $pageNum_department - 1), $queryString_department); ?>">Previous</a>
                <?php } // Show if not first page ?>              </td>
              <td width="23%" align="center"><?php if ($pageNum_department < $totalPages_department) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_department=%d%s", $currentPage, min($totalPages_department, $pageNum_department + 1), $queryString_department); ?>">Next</a>
                <?php } // Show if not last page ?>              </td>
              <td width="23%" align="center"><?php if ($pageNum_department < $totalPages_department) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_department=%d%s", $currentPage, $totalPages_department, $queryString_department); ?>">last</a>
                <?php } // Show if not last page ?>              </td>
            </tr>
          </table></td>
        <td width="55%">&nbsp; Showing <strong><?php echo ($startRow_department + 1) ?></strong> to <strong><?php echo min($startRow_department + $maxRows_department, $totalRows_department) ?></strong> of <strong><?php echo $totalRows_department ?></strong> </td>
      </tr>
    </table></td>
  </tr>
  <?php } else { ?>
  <tr>
    <td colspan="12" class="mainbase"><span class="style1">No departments Created! </span></td>
  </tr>
  <?php } ?>
</table>
</body>
</html>
<?php
mysqli_free_result($department);
?>

