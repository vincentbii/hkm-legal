<?php require_once('../connections/eProc.php'); 

 require_once('../activelog.php'); ?>
<?php
 unset($_SESSION['dept']);

$currentPage = $_SERVER["PHP_SELF"];

$maxRows_authlevel = 30;
$pageNum_authlevel = 0;
if (isset($_GET['pageNum_authlevel'])) {
  $pageNum_authlevel = $_GET['pageNum_authlevel'];
}
$startRow_authlevel = $pageNum_authlevel * $maxRows_authlevel;

mysqli_select_db($eProc, $database_eProc);
$query_authlevel = "SELECT approvers.*,employee.empcode ,employee.name AS emp,department.name AS dept,jobposition.name AS position  
					FROM approvers 
					INNER JOIN employee ON approvers.employee=employee.id 
					INNER JOIN department ON employee.department=department.id 
					INNER JOIN jobposition ON employee.jobposition=jobposition.id 
					ORDER BY employee.name ASC  ";
//echo $query_authlevel;
$query_limit_authlevel = sprintf("%s LIMIT %d, %d", $query_authlevel, $startRow_authlevel, $maxRows_authlevel);
$authlevel = mysqli_query($eProc, $query_limit_authlevel) or die(mysqli_error($eProc));
$row_authlevel = mysqli_fetch_assoc($authlevel);

if (isset($_GET['totalRows_authlevel'])) {
  $totalRows_authlevel = $_GET['totalRows_authlevel'];
} else {
  $all_authlevel = mysqli_query($query_authlevel);
  $totalRows_authlevel = mysqli_num_rows($all_authlevel);
}
$totalPages_authlevel = ceil($totalRows_authlevel/$maxRows_authlevel)-1;

$queryString_authlevel = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_authlevel") == false && 
        stristr($param, "totalRows_authlevel") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_authlevel = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_authlevel = sprintf("&totalRows_authlevel=%d%s", $totalRows_authlevel, $queryString_authlevel);
?>

<html>
<head>
<title>HR MANAGEMENT SYSTEM</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../styles/default.css" rel="stylesheet" type="text/css">

<style type="text/css">
<!--
.style1 {
	color: #FF0000;
	font-weight: bold;
}
-->
</style>
</head>
<body>

<table width="100%" border="0" cellpadding="4" cellspacing="0" >

  <tr>
  <td width="11%"   class="inputdeft" >Employee Code</td>
   <td width="20%"   class="inputdeft" >Approver</td>
   <td width="25%"   class="inputdeft" >Department</td>
     <td width="23%"   class="inputdeft" >Job Position</td>
   <td width="21%"   class="inputdeft" style="font-weight: bold" ><div align="right">
	  <table width="154"  border="0" cellspacing="0" cellpadding="3">
	    <tr class="inputdeft">
	      <td width="19"  ><img src="../images/icons/newproject.gif" width="15" height="13" border="0" /></td>
                
			<td width="123"   ><a href="../lv/newapprover.php">Add/Edit Approver</a></td>
          </tr>
	    </table>
	  </div></td>
  </tr>
  <?php if ($totalRows_authlevel > 0) { ?>
  <?php do { ?>
  <tr>
   <td ><?php echo $row_authlevel['empcode']?> </td>
  <td ><?php echo $row_authlevel['emp']?> </td>
    <td ><?php echo $row_authlevel['dept']?> </td>
   <td ><?php echo $row_authlevel['position']?> </td>
    <td ><!--<a href="../lv/newapprover.php?id=<?php //echo $row_authlevel['id'] ?>">Edit</a>--></td>
  </tr>
  <?php } while ($row_authlevel = mysqli_fetch_assoc($authlevel)); ?>
  <tr>
    <td colspan="7" class="mainbase"><table width="97%"  border="0" cellspacing="0" cellpadding="3">
      <tr align="center">
        <td width="45%">
          <table width="78%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
              <td width="23%" align="center"><?php if ($pageNum_authlevel > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_authlevel=%d%s", $currentPage, 0, $queryString_authlevel); ?>">First</a>
                <?php } // Show if not first page ?>              </td>
              <td width="31%" align="center"><?php if ($pageNum_authlevel > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_authlevel=%d%s", $currentPage, max(0, $pageNum_authlevel - 1), $queryString_authlevel); ?>">Previous</a>
                <?php } // Show if not first page ?>              </td>
              <td width="23%" align="center"><?php if ($pageNum_authlevel < $totalPages_authlevel) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_authlevel=%d%s", $currentPage, min($totalPages_authlevel, $pageNum_authlevel + 1), $queryString_authlevel); ?>">Next</a>
                <?php } // Show if not last page ?>              </td>
              <td width="23%" align="center"><?php if ($pageNum_authlevel < $totalPages_authlevel) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_authlevel=%d%s", $currentPage, $totalPages_authlevel, $queryString_authlevel); ?>">last</a>
                <?php } // Show if not last page ?>              </td>
            </tr>
          </table></td>
        <td width="55%">&nbsp; Showing <strong><?php echo ($startRow_authlevel + 1) ?></strong> to <strong><?php echo min($startRow_authlevel + $maxRows_authlevel, $totalRows_authlevel) ?></strong> of <strong><?php echo $totalRows_authlevel ?></strong> </td>
      </tr>
    </table></td>
  </tr>
  <?php } else { ?>
  <tr>
    <td colspan="7" class="mainbase"><span class="style1">No approvers Created! </span></td>
  </tr>
  <?php } ?>
</table>
</body>
</html>
<?php
mysqli_free_result($authlevel);
?>

