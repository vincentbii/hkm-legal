<?php require_once('../connections/eProc.php'); 

 require_once('../activelog.php'); ?>
<?php
$currentPage = $_SERVER["PHP_SELF"];

$maxRows_probationtype = 30;
$pageNum_probationtype = 0;
if (isset($_GET['pageNum_probationtype'])) {
  $pageNum_probationtype = $_GET['pageNum_probationtype'];
}
$startRow_probationtype = $pageNum_probationtype * $maxRows_probationtype;

mysqli_select_db($eProc, $database_eProc);
$query_probationtype = "SELECT * FROM probationtype ";
$query_limit_probationtype = sprintf("%s LIMIT %d, %d", $query_probationtype, $startRow_probationtype, $maxRows_probationtype);
$probationtype = mysqli_query($eProc, $query_limit_probationtype) or die(mysqli_error());
$row_probationtype = mysqli_fetch_assoc($probationtype);

if (isset($_GET['totalRows_probationtype'])) {
  $totalRows_probationtype = $_GET['totalRows_probationtype'];
} else {
  $all_probationtype = mysqli_query($eProc, $query_probationtype);
  $totalRows_probationtype = mysqli_num_rows($all_probationtype);
}
$totalPages_probationtype = ceil($totalRows_probationtype/$maxRows_probationtype)-1;

$queryString_probationtype = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_probationtype") == false && 
        stristr($param, "totalRows_probationtype") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_probationtype = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_probationtype = sprintf("&totalRows_probationtype=%d%s", $totalRows_probationtype, $queryString_probationtype);
?>

<html>
<head>
<title>LEGAL  MANAGEMENT INFORMATION  SYSTEM</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../styles/default.css" rel="stylesheet" type="text/css">

<style type="text/css">
<!--
.style1 {
	color: #FF0000;
	font-weight: bold;
}
-->
</style>
</head>
<body>

<table width="100%" border="0" cellpadding="4" cellspacing="0" >

  <tr>
   <td width="36%"   class="inputdeft" style="font-weight: bold"> Probation Type</td>
   
	<td width="64%"   class="inputdeft" style="font-weight: bold" ><div align="right">
	  <table width="154"  border="0" cellspacing="0" cellpadding="3">
	    <tr class="inputdeft">
	      <td width="19"  ><img src="../images/icons/newproject.gif" width="15" height="13" border="0" /></td>
                
			<td width="123"   ><a href="../adm/newprobationtype.php">New Probation Type  </a></td>
          </tr>
	    </table>
	  </div></td>
  </tr>
  <?php if ($totalRows_probationtype > 0) { ?>
  <?php do { ?>
  <tr>
  <td ><?php echo $row_probationtype['name']?> </td>
  
  <td ><a href="../adm/newprobationtype.php?id=<?php echo $row_probationtype['id'] ?>">Edit</a></td>
  </tr>
  <?php } while ($row_probationtype = mysqli_fetch_assoc($probationtype)); ?>
  <tr>
    <td colspan="12" class="mainbase"><table width="97%"  border="0" cellspacing="0" cellpadding="3">
      <tr align="center">
        <td width="45%">
          <table width="78%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
              <td width="23%" align="center"><?php if ($pageNum_probationtype > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_probationtype=%d%s", $currentPage, 0, $queryString_probationtype); ?>">First</a>
                <?php } // Show if not first page ?>              </td>
              <td width="31%" align="center"><?php if ($pageNum_probationtype > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_probationtype=%d%s", $currentPage, max(0, $pageNum_probationtype - 1), $queryString_probationtype); ?>">Previous</a>
                <?php } // Show if not first page ?>              </td>
              <td width="23%" align="center"><?php if ($pageNum_probationtype < $totalPages_probationtype) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_probationtype=%d%s", $currentPage, min($totalPages_probationtype, $pageNum_probationtype + 1), $queryString_probationtype); ?>">Next</a>
                <?php } // Show if not last page ?>              </td>
              <td width="23%" align="center"><?php if ($pageNum_probationtype < $totalPages_probationtype) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_probationtype=%d%s", $currentPage, $totalPages_probationtype, $queryString_probationtype); ?>">last</a>
                <?php } // Show if not last page ?>              </td>
            </tr>
          </table></td>
        <td width="55%">&nbsp; Showing <strong><?php echo ($startRow_probationtype + 1) ?></strong> to <strong><?php echo min($startRow_probationtype + $maxRows_probationtype, $totalRows_probationtype) ?></strong> of <strong><?php echo $totalRows_probationtype ?></strong> </td>
      </tr>
    </table></td>
  </tr>
  <?php } else { ?>
  <tr>
    <td colspan="12" class="mainbase"><span class="style1">No probationtypes Created! </span></td>
  </tr>
  <?php } ?>
</table>
</body>
</html>
<?php
mysqli_free_result($probationtype);
?>

