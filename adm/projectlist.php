<?php require_once('../connections/eProc.php'); ?>

<?php

$currentPage = $_SERVER["PHP_SELF"];

$maxRows_project = 30;
$pageNum_project = 0;


  unset($_SESSION['itype']);


if (isset($_GET['pageNum_project'])) {
  $pageNum_project = $_GET['pageNum_project'];
}
$startRow_project = $pageNum_project * $maxRows_project;

mysqli_select_db($eProc, $database_eProc);
$query_project = "SELECT * FROM project   ";

$query_limit_project = sprintf("%s LIMIT %d, %d", $query_project, $startRow_project, $maxRows_project);
$project = mysqli_query($eProc, $query_limit_project) or die(mysqli_error($eProc));
$row_project = mysqli_fetch_assoc($project);

if (isset($_GET['totalRows_project'])) {
  $totalRows_project = $_GET['totalRows_project'];
} else {
  $all_project = mysqli_query($query_project);
  $totalRows_project = mysqli_num_rows($all_project);
}
$totalPages_project = ceil($totalRows_project/$maxRows_project)-1;

$queryString_project = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_project") == false && 
        stristr($param, "totalRows_project") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_project = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_project = sprintf("&totalRows_project=%d%s", $totalRows_project, $queryString_project);
?>

<html>
<head>
<title>LEGAL MANAGEMENT SYSTEM</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../styles/default.css" rel="stylesheet" type="text/css">

<style type="text/css">
<!--
.style1 {
	color: #FF0000;
	font-weight: bold;
}
-->
</style>
</head>
<body>
<fieldset>
<table width="100%" border="0" cellpadding="4" cellspacing="0" >

  <tr>
  <td width="26%"   class="inputdef" style="font-weight: bold">Project Name</td>
  <td width="41%"   class="inputdef" style="font-weight: bold" ><div align="right">
	  <table width="154"  border="0" cellspacing="0" cellpadding="3">
	    <tr class="inputdef">
	      <td width="19"  ><img src="../images/icons/newproject.gif" width="15" height="13" border="0" /></td>
                
			<td width="123"    ><a href="newproject.php">New project</a></td>
            </tr>
	    </table>
	  </div></td>
  </tr>
  <?php if ($totalRows_project > 0) { ?>
  <?php do { ?>
  <tr>
   <td > <?php echo $row_project['name']?> </td>
   <td ><a href="newproject.php?id=<?php echo $row_project['id'] ?>">Edit</a></td>
  </tr>
  <?php } while ($row_project = mysqli_fetch_assoc($project)); ?>
  <tr>
    <td colspan="4" class="mainbase"><table width="97%"  border="0" cellspacing="0" cellpadding="3">
      <tr align="center">
        <td width="45%">
          <table width="78%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
              <td width="23%" align="center"><?php if ($pageNum_project > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_project=%d%s", $currentPage, 0, $queryString_project); ?>">First</a>
                <?php } // Show if not first page ?>              </td>
              <td width="31%" align="center"><?php if ($pageNum_project > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_project=%d%s", $currentPage, max(0, $pageNum_project - 1), $queryString_project); ?>">Previous</a>
                <?php } // Show if not first page ?>              </td>
              <td width="23%" align="center"><?php if ($pageNum_project < $totalPages_project) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_project=%d%s", $currentPage, min($totalPages_project, $pageNum_project + 1), $queryString_project); ?>">Next</a>
                <?php } // Show if not last page ?>              </td>
              <td width="23%" align="center"><?php if ($pageNum_project < $totalPages_project) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_project=%d%s", $currentPage, $totalPages_project, $queryString_project); ?>">last</a>
                <?php } // Show if not last page ?>              </td>
            </tr>
          </table></td>
        <td width="55%">&nbsp; Showing <strong><?php echo ($startRow_project + 1) ?></strong> to <strong><?php echo min($startRow_project + $maxRows_project, $totalRows_project) ?></strong> of <strong><?php echo $totalRows_project ?></strong> </td>
      </tr>
    </table></td>
  </tr>
  <?php } else { ?>
  <tr>
    <td colspan="4" class="mainbase"><span class="style1">No project Created! </span></td>
  </tr>
  <?php } ?>
</table>
</fieldset>
</body>
</html>
<?php
mysqli_free_result($project);
?>

