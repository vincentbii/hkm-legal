<?php require_once('../connections/eProc.php'); 

 require_once('../activelog.php'); ?>
<?php
$currentPage = $_SERVER["PHP_SELF"];

$maxRows_religion = 30;
$pageNum_religion = 0;
if (isset($_GET['pageNum_religion'])) {
  $pageNum_religion = $_GET['pageNum_religion'];
}
$startRow_religion = $pageNum_religion * $maxRows_religion;

mysqli_select_db($eProc, $database_eProc);
$query_religion = "SELECT * FROM religion ";
$query_limit_religion = sprintf("%s LIMIT %d, %d", $query_religion, $startRow_religion, $maxRows_religion);
$religion = mysqli_query($eProc,$query_limit_religion) or die(mysqli_error($eProc));
$row_religion = mysqli_fetch_assoc($religion);

if (isset($_GET['totalRows_religion'])) {
  $totalRows_religion = $_GET['totalRows_religion'];
} else {
  $all_religion = mysqli_query($eProc,$query_religion);
  $totalRows_religion = mysqli_num_rows($all_religion);
}
$totalPages_religion = ceil($totalRows_religion/$maxRows_religion)-1;

$queryString_religion = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_religion") == false && 
        stristr($param, "totalRows_religion") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_religion = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_religion = sprintf("&totalRows_religion=%d%s", $totalRows_religion, $queryString_religion);
?>

<html>
<head>
<title>LEGAL  MANAGEMENT INFORMATION  SYSTEM</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../styles/default.css" rel="stylesheet" type="text/css">

<style type="text/css">
<!--
.style1 {
	color: #FF0000;
	font-weight: bold;
}
-->
</style>
</head>
<body>

<table width="100%" border="0" cellpadding="4" cellspacing="0" >

  <tr>
   <td width="36%"   class="inputdeft" >Departments</td>
   
	<td width="64%"   class="inputdeft"  ><div align="right">
	  <table width="154"  border="0" cellspacing="0" cellpadding="3">
	    <tr class="inputdeft">
	      <td width="19"  ><img src="../images/icons/newproject.gif" width="15" height="13" border="0" /></td>
                
			<td width="123"   ><a href="../adm/newreligion.php">New Religion </a></td>
            </tr>
	    </table>
	  </div></td>
  </tr>
  <?php if ($totalRows_religion > 0) { ?>
  <?php do { ?>
  <tr>
  <td ><?php echo $row_religion['name']?> </td>
  
  <td ><a href="../adm/newreligion.php?id=<?php echo $row_religion['id'] ?>">Edit</a></td>
  </tr>
  <?php } while ($row_religion = mysqli_fetch_assoc($religion)); ?>
  <tr>
    <td colspan="12" class="mainbase"><table width="97%"  border="0" cellspacing="0" cellpadding="3">
      <tr align="center">
        <td width="45%">
          <table width="78%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
              <td width="23%" align="center"><?php if ($pageNum_religion > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_religion=%d%s", $currentPage, 0, $queryString_religion); ?>">First</a>
                <?php } // Show if not first page ?>              </td>
              <td width="31%" align="center"><?php if ($pageNum_religion > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_religion=%d%s", $currentPage, max(0, $pageNum_religion - 1), $queryString_religion); ?>">Previous</a>
                <?php } // Show if not first page ?>              </td>
              <td width="23%" align="center"><?php if ($pageNum_religion < $totalPages_religion) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_religion=%d%s", $currentPage, min($totalPages_religion, $pageNum_religion + 1), $queryString_religion); ?>">Next</a>
                <?php } // Show if not last page ?>              </td>
              <td width="23%" align="center"><?php if ($pageNum_religion < $totalPages_religion) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_religion=%d%s", $currentPage, $totalPages_religion, $queryString_religion); ?>">last</a>
                <?php } // Show if not last page ?>              </td>
            </tr>
          </table></td>
        <td width="55%">&nbsp; Showing <strong><?php echo ($startRow_religion + 1) ?></strong> to <strong><?php echo min($startRow_religion + $maxRows_religion, $totalRows_religion) ?></strong> of <strong><?php echo $totalRows_religion ?></strong> </td>
      </tr>
    </table></td>
  </tr>
  <?php } else { ?>
  <tr>
    <td colspan="12" class="mainbase"><span class="style1">No religions Created! </span></td>
  </tr>
  <?php } ?>
</table>
</body>
</html>
<?php
mysqli_free_result($religion);
?>

