<?php
require_once('../connections/eProc.php');
include('../activelog.php');
mysqli_select_db($eProc, $database_eProc);

$client_id = $_GET['cid'];

if (isset($_GET['iid'])) {
    echo $_GET['iid'];
    $iid = $_GET['iid'];

    $query_iid = "SELECT * FROM invoices WHERE id=$iid";
    $result_iid = mysqli_query($eProc, $query_iid) or die(mysqli_error());
    $row_iid = mysqli_fetch_assoc($result_iid);
}
echo $_GET['iid'];
?>

<?php
$amountdue = '';
if (isset($_POST['invoice_id'])) {
    $sql = "select * from `invoices` where `id`=" . mysqli_real_escape_string($eProc, $_POST['invoice_id']);
    $res = mysqli_query($eProc, $sql);
    $row = mysqli_fetch_array($res);
    $amountdue = $row['amount_due'];
}
?>


<?php
//
//if(isset($_POST['submit_transaction'])){
//    $sqlInsert = "insert into payments (amount, invoice_id, payment_method) values ('".$_POST['transaction_amount']."', '".$_POST['transaction_invoice']."', '".$_POST['payment_method']."')";
//    $sqlResult = mysqli_query($eProc, $sqlInsert) or die(mysqli_error($eProc));
//    if($sqlResult){
//        echo 'Successfully Added';
//    }
//}
?>

<html>
    <head>
        <title>LEGAL MANAGEMENT SYSTEM</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="../styles/default.css" rel="stylesheet" type="text/css">
        <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <LINK REL=StyleSheet  href="calendar.css"  TYPE="text/css">
        <script LANGUAGE="JavaScript"  src="weeklycalendar.js"></script>
        <script type="text/javascript" src="../bootstrap/js/jquery.min.js"></script>
        <script language="JavaScript" type="text/javascript">

            buildWeeklyCalendar(1);

            function confirmdelete(name, id) {
                if (confirm('Are you sure you want to delete  client ' + name + '?')) {
                    location.href = 'newclient.php?del=' + id
                }
            }

        </script>
        <script>
            function close() {
                location = 'clientslist.php'
            }
            function showAmount(str) {
                if (str == "") {
                    document.getElementById("txtHint").innerHTML = "";
                    return;
                } else {
                    if (window.XMLHttpRequest) {
                        // code for IE7+, Firefox, Chrome, Opera, Safari
                        xmlhttp = new XMLHttpRequest();
                    } else {
                        // code for IE6, IE5
                        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    xmlhttp.onreadystatechange = function () {
                        if (this.readyState == 4 && this.status == 200) {
                            document.getElementById("txtHint").innerHTML = this.responseText;
                        }
                    };
                    xmlhttp.open("GET", "process_payment.php?iid=" + str, true);
                    xmlhttp.send();
                }
            }

            $(document).ready(function () {
                $("#transaction_invoice").change(function () {
                    var invoice = $(this).val();
                    if (invoice !== "") {
                        $.ajax({
                            url: "process_payment.php",
                            data: {invoice_id: invoice},
                            type: 'POST',
                            success: function (response) {
                                var resp = $.trim(response);
                                $("#amount_due").html(resp);
                            }
                        });
                    } else {
                        $("#amount_due").html("<input type='text' value='' />");
                    }
                });
            });
        </script>
        <style type="text/css">
            <!--
            .style1 {
                color: #FF0000;
                font-weight: bold;
            }
            -->
        </style>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-sm-8">
                    <?php
                    if (isset($_POST['submit_transaction'])) {
                        $transaction_amount = $_POST['transaction_amount'];
                        $transaction_date = $_POST['transaction_date'];
                        $transaction_invoice = $_POST['transaction_invoice'];
                        $payment_method = $_POST['payment_method'];

                        $sql_payment = "INSERT INTO `payments`(`amount`, `invoice_id`, `payment_method`)
                                VALUES ($transaction_amount,$transaction_invoice,$payment_method)";
                        $result_payment = mysqli_query($eProc, $sql_payment);

                        $query_invoice = "SELECT * FROM invoices WHERE id=$transaction_invoice";
                        $results_invoice = mysqli_query($eProc, $query_invoice) or die(mysqli_error());
                        $row_invoice = mysqli_fetch_assoc($results_invoice);

                        $amount_paid = $row_invoice['amount_paid'] + $transaction_amount;
                        $amount_due = $row_invoice['invoice_amount'] - $amount_paid;

                        if ($amount_paid >= $row_invoice['invoice_amount']) {
                            $paid = 1;
                        } else {
                            $paid = 0;
                        }

//                        echo $row_invoice['id'];

                        $sql_update_invoice = "UPDATE `invoices` SET `paid`=$paid,`amount_paid`=$amount_paid,`amount_due`=$amount_due WHERE id=$transaction_invoice";
                        $update_invoice = mysqli_query($eProc, $sql_update_invoice) or die(mysqli_error($eProc));
                        ?> 
                        <script language='javascript'>
                            location = 'clientslist.php'
                        </script><?php
                    } else {
                        ?>
                        <form name="transaction_form" method="POST" enctype="multipart/form-data">
                            <table width="100%" border="0" cellpadding="4" cellspacing="0" >
                                <tr class="">
                                    <td>Select Invoice: </td>
                                    <td>
                                        <select required class="form-control input-sm" name="transaction_invoice" id="transaction_invoice" onchange="showAmount(this.value)">
                                            <option></option>
                                            <?php
                                            $payment_query = "select * from invoices where client_id = $client_id AND paid = 0";
                                            $payment_result = mysqli_query($eProc, $payment_query) or die(mysql_error());
                                            while ($row = mysqli_fetch_array($payment_result)) {
                                                ?>
                                                <option value="<?php echo $row['id']; ?>"><?php echo 'Invoice ' . $row['id']; ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;&nbsp;</td>
                                    <td>&nbsp;&nbsp;</td>
                                </tr>
                                <tr class="">
                                    <td>Amount</td>
                                    <td>
                                        <div id="amountdue">

                                        </div>
                                        <input required type="text" name="transaction_amount" value="<?php echo $amountdue; ?>" class="form-control input-sm" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;&nbsp;</td>
                                    <td>&nbsp;&nbsp;</td>
                                </tr>
                                <tr class="">
                                    <td>Date</td>
                                    <td>
                                        <input required onclick="w_displayDatePicker('transaction_date')" id="transaction_date" type="text" name="transaction_date" value="<?php echo date("Y-m-d"); ?>" class="form-control input-sm" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;&nbsp;</td>
                                    <td>&nbsp;&nbsp;</td>
                                </tr>
                                <tr class="">
                                    <td>Payment Method</td>
                                    <td>
                                        <select required class="form-control input-sm" name="payment_method">
                                            <option></option>
                                            <?php
                                            $payment_method_query = "select * from payment_method where status = 1";
                                            $payment_method__result = mysqli_query($eProc, $payment_method_query) or die(mysql_error());
                                            while ($payment_method_row = mysqli_fetch_array($payment_method__result)) {
                                                ?>
                                                <option value="<?php echo $payment_method_row['id']; ?>"><?php echo $payment_method_row['payment_name']; ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;&nbsp;</td>
                                    <td>&nbsp;&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <input type="submit" value="Submit" name="submit_transaction" class="btn btn-primary btn-xs" />
                                        <a class="btn btn-danger btn-xs" href="clientslist.php">Cancel</a>
                                    </td>
                                </tr>
                            </table>
                        </form>
                        <?php
                    }
                    ?>

                </div>
            </div>
        </div>
    </body>
</html>