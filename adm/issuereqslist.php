<?php require_once('../connections/eProc.php');
include('../activelog.php');
	$employee=getuserid();
$_SESSION['emp']=$employee;

 ?>

<?php
$currentPage = $_SERVER["PHP_SELF"];

$maxRows_clients = 30;
$pageNum_clients = 0;


  unset($_SESSION['itype']);


if (isset($_GET['pageNum_clients'])) {
  $pageNum_clients = $_GET['pageNum_clients'];
}
$startRow_clients = $pageNum_clients * $maxRows_clients;

mysqli_select_db($eProc, $database_eProc);


$query_clients = "SELECT requisition.* ,jobs.ftype,jobs.jobno FROM requisition INNER JOIN jobs ON requisition.job=jobs.id WHERE requisition.status='Pending Finance'
  
 ";
 //echo $query_clients;
$query_limit_clients = sprintf("%s LIMIT %d, %d", $query_clients, $startRow_clients, $maxRows_clients);
$clients = mysqli_query($eProc, $query_limit_clients) or die(mysqli_error($eProc));
$row_clients = mysqli_fetch_assoc($clients);

if (isset($_GET['totalRows_clients'])) {
  $totalRows_clients = $_GET['totalRows_clients'];
} else {
  $all_clients = mysqli_query($eProc, $query_clients);
  $totalRows_clients = mysqli_num_rows($all_clients);
}
$totalPages_clients = ceil($totalRows_clients/$maxRows_clients)-1;

$queryString_clients = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_clients") == false && 
        stristr($param, "totalRows_clients") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_clients = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_clients = sprintf("&totalRows_clients=%d%s", $totalRows_clients, $queryString_clients);



 
?>

<html>
<head>
<title>LEGAL MANAGEMENT SYSTEM</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../styles/default.css" rel="stylesheet" type="text/css">

<style type="text/css">
<!--
.style1 {
	color: #FF0000;
	font-weight: bold;
}
-->
</style>
</head>
<body>
<form action="" name="jobhistorylist" method="post">
<fieldset>


<table width="100%" border="0" cellpadding="4" cellspacing="0" >

  <tr>
  <td width="11%"   class="inputdef" style="font-weight: bold">Requisition No</td>
  <td width="14%"   class="inputdef" style="font-weight: bold">Raised By</td>
   <td width="10%"   class="inputdef" style="font-weight: bold"> Summary</td>
   <td width="12%"   class="inputdef" style="font-weight: bold"> Cost</td>
    <td width="13%"   class="inputdef" style="font-weight: bold"> Date Raised</td>
     <td width="18%"   class="inputdef" style="font-weight: bold"> Job Ref No</td>
      <td width="22%"   class="inputdef" style="font-weight: bold"> Client</td>
    </tr>
  <?php if ($totalRows_clients > 0) { ?>
  <?php do {
  
  
  if($row_clients['ftype']=='S'){
  $selectf= "SELECT clients.*,specificfile.dept, specificfile.filename,specificfile.id AS fid,specificfile.dateopened,jobtype.name as jtype,status.name as s,priority.name as p,requisition.cost,requisition.request,requisition.wdate,requisition.id AS reqid,requisition.emp
 FROM clients
 INNER JOIN specificfile ON clients.clientno=specificfile.client
 INNER JOIN jobs ON specificfile.id=jobs.fileno
 INNER JOIN jobtype ON jobs.jobtype=jobtype.id
 INNER JOIN status ON jobs.status=status.id
 INNER JOIN priority ON jobs.priority=priority.id
 INNER JOIN requisition ON jobs.id=requisition.job
 AND jobs.ftype='S'
 AND requisition.id='".$row_clients['id']."'

 ";
//echo  $selectf ;	

  }
  
   if($row_clients['ftype']=='G'){
  $selectf= "SELECT clients.*,generalfile.dept, generalfile.filename,generalfile.id AS fid,generalfile.dateopened,jobtype.name as jtype,status.name as s,priority.name as p,requisition.cost,requisition.request,requisition.wdate,requisition.id AS reqid,requisition.emp
 FROM clients
 INNER JOIN generalfile ON clients.clientno=generalfile.client
 INNER JOIN jobs ON generalfile.id=jobs.fileno
 INNER JOIN jobtype ON jobs.jobtype=jobtype.id
 INNER JOIN status ON jobs.status=status.id
 INNER JOIN priority ON jobs.priority=priority.id
 INNER JOIN requisition ON jobs.id=requisition.job
 AND jobs.ftype='G'
 AND requisition.id='".$row_clients['id']."'
 ";
//echo  $selectf ;	

  }
  $sql_f = mysqli_query($eProc, $selectf) or die(mysqli_error($eProc));
$row_f = mysqli_fetch_assoc($sql_f );
  
  
  $jb=$row_f['filename'].'-'.$row_f['dept'].  str_pad($row_clients['jobno'], 3, "0", STR_PAD_LEFT);
   ?>
  <tr>
  
   <td ><strong><a href="newissuereq.php?id=<?php echo $row_f['reqid'] ?>"><?php echo $row_f['reqid'] ;?> </a></strong></td>
  <td > <?php echo gettheUsername($row_f['emp']) ;?></td>
  <td ><?php echo $row_f['request'] ;?> </td>
   <td ><?php echo $row_f['cost'] ;?> </td>
   <td ><?php echo $row_f['wdate'] ;?> </td>
   <td ><?php echo $jb ;?> </td>
    <td ><?php echo $row_f['fulnames'] ;?> </td>
    </tr>
  <?php } while ($row_clients = mysqli_fetch_assoc($clients)); ?>
  <tr>
    <td colspan="7" class="mainbase"><table width="97%"  border="0" cellspacing="0" cellpadding="3">
      <tr align="center">
        <td width="45%">
          <table width="78%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
              <td width="23%" align="center"><?php if ($pageNum_clients > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_clients=%d%s", $currentPage, 0, $queryString_clients); ?>">First</a>
                <?php } // Show if not first page ?>              </td>
              <td width="31%" align="center"><?php if ($pageNum_clients > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_clients=%d%s", $currentPage, max(0, $pageNum_clients - 1), $queryString_clients); ?>">Previous</a>
                <?php } // Show if not first page ?>              </td>
              <td width="23%" align="center"><?php if ($pageNum_clients < $totalPages_clients) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_clients=%d%s", $currentPage, min($totalPages_clients, $pageNum_clients + 1), $queryString_clients); ?>">Next</a>
                <?php } // Show if not last page ?>              </td>
              <td width="23%" align="center"><?php if ($pageNum_clients < $totalPages_clients) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_clients=%d%s", $currentPage, $totalPages_clients, $queryString_clients); ?>">last</a>
                <?php } // Show if not last page ?>              </td>
            </tr>
          </table></td>
        <td width="55%">&nbsp; Showing <strong><?php echo ($startRow_clients + 1) ?></strong> to <strong><?php echo min($startRow_clients + $maxRows_clients, $totalRows_clients) ?></strong> of <strong><?php echo $totalRows_clients ?></strong> </td>
      </tr>
    </table></td>
  </tr>
  <?php } else { ?>
  <tr>
    <td colspan="7" class="mainbase"><span class="style1">No Pending Requisitions! </span></td>
  </tr>
  <?php } ?>
</table>

</fieldset>
</form>
</body>
</html>
<?php
//mysqli_free_result($clients);
?>

