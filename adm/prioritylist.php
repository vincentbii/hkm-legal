<?php require_once('../connections/eProc.php'); ?>

<?php

$currentPage = $_SERVER["PHP_SELF"];

$maxRows_priority = 30;
$pageNum_priority = 0;


  unset($_SESSION['itype']);


if (isset($_GET['pageNum_priority'])) {
  $pageNum_priority = $_GET['pageNum_priority'];
}
$startRow_priority = $pageNum_priority * $maxRows_priority;

mysqli_select_db($eProc, $database_eProc);
$query_priority = "SELECT * FROM priority   ";

$query_limit_priority = sprintf("%s LIMIT %d, %d", $query_priority, $startRow_priority, $maxRows_priority);
$priority = mysqli_query($eProc, $query_limit_priority) or die(mysqli_error());
$row_priority = mysqli_fetch_assoc($priority);

if (isset($_GET['totalRows_priority'])) {
  $totalRows_priority = $_GET['totalRows_priority'];
} else {
  $all_priority = mysqli_query($eProc, $query_priority);
  $totalRows_priority = mysqli_num_rows($all_priority);
}
$totalPages_priority = ceil($totalRows_priority/$maxRows_priority)-1;

$queryString_priority = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_priority") == false && 
        stristr($param, "totalRows_priority") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_priority = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_priority = sprintf("&totalRows_priority=%d%s", $totalRows_priority, $queryString_priority);
?>

<html>
<head>
<title>LEGAL MANAGEMENT SYSTEM</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../styles/default.css" rel="stylesheet" type="text/css">

<style type="text/css">
<!--
.style1 {
	color: #FF0000;
	font-weight: bold;
}
-->
</style>
</head>
<body>
<fieldset>
<table width="100%" border="0" cellpadding="4" cellspacing="0" >

  <tr>
  <td width="26%"   class="inputdef" style="font-weight: bold">priority Name</td>
  <td width="33%"   class="inputdef" style="font-weight: bold">Initials</td>
   <td width="41%"   class="inputdef" style="font-weight: bold" ><div align="right">
	  <table width="154"  border="0" cellspacing="0" cellpadding="3">
	    <tr class="inputdef">
	      <td width="19"  ><img src="../images/icons/newproject.gif" width="15" height="13" border="0" /></td>
                
			<td width="123"    ><a href="newpriority.php">New priority</a></td>
            </tr>
	    </table>
	  </div></td>
  </tr>
  <?php if ($totalRows_priority > 0) { ?>
  <?php do { ?>
  <tr>
   <td > <?php echo $row_priority['name']?> </td>
   <td > <?php echo $row_priority['initials']?> </td>
  <td ><a href="newpriority.php?id=<?php echo $row_priority['id'] ?>">Edit</a></td>
  </tr>
  <?php } while ($row_priority = mysqli_fetch_assoc($priority)); ?>
  <tr>
    <td colspan="5" class="mainbase"><table width="97%"  border="0" cellspacing="0" cellpadding="3">
      <tr align="center">
        <td width="45%">
          <table width="78%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
              <td width="23%" align="center"><?php if ($pageNum_priority > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_priority=%d%s", $currentPage, 0, $queryString_priority); ?>">First</a>
                <?php } // Show if not first page ?>              </td>
              <td width="31%" align="center"><?php if ($pageNum_priority > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_priority=%d%s", $currentPage, max(0, $pageNum_priority - 1), $queryString_priority); ?>">Previous</a>
                <?php } // Show if not first page ?>              </td>
              <td width="23%" align="center"><?php if ($pageNum_priority < $totalPages_priority) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_priority=%d%s", $currentPage, min($totalPages_priority, $pageNum_priority + 1), $queryString_priority); ?>">Next</a>
                <?php } // Show if not last page ?>              </td>
              <td width="23%" align="center"><?php if ($pageNum_priority < $totalPages_priority) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_priority=%d%s", $currentPage, $totalPages_priority, $queryString_priority); ?>">last</a>
                <?php } // Show if not last page ?>              </td>
            </tr>
          </table></td>
        <td width="55%">&nbsp; Showing <strong><?php echo ($startRow_priority + 1) ?></strong> to <strong><?php echo min($startRow_priority + $maxRows_priority, $totalRows_priority) ?></strong> of <strong><?php echo $totalRows_priority ?></strong> </td>
      </tr>
    </table></td>
  </tr>
  <?php } else { ?>
  <tr>
    <td colspan="5" class="mainbase"><span class="style1">No priority Created! </span></td>
  </tr>
  <?php } ?>
</table>
</fieldset>
</body>
</html>
<?php
mysqli_free_result($priority);
?>

