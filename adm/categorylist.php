<?php require_once('../connections/eProc.php'); ?>

<?php
$currentPage = $_SERVER["PHP_SELF"];

$maxRows_category = 30;
$pageNum_category = 0;


  unset($_SESSION['itype']);


if (isset($_GET['pageNum_category'])) {
  $pageNum_category = $_GET['pageNum_category'];
}
$startRow_category = $pageNum_category * $maxRows_category;

mysqli_select_db($eProc, $database_eProc);
$query_category = "SELECT * FROM category   ";

$query_limit_category = sprintf("%s LIMIT %d, %d", $query_category, $startRow_category, $maxRows_category);
$category = mysqli_query( $eProc,$query_limit_category) or die(mysqli_error());
$row_category = mysqli_fetch_assoc($category);

if (isset($_GET['totalRows_category'])) {
  $totalRows_category = $_GET['totalRows_category'];
} else {
  $all_category = mysqli_query( $eProc, $query_category);
  $totalRows_category = mysqli_num_rows($all_category);
}
$totalPages_category = ceil($totalRows_category/$maxRows_category)-1;

$queryString_category = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_category") == false && 
        stristr($param, "totalRows_category") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_category = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_category = sprintf("&totalRows_category=%d%s", $totalRows_category, $queryString_category);
?>

<html>
<head>
<title>DOC MANAGEMENT SYSTEM</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../styles/default.css" rel="stylesheet" type="text/css">

<style type="text/css">
<!--
.style1 {
	color: #FF0000;
	font-weight: bold;
}
-->
</style>
</head>
<body>

<table width="100%" border="0" cellpadding="4" cellspacing="0" >

  <tr>
  <td width="10%"   class="inputdef" style="font-weight: bold">Category Name</td>
   <td width="17%"   class="inputdef" style="font-weight: bold" ><div align="right">
	  <table width="154"  border="0" cellspacing="0" cellpadding="3">
	    <tr class="inputdef">
	      <td width="19"  ><img src="../images/icons/newproject.gif" width="15" height="13" border="0" /></td>
                
			<td width="123"    ><a href="newcategory.php">New Category</a></td>
            </tr>
	    </table>
	  </div></td>
  </tr>
  <?php if ($totalRows_category > 0) { ?>
  <?php do { ?>
  <tr>
   <td > <?php echo $row_category['name']?> </td>
  <td ><a href="newcategory.php?id=<?php echo $row_category['id'] ?>">Edit</a></td>
  </tr>
  <?php } while ($row_category = mysqli_fetch_assoc($category)); ?>
  <tr>
    <td colspan="5" class="mainbase"><table width="97%"  border="0" cellspacing="0" cellpadding="3">
      <tr align="center">
        <td width="45%">
          <table width="78%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
              <td width="23%" align="center"><?php if ($pageNum_category > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_category=%d%s", $currentPage, 0, $queryString_category); ?>">First</a>
                <?php } // Show if not first page ?>              </td>
              <td width="31%" align="center"><?php if ($pageNum_category > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_category=%d%s", $currentPage, max(0, $pageNum_category - 1), $queryString_category); ?>">Previous</a>
                <?php } // Show if not first page ?>              </td>
              <td width="23%" align="center"><?php if ($pageNum_category < $totalPages_category) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_category=%d%s", $currentPage, min($totalPages_category, $pageNum_category + 1), $queryString_category); ?>">Next</a>
                <?php } // Show if not last page ?>              </td>
              <td width="23%" align="center"><?php if ($pageNum_category < $totalPages_category) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_category=%d%s", $currentPage, $totalPages_category, $queryString_category); ?>">last</a>
                <?php } // Show if not last page ?>              </td>
            </tr>
          </table></td>
        <td width="55%">&nbsp; Showing <strong><?php echo ($startRow_category + 1) ?></strong> to <strong><?php echo min($startRow_category + $maxRows_category, $totalRows_category) ?></strong> of <strong><?php echo $totalRows_category ?></strong> </td>
      </tr>
    </table></td>
  </tr>
  <?php } else { ?>
  <tr>
    <td colspan="5" class="mainbase"><span class="style1">No category Created! </span></td>
  </tr>
  <?php } ?>
</table>

</body>
</html>
<?php
mysqli_free_result($category);
?>

