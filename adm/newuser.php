<?php require_once('../connections/eProc.php'); ?>
<?php include('../activelog.php'); 
mysqli_select_db($eProc, $database_eProc);

if(isset($_POST['val']) && $_POST['val']!=''){ 
	$_SESSION['val']=$_POST['val'];
	}
	
if(isset($_POST['val']) && $_POST['val']==''){ 
	unset($_SESSION['val']);
	}
$code=getnextcode('authcode' ,'users');

$uname=$_POST['username'];
$pw=$_POST['umd5'];
$cpw=$_POST['CUMD5'];

if (isset($_POST['save'])&& $_POST['save']!='') { 
		
	if (isset($_POST['groupid'])&& $_POST['groupid']!='') {
			$groupid=$_POST['groupid'];		
		
		if (trim($_POST['umd5'])==trim($_POST['CUMD5'])){			
		
			$MD5 = md5(trim($pw)); // md5 encrypted password
		
						//Check if user exists
				if (!userExists($_POST['username'])) {
										
					 $insertSQL = sprintf("INSERT INTO users (employee, username, umd5,authcode,groupid, com_id,status)
					 			 VALUES	(%s,%s,%s,%s,%s,%s,%s)",
									   GetSQLValueString($_SESSION['val'], "text"),
									   GetSQLValueString($_POST['username'], "text"),
									   GetSQLValueString($MD5, "text"),
									   GetSQLValueString($code, "text"),
									   GetSQLValueString($groupid, "text"),
									   GetSQLValueString($_SESSION['company'], "text"),
									   GetSQLValueString('A', "text"));
				 		 mysqli_select_db($eProc, $database_eProc);					
						 $Result1 = mysqli_query($eProc, $insertSQL) or die('ERROR CREATING USER: '.mysqli_error($eProc));			 
						 InsertUserGroups($code,$groupid);				 
				    			
						?><SCRIPT LANGUAGE=JAVASCRIPT>				
							location='usr.php';				
						</SCRIPT><?php 
				}else{
					?><SCRIPT LANGUAGE=JAVASCRIPT>
							alert ("The 'User name' you entered already exists. Enter a unique username");
							//location='newuser.php';
						</SCRIPT><?php
				} 
				
				}else { 		
				?>	<script> 
					alert('Passwords do not match !');
					//javascript:location='newuser.php';
				</script><?php
				} 
				}else {
					?><script> 
						alert('You have to select a default group !');
						//javascript:location='newuser.php';
					</script> <?php 
				}
	
	
} 


//echo"the username". $uname;
?>

<html>
<head>
<title>NBNET  MANAGEMENT INFORMATION  SYSTEM</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../styles/default.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../js/funcs.js"></script>
<?php 
// BRING IN THE JSCRIPT FILE WITH THE DROPDOWN DEstockITIONS
// --------------------------------------------------------
$url = "../tmp/".$_SESSION['company']."/jbs.js";
addJavascript($url); 
// --------------------------------------------------------
?>

<style type="text/css">
<!--
#Layer1 {
	position:absolute;
	width:451px;
	height:115px;
	z-index:1;
	left: 6px;
	top: 88px;
}
-->
</style>

<script language="JavaScript" type="text/javascript">
	
</script>

</head>
<body onLoad="initialize(dp, ut);">
 <form action="" method="post" name="newuser" id="newuser">
<table width="100%" border="0" cellspacing="0" cellpadding="5">
  <tr>
    <td width="49%" valign="top">
     
        <fieldset>
        <legend>User Details</legend>
        <table width="100%" border="0" cellspacing="0" cellpadding="4">
		 
          
  <tr valign="baseline">
    <td  align="right" nowrap>Full Name:</td>
    
    <td width="465"> 
  
    <select name="val" id="val"  onFocus="this.style.backgroundColor='#ffff00'"  onBlur="this.style.backgroundColor='#F2FAFF'" onChange="submit()" class='forms' >
      <?php 
 	showemployees($_SESSION['val']);
	 ?>
    </select>    </tr>
    
     <?php $selectstud = "SELECT employee.*,department.name AS dept FROM employee INNER JOIN department ON employee.department=department.id   WHERE employee.id='".$_SESSION['val']."'";
	$sql_stud = mysqli_query($eProc, $selectstud) or die(mysqli_error());
	$row_stud = mysqli_fetch_assoc($sql_stud );
	$studnum=mysqli_num_rows($sql_stud);?>
    
       <tr valign="baseline">
    <td align="right" nowrap>Department:</td>
    <td width="465"><strong><?php echo $row_stud['dept']?></strong></td>
  </tr>

  <tr valign="baseline">
    <td colspan="2" align="right" nowrap><hr size="1"></td>
    </tr>
 
  <tr valign="baseline">
    <td align="right" nowrap>User Name:</td>
    <td><input name="username" type="text" class="forms" value="<?php echo $uname;?>" size="40"></td>
    </tr>
  
  <tr valign="baseline">
    <td nowrap align="right">Password:</td>
    <td><input name="umd5" type="password" class="forms" value="<?php echo $pw;?>" size="40"></td>
    </tr>
  <tr valign="baseline">
    <td nowrap align="right">Confirm Password:</td>
    <td><input name="CUMD5" type="password" class="forms" id="CUMD5" value="<?php echo $cpw;?>" size="40" ></td>
    </tr>
  
    <tr valign="baseline">
    <td align="right" nowrap>User code:</td>
    <td><strong><?php echo $code?></strong></td>
    </tr>
	<!-- <tr valign="baseline">
    <td align="right" nowrap>Select member if not an internal fke user:</td>
    <td><select name="member" id="member" class="forms">
			       <?php //showmembers();?>
			       </select></td>
    </tr>-->
</table>
</fieldset>
   </td>
   <td width="51%" valign="top">
   <fieldset>
<legend>Groups </legend>
<table width="100%" border="0" cellspacing="0" cellpadding="4">
  <tr valign="baseline">
    
	<?php 
	
	$selectSQL = "SELECT groups.groupname, groups.groupid, groups.groupdesc, groups.sys FROM groups WHERE groups.groupname <> 'System administrator' AND groups.groupname <> 'Company Administrator' ORDER BY groups.groupname";
	
	$done = mysqli_query($eProc, $selectSQL) or die("ERROR: Trying to retrieve groups list - ".mysqli_error($eProc));
	$groupslist = mysqli_fetch_assoc($done);
	?>
    <td width="472" rowspan="4">
	<?php if (mysqli_num_rows($done)>0) {  ?>
		<table width="100%" border="0" > 
		<tr>
		    <td width="47%">&nbsp;</td>
		    <td width="32%">Default group </td>
		    <td width="21%">Other</td>
		    </tr>
		  <?php do {  ?>
		 
		  <tr>
			<td><?php echo $groupslist['groupname']; ?></td>
			<td>
                             <label>
                                  <input name="groupid" id="groupid" type="radio" value="<?php echo $groupslist['groupid'];  ?>">
			</label>
                        </td>
			<td>
                             <input name="<?php echo $groupslist['groupid'];  ?>" id="<?php echo $groupslist['groupid'];  ?>" type="checkbox" value="<?php echo $groupslist['groupid'];  ?>">
                        </td>
		  </tr>
		  <?php } while ($groupslist = mysqli_fetch_assoc($done)); ?>
		</table>
	<?php }  ?>	</td>
    </tr>
  <tr valign="baseline">    

    <td colspan="4" align="right" nowrap><hr size="1"></td>
  </tr>
</table>
</fieldset>
   </td>
   
  </tr>
  <tr align="center"><td colspan="3"><fieldset>
		<table width="100%" cellpadding="1" cellspacing="0">
          
          <tr valign="baseline">
           <td> 
                <div align="center">
             <input type="hidden" name="MM_insert" value="form1">
             <input name="save" type="submit" class="formsBlue" value="Save">
             <input name="button" type="button" class="formsorg" onClick="javascript:location='usr.php'" value="Cancel">
           </div>
           </td>
          </tr>
        </table>
</fieldset></td></tr>
</table>
 </form>
</body>
</html>
