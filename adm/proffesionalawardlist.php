<?php require_once('../connections/eProc.php'); 

 require_once('../activelog.php'); ?>
<?php
$currentPage = $_SERVER["PHP_SELF"];

$maxRows_proffesionalaward = 30;
$pageNum_proffesionalaward = 0;
if (isset($_GET['pageNum_proffesionalaward'])) {
  $pageNum_proffesionalaward = $_GET['pageNum_proffesionalaward'];
}
$startRow_proffesionalaward = $pageNum_proffesionalaward * $maxRows_proffesionalaward;

mysqli_select_db($eProc, $database_eProc);
$query_proffesionalaward = "SELECT * FROM proffesionalaward ";
$query_limit_proffesionalaward = sprintf("%s LIMIT %d, %d", $query_proffesionalaward, $startRow_proffesionalaward, $maxRows_proffesionalaward);
$proffesionalaward = mysqli_query($eProc,$query_limit_proffesionalaward) or die(mysqli_error($eProc));
$row_proffesionalaward = mysqli_fetch_assoc($proffesionalaward);

if (isset($_GET['totalRows_proffesionalaward'])) {
  $totalRows_proffesionalaward = $_GET['totalRows_proffesionalaward'];
} else {
  $all_proffesionalaward = mysqli_query($eProc, $query_proffesionalaward);
  $totalRows_proffesionalaward = mysqli_num_rows($all_proffesionalaward);
}
$totalPages_proffesionalaward = ceil($totalRows_proffesionalaward/$maxRows_proffesionalaward)-1;

$queryString_proffesionalaward = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_proffesionalaward") == false && 
        stristr($param, "totalRows_proffesionalaward") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_proffesionalaward = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_proffesionalaward = sprintf("&totalRows_proffesionalaward=%d%s", $totalRows_proffesionalaward, $queryString_proffesionalaward);
?>

<html>
<head>
<title>LEGAL  MANAGEMENT INFORMATION  SYSTEM</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../styles/default.css" rel="stylesheet" type="text/css">

<style type="text/css">
<!--
.style1 {
	color: #FF0000;
	font-weight: bold;
}
-->
</style>
</head>
<body>

<table width="100%" border="0" cellpadding="4" cellspacing="0" >

  <tr>
   <td width="80%"   class="inputdeft" style="font-weight: bold"> Proffesional Awards</td>
   
	<td width="20%"   class="inputdeft" style="font-weight: bold" ><div align="right">
	  <table width="100%"  border="0" cellspacing="0" cellpadding="3">
	    <tr class="inputdeft">
	      <td width="49"  ><img src="../images/icons/newproject.gif" width="15" height="13" border="0" /></td>
                
			<td width="580"   ><a href="../adm/newproffesionalaward.php">New Proffesionalc Award  </a></td>
          </tr>
	    </table>
	  </div></td>
  </tr>
  <?php if ($totalRows_proffesionalaward > 0) { ?>
  <?php do { ?>
  <tr>
  <td ><?php echo $row_proffesionalaward['name']?> </td>
  
  <td ><a href="../adm/newproffesionalaward.php?id=<?php echo $row_proffesionalaward['id'] ?>">Edit</a></td>
  </tr>
  <?php } while ($row_proffesionalaward = mysqli_fetch_assoc($proffesionalaward)); ?>
  <tr>
    <td colspan="12" class="mainbase"><table width="97%"  border="0" cellspacing="0" cellpadding="3">
      <tr align="center">
        <td width="45%">
          <table width="78%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
              <td width="23%" align="center"><?php if ($pageNum_proffesionalaward > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_proffesionalaward=%d%s", $currentPage, 0, $queryString_proffesionalaward); ?>">First</a>
                <?php } // Show if not first page ?>              </td>
              <td width="31%" align="center"><?php if ($pageNum_proffesionalaward > 0) { // Show if not first page ?>
                <a href="<?php printf("%s?pageNum_proffesionalaward=%d%s", $currentPage, max(0, $pageNum_proffesionalaward - 1), $queryString_proffesionalaward); ?>">Previous</a>
                <?php } // Show if not first page ?>              </td>
              <td width="23%" align="center"><?php if ($pageNum_proffesionalaward < $totalPages_proffesionalaward) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_proffesionalaward=%d%s", $currentPage, min($totalPages_proffesionalaward, $pageNum_proffesionalaward + 1), $queryString_proffesionalaward); ?>">Next</a>
                <?php } // Show if not last page ?>              </td>
              <td width="23%" align="center"><?php if ($pageNum_proffesionalaward < $totalPages_proffesionalaward) { // Show if not last page ?>
                <a href="<?php printf("%s?pageNum_proffesionalaward=%d%s", $currentPage, $totalPages_proffesionalaward, $queryString_proffesionalaward); ?>">last</a>
                <?php } // Show if not last page ?>              </td>
            </tr>
          </table></td>
        <td width="55%">&nbsp; Showing <strong><?php echo ($startRow_proffesionalaward + 1) ?></strong> to <strong><?php echo min($startRow_proffesionalaward + $maxRows_proffesionalaward, $totalRows_proffesionalaward) ?></strong> of <strong><?php echo $totalRows_proffesionalaward ?></strong> </td>
      </tr>
    </table></td>
  </tr>
  <?php } else { ?>
  <tr>
    <td colspan="12" class="mainbase"><span class="style1">No proffesionalawards Created! </span></td>
  </tr>
  <?php } ?>
</table>
</body>
</html>
<?php
mysqli_free_result($proffesionalaward);
?>

