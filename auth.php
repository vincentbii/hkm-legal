<?php
ob_start();
// AUTH FILE V1.1
// User Authentication File
// --------------------------------------------------------------
session_name('eprocure');
session_start();
// -------------------------------------
include('activelog.php');
require_once('connections/eProc.php');
// -------------------------------------
if (isset($_POST['UNM']) && $_POST['UNM'] != "") {
	// Gather Required Variables
	// --------------------------
	 $UNAME = $_POST['UNM']; 
	 $PWD = $_POST['PWD'];
	 $company = $_POST['COM'];
	// --------------------------
	// Authenticate User
	// --------------------------
	$RELVAL = AuthenticateUser($UNAME, $PWD, $company);
	list($LOGON, $UNQ,  $group, $groupid) = explode(';',$RELVAL,4);
	// --------------------------
	// Validate and Redirect Accordingly
	if ($LOGON == 1) {	
	
		// Create Session Variables
		$_SESSION['USER'] = $UNAME;		 
		$_SESSION['UNQ'] = $UNQ;
		$_SESSION['LOGIN'] = true;
		$_SESSION['GROUPID'] =  $groupid;
		$_SESSION['GROUP'] =  $group;
		mysqli_select_db( $eProc, $database_eProc);
		$action=getUsername($_SESSION['UNQ'])."  "."logged in ";
	 	$insertSQL =sprintf( "INSERT INTO audittrail (action) 
	 	VALUES (%s)", 
		GetSQLValueString($action, "text"));	
		//echo $insertSQL;
		$Result2 = mysqli_query($eProc,$insertSQL) or die(mysqli_error($eProc));
		
		$grouptype = getgrouptype(getgrouptypeid($_SESSION['GROUPID'])); 
		if (!file_exists ($grouptype)) { $grouptype = 'adm';}
		$PAGE = $grouptype.'/index.php';
		$_SESSION['USERTYPE'] = $_SESSION['GROUP'];
		$_SESSION['HOMEDIR'] = "../".$grouptype."/";
			
		if (getCompanyID($company)=="") {
			$_SESSION['company'] = $company;
		} else {
			$_SESSION['company'] = getCompanyID($company);
		}
		 
		if (trim($_SESSION['USER'])=='eadmin') {
			$PAGE = 'eadm/ahome.php';
			$_SESSION['USERTYPE'] = "Administrator";
			$_SESSION['HOMEDIR'] = "../eadm/";
		} 
		
		// ------------------------
		// Fill in the user details
		// ------------------------
		if ($company == 'ADM') {
			// Work this only for the admin
			$USR = getAdminUser($UNQ);
			$_SESSION['FULLNAME'] = $USR['fulname'];
			$_SESSION['company'] = "SYSTEM ADMINISTRATOR";
		} else {
			// Work this only for the company users
			$USR = getUserDetails($UNQ);
			$_SESSION['FULLNAME'] = $USR['fulname'];
			$_SESSION['department'] = $USR['departmentid'];
			$_SESSION['groupid'] = $USR['groupid'];
			
			if ($USR['admin'] == 1) {
				$_SESSION['ACCADMIN'] = true;
				$PAGE = "adm/index.php";
				$_SESSION['HOMEDIR'] = "../adm";
			}
		}

		$_SESSION['HOME'] = $PAGE;

		//echo $PAGE; 
		// ------------------------
		// Redirect to user home
		?>
		<script language="JavaScript" type="text/javascript">
			
			location='<?php echo $PAGE; ?>'
		</script>

		<?php
		//header("Location:$");
		//header(sprintf('location: %s', $PAGE));
		// ------------------------
	} else {
		// Failed Login
		// ------------------------------
		//header('location: index.php?e');
		// ------------------------------
		?>
		<script language="JavaScript" type="text/javascript">
			
			location='index.php?e'
		</script>

		<?php
	}
}

// --------------------------------------------------------------
// Created By Davies

ob_end_flush();
?>
